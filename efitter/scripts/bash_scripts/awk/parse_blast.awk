BEGIN {
	#cache size
	n=10;
 }

{	
	#populate cache
	window[NR%n]=$0;
}
/>/ 	{
		subject_id=$0;
		while (getline line){
			#populate cache
			window[NR%n]=line;
			if(line !~/Length/){
				gsub(/^\s*/,"",line);
				subject_id=subject_id" "line;
			} 
			else 
				break;
		}
		#postprocess subject_id
		subject_id=substr(subject_id,2,length(subject_id));
	}
		
/Query=/ {	query_id=$2;
		while (getline line){
			#populate cache
			window[NR%n]=line;
			if(line !~/letters/){
				gsub(/^\s*/,"",line);
				query_id=query_id" "line;
			}
			else
				break;
		}
}

/^Query:\s/ {	
		query=$0;
		subject="";
		empty_count=0;
		line_count=0;
		gap_line=window[(NR-n-2)%n];
		eval_line=window[(NR-n-3)%n];
		patsplit(gap_line, gap_array);
		patsplit(eval_line, eval_array);
		
		gaps=gap_array[11]
		gsub(/\/.*/,"",gaps);
		if (length(gaps) == 0)
		{
			gaps=0;
		}
		eval=eval_array[8];
		if (substr(eval,1,1) == "e")
		{
			eval="1"eval;
		}		
	
		while (getline line)
		{
			#populate cache
			window[NR%n]=line;
			line_count += 1;
			if (length(line) == 0)
			{
				empty_count+=1;
			}
			if (empty_count >= 2)
			{	
				break;
			} 
			if (line ~/^Query:\s/)
			{
				query=query""line;
				empty_count=0;
			}
			else if (line ~/^Sbjct:\s/)
			{
				subject=subject""line;
				empty_count=0;
			}
					
		
				
		}
		
		#postprocess query sequence
		match(query,/^Query: ([0-9]*)/,arr); #get start index of query 
		q_start=arr[1];
		match(query,/([0-9]*$)/,arr); #get stop index of query
		q_stop=arr[1];
		match(query,/^Query: [0-9]*(.*)\s[0-9]*$/,arr); #get unclean query sequence string
		q_seq=arr[1];
		gsub(/\s[0-9]*Query:\s[0-9]*\s/,"",q_seq);
		gsub(/ /,"",q_seq);
		
		#postprocess subject sequence
		match(subject,/^Sbjct: ([0-9]*)/,arr); #get start index of query 
		s_start=arr[1];
		match(subject,/([0-9]*$)/,arr); #get stop index of subject
		s_stop=arr[1];
		match(subject,/^Sbjct: [0-9]*(.*)\s[0-9]*$/,arr); #get unclean subject sequence string
		s_seq=arr[1];
		gsub(/\s[0-9]*Sbjct:\s[0-9]*\s/,"",s_seq);
		gsub(/ /,"",s_seq);
		
		#print q_start","q_stop","length(q_seq);
		#print s_start","s_stop","length(s_seq);
					
		print query_id","subject_id","q_start","q_stop","s_start","s_stop","eval","gaps","q_seq","s_seq;

 }

END {}
