import chimera
from chimera import runCommand as rc
import re
import getopt
import sys
try:
    opts, args = getopt.getopt(sys.argv[1:], '', [])
except getopt.error, message:
    raise chimera.NonChimeraError("%s: %s" % (__name__, message))

mapfn = args[0]
pdbfn = args[1]
resolution = args[2]
rc('open {0}'.format(mapfn))
rc('open {0}'.format(pdbfn))
rc('molmap #1 {0} onGrid #0'.format(resolution))
rc('vop resample #2 onGrid #0')
fn = chimera.openModels.list()[1].openedAs[0]
fnout = re.sub('.pdb|.cif$', '.mrc', fn)
rc('volume #3 save {0}'.format(fnout))
