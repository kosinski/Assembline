import tempfile
from traceback import print_exc

from TEMPy.MapParser import MapParser
from TEMPy.ScoringFunctions import ScoringFunctions
from TEMPy.StructureBlurrer import StructureBlurrer
from TEMPy.StructureParser import PDBParser
from TEMPy.class_arg import TempyParser
import sys,os, glob, numpy as np

import re
import subprocess
import numpy
from TEMPy.MapParser import MapParser
from TEMPy.StructureParser import PDBParser
from TEMPy.StructureBlurrer import StructureBlurrer
from TEMPy.ScoringFunctions import ScoringFunctions
import TEMPy.Vector  as Vector
import os
from pdb_utils import transform_pdb
import csv
import sys

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i+n]


def get_tempfilename(tempdir=None, ext=''):
    if tempdir is None:
        tempdir = tempfile._get_default_tempdir()

    basename = tempfile._get_candidate_names().next() + '.' + ext

    return os.path.join(tempdir, basename)


def read_solutions(solutions_filename):

    with open(solutions_filename, 'rU') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.readline(), [','])
        csvfile.seek(0)

        solutions = csv.DictReader(csvfile, dialect=dialect)

        for row in solutions:
            yield row

def get_fieldnames(solutions_filename):
    with open(solutions_filename, 'rU') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.readline(), [','])
        csvfile.seek(0)

        solutions = csv.DictReader(csvfile, dialect=dialect)

        return solutions.fieldnames

def get_pdb(sol, ori_pdb, dirname):
    '''
    Args:
        * solution
    Returns:
        * pdb filename
    '''
    rot = map(float, sol['rotation'].split())
    rot = list(chunks(rot, 3))

                
    newrot = [
        [],
        [],
        []
    ]
    for row in rot:
        for i in range(3):
            newrot[i].append(row[i])
    rot = newrot
    trans = map(float, sol['translation'].split())
    pdblines = transform_pdb.transform(ori_pdb, rot, trans)

    # outpdb_fn = os.path.join(os.getcwd(), re.sub('.pdb$', '.temp.pdb', sol['filename']))
    outpdb_fn = get_tempfilename(dirname, 'pdb')

    print outpdb_fn
    with open(outpdb_fn, 'w') as outpdb_f:
        outpdb_f.write(''.join(pdblines))

    return outpdb_fn

def get_molmap(em_fn, pdb_fn, resolution):
    '''
    Generate molmap on a grid exactly matching the experimental map as required by TEMPy
    '''
    cmd = 'chimera --nogui --script "{2}/gen_molmap.py {0} {1} {3}"'.format(em_fn, pdb_fn, os.path.dirname(os.path.realpath(__file__)), resolution)
    print(cmd)
    subprocess.call(cmd, shell=True)

    return re.sub('.pdb$', '.mrc', pdb_fn)

#calculate model contour
def model_contour(p,res=4.0,emmap=False,t=-1.):
    pName,modelmap = blur_model(p,res,emmap)
    c1 = None
    if t != -1.0:
        print 'calculating model derived map contour'
        c1 = t*emmap.std()#0.0
    return pName,modelmap,c1


def blur_model(p,res=4.0,emmap=False):
    '''An example how to blur a model but it was extremely slow for me, to test again.
    '''
    pName = os.path.basename(p).split('.')[0]
    print 'reading the model'
    structure_instance=PDBParser.read_PDB_file(pName,p,hetatm=False,water=False)
    print 'filtering the model'
    blurrer = StructureBlurrer()
    if res is None: sys.exit('Map resolution required..')
    #emmap = blurrer.gaussian_blur(structure_instance, res,densMap=emmap_1,normalise=True)
    modelmap = blurrer.gaussian_blur_real_space(structure_instance, res,sigma_coeff=0.187,densMap=emmap,normalise=True) 

    return pName,modelmap

#calculate map contour
def map_contour(m,t=-1.):
    mName = os.path.basename(m).split('.')[0]
    #print 'reading map'
    emmap=MapParser.readMRC(m)
    c1 = None
    if t != -1.0:
        print 'calculating map contour'
        zeropeak,ave,sigma1 = emmap._peak_density()
        if not zeropeak is None: c1 = zeropeak+(t*sigma1)
        else:
            c1 = 0.0

    return mName,emmap,c1


if len(sys.argv) <= 2:
    usage = '''Usage: {0} solutions.csv em_map_filename em_map_threshold ori_pdb_filename model_map_resolution model_map_threshold temp_directory outfilename.csv'''.format(os.path.basename(sys.argv[0]))
    print(usage)
    exit()

print '#', sys.argv
csv_filename = sys.argv[1]
assert os.path.isfile(csv_filename)
em_fn = sys.argv[2]
assert os.path.isfile(em_fn)
c1 = float(sys.argv[3]) #contour
ori_pdb = sys.argv[4]
assert os.path.isfile(ori_pdb)
resolution = float(sys.argv[5])
c2 = float(sys.argv[6])
temp_directory = sys.argv[7]
assert os.path.isdir(temp_directory)
outfilename = sys.argv[8]

use_chimera_molmap = True #if True, use molmap from Chimera, False use TEMPy blurrer which seems much slower

Name1 = os.path.basename(em_fn)
emmap1=MapParser.readMRC(em_fn)


solutions = list(read_solutions(csv_filename))

for sol in solutions:
    pdb_fn = get_pdb(sol, ori_pdb, temp_directory)

    Name2 = os.path.basename(sol['filename'])
    if use_chimera_molmap:
        model_map_fn = get_molmap(em_fn, pdb_fn, resolution)
        emmap2 = MapParser.readMRC(model_map_fn)
    else:
        # pName, emmap2 = blur_model(pdb_fn,res=resolution,emmap=emmap1)
        Name2,emmap2,c2 = model_contour(pdb_fn,res=resolution,emmap=emmap1,t=0.5)

    
    if None in [Name1,Name2]: sys.exit('Calculation failed, check input map and model files')
    print '#Scoring...', Name2
    sc = ScoringFunctions()
    #OVR
    try:
        ccc,ovr = sc.CCC_map(emmap1,emmap2,c1,c2,1,meanDist=True) #ovr is always 1.0 for non masked ccc_map function
    except:
        print 'Exception for lccc and overlap score'
        print_exc()

    #SCCC
    if ccc < -1.0 or ccc > 1.0:
        ccc = 0.0
    print 'Global correlation (SCCC) score: ', ccc
    sol['CCC'] = ccc


    if None in [Name1,Name2]: sys.exit('Calculation failed, check input map and model files')
    print '#Scoring...', Name2
    sc = ScoringFunctions()
    #OVR
    try:
        ccc_mask,ovr = sc.CCC_map(emmap1,emmap2,c1,c2,3,meanDist=True)
        if ovr < 0.0: ovr = 0.0
        print 'Percent overlap:', ovr
    except:
        print 'Exception for lccc and overlap score'
        print_exc()
        ovr = 0.0
    if ovr < 0.02:
        print "Maps do not overlap: ", Name2
        continue
    sol['OVR'] = ovr
    #SCCC
    if ccc_mask < -1.0 or ccc_mask > 1.0:
        ccc_mask = 0.0
    print 'Local correlation (SCCC) score: ', ccc_mask
    sol['SCCC'] = ccc_mask


    #LMI
    try:
        mi_mask = sc.MI(emmap1,emmap2,c1,c2,3)
        if mi_mask < 0.0: mi_mask = 0.0
        print 'Local Mutual Information (LMI) score: ', mi_mask
    except:
        print 'Exception for Local Mutual Information (LMI) score'
        print_exc()
        mi_mask = 0.0
      #NMI
    sol['LMI'] = mi_mask
    try:
        nmi = sc.MI(emmap1,emmap2,c1,c2,1,None,None,True)
        if nmi < 0.0: nmi = 0.0
        print 'Normalized Mutual Information (NMI) score:', nmi
    except:
        print 'Exception for Normalized Mutual Information (NMI) score'
        print_exc()
        nmi = 0.0

    sol['NMI'] = nmi
    #laplace_CCC
    try:
        laplace_ccc = sc.laplace_CCC(emmap1,emmap2)
        if laplace_ccc < -1.0 or laplace_ccc > 1.0:
            laplace_ccc = 0.0
        print 'Laplace CCC score:', laplace_ccc
    except:
        print 'Exception for Laplace CCC score'
        print_exc()
        nmi = 0.0
    sol['laplace_ccc'] = laplace_ccc

    #Normal vector score (NV)
    try:
        nv_Minimum = sc.normal_vector_score(emmap1,emmap2,float(c1),float(c1)+(emmap1.std()*0.05),Filter='Minimum')
        if nv_Minimum == 0.0 or nv_Minimum is None: nv_Minimum = sc.normal_vector_score(emmap1,emmap2,float(c1),float(c1)+(emmap1.std()*0.05))
        #print 'Normal vector score: ', nv_Minimum
        if nv_Minimum < 0.0: 
            nv_Minimum = 0.0
        print 'Normal vector (NV) score with Minimum filter:', nv_Minimum
    except:
        print 'Exception for Normal vector (NV) score'
        print_exc()
        nv_Minimum = 0.0

    sol['nv_Minimum'] = nv_Minimum

    #Normal vector score (NV) with Sobel filter
    try:
        nv_Sobel = sc.normal_vector_score(emmap1,emmap2,float(c1),float(c1)+(emmap1.std()*0.05),Filter='Sobel')
        if nv_Sobel == 0.0 or nv_Sobel is None: nv_Sobel = sc.normal_vector_score(emmap1,emmap2,float(c1),float(c1)+(emmap1.std()*0.05))
        #print 'Normal vector score: ', nv_Sobel
        if nv_Sobel < 0.0: 
            nv_Sobel = 0.0
        print 'Normal vector (NV) score with Sobel filter:', nv_Sobel
    except:
        print 'Exception for Normal vector (NV) score with Sobel filter '
        print_exc()
        nv_Sobel = 0.0

    sol['nv_Sobel'] = nv_Sobel


    #Normal vector score (NV) with Laplace filter
    try:
        nv_Laplace = sc.normal_vector_score(emmap1,emmap2,float(c1),float(c1)+(emmap1.std()*0.05),Filter='Laplace')
        if nv_Laplace == 0.0 or nv_Laplace is None: nv_Laplace = sc.normal_vector_score(emmap1,emmap2,float(c1),float(c1)+(emmap1.std()*0.05))
        #print 'Normal vector score: ', nv_Laplace
        if nv_Laplace < 0.0: 
            nv_Laplace = 0.0
        print 'Normal vector (NV) score with Laplacian filter:', nv_Laplace
    except:
        print 'Exception for Normal vector (NV) score with Laplace filter '
        print_exc()
        nv_Laplace = 0.0

    sol['nv_Laplace'] = nv_Laplace

    #Normal vector score (NV) with Mean filter
    try:
        nv_Mean = sc.normal_vector_score(emmap1,emmap2,float(c1),float(c1)+(emmap1.std()*0.05),Filter='Mean')
        if nv_Mean == 0.0 or nv_Mean is None: nv_Mean = sc.normal_vector_score(emmap1,emmap2,float(c1),float(c1)+(emmap1.std()*0.05))
        #print 'Normal vector score: ', nv_Mean
        if nv_Mean < 0.0: 
            nv_Mean = 0.0
        print 'Normal vector (NV) score with Mean filter:', nv_Mean
    except:
        print 'Exception for Normal vector (NV) score with Mean filter '
        print_exc()
        nv_Mean = 0.0

    sol['nv_Mean'] = nv_Mean


    #Chamfer distance
    try:
        chm = sc._surface_distance_score(emmap1,emmap2,c1,c2,'Minimum')
        if chm == 0.0 or chm is None:
            chm = sc._surface_distance_score(emmap1,emmap2,c1,c2,'Mean')
        #print 'Surface distance score: ', chm
        if chm < 0.0: chm = 0.0
        print 'Chamfer distance score:', chm
    except:
        print 'Exception for Chamfer distance score'
        print_exc()
        chm = 0.0

    sol['Chamfer_distance'] = chm


    #envelope score
    try:
        env_score = sc.envelope_score_map(emmap1,emmap2,c1,c2)
        print 'Envelope score:', env_score
    except:
        print 'Exception for envelope score'
        print_exc()
        env_score = 0.0

    sol['envelope_score'] = env_score

    subprocess.call('rm {0}'.format(pdb_fn), shell=True)
    if use_chimera_molmap:
        subprocess.call('rm {0}'.format(model_map_fn), shell=True)

mi_ov_scores = sc.scale_median([sol['OVR'] for sol in solutions], [sol['LMI'] for sol in solutions])
for sol, mi_ov in zip(solutions, mi_ov_scores):
    sol['LMI_OV'] = mi_ov

ccc_ov_scores = sc.scale_median([sol['OVR'] for sol in solutions], [sol['SCCC'] for sol in solutions])
for sol, ccc_ov in zip(solutions, ccc_ov_scores):
    sol['SCCC_OV'] = ccc_ov



with open(outfilename, 'w') as csvfile:
    fieldnames = get_fieldnames(csv_filename)
    fieldnames.extend([
        'OVR',
        'CCC',
        'SCCC',
        'LMI',
        'NMI',
        'laplace_ccc',
        'nv_Minimum',
        'nv_Sobel',
        'nv_Laplace',
        'nv_Mean',
        'Chamfer_distance',
        'envelope_score',
        'LMI_OV',
        'SCCC_OV'
    ])
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerows(solutions)


    
    