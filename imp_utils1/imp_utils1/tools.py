import sys

import RMF
import IMP
import IMP.rmf

def rmf_cat(inputs, output):
    """A slow version of rmf_cat that can merge RMF files coming from trajectories
    that do not have the same conformation in the first frame"""
    orh = RMF.create_rmf_file(output)
    # orh.set_producer("rmf_cat")
    orh.set_description("Universe.\n")

    def select_hs(hs):
        new_hs = [IMP.atom.create_clone(h) for h in hs]
        for h0, h in zip(hs, new_hs):
            children0 = h0.get_children()
            children = h.get_children()
            for c0, c in zip(children0, children):
                if IMP.atom.Copy.get_is_setup(c0.get_particle()):
                    IMP.atom.Copy.setup_particle(c, IMP.atom.Copy(c0.get_particle()).get_copy_index())

        return new_hs

    for i, fn in enumerate(inputs):
        rh = RMF.open_rmf_file_read_only(fn);

        if i == 0:
            m0 = IMP.Model()
            hs = IMP.rmf.create_hierarchies(rh, m0)
            clones0 = select_hs(hs)
            IMP.rmf.add_hierarchies(orh, clones0)
        else:
            m = IMP.Model()
            hs = IMP.rmf.create_hierarchies(rh, m)
            clones = select_hs(hs)
            for ni in rh.get_frames():
                rh.set_current_frame(ni)
            for h0, h in zip(clones0, clones):
                particles0 = m0.get_particle_indexes()
                particles = m.get_particle_indexes()
                for p0, p in zip(particles0, particles):
                    if IMP.core.XYZ.get_is_setup(m0, p0):
                        IMP.core.XYZ(m0, p0).set_coordinates(IMP.core.XYZ(m, p).get_coordinates())

                m0.update()

        IMP.rmf.save_frame(orh)
    