import random
from random import choice
import itertools
from collections import defaultdict

import numpy
import math

import IMP
from IMP.algebra import ReferenceFrame3D
import IMP.core

def get_parent_mol(h):
    p = h
    while p.get_parent():
        p = p.get_parent()
        if IMP.atom.Molecule.get_is_setup(p):
            return p


def calc_weights(values, steepness=10):
    #NEW WAY
    # hmm, why would it take absolute values
    # return [steepness**(math.fabs(s)) for s in values]

    #OLD WAY:
    #normalize to some positve range without 0 and 1
    start = 0.1
    end = 0.9
    width = end - start
    min_val = min(values)
    max_val = max(values)
    if min_val < max_val: #otherwise may lead to division by zero
        norm_values = [(value - min_val)/(max_val - min_val) * width + start for value in values]
    else:
        norm_values = [0.5] * len(values) #0.5 arbitray constant number, if min(values) == max(values) score is constant, all weights are equal
    # norm_values = values
    # print('1', norm_values[:20])
    weights = [1.0 / (w+0.000001) for w in norm_values]  #inverse for weighted_choice, 0.000001 to avoid division by 0.0 http://stackoverflow.com/questions/16228024/inverse-weighted-distribution
    # print('2', weights[:20])
    sum_weights = sum(weights)
    weights = [w / sum_weights for w in weights]   # Normalize weights, https://stackoverflow.com/questions/50763963/select-a-random-element-from-a-list-based-on-inverse-weight
    # print('3', weights[:20])

    return weights

class SymDiscreteMover(IMP.core.MonteCarloMover):
    def __init__(self, m, ref_rb, sym_rbs, symtr3ds, tr3ds, ini_reference_frame=None):
        '''
        tr3ds - trd3s to sample from
        '''
        IMP.core.MonteCarloMover.__init__(self, m, ref_rb.get_particle().get_name() + " SymRigidBodyMover_")
        self.ref_rb = ref_rb
        self.sym_rbs = sym_rbs
        self.symtr3ds = symtr3ds
        self.tr3ds = tr3ds
        self.len = len(tr3ds)
        self.tr3d_idx = None
        self.last_transformations = []
        self.last_tr3d_idx = None
        if ini_reference_frame is None:
            self.ini_reference_frame = self.ref_rb.get_reference_frame()
        else:
            self.ini_reference_frame = ini_reference_frame

    def do_propose(self):
        all_rbs = [self.ref_rb] + self.sym_rbs

        for rb in all_rbs:
            self.last_transformations.append(rb.get_reference_frame().get_transformation_to())
        self.last_tr3d_idx = self.tr3d_idx
        self.ref_rb.set_reference_frame(self.ini_reference_frame)

        self.tr3d_idx = choice(list(range(0, self.len)))
        tr3d_for_move = self.tr3ds[self.tr3d_idx]

        IMP.core.transform(self.ref_rb, tr3d_for_move)
        proposed_t3d = IMP.core.RigidBody(self.ref_rb).get_reference_frame().get_transformation_to()

        for sym_rb, symtr3d in zip(self.sym_rbs, self.symtr3ds):
            sym_rb.set_reference_frame(ReferenceFrame3D(symtr3d * proposed_t3d))

        return IMP.core.MonteCarloMoverResult([rb.get_particle_index() for rb in all_rbs], 1.0)

    def do_reject(self):
        all_rbs = [self.ref_rb] + self.sym_rbs
        for rb, tr3d in zip(all_rbs, self.last_transformations):
            rb.set_reference_frame(ReferenceFrame3D(tr3d))
        # self.last_transformations = []
        self.tr3d_idx = self.last_tr3d_idx

    def do_accept(self):
        all_rbs = [self.ref_rb] + self.sym_rbs

        for rb in all_rbs:
            self.last_transformations.append(rb.get_reference_frame().get_transformation_to())

    def do_get_inputs(self):
        all_rbs = [self.ref_rb] + self.sym_rbs
        return [pi.get_particle() for pi in all_rbs]


class DiscreteMover(IMP.core.MonteCarloMover):
    def __init__(self, m, pi, tr3ds, tr3ds_scores=None, scaleBetter=False, name=None, steepness=math.exp(1)):
        if name is None:
            name = "DiscreteMover_" + m.get_particle(pi).get_name()
        IMP.core.MonteCarloMover.__init__(self, m, name)
        self.pi = pi
        self.tr3ds = tr3ds
        self.tr3ds_scores = tr3ds_scores
        self.len = len(tr3ds)
        self.tr3d_idx = None
        self.rb = IMP.core.RigidBody(self.get_model(), self.pi)
        self.ini_transformation = self.rb.get_reference_frame().get_transformation_to()
        self.last_transformation = None
        self.last_tr3d_idx = None
        self.indexes = list(range(0, self.len))
        self.steepness = steepness
        self.weights = []
        self.scaleBetter = scaleBetter
        self.scaleFactor = 0.1

    def do_propose(self):
        self.last_transformation = self.rb.get_reference_frame().get_transformation_to()
        self.last_tr3d_idx = self.tr3d_idx

        self.rb.set_reference_frame(ReferenceFrame3D(self.ini_transformation))
            
        # print(self.get_name())
        # print('s0',self.tr3ds_scores[:20])
        if self.tr3ds_scores:
            if self.scaleBetter and self.last_tr3d_idx is not None:
                prev_score = self.tr3ds_scores[self.last_tr3d_idx]
                scores = []
                for s in self.tr3ds_scores:
                    if s < prev_score:
                        scores.append(self.scaleFactor*s)
                    else:
                        scores.append(s)
                self.weights = calc_weights(scores, steepness = self.steepness)
                indexes = list(range(0, len(scores)))
            else:
                scores = self.tr3ds_scores
                indexes = self.indexes
                if not self.weights:
                    self.weights = calc_weights(scores, steepness = self.steepness)
            
            self.tr3d_idx = random.choices(indexes, self.weights, k=1)[0]
        else:
            self.tr3d_idx = choice(list(range(0, self.len)))
        tr3d_for_move = self.tr3ds[self.tr3d_idx]

        IMP.core.transform(self.rb, tr3d_for_move)
        # rb.set_reference_frame(ReferenceFrame3D(tr3d_for_move))

        return IMP.core.MonteCarloMoverResult([self.pi], 1.0)

    def do_reject(self):
        rb = IMP.core.RigidBody(self.get_model(), self.pi)
        rb.set_reference_frame(ReferenceFrame3D(self.last_transformation))
        self.tr3d_idx = self.last_tr3d_idx

    def do_accept(self):
        rb = IMP.core.RigidBody(self.get_model(), self.pi)
        self.last_transformation = rb.get_reference_frame().get_transformation_to()

    def do_get_inputs(self):
        return [self.get_model().get_particle(self.pi)]

    def get_current_tr3d(self):
        if self.tr3d_idx is None:
            return None
        else:
            return self.tr3ds[self.tr3d_idx]

class DiscreteWeightedMover(DiscreteMover):
    def __init__(self, m, pi, tr3ds, score_fn, precalc=False, top=None):
        '''
        Shifts probability of moves according to passed scoring function.

        Warning: can be computationally costly!

        TODO: implement like this?:
        http://math.stackexchange.com/questions/760939/how-to-weight-three-different-variables-to-create-a-ranking
        '''
        DiscreteMover.__init__(self, m, pi, tr3ds, name="DiscreteWeightedMover_" + m.get_particle(pi).get_name())
        self.score_fn = score_fn
        self.scores = []
        self.weights = []
        self.indexes = list(range(0, len(self.tr3ds)))
        self.precalc = precalc
        self.top = top

    def calc_scores(self):
        rb = IMP.core.RigidBody(self.get_model(), self.pi)
        rb.set_reference_frame(ReferenceFrame3D(self.ini_transformation))
        print(self.get_name(), len(self.tr3ds))
        self.scores = []
        for i, tr3d in enumerate(self.tr3ds):
            IMP.core.transform(rb, tr3d)
            score = self.score_fn.evaluate(False)
            self.scores.append(score) 
            # print tr3d, self.scores[-1]
            rb.set_reference_frame(ReferenceFrame3D(self.ini_transformation))

    def calc_weights(self):
        self.indexes, self.scores = zip(*sorted(zip(self.indexes,self.scores),key=lambda x:x[1]))
        if self.top is not None:
            self.indexes = self.indexes[:self.top]
            self.scores = self.scores[:self.top]
        self.weights = calc_weights(self.scores)
        # print(self.indexes)
        # print(self.scores)
        # print(self.weights)

    def do_propose(self):
        if not self.precalc or len(self.scores) == 0:
            print("Calculating DiscreteWeightedMover weights for " + str(self))
            self.calc_scores()
            self.calc_weights()

        rb = IMP.core.RigidBody(self.get_model(), self.pi)
        self.last_transformation = rb.get_reference_frame().get_transformation_to()
        self.last_tr3d_idx = self.tr3d_idx

        rb.set_reference_frame(ReferenceFrame3D(self.ini_transformation))

        self.tr3d_idx = random.choices(self.indexes, self.weights, k=1)[0]
        # print(self.tr3d_idx, self.scores[self.indexes.index(self.tr3d_idx)])
        tr3d_for_move = self.tr3ds[self.tr3d_idx]

        IMP.core.transform(rb, tr3d_for_move)
        # rb.set_reference_frame(ReferenceFrame3D(tr3d_for_move))
        
        return IMP.core.MonteCarloMoverResult([self.pi], 1.0)

class DiscreteWeightedMover_test(DiscreteWeightedMover):
    def __init__(self, m, pi, tr3ds, score_fn, precalc=False, discrete_movers=None):
        '''
        Shifts probability of moves according to passed scoring function.

        Warning: can be computationally costly!

        TODO: implement like this?:
        http://math.stackexchange.com/questions/760939/how-to-weight-three-different-variables-to-create-a-ranking
        '''
        DiscreteWeightedMover.__init__(self, m, pi, tr3ds, score_fn, precalc)
        self.discrete_movers = discrete_movers
        self.map = defaultdict(list)

    def calc_scores(self):
        

        tr3d_lists = []
        counts = 1
        for mover in self.discrete_movers:
            tr3ds = []
            rb = IMP.core.RigidBody(mover.get_model(), mover.pi)
            for tr3d_idx, tr3d in enumerate(mover.tr3ds):
                tr3ds.append({
                        'mover': mover,
                        'tr3d': tr3d,
                        'tr3d_idx': tr3d_idx,
                        'rb': rb
                    })
            print('{0} fits for {1}'.format(len(tr3ds), mover))
            counts = counts * len(tr3ds)
            tr3d_lists.append(tr3ds)

            mover.last_transformation = rb.get_reference_frame().get_transformation_to()
            mover.last_tr3d_idx = mover.tr3d_idx

        combinations = itertools.product(*tr3d_lists)
        print('Will calculate weights for ', counts, ' combinations')

        for i, comb in enumerate(combinations):
            if i !=0 and i%100000 == 0:
                print('{0} done'.format(i))

            for tr3d_dict in comb:
                mover = tr3d_dict['mover']
                tr3d = tr3d_dict['tr3d']
                rb = tr3d_dict['rb']
                rb.set_reference_frame(ReferenceFrame3D(mover.ini_transformation))
                IMP.core.transform(rb, tr3d)
                mover.tr3d_idx = tr3d_dict['tr3d_idx']

            score = self.score_fn.evaluate(False)
            # key = tuple([id(tr3d_dict['tr3d']) for tr3d_dict in comb[1:]])
            key = tuple([tr3d_dict['tr3d_idx'] for tr3d_dict in comb[1:]])
            value = [comb[0]['tr3d_idx'], score]
            self.map[key].append(value)

        #undo moves
        for mover in self.discrete_movers:
            rb = IMP.core.RigidBody(mover.get_model(), mover.pi)
            rb.set_reference_frame(ReferenceFrame3D(mover.last_transformation))
            mover.tr3d_idx = mover.last_tr3d_idx

        #sort and add indexes:
        for key, indexes_scores in self.map.items():
            indexes, scores = zip(*indexes_scores)
            indexes, scores = zip(*sorted(zip(indexes, scores),key=lambda x:x[1]))
            i = 0
            for index, score in zip(indexes, scores):
                indexes_scores[i] = [index, score]
                i = i + 1
        

    def calc_weights(self):
        self.calc_scores()

        for key, indexes_scores in self.map.items():
            indexes, scores = zip(*indexes_scores)
            weights = calc_weights(scores)
            # print(scores)
            # print(indexes)
            # print(weights)
            # sys.exit()
            i = 0
            for index, weight in zip(indexes, weights):
                indexes_scores[i] = [index, weight]
                i = i + 1


    def do_propose(self):
        if not self.precalc or len(self.map) == 0:
            print("Calculating DiscreteWeightedMover weights for " + str(self))
            self.calc_weights()

        rb = IMP.core.RigidBody(self.get_model(), self.pi)
        self.last_transformation = rb.get_reference_frame().get_transformation_to()
        self.last_tr3d_idx = self.tr3d_idx
        rb.set_reference_frame(ReferenceFrame3D(self.ini_transformation))

        # other_tr3ds = [mover.get_current_tr3d() for mover in self.discrete_movers[1:]]
        other_rbs_tr3ds_ids = tuple([mover.tr3d_idx for mover in self.discrete_movers[1:]])
        if None in other_rbs_tr3ds_ids:
            #if other rbs are not positioned yet, move using non-weigthed way
            self.tr3d_idx = choice(list(range(0, self.len)))
            tr3d_for_move = self.tr3ds[self.tr3d_idx]
            IMP.core.transform(rb, tr3d_for_move)
            self.discrete_movers[0].tr3d_idx = self.tr3d_idx
            print("here")
        else:
            indexes, weights = zip(*self.map[other_rbs_tr3ds_ids])

            self.tr3d_idx = random.choices(indexes, weights, k=1)[0]
            self.discrete_movers[0].tr3d_idx = self.tr3d_idx
            tr3d_for_move = self.tr3ds[self.tr3d_idx]
            IMP.core.transform(rb, tr3d_for_move)
            # if IMP.core.RigidBody(self.get_model(), self.pi).get_name() == 'rb_SpY_on4xmm.model_Seh1_Nup85_8-499_break_3F3F.pdb_0':
            #     print(indexes)
            #     print(weights)
            #     print()
            #     print(other_tr3ds)
            #     sys.exit()            # print(indexes)
            # print(weights)
            # print('index', indexes.index(self.tr3d_idx))



        return IMP.core.MonteCarloMoverResult([self.pi], 1.0)


    def do_reject(self):
        rb = IMP.core.RigidBody(self.get_model(), self.pi)
        rb.set_reference_frame(ReferenceFrame3D(self.last_transformation))
        self.tr3d_idx = self.last_tr3d_idx
        self.discrete_movers[0].tr3d_idx = self.tr3d_idx

class DiscreteConnMover(IMP.core.MonteCarloMover):
    def __init__(self, m, pi, tr3ds, conn_restr, target_distance, tr3ds_scores=None, scaleBetter=False, name=None):
        '''
        DiscreteMover that samples only from tr3ds that lead to satisfying the passed connectivity restraint
        '''
        if name is None:
            name = "DiscreteConnMover_" + m.get_particle(pi).get_name()
        IMP.core.MonteCarloMover.__init__(self, m, name)
        self.pi = pi
        self.tr3ds = tr3ds
        self.tr3ds_scores = tr3ds_scores
        self.len = len(tr3ds)
        self.indexes = list(range(0, self.len))
        self.tr3d_idx = None
        self.rb = IMP.core.RigidBody(self.get_model(), self.pi)
        self.ini_transformation = self.rb.get_reference_frame().get_transformation_to()
        self.last_transformation = None
        self.last_tr3d_idx = None

        self.conn_restr = conn_restr
        self.target_distance = target_distance
        self.min_distance_dev = max(2.0, 0.2*self.target_distance)
        self.scaleBetter = scaleBetter
        self.scaleFactor = 10

    def do_propose(self):
        self.last_transformation = self.rb.get_reference_frame().get_transformation_to()
        self.last_tr3d_idx = self.tr3d_idx

        self.rb.set_reference_frame(ReferenceFrame3D(self.ini_transformation))

        close_tr3ds_idxs = self.find_close_tr3ds() #assumes tr3ds are sorted by score!


        if len(close_tr3ds_idxs) > 0:
            if self.tr3ds_scores:
                scores = [self.tr3ds_scores[i] for i in close_tr3ds_idxs]
            else:
                scores = None
            indexes = close_tr3ds_idxs
        else:
            if self.tr3ds_scores:
                scores = self.tr3ds_scores
            else:
                scores = None
            indexes = self.indexes

        # if self.tr3ds_scores:
        #     if self.scaleBetter and self.last_tr3d_idx is not None:
        #         prev_score = self.tr3ds_scores[self.last_tr3d_idx]
        #         scores = []
        #         for s in self.tr3ds_scores:
        #             if s < prev_score:
        #                 scores.append(self.scaleFactor*s)
        #             else:
        #                 scores.append(s)
        #         self.weights = calc_weights(scores)
        #         indexes = list(range(0, len(scores)))
        #     else:
        #         scores = self.tr3ds_scores
        #         indexes = self.indexes
        #         if not self.weights:
        #             self.weights = calc_weights(scores)
                
        #     # print('s0',scores[:20])
            
        #     self.tr3d_idx = random.choices(indexes, self.weights, k=1)[0]


        if scores:
            if self.scaleBetter and self.last_tr3d_idx is not None:
                prev_score = self.tr3ds_scores[self.last_tr3d_idx]
                scores_temp = []
                for s in self.tr3ds_scores:
                    if s < prev_score:
                        scores_temp.append(self.scaleFactor*s)
                    else:
                        scores_temp.append(s)
                scores = scores_temp

            indexes = list(range(0, len(scores)))
            weights = calc_weights(scores)
            self.tr3d_idx = random.choices(indexes, weights, k=1)[0]
        else:
            self.tr3d_idx = choice(indexes)

        #TO CHECK: IS THE FOLLOWING USEFUL?
        # if len(close_tr3ds_idxs) > 0:
        #     if self.tr3ds_scores:
        #         close_tr3ds_scores = [self.tr3ds_scores[i] for i in close_tr3ds_idxs]

        #         self.tr3d_idx = weighted_choice(zip(close_tr3ds_idxs, close_tr3ds_scores))
        #     else:
        #         self.tr3d_idx = choice(close_tr3ds_idxs)
        # else:
        #     if self.tr3ds_scores:
        #         self.tr3d_idx = weighted_choice(zip(range(0, self.len), self.tr3ds_scores))
        #     else:
        #         self.tr3d_idx = choice(range(0, self.len))

        tr3d_for_move = self.tr3ds[self.tr3d_idx]
        IMP.core.transform(self.rb, tr3d_for_move)

        return IMP.core.MonteCarloMoverResult([self.pi], 1.0)

    def find_close_tr3ds(self):
        out = []
        for i, tr3d in enumerate(self.tr3ds):
            IMP.core.transform(self.rb, tr3d)
            k = 10
            if self.conn_restr.evaluate(False)/self.conn_restr.get_weight() <= (self.min_distance_dev**2)*k:
                out.append(i)
            self.rb.set_reference_frame(ReferenceFrame3D(self.ini_transformation))

        return out

    def do_reject(self):
        self.rb.set_reference_frame(ReferenceFrame3D(self.last_transformation))
        self.tr3d_idx = self.last_tr3d_idx

    def do_accept(self):
        self.last_transformation = self.rb.get_reference_frame().get_transformation_to()

    def do_get_inputs(self):
        return [self.get_model().get_particle(self.pi)]


class DiscreteSoftConnMover(DiscreteConnMover):
    def __init__(self, m, pi, tr3ds, conn_restr, how_many):
        '''
        DiscreteMover that samples only from top how_many tr3ds that best satisfy the passed connectivity restraint
        '''
        self.how_many = how_many
        DiscreteConnMover.__init__(self, m, pi, tr3ds, conn_restr, target_distance=0, tr3ds_scores=None, scaleBetter=False, name="DiscreteSoftConnMover_" + m.get_particle(pi).get_name())

    def find_close_tr3ds(self):
        out = []
        tr3ds_and_conn_scores = []
        for tr3d_i, tr3d in enumerate(self.tr3ds):
            IMP.core.transform(self.rb, tr3d)
            tr3ds_and_conn_scores.append([tr3d_i, self.conn_restr.evaluate(False)/self.conn_restr.get_weight()])
            self.rb.set_reference_frame(ReferenceFrame3D(self.ini_transformation))

        out = sorted(tr3ds_and_conn_scores, key=lambda x: x[1])[:self.how_many]

        return [x[0] for x in out]

class SymRigidBodyMover(IMP.core.MonteCarloMover):
    def __init__(self, m, ref_rb, sym_rbs, symtr3ds, max_translation=2, max_rotation=0.5):
        IMP.core.MonteCarloMover.__init__(self, m, ref_rb.get_particle().get_name() + " SymRigidBodyMover_")
        self.ref_rb = ref_rb
        self.sym_rbs = sym_rbs
        self.symtr3ds = symtr3ds
        self.max_translation = max_translation
        self.max_rotation = max_rotation
        self.last_transformations = []

    def do_propose(self):
        all_rbs = [self.ref_rb] + self.sym_rbs

        for rb in all_rbs:
            self.last_transformations.append(rb.get_reference_frame().get_transformation_to())

        mover = IMP.core.RigidBodyMover(self.ref_rb.get_model(), self.ref_rb.get_particle_index(), self.max_translation, self.max_rotation)
        mover.propose()

        proposed_t3d = IMP.core.RigidBody(self.ref_rb).get_reference_frame().get_transformation_to()

        for sym_rb, symtr3d in zip(self.sym_rbs, self.symtr3ds):
            sym_rb.set_reference_frame(ReferenceFrame3D(symtr3d * proposed_t3d))

        return IMP.core.MonteCarloMoverResult([rb.get_particle_index() for rb in all_rbs], 1.0)

    def do_reject(self):
        all_rbs = [self.ref_rb] + self.sym_rbs
        for rb, tr3d in zip(all_rbs, self.last_transformations):
            rb.set_reference_frame(ReferenceFrame3D(tr3d))
        self.last_transformations = []

    def do_accept(self):
        self.last_transformations = []

    def do_get_inputs(self):
        all_rbs = [self.ref_rb] + self.sym_rbs
        return [pi.get_particle() for pi in all_rbs]


class AlongSymAxisRigidBodyMover(IMP.core.RigidBodyMover):
    def __init__(self, m, pi, symtr3d, max_translation=2, max_rotation=0.5):
        IMP.core.RigidBodyMover.__init__(self, m, pi, max_translation, max_rotation)
        self.pi = pi
        self.symtr3d = symtr3d
        self.max_translation = max_translation
        self.max_rotation = max_rotation

        self.set_name(m.get_particle(pi).get_name() + " AlongSymAxisRigidBodyMover_")

    def do_propose(self):
        rb = IMP.core.RigidBody(self.get_model(), self.pi)

        #get random translation along the axis:
        axis_angle = IMP.algebra.get_axis_and_angle(self.symtr3d.get_rotation())
        axis = axis_angle[0]
        dist = random.uniform(-self.max_translation, self.max_translation)
        trans = axis*dist

        #get random rotation:
        angle = random.uniform(-self.max_rotation, self.max_rotation)
        rot3d = IMP.algebra.get_rotation_about_axis(axis, angle)
        rc = rot3d * rb.get_reference_frame().get_transformation_to().get_rotation()
        tr3d = IMP.algebra.Transformation3D(rc, trans)
        rb.set_reference_frame(ReferenceFrame3D(tr3d))

class DiscreteMoverRestraint(IMP.Restraint):
    def __init__(self, m, ps, mover, scores):
        IMP.Restraint.__init__(self, m, "DiscreteMoverRestraint "+ ps[0].get_name())
        self.ps = ps
        self.scores = scores
        self.mover = mover

    def unprotected_evaluate(self, da):
        if self.mover.tr3d_idx is not None:
            return self.scores[self.mover.tr3d_idx]
        else:
            return max(self.scores) ### EEE? what to return here?

    def do_get_inputs(self):
        return self.ps

class ExcludeMapRestraint(IMP.Restraint):
    def __init__(self, m, ps, dmap, threshold):
        '''
        Penalizes particles inside the envelope of a density map at a given threshold,
        using IMP.em.get_percentage_of_voxels_covered_by_particles.
        Score is a fraction (not percantage, look into get_percentage_of_voxels_covered_by_particles code) of voxels_covered_by_particles

        ps - particles
        dmap - density map
        threshold - a threshold that determines the envelope of the map
        max_distance - how far from the envelope to compute the transform
        '''

        IMP.Restraint.__init__(self, m, "ExcludeMapRestraint")
        if len(ps) == 0:
            raise Exception('Empty list of particles passed to ExcludeMapRestraint')
        self.ps = ps
        self.dmap = dmap
        self.threshold = threshold

    def unprotected_evaluate(self, da):
        covered_percentage = IMP.em.get_percentage_of_voxels_covered_by_particles(
            self.dmap,
            self.ps,
            1.0,
            IMP.algebra.get_identity_transformation_3d(),
            self.threshold)

        return covered_percentage

    def do_get_inputs(self):
        return self.ps


class ExcludeMapRestraint1(IMP.Restraint):
    def __init__(self, m, ps, dmap, threshold, max_distance, offset=0):
        '''
        Penalizes particles inside the envelope of a density map at a given threshold,
        using IMP.em.MapDistanceTransform.

        ps - particles
        dmap - density map
        threshold - a threshold that determines the envelope of the map
        max_distance - how far from the envelope to compute the transform
        '''
        IMP.Restraint.__init__(self, m, "ExcludeMapRestraint1")
        if len(ps) == 0:
            raise Exception('Empty list of particles passed to ExcludeMapRestraint1')
        self.ps = ps
        self.dmap = dmap
        self.threshold = threshold
        self.max_distance = max_distance
        self.offset = offset
        self.mapDistanceTransform = IMP.em.MapDistanceTransform(dmap, threshold, max_distance)

    def unprotected_evaluate(self, da):
        '''Return the score corresponding to the average MapDistanceTransform distance of particles
        inside the envelope (distances of all particles outside the envelope are set to 0)
        '''
        coords = [IMP.core.XYZ(p).get_coordinates() for p in self.ps]

        score = 0.
        for p in self.ps:
            try:
                dist = self.mapDistanceTransform.get_distance_from_envelope(IMP.core.XYZ(p).get_coordinates())
            except IMP._IMP_kernel.UsageException as e:
                    if 'The point is not part of the grid' in str(e):
                        dist = -100000
                    else:
                        raise e

            dist = dist + self.offset

            if dist < 0:
                dist = 0

            score = score + dist

        return score / len(self.ps)

    def do_get_inputs(self):
        return self.ps

class EnvelopePenetrationRestraint1(IMP.Restraint):
    def __init__(self, m, ps, dmap, threshold, max_distance):
        '''
        A restraint promoting particles to be covered by an EM map envelope at a defined density threshold.

        This is similar to IMP.em.EnvelopePenetrationRestraint but implemented using IMP.em.apDistanceTransform

        threshold - a threshold that determines the envelope of the map
        max_distance - how far from the envelope to compute the transform
        '''
        IMP.Restraint.__init__(self, m, "OutsideMapDistanceRestraint")
        if len(ps) == 0:
            raise Exception('Empty list of particles passed to OutsideMapDistanceRestraint')
        self.ps = ps
        self.dmap = dmap
        self.threshold = threshold
        self.max_distance = max_distance
        self.mapDistanceTransform = IMP.em.MapDistanceTransform(dmap, threshold, max_distance)

    def unprotected_evaluate(self, da):
        coords = [IMP.core.XYZ(p).get_coordinates() for p in self.ps]

        score = 0.
        for p in self.ps:
            try:
                dist = self.mapDistanceTransform.get_distance_from_envelope(IMP.core.XYZ(p).get_coordinates())
            except IMP._IMP_kernel.UsageException as e:
                    if 'The point is not part of the grid' in str(e):
                        dist = -100000
                    else:
                        raise e

            if dist > 0:
                dist = 0
            else:
                dist = -dist
            score = score + dist

        return score / len(self.ps)

    def do_get_inputs(self):
        return self.ps

class CloseToEnvelopeRestraint(IMP.Restraint):
    def __init__(self, m, ps, dmap, threshold, max_distance):
        '''
        Promotes particles to be close to the envelope of a density map at a given threshold.

        The evaluated value is a distance of the particle closest to the envelope

        ps - particles
        dmap - density map
        threshold - a threshold that determines the envelope of the map
        max_distance - how far from the envelope to compute the transform
        '''
        
        IMP.Restraint.__init__(self, m, "CloseToEnvelopeRestraint")
        if len(ps) == 0:
            raise Exception('Empty list of particles passed to CloseToEnvelopeRestraint')
        self.ps = ps
        self.dmap = dmap
        self.threshold = threshold
        self.max_distance = max_distance
        self.mapDistanceTransform = IMP.em.MapDistanceTransform(dmap, threshold, max_distance)

    def unprotected_evaluate(self, da):
        '''Ruturn the score corresponding to the distance of the closest particle. The lower score the closer to envelope.
        '''

        out_distance = self.max_distance
        for p in self.ps:
            try:
                distance = self.mapDistanceTransform.get_distance_from_envelope(IMP.core.XYZ(p).get_coordinates())
            except IMP._IMP_kernel.UsageException as e:
                    if 'The point is not part of the grid' in str(e):
                        distance = -100000
                    else:
                        raise e
                
            if distance < 0:
                distance = -distance 
            out_distance = min(out_distance, distance) 

        return out_distance

    def do_get_inputs(self):
        return self.ps

class SimilarTwoDistancesRestraint(IMP.Restraint):
    def __init__(self, m, ps_pair1, ps_pair2, k=1):
        IMP.Restraint.__init__(self, m, "SimilarTwoScoresRestraint")
        self.ps_pair1 = ps_pair1
        self.ps_pair2 = ps_pair2
        self.k = k

    def unprotected_evaluate(self, da):
        dist1 = IMP.core.get_distance(IMP.core.XYZR(self.ps_pair1[0]), IMP.core.XYZR(self.ps_pair1[1]))
        dist2 = IMP.core.get_distance(IMP.core.XYZR(self.ps_pair2[0]), IMP.core.XYZR(self.ps_pair2[1]))
        return self.k * (dist1 - dist2)**2

    def do_get_inputs(self):
        return self.ps_pair1 + self.ps_pair2

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / numpy.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    # print(v1, v2)
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return numpy.arccos(numpy.clip(numpy.dot(v1_u, v2_u), -1.0, 1.0))

class AngleRestraint(IMP.Restraint):
    def __init__(self, m, rb1, rb2, angle, k=1, resolution=1):
        IMP.Restraint.__init__(self, m, "AngleRestraint")
        print('Warning: AngleRestraint is not thoroughly tested')
        self.rb1 = rb1
        self.rb2 = rb2
        self.angle = angle
        self.k = k
        self.resolution = resolution
        self.ini_tr3d1 = self.rb1.get_reference_frame().get_transformation_to()
        self.ini_tr3d2 = self.rb2.get_reference_frame().get_transformation_to()

        self.vectors = []
        self.centroids = []
        for rb in (self.rb1, self.rb2):
            ps = select_rb_particles_at_resolution(self.get_model(), rb, self.resolution)
            pca = IMP.algebra.get_principal_components([IMP.core.XYZ(p).get_coordinates() for p in ps])
            # eigen_values = pca.get_principal_values()
            # print(eigen_values)
            eigen_vecs = pca.get_principal_components() # vectors with origin in the centroid of the rigid body
            self.centroids.append(pca.get_centroid())
            self.vectors.append(eigen_vecs[0]+pca.get_centroid()) # take the longest vector

    def unprotected_evaluate(self, da):
        tr3d1 = self.rb1.get_reference_frame().get_transformation_to() * self.ini_tr3d1.get_inverse()
        tr3d2 = self.rb2.get_reference_frame().get_transformation_to() * self.ini_tr3d2.get_inverse()
        vector1 = tr3d1.get_transformed(self.vectors[0])
        vector2 = tr3d2.get_transformed(self.vectors[1])
        centroid1 = tr3d1.get_transformed(self.centroids[0])
        centroid2 = tr3d2.get_transformed(self.centroids[1])

        angle = angle_between(list(centroid1-vector1), list(centroid2-vector2))
        
        return self.k * (angle-self.angle)**2

    def do_get_inputs(self):
        return self.rb1.get_particle(), self.rb2.get_particle()

class CollinearRestraint(IMP.Restraint):
    def __init__(self, m, rb1, rb2, k=1, resolution=1):
        IMP.Restraint.__init__(self, m, "CollinearRestraint")
        print('Warning: CollinearRestraint is not thoroughly tested')
        self.rb1 = rb1
        self.rb2 = rb2
        self.k = k
        self.resolution = resolution
        self.ini_tr3d1 = self.rb1.get_reference_frame().get_transformation_to()
        self.ini_tr3d2 = self.rb2.get_reference_frame().get_transformation_to()

        self.vectors = []
        self.centroids = []
        for rb in (self.rb1, self.rb2):
            ps = select_rb_particles_at_resolution(self.get_model(), rb, self.resolution)
            pca = IMP.algebra.get_principal_components([IMP.core.XYZ(p).get_coordinates() for p in ps])
            # eigen_values = pca.get_principal_values()
            # print(eigen_values)
            eigen_vecs = pca.get_principal_components() # vectors with origin in the centroid of the rigid body
            self.centroids.append(pca.get_centroid())
            self.vectors.append(eigen_vecs[0]+pca.get_centroid()) # take the longest vector

    def unprotected_evaluate(self, da):
        tr3d1 = self.rb1.get_reference_frame().get_transformation_to() * self.ini_tr3d1.get_inverse()
        tr3d2 = self.rb2.get_reference_frame().get_transformation_to() * self.ini_tr3d2.get_inverse()
        vector1 = tr3d1.get_transformed(self.vectors[0])
        vector2 = tr3d2.get_transformed(self.vectors[1])
        centroid1 = tr3d1.get_transformed(self.centroids[0])
        centroid2 = tr3d2.get_transformed(self.centroids[1])
        #TODO: an option to enforce rbs really on the same line, this implementation treats parallel vectors as collinear

        #Method 1: Two vectors are collinear if they crossproduct is a null vector
        # crossproduct = numpy.cross(centroid1-vector1, centroid2-vector2)
        # dist = numpy.linalg.norm(crossproduct-[0, 0, 0])
        # return self.k * (dist)**2

        # #Method 2: Two vectors are collinear if relations of their coordinates are equal, i.e. x1 / x2 = y1 / y2 = z1 / z2
        # scalars = [a/b for a, b in zip(centroid1-vector1, centroid2-vector2)]
        # dist = numpy.linalg.norm([a-b for a,b in zip(scalars, [1, 1, 1])])
        # return self.k * (dist)**2

        crossproduct1 = numpy.cross(centroid1-vector1, centroid1-centroid2)
        dist1 = numpy.linalg.norm(crossproduct1-[0, 0, 0])
        crossproduct2 = numpy.cross(centroid2-vector2, centroid1-centroid2)
        dist2 = numpy.linalg.norm(crossproduct2-[0, 0, 0])

        return self.k * (dist1+dist2)**2


        #this is wrong, can't do with three points
        # point1 = tr3d1.get_transformed(self.points[0][-2])
        # point2 = tr3d1.get_transformed(self.points[0][-1])
        # point3 = tr3d1.get_transformed(self.points[1][-1])
        # crossproduct = numpy.cross(point1-point2, point1-point3)
        # dist = numpy.linalg.norm(crossproduct-[0, 0, 0])
        # return self.k * (dist)**2

    def do_get_inputs(self):
        return self.rb1.get_particle(), self.rb2.get_particle()

class PreconditionedRigidBodyMover(IMP.core.MonteCarloMover):
    def __init__(self, m, rb, score_function, max_translation=2, max_rotation=0.5):
        IMP.core.MonteCarloMover.__init__(self, m, rb.get_particle().get_name())
        self.pi = rb.get_particle()
        self.rb = rb
        self.score_function = score_function
        self.extra_mover = IMP.core.RigidBodyMover(self.rb.get_model(), self.rb.get_particle_index(), max_translation, max_rotation)
        self.last_transformation = self.rb.get_reference_frame().get_transformation_to()


    def do_propose(self):
        moves = []
        self.last_transformation = self.rb.get_reference_frame().get_transformation_to()
        for i in range(10):
            self.extra_mover.propose()
            score = self.score_function.evaluate(False)
            move = self.rb.get_reference_frame().get_transformation_to()
            moves.append([move, score])
            self.extra_mover.reject()

        move_to_use = sorted(moves, key=lambda x: x[1])[0][0]

        self.rb.set_reference_frame(ReferenceFrame3D(move_to_use))

        return IMP.core.MonteCarloMoverResult([self.pi], 1.0)

    def do_reject(self):
        self.rb.set_reference_frame(ReferenceFrame3D(self.last_transformation))

    def do_get_inputs(self):
        return [self.get_model().get_particle(self.pi)]

class DummyRestraint(IMP.Restraint):
    def __init__(self, m, ps):
        '''
        Just a restraint that does nothing.
        '''
        self.ps = ps
        IMP.Restraint.__init__(self, m, "DummyRestraint")


    def unprotected_evaluate(self, da):
        '''Ruturn nothing.
        '''

        return 0.0

    def do_get_inputs(self):
        return self.ps

def select_rb_particles_at_resolution(m, rb, resolution):
    rb_ps = [m.get_particle(pi) for pi in rb.get_member_indexes()]
    mols = set([])
    for p in rb_ps:
        mols.add(get_parent_mol(IMP.atom.Hierarchy(p)))

    mol_ps = []
    for mol in mols:
        mol_ps.extend(IMP.atom.Selection(mol, resolution=resolution).get_selected_particles())
    resol_ps = []
    for p in rb_ps:
        if p in mol_ps:
            resol_ps.append(p)

    return resol_ps

class TransformedRotationPairRestraint(IMP.Restraint):
    """Score the distance between two rigid bodies after transforming the second.

    The distance is calculated based on transformations of the rigid bodies using Frobenius norm.

    * mode - 
        rmsd - very costly but accurate, use resolution and max_number_of_particles options to optimize for speed
    * max_number_of_particles - this many particles will be take from each rb for calculations, for speed, suggest to use at least five
    * scaleTranslationFactor - scaling factor for translation before calculating the norm

    TODO: the transformation based restraint relies currently on arbitrary weighting between 
    rotation and translation.
    This might be possible to be done more objectively as in:
    Romain Brégier, Frédéric Devernay, Laetitia Leyrit, James L. Crowley.
    Defining the Pose of any 3D Rigid Object and an Associated Distance. 
    International Journal of Computer Vision, Springer Verlag, 2018, 126 (6), pp.571-596.
    10.1007/s11263-017-1052-4 . hal-01415027v3
    """
    def __init__(self, m, p1, p2, tr3d, k=1, scaleTranslationFactor=1, mode='rmsd', resolution=10, max_number_of_particles=5):
        IMP.Restraint.__init__(self, m, "TransformedRotationsPairRestraint")
        raise NotImplementedError
        self.p1 = p1
        self.rb1 = IMP.core.RigidBody(m, self.p1)
        self.p2 = p2
        self.rb2 = IMP.core.RigidBody(m, self.p2)
        if self.rb1 is self.rb2:
            raise Exception('TransformedRotationPairRestraint will not work if an rb restrained with itself.')
        self.tr3d = tr3d
        self.mode = mode
        self.rb1_ps = select_rb_particles_at_resolution(m, self.rb1, resolution)
        self.rb2_ps = select_rb_particles_at_resolution(m, self.rb2, resolution)
        
        #take max_number_of_particles or approximately every nth to get max_number_of_particles (for speed)
        if len(self.rb1_ps) > max_number_of_particles:
            every_nth = int(len(self.rb1_ps)/max_number_of_particles)
            self.selected_rb1_ps = self.rb1_ps[::every_nth]
            self.selected_rb2_ps = self.rb2_ps[::every_nth]
        else:
            self.selected_rb1_ps = self.rb1_ps
            self.selected_rb2_ps = self.rb2_ps

        self.selected_rb1_XYZs = IMP.core.XYZs(self.selected_rb1_ps)
        self.selected_rb2_XYZs = IMP.core.XYZs(self.selected_rb2_ps)

        if self.mode in ('transformation', 'transformation1'):
            self.k = k
            self.scaleTranslationFactor = scaleTranslationFactor
            self.ini_tr3d1 = self.rb1.get_reference_frame().get_transformation_to()
            self.ini_tr3d2 = self.rb2.get_reference_frame().get_transformation_to()
            self.ini_tr3d = IMP.atom.get_transformation_aligning_first_to_second(self.rb2_ps, self.rb1_ps)
            # print(IMP.algebra.get_axis_and_angle(self.ini_tr3d.get_rotation()), self.ini_tr3d.get_translation())
            # print(IMP.algebra.get_axis_and_angle(self.tr3d.get_rotation()), self.tr3d.get_translation())

    def get_matrix_from_transformation3d(self, tr3d):
        rot = tr3d.get_rotation()
        row1 = list(rot.get_rotation_matrix_row(0))
        row2 = list(rot.get_rotation_matrix_row(1))
        row3 = list(rot.get_rotation_matrix_row(2))
        trans = [x for x in tr3d.get_translation()]

        # print('before', self.rb1.get_name(), self.rb2.get_name(), [
        #     row1 + [trans[0]],
        #     row2 + [trans[1]],
        #     row3 + [trans[2]]
        # ])

        #normalize translation vector elements to 0 - 2pi range
        min_trans = min(trans)
        max_trans = max(trans)
        trans_range = max_trans-min_trans
        auto_scale_factor = max([item for sublist in [row1, row2, row3] for item in sublist]) - min([item for sublist in [row1, row2, row3] for item in sublist])
        # auto_scale_factor = 6.28319
        if trans_range != 0:
            norm_trans = [(x-min_trans)/(trans_range)*(auto_scale_factor) for x in trans] #https://stats.stackexchange.com/questions/281162/scale-a-number-between-a-range
            # print(1)
        elif max_trans != 0:
            # print(2)
            norm_trans = [x/max_trans for x in trans]
        else: #happens only when translation is [0,0,0], or ... can translation be negative here?
            # print(3)
            norm_trans = trans

        # print('after ', self.rb1.get_name(), self.rb2.get_name(), [
        #     row1 + [norm_trans[0]/self.scaleTranslationFactor],
        #     row2 + [norm_trans[1]/self.scaleTranslationFactor],
        #     row3 + [norm_trans[2]/self.scaleTranslationFactor]
        # ])

        return [
            row1 + [norm_trans[0]/self.scaleTranslationFactor],
            row2 + [norm_trans[1]/self.scaleTranslationFactor],
            row3 + [norm_trans[2]/self.scaleTranslationFactor]
        ]

        #keep for testing
        # return [
        #     row1 + [trans[0]/self.scaleTranslationFactor],
        #     row2 + [trans[1]/self.scaleTranslationFactor],
        #     row3 + [trans[2]/self.scaleTranslationFactor]
        # ]

        # return [
        #     row1 + [trans[0]],
        #     row2 + [trans[1]],
        #     row3 + [trans[2]]
        # ]

        # return [
        #     row1 + [trans[0]*10],
        #     row2 + [trans[1]*10],
        #     row3 + [trans[2]*10]
        # ]

    def unprotected_evaluate(self, da):
        if self.mode == 'transformation':
            # print('1', self.rb1.get_name(), self.rb1.get_reference_frame().get_transformation_to(), self.ini_tr3d1)
            # print('2', self.rb2.get_name(), self.rb2.get_reference_frame().get_transformation_to(), self.ini_tr3d2)
            tr3d1 = self.rb1.get_reference_frame().get_transformation_to() * self.ini_tr3d1.get_inverse()
            tr3d2 = self.rb2.get_reference_frame().get_transformation_to() * self.ini_tr3d2.get_inverse()
            # print('>', self.rb1.get_name(), self.rb2.get_name(), tr3d1, tr3d2)
            t = tr3d2.get_inverse() * self.ini_tr3d * tr3d1
            rot1 = self.get_matrix_from_transformation3d(self.tr3d)
            rot2 = self.get_matrix_from_transformation3d(t)
            # # keep for debugging
            # rot_dist = numpy.linalg.norm(numpy.array(self.get_matrix_from_transformation3d(self.tr3d))[:,0:3]-numpy.array(self.get_matrix_from_transformation3d(t))[:,0:3])
            # trans_dist = numpy.linalg.norm(numpy.array(self.get_matrix_from_transformation3d(self.tr3d))[:,3]-numpy.array(self.get_matrix_from_transformation3d(t))[:,3])
            dist = numpy.linalg.norm(numpy.array(rot1)-numpy.array(rot2))
            # print(dist, rot_dist, trans_dist)
            return self.k * (dist)**2
        elif self.mode == 'transformation1':
            tr3d2 = IMP.atom.get_transformation_aligning_first_to_second(self.selected_rb2_ps, self.selected_rb1_ps)
            rot1 = self.get_matrix_from_transformation3d(self.tr3d)
            rot2 = self.get_matrix_from_transformation3d(tr3d2)
            # print(tr3d2)
            dist = numpy.linalg.norm(numpy.array(rot1)-numpy.array(rot2))
            # print(IMP.algebra.get_axis_and_angle(self.tr3d.get_rotation()), self.tr3d.get_translation())
            # print('>', IMP.algebra.get_axis_and_angle(tr3d2.get_rotation()), tr3d2.get_translation(), dist)

            return self.k * (dist)**2

        elif self.mode == 'rmsd':
            coords1 = [self.tr3d.get_transformed(x.get_coordinates()) for x in self.selected_rb1_XYZs] #transform copy of coords, not rbs, perhaps for speed and numerical stability of rb transforms
            coords2 = [x.get_coordinates() for x in self.selected_rb2_XYZs]
            rmsd = calc_rmsd(coords2, coords1)
            
            return rmsd

        

    def do_get_inputs(self):
        if self.mode == 'transformation':
            return self.rb1_ps + self.rb2_ps
        else:
            return self.selected_rb1_ps + self.selected_rb2_ps

def calc_rmsd(V, W):
    """
    Calculate Root-mean-square deviation from two sets of vectors V and W.
    Parameters
    ----------
    V : array
        (N,D) matrix, where N is points and D is dimension.
    W : array
        (N,D) matrix, where N is points and D is dimension.
    Returns
    -------
    rmsd : float
        Root-mean-square deviation between the two vectors
    """
    diff = numpy.array(V) - numpy.array(W)
    N = len(V)
    return numpy.sqrt((diff * diff).sum() / N)
