#!/usr/bin/python
from optparse import OptionParser
import os
from collections import defaultdict

import IMP

import superconfig
import imp_utils1
from imp_utils1 import params_reader

def main():
    usage = "usage: %prog [options] cfg_filename params_file"
    parser = OptionParser(usage=usage)

    def list_split_callback(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    parser.add_option("--pdb_dir", dest="pdb_dir",
                      help="Replace basename dir for pdb files with this pdb_dir", metavar="DIR")

    parser.add_option("--project_dir", dest="project_dir",
                      help="Replace basename dir for data with this dir", metavar="DIR")

    parser.add_option("-o", dest="outfilename", default='symm_groups.txt',
                      help="Custom outfilename, symm_groups.txt")

    parser.add_option('--other_states',
                      type='string',
                      action='callback',
                      callback=list_split_callback,
                      default=[])

    choices = ['subunit', 'series', 'symmetry']
    default = 'subunit'
    parser.add_option("--by", dest="by", type="choice", choices=choices,
                      help="Group by what, choices: {0}, default: {1}".format(choices, default), default=default)

    parser.add_option("--extra_series_groups", dest="extra_series_groups", default=None,
                      help="Group series, comma- and semicolon-separated, e.g. --extra_series_groups NR_1,NR_2;CR_1,CR_2")

    (options, args) = parser.parse_args()


    cfg_filenames = [args[0]] + options.other_states
    config = superconfig.Config(cfg_filenames, project_dir=options.project_dir, do_read_data=True)

    params_file = args[1]

    if options.pdb_dir:
        config.change_pdb_dir(options.pdb_dir)


    params = params_reader.read_params(params_file)
    if params.add_symmetry_constraints:
        config.add_auto_symmetries(use_constraints=True)
    if params.add_symmetry_restraints:
        config.add_auto_symmetries(use_constraints=False)

    converter = imp_utils1.Config2SystemConverter(config)
    converter.create_system(states=config.number_of_states)

    pmi_system = converter.system

    groups = []
    if options.by == 'subunit':
        for state in pmi_system.states:
            for subunit, molecules in state.molecules.items():
                groups.append([])
                for mol in molecules:
                    molname = mol.get_name()+'.'+ str(IMP.atom.Copy(mol.hier).get_copy_index())
                    groups[-1].append(molname)


    elif options.by in ('series', 'symmetry'):
        if options.by == 'symmetry':
            #TODO: handle properly symmetry-related series
            print('Warning: series related by symmetry to other series (through ref_serie) are not grouped together at the moment')
        

        if options.extra_series_groups:
            extra_series_groups = {}
            for x in options.extra_series_groups.split(';'):
                series_grouped_by_subunit = defaultdict(list)
                for serie_name in x.split(','):
                    #group by subunit
                    for serie in config.get_series_by_name(serie_name):
                        series_grouped_by_subunit[serie.subunit].append(serie)

                for subunit, series in series_grouped_by_subunit.items():
                    extra_series_groups[tuple(series)] = []

        for serie in config.series:
            if options.by == 'series':
                if not options.extra_series_groups:
                    groups.append([])
                    curr_group = groups[-1]
                else:
                    found = False
                    for group_key, group in extra_series_groups.items():
                        if serie in group_key:
                            found = True
                            if group not in groups:
                                groups.append(group)
                                curr_group = groups[-1]
                            else:
                                curr_group = group
                            break
                    if not found:
                        groups.append([])
                        curr_group = groups[-1]

            for state_idx, state_mols in enumerate(serie.molecules):
                for copy_idx, mol in enumerate(state_mols):
                    molname = mol.get_name()+'.'+ str(IMP.atom.Copy(mol.hier).get_copy_index())

                    if options.by == 'symmetry':
                        selector = {'subunit': mol.get_name(), 'state': state_idx, 'serie': serie.name, 'copies': [copy_idx]}
                        if config.is_selector_symmetry_constrained_or_restrained(selector):
                            groups[-1].append(molname)
                        else:
                            groups.append([molname])

                    elif options.by == 'series':
                        curr_group.append(molname)

    with open(options.outfilename, 'w') as f:
        f.write('\n'.join([' '.join(group) for group in groups]))

if __name__ == '__main__':
    main()