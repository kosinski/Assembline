#!/usr/bin/python
from optparse import OptionParser
from string import Template
import os
import sys
import subprocess
import shutil
import logging

import IMP

import superconfig
import imp_utils1
from imp_utils1.optimization import PrintOptimizerState, WriteTotalScoreOptimizerState, DeNovoMCSAProtocol, DeNovoMCSACGProtocol, RefineProtocol

from imp_utils1 import params_reader


def get_used_discrete_restraints(all_discrete_restraints, movers):
    out = []
    for restr in all_discrete_restraints:
        if restr.mover in movers:
            out.append(restr)

    return out

def setup_the_system(params, cfg_filenames, params_file, out_dir, options):

###########################################################################
############               Intitiate the system                ############
###########################################################################

    config = superconfig.Config(cfg_filenames)

    for cfg_filename in cfg_filenames:
        shutil.copy(cfg_filename, out_dir)
    shutil.copy(params_file, out_dir)

    if options.pdb_dir:
        config.change_pdb_dir(options.pdb_dir)

    converter = imp_utils1.Config2SystemConverter(config)
    print("Number of states: {0}".format(config.number_of_states))
    converter.create_system(states=config.number_of_states)

    system = converter.system

    r = imp_utils1.MultiRepresentation(config,
                                        pmi_system=system,
                                        struct_resolutions=params.struct_resolutions,
                                        add_missing=params.add_missing,
                                        missing_resolution=params.missing_resolution,
                                        add_rbs_from_pdbs=params.add_rbs_from_pdbs,
                                        rb_max_rot=params.rb_max_rot,
                                        rb_max_trans=params.rb_max_trans,
                                        beads_max_trans=params.beads_max_trans,
                                        ca_only=params.ca_only
                                        )
    # keep for debugging
    # r.write_as_rmf('ini0.rmf', [])
    # sys.exit()

###########################################################################
############            Add connectivity restraints            ############
###########################################################################

    if params.add_connectivity_restraints:
        conn_kwargs = {
            'connectivity_restraint_weight': params.connectivity_restraint_weight,
            'max_conn_gap': params.max_conn_gap,
            'k': params.connectivity_restraint_k,
            'conn_reweight_fn': params.conn_reweight_fn,
            'first_copy_only': params.conn_first_copy_only
        }

        r.add_connectivity_restraints(**conn_kwargs)

        r.all_restraints['conn_restraints'] = r.conn_restraints
        
        print("Added {0} connectivity restraints".format(len(r.conn_restraints)))

###########################################################################
############              Add symmetry restraints              ############
###########################################################################

    if params.add_symmetry_constraints:
        print('Adding symmetry constraints')
        r.add_symmetry_restraints(use_constraints=True, use_restraints=False)
        print('Constrained {0} pairs of rigid bodies and beads'.format(len(r.sym_constraint_pairs)))
        assert len(r.sym_constraint_pairs) > 0

    if params.add_symmetry_restraints:
        print('Adding symmetry restraints')
        sym_restraints = r.add_symmetry_restraints(params.symmetry_restraints_weight, use_constraints=False, use_restraints=True)
        r.all_restraints['sym_restraints'] = sym_restraints
        print('Created {0} symmetry restraints'.format(len(sym_restraints)))
        assert len(sym_restraints) > 0
    

###########################################################################
############          Add excluded volume restraints           ############
###########################################################################
    print('Adding excluded volume constraints')
    for ev_restr_cfg in params.ev_restraints:
        if ev_restr_cfg['name'] == 'ev_restraints_only_with_structure':
            ev_r = r.add_ev_restraints_only_atomic_temp(weight=ev_restr_cfg.get('weight'),
                                       resolution=ev_restr_cfg.get('repr_resolution'),
                                       copies=ev_restr_cfg.get('copies'),
                                       distance_cutoff=ev_restr_cfg.get('distance_cutoff') or 0.0,
                                       slack=ev_restr_cfg.get('slack') or 5.0)
        else:
            ev_r = r.add_ev_restraints(weight=ev_restr_cfg.get('weight'),
                                       resolution=ev_restr_cfg.get('repr_resolution'),
                                       copies=ev_restr_cfg.get('copies'),
                                       distance_cutoff=ev_restr_cfg.get('distance_cutoff') or 0.0,
                                       slack=ev_restr_cfg.get('slack') or 5.0)


        assert len(ev_r) > 0
        assert len(ev_r) == r.config.number_of_states
        r.all_restraints[ev_restr_cfg['name']] = ev_r

###########################################################################
############              Add cross-link restraints            ############
###########################################################################

    if params.add_xlink_restraints:
        print('Adding xlink restraints')
        if config.xlinksSetsMerged is None:
            logger.error('No cross-links read from JSON')
            sys.exit()

        r.all_restraints['xlink_restraints'] = r.add_xlink_restraints(config.xlinksSetsMerged,
                               min_ld_score=params.x_min_ld_score,
                               weight=params.x_weight,
                               xlink_distance=params.x_xlink_distance,
                               k=params.x_k,
                               inter_xlink_scale=params.x_inter_xlink_scale,
                               first_copy_only=params.x_first_copy_only,
                               xlink_score_type=params.x_xlink_score_type,
                               skip_pair_fn=params.x_skip_pair_fn,
                               log_filename=params.x_log_filename,
                               score_weighting=params.x_score_weighting,
                               reweight_function=params.xlink_reweight_fn,
                               x_random_sample_fraction=params.x_random_sample_fraction
                               )

        print('Added {0} xlink restraints'.format(len(r.all_restraints['xlink_restraints'])))

###########################################################################
############                    Add EM restraints              ############
###########################################################################
        
    print('Adding EM restraints')
    em_restraints = r.add_em_restraints()
    print('Added {0} EM restraints'.format(len(em_restraints)))
    for em_r in em_restraints:
        r.all_restraints[em_r.get_name()] = [em_r]


###########################################################################
#####  Add restraints that keep the states similar if possible  ###########
###########################################################################

    if params.add_parsimonious_states_restraints:
        args = {}
        args['weight'] = params.parsimonious_states_weight
        args['distance_threshold'] = params.parsimonious_states_distance_threshold
        args['exclude_rbs_fn'] = params.parsimonious_states_exclude_rbs
        args['resolution'] = params.parsimonious_states_representation_resolution
        args['restrain_rb_transformations'] = params.parsimonious_states_restrain_rb_transformations

        r.all_restraints['parsimonious_states_restraints'] = r.add_parsimonious_states_restraints(**args)

###########################################################################
############                Add elastic network                ############
###########################################################################

    elastic_restraints = r.create_elastic_network()
    for el_r in elastic_restraints:
        r.all_restraints[el_r.get_name()] = [el_r]
    print('Added {0} elastic restraints'.format(len(elastic_restraints)))

###########################################################################
############                Add interaction restraints                ############
###########################################################################

    interaction_restraints = r.create_interaction_restraints()
    for int_r in interaction_restraints:
        if int_r.get_name() not in r.all_restraints:
            r.all_restraints[int_r.get_name()] = [int_r]
        else:
            r.all_restraints[int_r.get_name()].append(int_r)

###########################################################################
############                Add interaction restraints                ############
###########################################################################

    similar_orientation_restraints = r.add_similar_orientation_restraints()
    for restr in similar_orientation_restraints:
        r.all_restraints[restr.get_name()] = [restr]
    print('Added {0} similar_orientation_restraints'.format(len(similar_orientation_restraints)))

###########################################################################
############                Add custom restraints              ############
###########################################################################

    if params.create_custom_restraints:
        custom_restraints = params.create_custom_restraints(r)
    else:
        custom_restraints = {}
    for restr_name, custom_restr in custom_restraints.items():
        r.all_restraints[restr_name] = custom_restr
    print('Added {0} custom_restraints'.format(len(custom_restraints)))

###########################################################################
######### Inititate lists of scoring functions and restraints  ############
###########################################################################

    score_functions = {} # maps scoring functions names to IMP ScoringFunctions

###########################################################################
############   Add discrete movers and restraints              ############
###########################################################################

    if params.discrete_mover_weight_score_fn:
        raise NotImplementedError
        sel_restraints = []
        #TODO: add code for adding restraints for DiscreteWeightedMovers

        discrete_mover_weight_score_fn = IMP.core.RestraintsScoringFunction(sel_restraints, "scoring function")
        score_functions[params.discrete_mover_weight_score_fn] = discrete_mover_weight_score_fn
        score_functions_restraints[params.discrete_mover_weight_score_fn] = sel_restraints

    discrete_movers = r.make_discrete_movers(restr_weight=params.discrete_restraints_weight)

##################################################################################################################
############          Custom movers specified in a user defined params.add_custom_movers function     ############
##################################################################################################################

    if params.add_custom_movers:
        params.add_custom_movers(r, r.all_restraints)

###########################################################################
############          Randomize initial positions              ############
###########################################################################

    if params.randomize_initial_positions:
        r.randomize(remove_clashes=params.randomize_initial_positions_remove_clashes)
        # r.write_as_rmf('/tmp/test.rmf', r.xlink_restraints+r.conn_restraints)

    if params.custom_preprocessing:
        params.custom_preprocessing(r)

    return r

def run_DeNovoMCSAProtocol(r, params, options):
    if params.do_ini_opt:
        print()
        print('Initial optimization:')
        protocol = DeNovoMCSAProtocol(r, params,
                                    options.prefix,
                                    options.outdir,
                                    save_traj=False,
                                    schedule_name='ini_opt')
        protocol.set_movers(params.get_movers_for_ini_opt)
        r.all_restraints['discrete_restraints'] = get_used_discrete_restraints(r.discrete_restraints, protocol.movers)
        sf_cfg = params.scoring_functions[params.score_func_ini_opt]
        restraints_ini_opt = []
        for restr in sf_cfg['restraints']:
            restraints_ini_opt.extend(r.all_restraints[restr])

        protocol.create_scoring_function(restraints_ini_opt)
        protocol.run(SA_schedule=params.ini_opt_SA_schedule, move_floppy_rbs_close_to_hierarchies=True)

    print()
    print("Main optimization:")
    protocol = DeNovoMCSAProtocol(r, params,
                                options.prefix,
                                options.outdir,
                                save_traj=options.save_traj)
    protocol.set_movers(get_movers_fn=params.get_movers_for_main_opt)
    r.all_restraints['discrete_restraints'] = get_used_discrete_restraints(r.discrete_restraints, protocol.movers)
    sf_cfg = params.scoring_functions[params.score_func]
    restraints_main = []
    for restr in sf_cfg['restraints']:
        restraints_main.extend(r.all_restraints[restr])        
    protocol.create_scoring_function(restraints_main)
    if not params.do_ini_opt:
        protocol.run(SA_schedule=params.SA_schedule, move_floppy_rbs_close_to_hierarchies=True)
    else:
        protocol.run(SA_schedule=params.SA_schedule, move_floppy_rbs_close_to_hierarchies=False)

    protocol.process_output(save_rmf_models=options.save_rmf_models)

    return protocol

def run_DeNovoMCSACGProtocol(r, params, options):

    if params.do_ini_opt:
        print()
        print('Initial optimization:')
        protocol = DeNovoMCSACGProtocol(r, params,
                                    options.prefix,
                                    options.outdir,
                                    save_traj=False,
                                    schedule_name='ini_opt')
        protocol.set_movers(params.get_movers_for_ini_opt)
        r.all_restraints['discrete_restraints'] = get_used_discrete_restraints(r.discrete_restraints, protocol.movers)

        sf_cfg = params.scoring_functions[params.score_func_ini_opt]
        restraints_ini_opt = []
        for restr in sf_cfg['restraints']:
            restraints_ini_opt.extend(r.all_restraints[restr])

        if hasattr(params, 'score_func_for_CG'):
            sf_cfg = params.scoring_functions[params.score_func_for_CG]
            restraints_for_CG = []
            for restr in sf_cfg['restraints']:
                restraints_for_CG.extend(r.all_restraints[restr])  
        else:
            restraints_for_CG = restraints_ini_opt

        protocol.create_scoring_function(restraints_ini_opt, restraints_for_CG)
        protocol.move_floppy_rbs_close_to_hierarchies()
        protocol.run(SA_schedule=params.ini_opt_SA_schedule)

    print()
    print("Main optimization:")
    r.all_restraints['discrete_restraints'] = get_used_discrete_restraints(r.discrete_restraints, protocol.movers)
    sf_cfg = params.scoring_functions[params.score_func]
    restraints_main = []
    for restr in sf_cfg['restraints']:
        restraints_main.extend(r.all_restraints[restr])

    if hasattr(params, 'score_func_for_CG'):
        sf_cfg = params.scoring_functions[params.score_func_for_CG]
        restraints_for_CG = []
        for restr in sf_cfg['restraints']:
            restraints_for_CG.extend(r.all_restraints[restr])  
    else:
        restraints_for_CG = restraints_main

    protocol = DeNovoMCSACGProtocol(r, params,
                                options.prefix,
                                options.outdir,
                                save_traj=options.save_traj)
    protocol.set_movers(get_movers_fn=params.get_movers_for_main_opt)
    protocol.create_scoring_function(restraints_main, restraints_for_CG)
    if not params.do_ini_opt:
        protocol.move_floppy_rbs_close_to_hierarchies()

    sf = protocol.run(SA_schedule=params.SA_schedule)

    protocol.process_output(save_rmf_models=options.save_rmf_models)


    return protocol

def run_RefineProtocol(r, params, options):
    r.move_floppy_beads_close_to_hierarchies()

    if params.do_ini_opt:
        print()
        print('Initial optimization:')
        protocol = RefineProtocol(r, params,
                                    options.prefix,
                                    options.outdir,
                                    save_traj=options.save_traj,
                                    schedule_name='ini_opt')

        protocol.set_movers(params.get_movers_for_ini_opt)

        sf_cfg = params.scoring_functions[params.score_func_ini_opt]
        restraints_ini_opt = []
        for restr in sf_cfg['restraints']:
            restraints_ini_opt.extend(r.all_restraints[restr])

        if hasattr(params, 'score_func_for_CG'):
            sf_cfg = params.scoring_functions[params.score_func_for_CG]
            restraints_for_CG = []
            for restr in sf_cfg['restraints']:
                restraints_for_CG.extend(r.all_restraints[restr])  
        else:
            restraints_for_CG = restraints_ini_opt

        protocol.create_scoring_function(restraints_ini_opt, restraints_for_CG)            
        protocol.run(SA_schedule=params.ini_opt_SA_schedule)
        

    print()
    print("Main optimization:")
    protocol = RefineProtocol(r, params,
                                options.prefix,
                                options.outdir,
                                save_traj=options.save_traj)

    protocol.set_movers(get_movers_fn=params.get_movers_for_refine)

    sf_cfg = params.scoring_functions[params.score_func]
    restraints_main = []
    for restr in sf_cfg['restraints']:
        restraints_main.extend(r.all_restraints[restr])

    if hasattr(params, 'score_func_for_CG'):
        sf_cfg = params.scoring_functions[params.score_func_for_CG]
        restraints_for_CG = []
        for restr in sf_cfg['restraints']:
            restraints_for_CG.extend(r.all_restraints[restr])  
    else:
        restraints_for_CG = restraints_main

    protocol.create_scoring_function(restraints_main, restraints_for_CG)
    protocol.run(SA_schedule=params.SA_schedule)

    protocol.process_output(save_rmf_models=options.save_rmf_models)

    return protocol

def main():
    """
    """
    usage = "usage: %prog [options] cfg_filename protocol_name(denovo or refine)"
    parser = OptionParser(usage=usage)
    
    em_score_choices = ('FitRestraint', 'EnvelopePenetrationRestraint', 'EnvelopeFitRestraint', 'ExcludeMapRestraint')

    def list_split_callback(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))
    
    parser.add_option("-o", "--outdir", dest="outdir",
                      help="optional DIR with output", metavar="DIR")

    # parser.add_option("-r", "--runs", type="int", dest="no_of_runs", default=1,
    #                   help="Number of runs [default: %default]")

    parser.add_option("--pdb_dir", dest="pdb_dir",
                      help="Replace basename dir for pdb files with this pdb_dir", metavar="DIR")

    parser.add_option("--prefix", type="str", dest="prefix", default='',
                      help="Prefix for model names, e.g. for differential naming of models like 0000001, 0000002 when the script is run multiple times [default: %default]")

    parser.add_option("--traj", action="store_true", dest="save_traj", default=False,
                      help="save trajectories? [default: %default]")

    parser.add_option("--models", action="store_true", dest="save_rmf_models", default=False,
                      help="save models in RMF format? [default: %default]")

    parser.add_option('--other_states',
                      type='string',
                      action='callback',
                      callback=list_split_callback,
                      default=[])

    parser.add_option(
        "-v",
        "--verbose",
        action="store_true",
        dest="verbose",
        default=False,
        help="verbose output [default: %default]")


    (options, args) = parser.parse_args()


###########################################################################
############            Print software versions                ############
###########################################################################

    # defunct because of problems with git on some cluster nodes

    if shutil.which('git') is not None:
        print('==================')
        print()
        label = subprocess.check_output(["git", "describe", '--always'], cwd=os.path.dirname(os.path.realpath(__file__)))
        print('imp_utils1 Version (git hashtag):', label.strip())
        print('==================')

        print('==================')
        print()
        label = subprocess.check_output(["git", "describe", '--always'], cwd=os.path.dirname(os.path.realpath(superconfig.__file__)))
        print('SuperConfig Version (git hashtag):', label.strip())
        print('==================')

    cfg_filenames = [args[0]] + options.other_states
    params_file = args[1]

    params = params_reader.read_params(params_file)

    print('Parameters:')
    print()
    print('Config files:', ', '.join(cfg_filenames))
    print('Parameters file:', params_file)
    for opt in vars(options):
        print(opt, getattr(options, opt))
    for k, v in params.__dict__.items():
        if not k.startswith('__'):
            print(k, v)
    print('==================')
    print()

    if options.verbose:
        IMP.set_log_level(IMP.VERBOSE)
    else:
        IMP.set_log_level(IMP.SILENT)

    if params.debug:
        logging.getLogger().setLevel(logging.DEBUG)

    if options.outdir is not None:
        out_dir = options.outdir
    else:
        out_dir = os.getcwd()


    r = setup_the_system(params, cfg_filenames, params_file, out_dir, options)

###########################################################################
############                Main optimization                  ############
###########################################################################

    # keep for debugging
    # r.write_as_rmf('ini.rmf', [])
    # sys.exit()

    if params.protocol == 'denovo_MC-SA':
        run_DeNovoMCSAProtocol(r, params, options)

    elif params.protocol == 'denovo_MC-SA-CG':
        run_DeNovoMCSACGProtocol(r, params, options)

    elif params.protocol == 'refine':
        run_RefineProtocol(r, params, options)

    elif params.protocol == 'all_combinations':
        sf = all_combinations_protocol(r, params,
                                params.score_func,
                                r.all_restraints,
                                score_functions,
                                score_functions_restraints,
                                options)

    elif params.protocol == 'all_combinations_parallel':
        sf = all_combinations_parallel_protocol(r, params,
                                params.score_func,
                                r.all_restraints,
                                score_functions,
                                score_functions_restraints,
                                options)

    elif params.protocol == 'custom':
        params.custom_protocol(r, params,
                                params.score_func,
                                r.all_restraints,
                                score_functions,
                                score_functions_restraints,
                                options)


if __name__ == '__main__':
    main()
