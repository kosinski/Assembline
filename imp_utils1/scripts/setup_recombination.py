#!/usr/bin/python

from optparse import OptionParser
import os
import sys
import json
import subprocess
from collections import defaultdict
import csv
import math

import superconfig
from pdb_utils import transform_pdb

def iter_solutions(solution_filename):
    with open(solution_filename, 'rU') as f:
        for line in f:
            try:
                yield json.loads(line.strip())
            except ValueError:
                print('Bad combination line:')
                print(line.strip())

def transpose(rot):
    return list(zip(*rot))

def read_scores(score_filename):
    scores = {}
    with open(score_filename) as f:
        for line in f:
            if line.startswith('"DiscreteMoverRestraint'):
                line = line.strip()
                name, score, eval_score, weight = line.split(',')
                rb_name = name.rsplit(' ', 1)[1].strip('"')

                scores[rb_name] = float(eval_score)/float(weight)

    return scores

def create_out_json(json_project_fn, ori_project_dir, outfilename, new_positions_dir):
    in_cfg = superconfig.read_json(json_project_fn)
    for key, item in in_cfg.items():
        if key == 'data':
            for data_item in item:
                if data_item['type'] == 'pdb_files':
                    for pdb_spec in data_item['data']:
                        pdb_fn = pdb_spec['components'][0]['filename']
                        if 'positions' in pdb_spec:
                            pdb_spec['positions'] = os.path.join(new_positions_dir, os.path.basename(pdb_fn), 'solutions_pvalues.csv')
                        for c in pdb_spec['components']:
                            c['filename'] = os.path.join(ori_project_dir, c['filename'])


    with open(outfilename, 'w') as json_file:
        json.dump(in_cfg, json_file, indent=4)

    return outfilename


def main():
    usage = "usage: %prog [options] cfg_filename sols_for_recomb.txt"
    parser = OptionParser(usage=usage)

    def list_split_callback(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))
    
    parser.add_option("-o", "--outdir", dest="outdir", default=os.getcwd(),
                      help="optional DIR with output", metavar="DIR")

    parser.add_option("--json", dest="json",
                      help="Project JSON FILE")

    parser.add_option('--other_states',
                      type='string',
                      action='callback',
                      callback=list_split_callback,
                      default=[])

    parser.add_option("--top", dest="top", default=None, type="int",
                      help="How many models from the provided list [default: all]")

    parser.add_option("--score_thresh", dest="score_thresh", default=None, type="float",
                      help="Score threshold")

    parser.add_option("--scores", dest="scores_file", default=None,
                      help="The CSV file with model scores as returned from extract_scores.py [default: %default]")

    parser.add_option("--project_dir", dest="project_dir",
                      help="Replace basename dir for data with this dir", metavar="DIR")

    parser.add_option("--json_outfile", dest="json_outfile", default=None,
                      help="Name for the output JSON [default: %default]")

    (options, args) = parser.parse_args()

    if options.other_states:
        raise NotImplementedError

    with open(options.scores_file) as f:
        score_file_data = list(csv.DictReader(f))

        if options.score_thresh is not None:
            score_file_data = list(filter(lambda x: float(x['total_score']) <= options.score_thresh, score_file_data))
        
        if options.top:
            score_file_data.sort(key=lambda x: float(x['total_score']))
            score_file_data = score_file_data[:options.top]


    cfg_filenames = [options.json] + options.other_states
    solution_filenames = ['models_txt/{0}model.txt'.format(m['model']) for m in score_file_data]

    config = superconfig.Config(cfg_filenames, project_dir=options.project_dir)
    create_out_json(options.json, options.project_dir, options.json_outfile, options.outdir)

    out_sols = defaultdict(list)
    for line in solution_filenames:
        solution_filename = line.strip()

        score_filename_base = solution_filename.replace('models_txt/', '')
        scores = None
        try:
            score_filename = score_filename_base.replace('model.txt', 'scores.txt')
            score_filename = os.path.join('scores', score_filename)
            scores = read_scores(score_filename)
        except IOError:
            score_filename = score_filename_base.replace('model0.txt', 'scores0.txt')
            scores = read_scores(score_filename)

        if scores is None:
            score_filename = score_filename_base.replace('model1.txt', 'scores1.txt')
            scores = read_scores(score_filename)

        print(solution_filename)
        print(score_filename)

        for sol in iter_solutions(solution_filename):
            out_names = []

            #this should be done within config based on 
            #a parameter in the config itself?
            if len(config.rigid_bodies) == 0:
                config.define_rbs_from_pdbs()

            for rb_cfg in config.rigid_bodies:
                name = rb_cfg['name']
                try:
                    fit = sol[name][0]
                except KeyError:
                    print(rb_cfg['name'] + ' not in solution')
                    continue
                
                #this is not used ...   

            # for rb_cfg in config.rigid_bodies:
            #     fit = sol[rb_cfg['name']][0]
            #     copy_idx = fit['copy']
                #collect pdb files for this rb:
                pdb_filenames = set([])
                for rb_comp in rb_cfg['components']:
                    for struct_cfg in config.struct_files:
                        for struct_comp in struct_cfg['components']:
                            if config.is_selector_subset_of_another(struct_comp,
                                            rb_comp,
                                            fields=['subunit', 'domain', 'serie', 'copies', 'filename']):
                                pdb_filenames.add(struct_comp['filename'])

                for pdb_fn in pdb_filenames:
                    score = None
                    try:
                        score = scores[rb_cfg['name']]
                    except KeyError:
                        print("No score for", rb_cfg['name'])
                        continue

                    if rb_cfg.get('positions_score') is None or rb_cfg.get('positions_score').startswith('log_'):
                        score = math.pow(10, score)
                    elif rb_cfg.get('positions_score') in ('overlap', 'cc_score', 'cam_score', 'score_z_c', 'score_z'):
                        score = -score

                    pdb_fn = os.path.basename(pdb_fn)
                    for_rot = [item for sublist in fit['rot'] for item in sublist]

                    rotation = ' '.join(map(str, for_rot))
                    translation = ' '.join(map(str, fit['trans']))

                    if rotation+translation not in [x['rotation']+x['translation'] for x in out_sols[pdb_fn]]: #this is not always working :-( Sometimes there are identical solutions with numerically slightly different rotation values, TODO: fix it
                        out_sols[pdb_fn].append(
                                {
                                    'rotation': rotation,
                                    'translation': translation,
                                    'filename': pdb_fn,
                                    'overlap': 0,
                                    'cc_score': 0,
                                    'cam_score': 0,
                                    'score_z_c': 0,
                                    'pvalues': 0,
                                    'BH_adjusted_pvalues': 0,
                                    'BH_adjusted_pvalues_one_tailed': 0 
                                }

                            )
                        score_name = rb_cfg.get('positions_score') or 'BH_adjusted_pvalues_one_tailed'
                        score_name = score_name.lstrip('log_')
                        out_sols[pdb_fn][-1][score_name] = score

    os.makedirs(options.outdir, exist_ok=True)

    for pdb_fn, sols in out_sols.items():
        rows = []
        sorted_sols = sorted(sols, key=lambda x: x[score_name], reverse=False) #yeah, sort by the last score met in the previous loop, most of the time it's the same for all rbs
        for i,sol in enumerate(sorted_sols):
            sol['solution_id'] = i
            sol['filename'] = sol['filename'].replace('.pdb', '_fit'+str(i)+'.pdb')
            rows.append(sol)

        outdir = os.path.join(options.outdir, pdb_fn)
        os.makedirs(outdir, exist_ok=True)
        outfilename = os.path.join(outdir, 'solutions_pvalues.csv')
        ori_pdb_full_path = config.get_path_for_pdb_filename(pdb_fn)
        symlink_path = os.path.join(outdir, 'ori_pdb.pdb')
        if not os.path.exists(symlink_path):
            os.symlink(ori_pdb_full_path, symlink_path)

        with open(outfilename, 'w') as f:
            csv_fieldnames = ['solution_id','filename','rotation','translation','overlap','cc_score','cam_score', 'score_z_c','pvalues', 'BH_adjusted_pvalues', 'BH_adjusted_pvalues_one_tailed']
            wr = csv.DictWriter(f, csv_fieldnames, delimiter=',')
            if sys.version_info >= (3, 0):
                wr.writeheader()
            else:
                wr.writerow(dict((fn,fn) for fn in csv_fieldnames)) #do not use wr.writeheader() because python 2.6 doesn't have it
            
            wr.writerows(rows)

if __name__ == '__main__':
    main()