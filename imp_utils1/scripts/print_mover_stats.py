import sys
import glob
from collections import defaultdict

def mean(numbers):
    return float(sum(numbers))/len(numbers) if len(numbers) > 0 else float('nan')

dirname = sys.argv[1]

filenames =  glob.glob(dirname+'/*log.txt')

accept_frac_for_movers = defaultdict(list)
for fn in filenames:
    with open(fn) as f:
        for line in f:
            if line.startswith('mover stats'):
                items = line.split()
                mover = items[2]
                proposed = float(items[3])
                accepted = float(items[4])
                accept_frac = float(items[5])

                accept_frac_for_movers[mover].append(accept_frac)

for mover, fracs in accept_frac_for_movers.iteritems():
    print mover, mean(fracs)
            