from optparse import OptionParser
import os
import sys
import itertools
from collections import defaultdict
import superconfig
import compare_models

def main():
    usage = "usage: %prog [options] cfg_filename solutions_list"
    parser = OptionParser(usage=usage)
    
    parser.add_option("-o", "--outfile", dest="outfile", default=None,
                      help="optional FILE with output", metavar="FILE")

    parser.add_option("--pdb_dir", dest="pdb_dir",
                      help="Replace basename dir for pdb files with this pdb_dir", metavar="DIR")

    (options, args) = parser.parse_args()

    cfg_filename = args[0]

    models_fn = args[1]

    config = superconfig.Config(cfg_filename)

    if options.pdb_dir:
        config.change_pdb_dir(options.pdb_dir)

    with open(models_fn) as f:
        models = f.read().splitlines()

    dists = defaultdict(dict)

    for model1, model2 in itertools.combinations(models,2):
        dist = compare_models.compare(model1, model2)
        # dists.append((model1, model2, dist))
        dists[model1][model2] = dist
        dists[model2][model1] = dist

    for model in models:
        dists[model][model] = 0.0
    # print '\n'.join([','.join(map(str, l)) for l in dists])

    out = [[' ']+models[:]]
    for model1 in models:
        row = [model1]
        for model2 in models:
            row.append(str(dists[model1][model2]))

        out.append(row)

    if options.outfile:
        with open(options.outfile, 'w') as f:
            f.write('\n'.join([','.join(map(str,fields)) for fields in out]))
    else:
        print('\n'.join([','.join(map(str,fields)) for fields in out]))


if __name__ == '__main__':
    main()