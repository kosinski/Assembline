#!/usr/bin/python
import subprocess
import glob
import os
import ntpath
import sys
from optparse import OptionParser
import time
from subprocess import Popen, PIPE
from subprocess import check_output
import re
from string import Template
import getpass
from multiprocessing import Process, Queue, Manager, Pool
from itertools import zip_longest

from imp_utils1 import params_reader

usage = "usage: %prog [options] project.json params.py outdir start max_no_runs"
parser = OptionParser(usage=usage)

parser.add_option("--prefix", type="str", dest="prefix", default='',
                  help="prefix, ({0}), [default: %default]")

parser.add_option("--daemon", action="store_true", dest="daemon", default=False,
                  help="start a daemon that re-submits jobs? [default: %default]")

parser.add_option("--traj", action="store_true", dest="traj", default=False,
                  help="record trajectory? [default: %default]")

parser.add_option("--max_concurrent_jobs", type="int", dest="max_concurrent_jobs", default=10000,
                  help="Maximum number of jobs to run concurrently, ({0}), [default: %default]")

parser.add_option("--min_concurrent_jobs", type="int", dest="min_concurrent_jobs", default=0,
                  help="Minimum number of jobs to run concurrently, ({0}), [default: %default]")

parser.add_option('--other_states',
                  type='string',
                  default='')

(options, args) = parser.parse_args()

def get_free():
    """Return the number of free CPUs.

    The code below works only at EMBL cluster, you can edit it to adjust to your cluster.
    """
    output = check_output(['/etc/profile.d/clusterstatus.sh'])

    m = re.search('used: (?P<used>\d+) out of (?P<available>\d+) available cores', str(output))
    if m:
        free = int(m.group('available')) - int(m.group('used'))
        return free
    else:
        return 0

def get_my_jobs_number():
    """Return the number of CPUs you currently use.

    The code below works only for Slurm queuing system, you can edit it to adjust to your cluster.
    """
    output = check_output(['squeue', '-u', getpass.getuser()])
    return len(output.splitlines()) - 1

def get_no_jobs_to_run():
    try:
        free = get_free()
    except:
        free = 1000000
        print('Cannot estimate the number of free CPUs. Setting free CPU count to {0}'.format(free))        

    my_jobs_number = get_my_jobs_number()
    my_free = max(0, options.max_concurrent_jobs - my_jobs_number)
    to_run = max(free, options.min_concurrent_jobs - my_jobs_number)

    return min(to_run, my_free)

def do_runs(project, params_fn, outdir, start, max_no_runs):
    params = params_reader.read_params(params_fn)

    cmds = []
    for run in range(start, start+max_no_runs):

        prefix = str(run).rjust(7,'0')
        if options.prefix:
            prefix = options.prefix + '_' + prefix

        cmd_options = ''
        if options.traj:
            cmd_options += '--traj '
        if options.other_states:
            cmd_options += '--other_states '+options.other_states

        cmd = 'time python {script} {options} --prefix {prefix} -o {outdir} --models {project} {params_fn}'.format(
                script=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'optimize.py'),
                options=cmd_options,
                prefix=prefix,
                params_fn=params_fn,
                project=project,
                outdir=outdir
                )

        cmds.append({'prefix': prefix, 'outdir': outdir, 'cmd': cmd})
    
    if params.cluster_submission_command: 
        for chunk in grouper(cmds, params.ntasks):
            if len(chunk) == 1:
                cmd_lines = [chunk[0]['cmd']]
                prefix = chunk[0]['prefix']

            elif len(chunk) > 1:
                cmd_lines = []
                for cmd in chunk:
                    cmd_lines.append('{cmd} 2> {stderr} 1> {stdout} &'.format(
                                                                            cmd=cmd['cmd'],
                                                                            stderr=os.path.join(cmd['outdir'], 'logs', cmd['prefix']+'_err.txt'),
                                                                            stdout=os.path.join(cmd['outdir'], 'logs', cmd['prefix']+'_out.txt')
                                                                            )
                                    )
                prefix = '_'.join([cmd['prefix'] for cmd in chunk])

            run_script = params.run_script_templ.substitute(
                prefix=prefix,
                outdir=outdir,
                ntasks=params.ntasks,
                cmd='\n'.join(cmd_lines))

            sbatch_fn = os.path.join(outdir, 'runscripts', 'run'+prefix+'.sh')
            with open(sbatch_fn, 'w') as f:
                f.write(run_script)

            cmd = params.cluster_submission_command + ' '+ sbatch_fn
            print(cmd)
            subprocess.call(cmd, shell=True)
            time.sleep(0.1) #to avoid Slurm hiccups
    else:
        pool = Pool(processes=params.ntasks)
        for cmd in cmds:
            pool.apply_async(run_cmd, args=(cmd,))
        pool.close()
        pool.join()

def run_cmd(cmd):
    print('Running '+cmd['prefix'])
    stderr = open(os.path.join(cmd['outdir'], 'logs', cmd['prefix']+'_err.txt'), 'w')
    stdout = open(os.path.join(cmd['outdir'], 'logs', cmd['prefix']+'_out.txt'), 'w')
    try:
        subprocess.call(cmd['cmd'].split(), stdout=stdout, stderr=stderr)
    finally:
        stderr.close()
        stdout.close()

def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)

if __name__ == '__main__':
    project = args[0]
    params_fn = args[1]
    outdir = args[2]
    start = int(args[3])
    max_no_runs = int(args[4])

    subprocess.call('mkdir -p {0}'.format(outdir), shell=True)
    subprocess.call('mkdir -p {0}/logs'.format(outdir), shell=True)
    subprocess.call('mkdir -p {0}/runscripts'.format(outdir), shell=True)

    if options.daemon:
        """
        Submit a limited number of jobs a time,
        depending how many jobs you have now running and in the queue
        """
        done = 0
        while True:
            nruns = get_no_jobs_to_run()
            nruns = min(nruns, max_no_runs-done)
            nruns = max(0, nruns)
            if nruns:
                do_runs(project, params_fn, outdir, start, nruns)

            done += nruns
            start += nruns
            print("Run:", done, 'out of', max_no_runs)
            if done >= max_no_runs:
                break
            time.sleep(60)
    else:
        """
        Just submit all jobs
        """
        do_runs(project, params_fn, outdir, start, max_no_runs)

