#!/usr/bin/python

import sys
import os
from itertools import groupby

#this script can be run from chimeras internal environment, the toplevel of bash scripts contains
#an example how to run scripts such as this

import chimera
import BuildStructure
from BuildStructure import placePeptide
from chimera import runCommand

MIN_HELIX_LENGTH=10
PHI=-57.0
PSI=-47.0

def fasta_dict_from_file(fasta_file):
	fh = open(fasta_file)
	items = ["".join(list(x[1])) for x in groupby(fh,lambda line: line[0] == ">")]
	_dict = dict([(key[1:-1],sequence.strip()) for key,sequence in zip(items[::2],items[1::2])])
	for key,sequence in _dict.items():
 		_dict[key]=sequence.replace('\n','')
	fh.close()
	return _dict

def main(sysargs):
	seq_fasta=sysargs[1]
	mask_fasta=sysargs[2]
	if not (os.path.exists(seq_fasta) and os.path.exists(mask_fasta)):
		print("%s and/or %s are non existing files.\n"%(seq_fasta,mask_fasta))
	seq_dict=fasta_dict_from_file(seq_fasta)
	mask_dict=fasta_dict_from_file(mask_fasta)
	missing_keys=[key for key in seq_dict.keys() if not key in mask_dict.keys()]
	if len(missing_keys) != 0:
		print("WARNING: for keys %s there is not match in the mask fasta file.\n"%(str(missing_keys)[1:-1]))
	for key in [k for k in seq_dict.keys() if not k in missing_keys]:
		if not len(seq_dict[key]) == len(mask_dict[key]):
			print("WARNING: for key '%s' the length of sequence and mask do not match.\n"%key)
			continue
		helix_ranges = []
		mask_seq=mask_dict[key]
		sequence=seq_dict[key]
		switch=False
		sub_range = []
		for i in range(len(mask_seq)):
			char = mask_seq[i]
			if char == "H" and not switch:
				switch = True
				sub_range=[]
			elif char != "H" and switch:
				switch = False
				helix_ranges.append(sub_range)
			if switch:
				sub_range.append(i)
		for helix_res_ids in helix_ranges:
			helix_seq="".join([sequence[i] for i in helix_res_ids])
			if len(helix_seq)>=MIN_HELIX_LENGTH:
				helix_id = "%s_%s_%s_helix"%(key[:10],str(helix_res_ids[0]),str(helix_res_ids[-1]))
				phiPsis = [(PHI,PSI)]*len(helix_seq)
				try:
					placePeptide(helix_seq,phiPsis,model=helix_id)
					runCommand("write #0 %s.pdb"%helix_id)
					runCommand("close all")
				except Exception as e:
					print("Helix of sequence %s of id %s is not being created due to the following exception:\n%s"%(helix_seq,key,e))

if __name__ == "__main__":
	def usage():
		print("python generate_helix.py sequences.fasta mask.fasta\n"\
		      "mask.fasta needs the same sequence ids, the same sequence\n"\
		      "length and the sequences need to consist of the characters '-' and 'H'.")
	if len(sys.argv) != 3:
		usage()	
	main(sys.argv)
	
