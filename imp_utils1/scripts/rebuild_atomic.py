#!/usr/bin/python
from optparse import OptionParser
import os
import json
import subprocess
from collections import defaultdict
import itertools
import io
import copy
import tempfile
import shutil
import sys
from pprint import pprint
import csv
import glob
import importlib
from pathlib import Path

import RMF
import IMP
import IMP.rmf

from imp_utils1 import params_reader

import superconfig
from pdb_utils import transform_pdb

from pdbtools import pdb_tocif

import ihm.model
import ihm
import ihm.location
import ihm.dataset
import ihm.representation
import ihm.restraint
import ihm.protocol
import ihm.model
import ihm.dumper

def iter_solutions(solution_filename):
    with open(solution_filename, 'rU') as f:
        for line in f:
            try:
                yield json.loads(line.strip())
            except ValueError:
                print('Bad combination line:')
                print(line.strip())

def transpose(rot):
    return list(zip(*rot))

def is_coord_line(line):
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def coord_lines(f):
    for line in f:
        if is_coord_line(line):
            yield True, line
        else:
            yield False, line

def get_pdb_resid(line):
    return line[22:26]

def resi_grouper(sth):
    is_coord, line = sth
    if is_coord:
        chain = line[21]
        resi = get_pdb_resid(line)
        marker = chain + resi
    else:
        marker = line
    
    return marker

def chain_grouper(sth):
    is_coord, line = sth
    if is_coord:
        marker = line[21]
    else:
        marker = line
    
    return marker

def read_structure(config, struct_comp, rb_comp=None, selected_residues=None):
    struct_resi_ids = config.get_resi_ids_from_comp_cfg(struct_comp)
    if rb_comp:
        rb_resi_ids = config.get_resi_ids_from_comp_cfg(rb_comp)
        resi_ids = sorted(list(set(struct_resi_ids).intersection(rb_resi_ids)))
    else:
        resi_ids = struct_resi_ids

    if selected_residues:
        resi_ids = [r for r in resi_ids if r in selected_residues]

    chain_id = struct_comp.get('chain_id')
    if chain_id is None:
        subunit = config.get_subunit_by_name(struct_comp['subunit'])
        if subunit is not None:
            chain_id = subunit.chain_ids[0] #because in add_structures the 1st chain id of the subunit is used for reading pdb if chain not provided
    print("Reading "+struct_comp['filename'])
    pdblines = cut_pdb(struct_comp['filename'], chain_id, resi_ids)
    no_hetatm = [line for line in pdblines if not line.startswith('HETATM')]

    return no_hetatm

def cut_pdb(pdb_filename, chain, resi_ids):
    out_lines = []
    with open(pdb_filename) as f:
        for k, lines in itertools.groupby(coord_lines(f), resi_grouper):
            is_coord, line = next(lines)

            if is_coord:
                cur_chain = line[21]
                cur_resid = int(get_pdb_resid(line).strip())
                if chain is not None:
                    if (cur_chain == chain) and (cur_resid in resi_ids):
                        out_lines.append(line)
                        for is_coord, line in lines:
                            out_lines.append(line)
                elif (cur_resid in resi_ids):
                    out_lines.append(line)
                    for is_coord, line in lines:
                        out_lines.append(line)
    return out_lines


def get_cif_chain_id(line, auth_asym_id_idx):
    return line.split()[auth_asym_id_idx].strip()

def cif_chain_grouper(line, auth_asym_id_idx):
    marker = get_cif_chain_id(line, auth_asym_id_idx)
    
    return marker

def sort_pdblines_sets(rotated_pdblines_sets):
    sorted_lines = []
    all_chain_lines = itertools.chain(*rotated_pdblines_sets)
    grouped_by_resi = itertools.groupby(coord_lines(all_chain_lines), resi_grouper)
    grouped_by_resi1 = []
    for k, lines in grouped_by_resi:
        grouped_by_resi1.append((k, list([y for x,y in lines])))
    lines = []
    for k, resi_lines in sorted(grouped_by_resi1, key=lambda the_tuple: int(the_tuple[0][1:])): #[1:] because resi_grouper returns chain+resi
        lines.extend(resi_lines)
    sorted_lines.extend(lines)

    return sorted_lines

def fix_atom_numbers_cif(cif_atom_keys, lines):
    out_lines = []
    i = 1
    for line in lines:
        if is_coord_line(line):
            line_dict = dict(zip(cif_atom_keys, line.split()))
            line_dict['_atom_site.id'] = str(i)
            line = '  '.join([line_dict[key] for key in cif_atom_keys]) + '\n' #yes, double spacing, TODO: nicely format            
            i = i + 1

        out_lines.append(line)

    return out_lines

def is_chainy(line):
    '''
    Return True if the passed PDB line contains chain id according to PDB format spec
    '''
    return line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU', 'TER   ')

def fix_atom_numbers_pdb(lines):
    out_lines = []
    i = 1
    for line in lines:
        if is_chainy(line):
            atom_num = line[6:11]
            line = line[:6] + str(i).rjust(5) + line[11:]

            i = i + 1

        out_lines.append(line)

    return out_lines

def parse_pdb_lines_for_ihm(lines):
    for line in lines:
        if line[0:6] in ('ATOM  ', 'HETATM', 'ANISOU',):
            seq_id = int(line[22:26])
            atom_id = line[12:16].strip()
            type_symbol = line[76:78].strip()
            x = float(line[30:38])
            y = float(line[38:46])
            z = float(line[46:54])
            yield seq_id, type_symbol, atom_id, x, y, z

def extract_beads(rh, state_idx=0, chain_id=None):
    """Extract 1-residue beads that are not parts of rigid bodies.

    :params: rh Hierarchy, e.g. that read from RMF file (top level: states, then proteins, and then fragments)
    """
    beads = []

    for state in rh:
        if IMP.atom.State(state).get_state_index() == state_idx:
            components = state.get_children()
            for comp in components:
                this_chain_id = IMP.atom.Chain(comp).get_id()
                for fragment in comp.get_children():
                    for h in fragment.get_children():
                        for resih in h.get_children():
                            resi = IMP.atom.Residue(resih)
                            if not resih.get_children(): #resolution 1 and higher
                                if not IMP.core.RigidMember.get_is_setup(resih):
                                    xyz = list(IMP.core.XYZ(resi).get_coordinates())
                                    if chain_id is not None and chain_id == this_chain_id:
                                        pdb_str = IMP.atom.get_pdb_string(xyz,
                                                        resi.get_index(),
                                                        IMP.atom.AT_CA,
                                                        resi.get_residue_type(),
                                                        this_chain_id[0], #this will strip multi-character chain ids but doesn't matter, these chain ids are not used for anything later
                                                        resi.get_index())
                                        beads.append(pdb_str.strip())
                            else: # resolution 0
                                for atom in resih.get_children():
                                    if not IMP.core.RigidMember.get_is_setup(atom):
                                        if atom.get_name() == 'CA':
                                            xyz = list(IMP.core.XYZ(atom).get_coordinates())
                                            if chain_id is not None and chain_id == this_chain_id:
                                                pdb_str = IMP.atom.get_pdb_string(xyz,
                                                                resi.get_index(),
                                                                IMP.atom.AT_CA,
                                                                resi.get_residue_type(),
                                                                this_chain_id[0], #this will strip multi-character chain ids but doesn't matter, these chain ids are not used for anything later
                                                                resi.get_index())
                                                beads.append(pdb_str.strip())

    return beads

def get_resi_ids_from_pdblines(pdblines):
    grouped_by_resi = itertools.groupby(coord_lines(pdblines), resi_grouper)
    resi_ids = []
    for k, lines in grouped_by_resi:
        lines = list([y for x,y in lines])
        resid = int(lines[0][22:26])
        if len(lines) == 1: #CA only resi
            resi_ids.append(resid)

    return resi_ids

def build_loops(pdblines, loop_resi_ids):
    resi_ranges = list(superconfig.ranges_from_list(loop_resi_ids))
    if resi_ranges:
        # Loop refinement of an existing model
        from modeller import environ, log, selection
        from modeller.automodel import loopmodel, refine, autosched
        dirpath = tempfile.mkdtemp()
        try:
            old_cwd = os.getcwd()
            os.chdir(dirpath)
            renamed_pdblines = []
            for line in pdblines:
                renamed_pdblines.append(line[:21]+'X'+line[22:])

            model_fn = 'model.pdb'
            with open(os.path.join(dirpath, model_fn), 'w') as f:
                f.write('\n'.join(renamed_pdblines))

            log.verbose()
            env = environ()

            env.io.atom_files_directory = ['./', dirpath]

            class MyLoop(loopmodel):
                # This routine picks the residues to be refined by loop modeling
                def select_loop_atoms(self):
                    selections = []
                    for resi_range in resi_ranges:
                        selections.append(self.residue_range('{0}:{1}'.format(resi_range[0], 'X'), '{0}:{1}'.format(resi_range[1], 'X')))

                    return selection(*selections)

                def build_ini_loop(self, atmsel):
                    pass

            m = MyLoop(env,
                       inimodel=model_fn,   # initial model of the target
                       sequence='xxxx',     # code of the target
            )

            m.loop.md_level = None  # loop refinement method
            # m.loop.max_var_iterations = 300
            m.loop.library_schedule = autosched.fast
            m.make()

            outpdblines = []
            with(open(os.path.join(dirpath, 'xxxx.BL00010001.pdb'))) as f:
                for line in f.read().splitlines():
                    if is_coord_line(line):
                        outpdblines.append(line)
            
            return outpdblines

        finally:
            os.chdir(old_cwd)
            shutil.rmtree(dirpath)

    else:
        return pdblines

def convert_pdblines_to_cif(pdblines, new_chain_id):
    f = io.StringIO('\n'.join(pdblines))
    f.name = os.path.basename('chupacabra')
    new_cif = list(pdb_tocif.convert_to_mmcif(f))

    header = []
    cif_coord_lines = []
    tail = []
    for line in new_cif:
        if is_coord_line(line):
            cif_coord_lines.append(line)
        elif len(cif_coord_lines) == 0:
            header.append(line)
        else:
            tail.append(line)

    cif_atom_keys = [line.strip() for line in header if not line.startswith('#') and not line.startswith('loop') and not line.startswith('data_')]
    auth_asym_id_idx = cif_atom_keys.index('_atom_site.auth_asym_id')
    label_asym_id_idx = cif_atom_keys.index('_atom_site.label_asym_id')

    out_lines = []
    for chain_id, chain_lines in itertools.groupby(cif_coord_lines, lambda x: cif_chain_grouper(x, auth_asym_id_idx)):
        
        line = next(chain_lines)

        for line in itertools.chain([line], chain_lines):
            line_dict = dict(zip(cif_atom_keys, line.split()))
            line_dict['_atom_site.label_asym_id'] = new_chain_id
            line_dict['_atom_site.auth_asym_id'] = new_chain_id
            line_dict['_atom_site.label_entity_id'] = new_chain_id[0]
            outline = '  '.join([line_dict[key] for key in cif_atom_keys]) + '\n' #yes, double spacing, TODO: nicely format
            out_lines.append(outline)

    return header, cif_atom_keys, out_lines


def is_single_solution_file(fh):
    line = fh.readline()
    out = line.startswith('{')
    fh.seek(0)

    return out

def is_solution_list_file(fh):
    line = fh.readline()
    out = line.startswith('model')
    fh.seek(0)

    return out

class IhmModel(ihm.model.Model):
    """
    system - IHM System
    chainId_to_pdblines - a dictionary mapping chainId of the AsymUnits in the System to PDB lines
    """
    def __init__(self, system, chainId_to_pdblines, **kwargs):
        super(IhmModel, self).__init__(**kwargs)
        self.system = system
        self.chainId_to_pdblines = chainId_to_pdblines
    def get_atoms(self):
        for asym in self.system.asym_units:
            lines = self.chainId_to_pdblines[asym.id]
            for seq_id, type_symbol, atom_id, x, y, z in parse_pdb_lines_for_ihm(lines):
                yield ihm.model.Atom(asym_unit=asym,
                                     type_symbol=type_symbol, seq_id=seq_id,
                                     atom_id=atom_id, x=x, y=y, z=z, biso=0.0, occupancy=1.0)

class StartingModel(ihm.startmodel.StartingModel):
    # Override StartingModel so we can provide coordinates

    def __init__(self, asym_unit, config, pdb_comp_cfg, **kwargs):
        super(StartingModel, self).__init__(asym_unit, **kwargs)
        self.asym_unit = asym_unit
        self.config = config
        self.pdb_comp_cfg = pdb_comp_cfg

    def get_atoms(self):
        lines = read_structure(self.config, self.pdb_comp_cfg)

        for seq_id, type_symbol, atom_id, x, y, z in parse_pdb_lines_for_ihm(lines):
            yield ihm.model.Atom(
                                 asym_unit=self.asym_unit,
                                 type_symbol=type_symbol, seq_id=seq_id,
                                 atom_id=atom_id, x=x, y=y, z=z)



def apply_extra_ihm_commands(system, reps, assembly, protocol, config, options, python_file):
    spec = importlib.util.spec_from_file_location('ihm_commands', python_file)
    ihm_commands = importlib.util.module_from_spec(spec)
    sys.modules['ihm_commands'] = ihm_commands
    spec.loader.exec_module(ihm_commands)

    class utils(object):
        StartingModel = StartingModel

    ihm_commands.set_ihm_records(system, reps, assembly, protocol, config, options, utils)

def read_struct_files():

    configs = [
        '/g/kosinski/kosinski/devel/old_pipeline/SuperConfig/test/data/SpNPC/CR_Y_complex/config.json',
        '/g/kosinski/kosinski/devel/old_pipeline/SuperConfig/test/data/SpNPC/NR_Y_complex/config_sel_fits_Nup132.json',
        '/g/kosinski/kosinski/devel/old_pipeline/SuperConfig/test/data/SpNPC/IR_asym_unit/SpNPC_modeling_refine_IR_Nup97.json'
    ]

    struct_files = []
    for config_fn in configs:
        config = superconfig.Config(config_fn)
        struct_files.extend(config.struct_files)

    return struct_files


    ihm_commands.set_ihm_records(system, reps, assembly, protocol, config, options, utils)


class FakeReleaseDateDumper(ihm.dumper.Dumper):
    """Write a fake release date for compatibility with AlphaFold"""
    def dump(self, system, writer):
        with writer.loop("_pdbx_audit_revision_history",
                         ["data_content_type", "major_revision", "minor_revision",
                          "revision_date"]) as lp:
            lp.write(data_content_type='Structure model', major_revision=1, minor_revision=0, revision_date="1000-01-01")

def main():
    usage = "usage: %prog [options] cfg_filename solution_filename"
    parser = OptionParser(usage=usage)

    def list_split_callback(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))
    def list_split_callback_int(option, opt, value, parser):
        setattr(parser.values, option.dest, list(map(int, value.split(','))))

    formats = ("cif","pdb","multi_pdb", "multi_cif", "ihm")

    parser.add_option("--format", dest="format", default='cif',
                      type = "choice", choices = formats, 
                      help="Output format. Choices: {0}. Default: {1}".format(formats, 'cif'))

    parser.add_option("--pdb_dir", dest="pdb_dir",
                      help="Replace basename dir for pdb files with this pdb_dir", metavar="DIR")

    parser.add_option("--project_dir", dest="project_dir",
                      help="Replace basename dir for data with this dir", metavar="DIR")

    # parser.add_option("--rename_chains", action="store_true", dest="rename_chains", default=False,
    #                   help="rename chains to the chains from json? [default: %default]")

    parser.add_option("--first_copy_only", dest="first_copy_only", action="store_true", default=False,
                      help="Whether to save only the first copy of each series")

    parser.add_option("--first_two_copies", dest="first_two_copies", action="store_true", default=False,
                      help="Whether to save only the first two copies of each series")

    parser.add_option("--copies", dest="copies", type="string", default=None, action='callback', callback=list_split_callback_int,
                      help="Which copies of each series to save, comma separated, no space, e.g. 0,1,7")

    parser.add_option("-o", dest="outfilename",
                      help="Custom outfilename, otherwise $MODEL.cif")

    parser.add_option("--outdir", "--outdir", dest="outdir", default=None,
                      help="optional DIR with output for multi_pdb and multi_cif formats", metavar="DIR")

    parser.add_option("--rmf", dest="rmf_file",
                      help="The model in RMF file for reading out 1-residue beads that will be used to rebuild full atomic loops (to use only when modeling was run including resolution 1 or Calpha). For many long loops it can take a long time!")

    parser.add_option("--rmf_auto", dest="rmf_auto", action="store_true",
                      help="As --rmf option but automatically finds the rmf file based on the model id", default=False)

    parser.add_option('--other_states',
                      type='string',
                      action='callback',
                      callback=list_split_callback,
                      default=[])

    parser.add_option("--subunit", dest="selected_subunit",
                      help="Extract only a selected subunit", default=False)

    parser.add_option("--subunits", dest="selected_subunits",
                      help="Extract only selected subunits",
                      type='string',
                      action='callback',
                      callback=list_split_callback,
                      default=[])

    parser.add_option("--serie", dest="selected_serie",
                      help="Extract only a selected serie", default=False)

    parser.add_option("--resi_range", dest="resi_range",
                      help="Extract only a selected range", default=None)

    parser.add_option("--add_series", dest="add_series", action="store_true",
                      help="Add series to names in multi_pdb and multi_cif formats", default=False)

    parser.add_option("--top", dest="num_top_models", type=int,
                      help="Extract N number of top models. Default: 1", default=1)

    parser.add_option("--multimodel", dest="multimodel", action="store_true",
                      help="Put all models in a single multimodel file. Only supported for IHM.", default=False)

    parser.add_option("--ihm_cmds", dest="extra_ihm_commands",
                      help="Definitions for IHM (experimental feature)")

    (options, args) = parser.parse_args()

    guess_jsons = False
    if len(args) == 2:
        cfg_filenames = [args[0]] + options.other_states
        solution_filename = args[1]
        config = superconfig.Config(cfg_filenames, project_dir=options.project_dir)

        if options.pdb_dir:
            config.change_pdb_dir(options.pdb_dir)
    elif len(args) == 1:
        guess_jsons = True #will be guessing configs based on model paths
        solution_filename = args[0]

    with open(solution_filename, 'rU') as f:
        if is_single_solution_file(f):
            solution_filenames = [solution_filename]
        elif is_solution_list_file(f):
            solution_filenames = []
            csv_reader = csv.DictReader(f)
            csv_sorted = sorted(csv_reader, key=lambda d: float(d['total_score']))
            for row in itertools.islice(csv_sorted, options.num_top_models):
                if '/' in row['model']: #id is already a path (e.g. in "multi" mode of extract_scores.py)
                    solution_filenames.append(row['model'])
                else:
                    #get paths to all models in case of mulitstate modeling
                    path_str = os.path.join('models_txt', row['model']+'model') + '*'
                    solution_filenames.extend(glob.glob(path_str))
        else:
            print('Cannot recognize the format of solution_filename')
            sys.exit()

    selected_residues = []
    if options.resi_range:
        if not (options.selected_subunit or options.selected_serie):
            print('--resi_range option currently works only with the --subunit or --serie options')
            sys.exit()
        selected_residues =  superconfig.get_resi_ranges_as_resi_ids([list(map(int, options.resi_range.split(',')))])

    selected_subunits = []
    if options.selected_subunit:
        selected_subunits = []

    if options.format == 'ihm':
        system = None
        dumpers = [FakeReleaseDateDumper]

    cfg_filenames = []
    for solution_filename in solution_filenames:
        try:
            if options.outfilename:
                outfilename = options.outfilename
            else:
                outfilename = os.path.splitext(os.path.basename(solution_filename))[0] + '.cif'    

            if options.outdir:
                Path(options.outdir).mkdir(parents=True, exist_ok=True)
                outfilename = os.path.join(options.outdir, outfilename)

            if guess_jsons:
                old_cfg_filenames = cfg_filenames[:]
                outdir, model = os.path.realpath(solution_filename).split('/models_txt/')
                #get paths to all jsons in case of multistate modeling
                path_str = outdir + '/*.json'
                cfg_filenames = glob.glob(path_str) #TODO: problem - jsons need to be read in order
                if old_cfg_filenames != cfg_filenames:
                    config = superconfig.Config(cfg_filenames, project_dir=options.project_dir)
                    if options.pdb_dir:
                        config.change_pdb_dir(options.pdb_dir)

            if options.format in ("multi_pdb", "multi_cif"):
                if not options.outdir:
                    options.outdir = os.path.join(os.getcwd(), solution_filename+'_outpdbs')

                if selected_subunits or options.selected_serie or options.resi_range:
                    print('Options --subunit, --serie, --resi_range not supported for multi_pdb and multi_cif formats')
                    sys.exit()

            sol_i = 0

            #yes, the solution file may contain multiple solutions, e.g. in the case of all_combinations protocol
            for sol in iter_solutions(solution_filename):

                if options.format == 'ihm':

                    if not options.multimodel or system is None:
                        #create entities
                        chainId_to_AsymUnit_map = {}
                        system = ihm.System(title=config.in_cfg.get('title'))

                        reps = []
                        for subunit in config.subunits:
                            sequence = config.get_seq(subunit.name)
                            accession_dict = config.get_accession(subunit.name)
                            if accession_dict:
                                if accession_dict['db_name'] == 'UniProt':
                                    ref = ihm.reference.UniProtSequence.from_accession(accession_dict['accession'])
                                else:
                                    ref = ihm.reference.Sequence(accession_dict['db_name'], accession_dict['db_code'], accession_dict['accession'], sequence, details=None)
                                # print(dir(ref))
                                # print(ref.sequence)
                                # print(ref.alignments)

                                if ref.sequence == sequence:
                                    entity = ihm.Entity(sequence, description=subunit.name, references=[ref])
                                else:
                                    entity = ihm.Entity(sequence, description=subunit.name)    
                            else:
                                entity = ihm.Entity(sequence, description=subunit.name)

                            system.entities.append(entity)

                            #define asyms
                            for serie in config.series:
                                if options.selected_serie and options.selected_serie != serie.name:
                                    continue
                                if serie.subunit == subunit:
                                    #TODO: here check if the right state
                                    for mol_copy, chain_id in enumerate(serie.chainIds):
                                        if options.first_copy_only and mol_copy > 0:
                                            continue
                                        if options.first_two_copies and mol_copy > 1:
                                            continue
                                        if options.copies is not None:
                                            if mol_copy not in options.copies:
                                                continue
                                        #Define auth_seq_id_map to have all atom_site elements, including pdbx_PDB_ins_code,
                                        #as some parsers crash without those elements
                                        auth_seq_id_map = {}
                                        for i in enumerate(sequence):
                                            auth_seq_id_map[i] = (i, '?')

                                        asym = ihm.AsymUnit(entity, details=subunit.name, id=chain_id, auth_seq_id_map=auth_seq_id_map)
                                        system.asym_units.append(asym)
                                        chainId_to_AsymUnit_map[chain_id] = asym
                                        reps.append(ihm.representation.AtomicSegment(asym, rigid=False))

                                        #TODO: this code works but needs to be thought over regarding paths passed to InputFileLocation
                                        #(ChimeraX would say path does not exist)
                                        # for struct_cfg in config.struct_files:
                                        #     for struct_comp in struct_cfg['components']:

                                        #         struct_comp = copy.deepcopy(struct_comp)
                                        #         this_cfg = {'subunit': subunit.name, 'serie': serie.name, 'copies': [mol_copy]}
                                        #         if config.do_selectors_overlap(this_cfg, struct_comp, fields=('subunit', 'state', 'states', 'serie', 'copies')): #without selecting for chain ids!
                                        #             #iterate comp_cfg for this asym somehow
                                        #             l = ihm.location.InputFileLocation(struct_comp['filename'], details=os.path.basename(struct_comp['filename'])+" structure used for integrative modeling")
                                        #             system.locations.append(l)
                                        #             dataset = ihm.dataset.ComparativeModelDataset(l, details="Structure used for integrative modeling")
                                        #             resi = config.get_resi_ids_from_comp_cfg(struct_comp)
                                        #             starting_model = StartingModel(asym_unit=asym(resi[0], resi[-1]), config=config, pdb_comp_cfg=struct_comp, dataset=dataset)
                                        #             reps.append(ihm.representation.AtomicSegment(asym, rigid=False, description='name',starting_model=starting_model))
                    

                        assembly = ihm.Assembly(system.asym_units, name='<ADD_TO_JSON>')
                        protocol = ihm.protocol.Protocol(name='Assembline')

                        g = ihm.model.StateGroup()
                        m_g = ihm.model.ModelGroup()
                        s = ihm.model.State([m_g], type='Representative conformation', name='Representative conformation')                            
                        g.append(s)
                        #append all atoms coords to system
                        system.state_groups.append(g)

                        if options.extra_ihm_commands:
                            apply_extra_ihm_commands(system, reps, assembly, protocol, config, options, options.extra_ihm_commands)

                        rep = ihm.representation.Representation(reps)

                grouped_by_subunit_copy = defaultdict(list)
                out_names = []

                #this should be done within config based on 
                #a parameter in the config itself?
                if len(config.rigid_bodies) == 0:
                    config.define_rbs_from_pdbs()

                rmf_hierarchy = None
                if options.rmf_file or options.rmf_auto:
                    if options.rmf_file:
                        rmf_filename = options.rmf_file
                    elif options.rmf_auto:
                        rmf_filename = solution_filename.replace('models_txt', 'models_rmf').replace('.txt', '.rmf')
                    
                    rh = RMF.open_rmf_file_read_only(rmf_filename)
                    m = IMP.Model()
                    rmf_hierarchy = IMP.rmf.create_hierarchies(rh, m)

                for rb_cfg in config.rigid_bodies:
                    # print('1', [(k, v) if k != 'positions' else (k, len(v)) for k, v in rb_cfg.items() ])
                    name = rb_cfg['name']
                    try:
                        fit = sol[name][0]
                        print('Processing rigid body: '+name)
                    except KeyError:
                        print(name + ' not in solution')
                        continue

                    if options.format in ("multi_pdb", "multi_cif"):
                        rb_cfg['pdblines'] = []
                    for rb_comp in rb_cfg['components']:
                        # print('2', rb_comp)
                        state_idx = rb_comp.get('state') or rb_comp['states'][0] #we assume no rbs across more than one state!
                        for struct_cfg in config.struct_files:
                            for struct_comp in struct_cfg['components']:
                                struct_comp = copy.deepcopy(struct_comp)
                                if config.do_selectors_overlap(rb_comp, struct_comp, fields=('subunit', 'state', 'states', 'domain', 'serie', 'copies', 'filename', 'resi_ranges')): #without selecting for chain ids!

                                    # print('3', [(k, v) if k != 'rotated_pdblines' else (k, len(v)) for k, v in struct_comp.items() ])
                                    copies = rb_comp.get('copies') or [0]

                                    for mol_copy in copies:
                                        # print('4', mol_copy)
                                        if options.first_copy_only and mol_copy > 0:
                                            continue
                                        if options.first_two_copies and mol_copy > 1:
                                            continue
                                        if options.copies is not None:
                                            if mol_copy not in options.copies:
                                                continue
                                        if struct_comp.get('copies') and mol_copy not in struct_comp.get('copies'):
                                            continue
                                        if options.selected_subunit and options.selected_subunit != rb_comp['subunit']:
                                            continue
                                        serie = rb_comp.get('serie') or config.get_series_by_subunit(config.get_subunit_by_name(rb_comp['subunit']))[0].name
                                        if options.selected_serie and options.selected_serie != serie:
                                            continue

                                        pdblines = read_structure(config, struct_comp, rb_comp, selected_residues)

                                        rotated_pdblines = transform_pdb.transform_pdblines(pdblines, transpose(fit['rot']), fit['trans']).splitlines()

                                        

                                        grouped_by_subunit_copy[ (rb_comp['subunit'], state_idx, serie, mol_copy) ].append(rotated_pdblines)
                                        if options.format in ("multi_pdb", "multi_cif"):
                                            rb_cfg['pdblines'].extend(rotated_pdblines)

                    if options.format in ("multi_pdb", "multi_cif") and rb_cfg['pdblines']:
                        series = []
                        if options.add_series:
                            for rb_comp in rb_cfg['components']:
                                serie = rb_comp.get('serie') or config.get_series_by_subunit(config.get_subunit_by_name(rb_comp['subunit']))[0].name
                                if serie not in series:
                                    series.append(rb_comp['serie'])
                            base, idx = rb_cfg['name'].rsplit('_', maxsplit=1)
                            base = base.replace('.pdb','')
                            name = '{base}_{series}_{idx}.pdb'.format(base=base, series='_'.join(series), idx=idx)
                        else:
                            name = rb_cfg['name'] + '.pdb'
                        Path(options.outdir).mkdir(parents=True, exist_ok=True)
                        outpdb_path = os.path.join(options.outdir, name)
                        with open(outpdb_path, 'w') as outpdb_f:
                            outpdb_f.write('\n'.join(rb_cfg['pdblines']))

                if not grouped_by_subunit_copy:
                    raise Exception('Sth not right, grouped_by_subunit_copy is empty')

                if options.format in ("cif", "pdb", "ihm") or (options.format in ("multi_pdb", "multi_cif") and rmf_hierarchy):
                    if options.format == 'cif':
                        all_out_lines = []

                    if options.format == 'ihm':
                        newChainId_to_pdblines = {}

                    for subunit_state_serie_copy, rotated_pdblines_sets in grouped_by_subunit_copy.items():
                        subunit, state_idx, serie, copy_id = subunit_state_serie_copy
                        serie = config.get_serie_by_subunit_and_name(config.get_subunit_by_name(subunit), serie)
                        new_chain_id = serie.chainIds[copy_id]

                        if rmf_hierarchy:
                            beads_pdb_lines = extract_beads(rmf_hierarchy, state_idx=state_idx, chain_id=new_chain_id)
                            rotated_pdblines_sets.append(beads_pdb_lines)

                        sorted_lines = sort_pdblines_sets(rotated_pdblines_sets)

                        if rmf_hierarchy:
                            loop_resi_ids = get_resi_ids_from_pdblines(sorted_lines)
                            if loop_resi_ids:
                                print('Building loops for {fn}, subunit: {subunit}, serie: {serie}, copy:{copy_id}'.format(fn=solution_filename, subunit=subunit, serie=serie.name, copy_id=copy_id))
                                sorted_lines = build_loops(sorted_lines, loop_resi_ids)

                                if options.format == 'multi_pdb':
                                    resi_ranges = superconfig.ranges_from_list(loop_resi_ids)
                                    for loop_range in resi_ranges:
                                        loop_lines = []
                                        loop_resi = list(range(loop_range[0], loop_range[1]+1))
                                        for line in sorted_lines:
                                            if int(get_pdb_resid(line)) in loop_resi:
                                                loop_lines.append(line)

                                        outpdb_path = os.path.join(options.outdir, '{subunit}_{serie}_{copy}_{start}-{end}.pdb'.format(subunit=subunit,
                                                                                                                                       serie=serie.name,
                                                                                                                                       copy=copy_id,
                                                                                                                                       start=loop_range[0],
                                                                                                                                       end=loop_range[1]))
                                        print(outpdb_path)
                                        with open(outpdb_path, 'w') as outpdb_f:
                                            outpdb_f.write('\n'.join(loop_lines))

                        if options.format == 'cif':
                            header, cif_atom_keys, out_lines = convert_pdblines_to_cif(sorted_lines, new_chain_id)
                            all_out_lines.extend(out_lines)

                        if options.format == 'ihm':
                            newChainId_to_pdblines[new_chain_id] = sorted_lines

                    if options.format == 'cif':
                        all_out_lines = header + fix_atom_numbers_cif(cif_atom_keys, all_out_lines)
                        with open(outfilename, 'w') as f:
                            f.write(''.join((all_out_lines)))
                    elif options.format == 'pdb':
                        all_out_lines = header + fix_atom_numbers_pdb(cif_atom_keys, all_out_lines) 
                        with open(outfilename, 'w') as f:
                            f.write(''.join((all_out_lines)))
                    elif options.format == 'ihm':
                        m = IhmModel(system=system, chainId_to_pdblines=newChainId_to_pdblines, assembly=assembly, protocol=protocol, representation=rep)
                        m_g.append(m)

                        if not options.multimodel:
                            with open(outfilename, 'w') as fh:
                                ihm.dumper.write(fh, [system], dumpers=dumpers)

                sol_i = sol_i + 1
        except FileNotFoundError as e:
            if '.BL00010001.pdb' in str(e):
                print('Could not build the model ' + solution_filename)
                print('Error was:')
                print(e)
            else:
                raise e

    if options.multimodel:
        if options.format == 'ihm':
            with open(outfilename, 'w') as fh:
                ihm.dumper.write(fh, [system], dumpers=dumpers)        

if __name__ == '__main__':
    main()