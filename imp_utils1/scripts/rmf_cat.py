#!/usr/bin/python

import sys

import RMF
import IMP
import IMP.rmf

import imp_utils1.tools

if len(sys.argv) < 4:
    print("Usage: rmf_cat.py input1.rmf input2.rmf ... output.rmf")
    sys.exit()

inputs = sys.argv[1:-1]
output = sys.argv[-1]

imp_utils1.tools.rmf_cat(inputs, output)
