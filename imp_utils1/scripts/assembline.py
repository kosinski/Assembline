#!/usr/bin/python
from optparse import OptionParser
from string import Template
import os
import sys
import subprocess
import shutil
import logging
import glob
import ntpath
import time
from subprocess import Popen, PIPE
from subprocess import check_output
import re
import getpass
from multiprocessing import Process, Queue, Manager, Pool
from itertools import zip_longest

import IMP

import superconfig
import imp_utils1
from imp_utils1.optimization import PrintOptimizerState, WriteTotalScoreOptimizerState, DeNovoMCSAProtocol, DeNovoMCSACGProtocol, RefineProtocol, AllCombinationsProtocol

from imp_utils1 import params_reader


def get_used_discrete_restraints(all_discrete_restraints, movers):
    out = []
    for restr in all_discrete_restraints:
        if restr.mover in movers:
            out.append(restr)

    return out

def setup_the_system(params, cfg_filenames, params_file, out_dir, options):

###########################################################################
############               Initiate the system                ############
###########################################################################

    config = superconfig.Config(cfg_filenames)

    for cfg_filename in cfg_filenames:
        try:
            shutil.copy(cfg_filename, out_dir)
        except shutil.SameFileError:
            pass
    try:
        shutil.copy(params_file, out_dir)
    except shutil.SameFileError:
        pass

    if options.pdb_dir:
        config.change_pdb_dir(options.pdb_dir)

    converter = imp_utils1.Config2SystemConverter(config)
    print("Number of states: {0}".format(config.number_of_states))
    converter.create_system(states=config.number_of_states)

    system = converter.system

    r = imp_utils1.MultiRepresentation(config,
                                        pmi_system=system,
                                        struct_resolutions=params.struct_resolutions,
                                        add_missing=params.add_missing,
                                        missing_resolution=params.missing_resolution,
                                        add_rbs_from_pdbs=params.add_rbs_from_pdbs,
                                        rb_max_rot=params.rb_max_rot,
                                        rb_max_trans=params.rb_max_trans,
                                        beads_max_trans=params.beads_max_trans,
                                        ca_only=params.ca_only
                                        )
    # keep for debugging
    # r.write_as_rmf('ini0.rmf', [])
    # sys.exit()

###########################################################################
############            Add connectivity restraints            ############
###########################################################################

    if params.add_connectivity_restraints:
        conn_kwargs = {
            'connectivity_restraint_weight': params.connectivity_restraint_weight,
            'max_conn_gap': params.max_conn_gap,
            'k': params.connectivity_restraint_k,
            'conn_reweight_fn': params.conn_reweight_fn,
            'first_copy_only': params.conn_first_copy_only,
            'ca_ca_connectivity_scale': params.ca_ca_connectivity_scale
        }

        r.add_connectivity_restraints(**conn_kwargs)

        r.all_restraints['conn_restraints'] = r.conn_restraints
        
        print("Added {0} connectivity restraints".format(len(r.conn_restraints)))

###########################################################################
############              Add symmetry restraints              ############
###########################################################################

    if params.add_symmetry_constraints:
        print('Adding symmetry constraints')
        r.add_symmetry_restraints(use_constraints=True, use_restraints=False)
        print('Constrained {0} pairs of rigid bodies and beads'.format(len(r.sym_constraint_pairs)))
        assert len(r.sym_constraint_pairs) > 0

    if params.add_symmetry_restraints:
        print('Adding symmetry restraints')
        sym_restraints = r.add_symmetry_restraints(params.symmetry_restraints_weight, use_constraints=False, use_restraints=True)
        r.all_restraints['sym_restraints'] = sym_restraints
        print('Created {0} symmetry restraints'.format(len(sym_restraints)))
        assert len(sym_restraints) > 0
    

###########################################################################
############          Add excluded volume restraints           ############
###########################################################################
    print('Adding excluded volume constraints')
    for ev_restr_cfg in params.ev_restraints:
        if ev_restr_cfg['name'] == 'ev_restraints_only_with_structure':
            ev_r = r.add_ev_restraints_only_atomic_temp(weight=ev_restr_cfg.get('weight'),
                                       resolution=ev_restr_cfg.get('repr_resolution'),
                                       copies=ev_restr_cfg.get('copies'),
                                       distance_cutoff=ev_restr_cfg.get('distance_cutoff') or 0.0,
                                       slack=ev_restr_cfg.get('slack') or 5.0)
        else:
            ev_r = r.add_ev_restraints(weight=ev_restr_cfg.get('weight'),
                                       resolution=ev_restr_cfg.get('repr_resolution'),
                                       copies=ev_restr_cfg.get('copies'),
                                       distance_cutoff=ev_restr_cfg.get('distance_cutoff') or 0.0,
                                       slack=ev_restr_cfg.get('slack') or 5.0)


        assert len(ev_r) > 0
        assert len(ev_r) == r.config.number_of_states
        r.all_restraints[ev_restr_cfg['name']] = ev_r

###########################################################################
############              Add cross-link restraints            ############
###########################################################################

    if params.add_xlink_restraints:
        print('Adding xlink restraints')
        if config.xlinksSetsMerged is None:
            logger.error('No cross-links read from JSON')
            sys.exit()

        r.all_restraints['xlink_restraints'] = r.add_xlink_restraints(config.xlinksSetsMerged,
                               min_ld_score=params.x_min_ld_score,
                               weight=params.x_weight,
                               xlink_distance=params.x_xlink_distance,
                               k=params.x_k,
                               inter_xlink_scale=params.x_inter_xlink_scale,
                               first_copy_only=params.x_first_copy_only,
                               xlink_score_type=params.x_xlink_score_type,
                               skip_pair_fn=params.x_skip_pair_fn,
                               log_filename=params.x_log_filename,
                               score_weighting=params.x_score_weighting,
                               reweight_function=params.xlink_reweight_fn,
                               x_random_sample_fraction=params.x_random_sample_fraction
                               )

        print('Added {0} xlink restraints'.format(len(r.all_restraints['xlink_restraints'])))

###########################################################################
############                    Add EM restraints              ############
###########################################################################
        
    print('Adding EM restraints')
    em_restraints = r.add_em_restraints()
    print('Added {0} EM restraints'.format(len(em_restraints)))
    for em_r in em_restraints:
        r.all_restraints[em_r.get_name()] = [em_r]


###########################################################################
#####  Add restraints that keep the states similar if possible  ###########
###########################################################################

    if params.add_parsimonious_states_restraints:
        args = {}
        args['weight'] = params.parsimonious_states_weight
        args['distance_threshold'] = params.parsimonious_states_distance_threshold
        args['exclude_rbs_fn'] = params.parsimonious_states_exclude_rbs
        args['resolution'] = params.parsimonious_states_representation_resolution
        args['restrain_rb_transformations'] = params.parsimonious_states_restrain_rb_transformations

        r.all_restraints['parsimonious_states_restraints'] = r.add_parsimonious_states_restraints(**args)

###########################################################################
############                Add elastic network                ############
###########################################################################

    elastic_restraints = r.create_elastic_network()
    for el_r in elastic_restraints:
        r.all_restraints[el_r.get_name()] = [el_r]
    print('Added {0} elastic restraints'.format(len(elastic_restraints)))

###########################################################################
############                Add interaction restraints                ############
###########################################################################

    interaction_restraints = r.create_interaction_restraints()
    for int_r in interaction_restraints:
        if int_r.get_name() not in r.all_restraints:
            r.all_restraints[int_r.get_name()] = [int_r]
        else:
            r.all_restraints[int_r.get_name()].append(int_r)

###########################################################################
############                Add interaction restraints                ############
###########################################################################

    similar_orientation_restraints = r.add_similar_orientation_restraints()
    for restr in similar_orientation_restraints:
        r.all_restraints[restr.get_name()] = [restr]
    print('Added {0} similar_orientation_restraints'.format(len(similar_orientation_restraints)))

    #just for testing, remove!
    if hasattr(params, 'testrandomize') and params.testrandomize:
        r.randomize(
                center=params.randomize_initial_positions_center,
                max_translation=params.randomize_initial_positions_max_translation,
                max_rotation=params.randomize_initial_positions_max_rotation,
                remove_clashes=params.randomize_initial_positions_remove_clashes,
                remove_clashes_iter=params.randomize_initial_positions_remove_clashes_iter
            )

###########################################################################
############                Add angle restraints                ############
###########################################################################

    angle_restraints = r.add_angle_restraints()
    for restr in angle_restraints:
        r.all_restraints[restr.get_name()] = [restr]
    print('Added {0} angle restraints'.format(len(angle_restraints)))

###########################################################################
############                Add linear restraints                ############
###########################################################################

    collinear_restraints = r.add_collinear_restraints()
    for restr in collinear_restraints:
        r.all_restraints[restr.get_name()] = [restr]
    print('Added {0} collinear restraints'.format(len(collinear_restraints)))

###########################################################################
############                Add custom restraints              ############
###########################################################################

    if params.create_custom_restraints:
        custom_restraints = params.create_custom_restraints(r)
    else:
        custom_restraints = {}
    for restr_name, custom_restr in custom_restraints.items():
        r.all_restraints[restr_name] = custom_restr
    print('Added {0} custom_restraints'.format(len(custom_restraints)))

###########################################################################
######### Inititate lists of scoring functions and restraints  ############
###########################################################################

    score_functions = {} # maps scoring functions names to IMP ScoringFunctions

###########################################################################
############   Add discrete movers and restraints              ############
###########################################################################

    if params.discrete_mover_weight_score_fn:
        raise NotImplementedError
        sel_restraints = []
        #TODO: add code for adding restraints for DiscreteWeightedMovers

        discrete_mover_weight_score_fn = IMP.core.RestraintsScoringFunction(sel_restraints, "scoring function")
        score_functions[params.discrete_mover_weight_score_fn] = discrete_mover_weight_score_fn
        score_functions_restraints[params.discrete_mover_weight_score_fn] = sel_restraints

    discrete_movers = r.make_discrete_movers(restr_weight=params.discrete_restraints_weight)

##################################################################################################################
############          Custom movers specified in a user defined params.add_custom_movers function     ############
##################################################################################################################

    if params.add_custom_movers:
        params.add_custom_movers(r, r.all_restraints)


###########################################################################
############          Set user defined initial positions              ############
###########################################################################

    r.set_initial_positions()

###########################################################################
############          Randomize initial positions              ############
###########################################################################

    if params.randomize_initial_positions:
        r.randomize(
                center=params.randomize_initial_positions_center,
                max_translation=params.randomize_initial_positions_max_translation,
                max_rotation=params.randomize_initial_positions_max_rotation,
                remove_clashes=params.randomize_initial_positions_remove_clashes,
                remove_clashes_iter=params.randomize_initial_positions_remove_clashes_iter
            )
        # r.write_as_rmf('/tmp/test.rmf', r.xlink_restraints+r.conn_restraints)

    if params.custom_preprocessing:
        params.custom_preprocessing(r)

    return r

def run_DeNovoMCSAProtocol(r, params, options):
    if params.do_ini_opt:
        print()
        print('Initial optimization:')
        protocol = DeNovoMCSAProtocol(r, params,
                                    options.prefix,
                                    options.outdir,
                                    save_traj=False,
                                    schedule_name='ini_opt')
        protocol.set_movers(params.get_movers_for_ini_opt)
        r.all_restraints['discrete_restraints'] = get_used_discrete_restraints(r.discrete_restraints, protocol.movers)
        sf_cfg = params.scoring_functions[params.score_func_ini_opt]
        restraints_ini_opt = []
        for restr in sf_cfg['restraints']:
            restraints_ini_opt.extend(r.all_restraints[restr])

        protocol.create_scoring_function(restraints_ini_opt)
        protocol.run(SA_schedule=params.ini_opt_SA_schedule, move_floppy_rbs_close_to_hierarchies=True)

    print()
    print("Main optimization:")
    protocol = DeNovoMCSAProtocol(r, params,
                                options.prefix,
                                options.outdir,
                                save_traj=options.save_traj)
    protocol.set_movers(get_movers_fn=params.get_movers_for_main_opt)
    r.all_restraints['discrete_restraints'] = get_used_discrete_restraints(r.discrete_restraints, protocol.movers)
    sf_cfg = params.scoring_functions[params.score_func]
    restraints_main = []
    for restr in sf_cfg['restraints']:
        restraints_main.extend(r.all_restraints[restr])        
    protocol.create_scoring_function(restraints_main)
    if not params.do_ini_opt:
        protocol.run(SA_schedule=params.SA_schedule, move_floppy_rbs_close_to_hierarchies=True)
    else:
        protocol.run(SA_schedule=params.SA_schedule, move_floppy_rbs_close_to_hierarchies=False)

    protocol.process_output(save_rmf_models=options.save_rmf_models)

    return protocol

def run_DeNovoMCSACGProtocol(r, params, options):

    if params.do_ini_opt:
        print()
        print('Initial optimization:')
        protocol = DeNovoMCSACGProtocol(r, params,
                                    options.prefix,
                                    options.outdir,
                                    save_traj=False,
                                    schedule_name='ini_opt')
        protocol.set_movers(params.get_movers_for_ini_opt)
        r.all_restraints['discrete_restraints'] = get_used_discrete_restraints(r.discrete_restraints, protocol.movers)

        sf_cfg = params.scoring_functions[params.score_func_ini_opt]
        restraints_ini_opt = []
        for restr in sf_cfg['restraints']:
            restraints_ini_opt.extend(r.all_restraints[restr])

        if hasattr(params, 'score_func_for_CG'):
            sf_cfg = params.scoring_functions[params.score_func_for_CG]
            restraints_for_CG = []
            for restr in sf_cfg['restraints']:
                restraints_for_CG.extend(r.all_restraints[restr])  
        else:
            restraints_for_CG = restraints_ini_opt

        protocol.create_scoring_function(restraints_ini_opt, restraints_for_CG)
        protocol.move_floppy_rbs_close_to_hierarchies()
        protocol.run(SA_schedule=params.ini_opt_SA_schedule)

    print()
    print("Main optimization:")
    protocol = DeNovoMCSACGProtocol(r, params,
                                options.prefix,
                                options.outdir,
                                save_traj=options.save_traj)
    protocol.set_movers(get_movers_fn=params.get_movers_for_main_opt)
    
    r.all_restraints['discrete_restraints'] = get_used_discrete_restraints(r.discrete_restraints, protocol.movers)
    sf_cfg = params.scoring_functions[params.score_func]
    restraints_main = []
    for restr in sf_cfg['restraints']:
        restraints_main.extend(r.all_restraints[restr])

    if hasattr(params, 'score_func_for_CG'):
        sf_cfg = params.scoring_functions[params.score_func_for_CG]
        restraints_for_CG = []
        for restr in sf_cfg['restraints']:
            restraints_for_CG.extend(r.all_restraints[restr])  
    else:
        restraints_for_CG = restraints_main

    protocol.create_scoring_function(restraints_main, restraints_for_CG)
    if not params.do_ini_opt:
        protocol.move_floppy_rbs_close_to_hierarchies()

    sf = protocol.run(SA_schedule=params.SA_schedule)

    protocol.process_output(save_rmf_models=options.save_rmf_models)


    return protocol

def run_RefineProtocol(r, params, options):
    if params.move_floppy_beads_close_to_hierarchies == True:
        r.move_floppy_beads_close_to_hierarchies(params.move_floppy_beads_close_to_hierarchies_mode)

    if params.do_ini_opt:
        print()
        print('Initial optimization:')
        protocol = RefineProtocol(r, params,
                                    options.prefix,
                                    options.outdir,
                                    save_traj=options.save_traj,
                                    schedule_name='ini_opt')

        protocol.set_movers(params.get_movers_for_ini_opt)

        sf_cfg = params.scoring_functions[params.score_func_ini_opt]
        restraints_ini_opt = []
        for restr in sf_cfg['restraints']:
            restraints_ini_opt.extend(r.all_restraints[restr])

        if hasattr(params, 'score_func_for_CG'):
            sf_cfg = params.scoring_functions[params.score_func_for_CG]
            restraints_for_CG = []
            for restr in sf_cfg['restraints']:
                restraints_for_CG.extend(r.all_restraints[restr])  
        else:
            restraints_for_CG = restraints_ini_opt

        protocol.create_scoring_function(restraints_ini_opt, restraints_for_CG)            
        protocol.run(SA_schedule=params.ini_opt_SA_schedule)
        

    print()
    print("Main optimization:")
    protocol = RefineProtocol(r, params,
                                options.prefix,
                                options.outdir,
                                save_traj=options.save_traj)

    protocol.set_movers(get_movers_fn=params.get_movers_for_refine)

    sf_cfg = params.scoring_functions[params.score_func]
    restraints_main = []
    for restr in sf_cfg['restraints']:
        restraints_main.extend(r.all_restraints[restr])

    if hasattr(params, 'score_func_for_CG'):
        sf_cfg = params.scoring_functions[params.score_func_for_CG]
        restraints_for_CG = []
        for restr in sf_cfg['restraints']:
            restraints_for_CG.extend(r.all_restraints[restr])  
    else:
        restraints_for_CG = restraints_main

    protocol.create_scoring_function(restraints_main, restraints_for_CG)
    protocol.run(SA_schedule=params.SA_schedule)

    protocol.process_output(save_rmf_models=options.save_rmf_models)

    return protocol

def run_all_combinations_Protocol(r, params, options):
    protocol = AllCombinationsProtocol(r, params,
                                options.prefix,
                                options.outdir,
                                save_traj=options.save_traj)


    protocol.set_movers(get_movers_fn=params.get_movers_for_main_opt)
    r.all_restraints['discrete_restraints'] = get_used_discrete_restraints(r.discrete_restraints, protocol.movers)
    sf_cfg = params.scoring_functions[params.score_func]
    restraints_main = []
    print(r.all_restraints)
    for restr in sf_cfg['restraints']:
        restraints_main.extend(r.all_restraints[restr])

    if hasattr(params, 'score_func_for_CG'):
        sf_cfg = params.scoring_functions[params.score_func_for_CG]
        restraints_for_CG = []
        for restr in sf_cfg['restraints']:
            restraints_for_CG.extend(r.all_restraints[restr])  
    else:
        restraints_for_CG = restraints_main

    protocol.create_scoring_function(restraints_main)
    protocol.run()

def get_free():
    """Return the number of free CPUs.

    The code below works only at EMBL cluster, you can edit it to adjust to your cluster.
    """
    output = check_output(['/etc/profile.d/clusterstatus.sh'])

    m = re.search('used: (?P<used>\d+) out of (?P<available>\d+) available cores', str(output))
    if m:
        free = int(m.group('available')) - int(m.group('used'))
        return free
    else:
        return 0

def get_my_jobs_number():
    """Return the number of CPUs you currently use.

    The code below works only for Slurm queuing system, you can edit it to adjust to your cluster.
    """
    output = check_output(['squeue', '-u', getpass.getuser()])
    return len(output.splitlines()) - 1

def get_no_jobs_to_run(min_concurrent_jobs, max_concurrent_jobs):
    try:
        free = get_free()
    except:
        free = 1000000
        print('Cannot estimate the number of free CPUs. Setting free CPU count to {0}'.format(free))        

    my_jobs_number = get_my_jobs_number()
    my_free = max(0, max_concurrent_jobs - my_jobs_number)
    to_run = max(free, min_concurrent_jobs - my_jobs_number)

    return min(to_run, my_free)

def do_runs(project, params_fn, outdir, start, max_no_runs, options):
    params = params_reader.read_params(params_fn)

    cmds = []
    for run in range(start, start+max_no_runs):

        prefix = str(run).rjust(7,'0')
        if options.prefix:
            prefix = options.prefix + '_' + prefix

        cmd_options = ''
        if options.save_traj:
            cmd_options += '--traj '
        if options.other_states:
            cmd_options += '--other_states '+','.join(options.other_states)

        cmd = 'time python {script} {options} --prefix {prefix} -o {outdir} --models {project} {params_fn}'.format(
                script=os.path.realpath(__file__),
                options=cmd_options,
                prefix=prefix,
                params_fn=params_fn,
                project=project,
                outdir=outdir
                )

        cmds.append({'prefix': prefix, 'outdir': outdir, 'cmd': cmd})
    
    if params.cluster_submission_command: 
        for chunk in create_chunks(cmds, params.ntasks):
            if params.ntasks == 1:
                cmd_lines = [chunk[0]['cmd']]
                prefix = chunk[0]['prefix']

            elif params.ntasks > 1:
                cmd_lines = []
                for cmd in chunk:
                    cmd_lines.append('{cmd} 2> {stderr} 1> {stdout} &'.format(
                                                                            cmd=cmd['cmd'],
                                                                            stderr=os.path.join(cmd['outdir'], 'logs', cmd['prefix']+'_err.txt'),
                                                                            stdout=os.path.join(cmd['outdir'], 'logs', cmd['prefix']+'_out.txt')
                                                                            )
                                    )
                prefix = '_'.join([cmd['prefix'] for cmd in chunk])

            run_script = params.run_script_templ.substitute(
                prefix=prefix,
                outdir=outdir,
                ntasks=len(cmd_lines),
                cmd='\n'.join(cmd_lines))

            sbatch_fn = os.path.join(outdir, 'runscripts', 'run'+prefix+'.sh')
            with open(sbatch_fn, 'w') as f:
                f.write(run_script)

            cmd = params.cluster_submission_command + ' '+ sbatch_fn
            print(cmd)
            subprocess.call(cmd, shell=True)
            time.sleep(0.1) #to avoid Slurm hiccups
    else:
        pool = Pool(processes=params.ntasks)
        for cmd in cmds:
            pool.apply_async(run_cmd, args=(cmd,))
        pool.close()
        pool.join()

def run_cmd(cmd):
    print('Running '+cmd['prefix'])
    stderr = open(os.path.join(cmd['outdir'], 'logs', cmd['prefix']+'_err.txt'), 'w')
    stdout = open(os.path.join(cmd['outdir'], 'logs', cmd['prefix']+'_out.txt'), 'w')
    try:
        subprocess.call(cmd['cmd'].split(), stdout=stdout, stderr=stderr)
    finally:
        stderr.close()
        stdout.close()

def grouper(iterable, n):
    args = [iter(iterable)] * n
    return zip_longest(*args)

def create_chunks(list_name, n):
    for i in range(0, len(list_name), n):
        yield list_name[i:i + n]

def is_git_repo(path):
    return subprocess.call(['git', '-C', path, 'status'], stderr=subprocess.STDOUT, stdout = open(os.devnull, 'w')) == 0

def main():
    """
    """
    usage = "usage: %prog [options] cfg_filename protocol_name(denovo or refine)"
    parser = OptionParser(usage=usage)
    
    em_score_choices = ('FitRestraint', 'EnvelopePenetrationRestraint', 'EnvelopeFitRestraint', 'ExcludeMapRestraint')

    def list_split_callback(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))
    
    parser.add_option("-o", "--outdir", dest="outdir",
                      help="optional DIR with output", metavar="DIR")

    # parser.add_option("-r", "--runs", type="int", dest="no_of_runs", default=1,
    #                   help="Number of runs [default: %default]")

    parser.add_option("--pdb_dir", dest="pdb_dir",
                      help="Replace basename dir for pdb files with this pdb_dir", metavar="DIR")

    parser.add_option("--prefix", type="str", dest="prefix", default='',
                      help="Prefix for model names, e.g. for differential naming of models like 0000001, 0000002 when the script is run multiple times [default: %default]")

    parser.add_option("--traj", action="store_true", dest="save_traj", default=False,
                      help="save trajectories? [default: %default]")

    parser.add_option("--models", action="store_true", dest="save_rmf_models", default=False,
                      help="save models in RMF format? [default: %default]")

    parser.add_option('--other_states',
                      type='string',
                      action='callback',
                      callback=list_split_callback,
                      default=[])

    parser.add_option("--multi", action="store_true", dest="multi_mode", default=False,
                      help="run multiple jobs [default: %default]")

    parser.add_option("--start_idx", type="int", dest="start_idx", default=0,
                      help="Start index of the model in the multi mode, ({0}), [default: %default]")

    parser.add_option("--njobs", type="int", dest="njobs", default=1,
                      help="Number of jobs in the multi mode, ({0}), [default: %default]")

    parser.add_option("--daemon", action="store_true", dest="daemon", default=False,
                      help="start a daemon that re-submits jobs? [default: %default]")

    parser.add_option("--max_concurrent_jobs", type="int", dest="max_concurrent_jobs", default=10000,
                      help="Maximum number of jobs to run concurrently, ({0}), [default: %default]")

    parser.add_option("--min_concurrent_jobs", type="int", dest="min_concurrent_jobs", default=0,
                      help="Minimum number of jobs to run concurrently, ({0}), [default: %default]")

    parser.add_option(
        "-v",
        "--verbose",
        action="store_true",
        dest="verbose",
        default=False,
        help="verbose output [default: %default]")


    (options, args) = parser.parse_args()


###########################################################################
############            Print software versions                ############
###########################################################################

    # defunct because of problems with git on some cluster nodes

    if shutil.which('git') is not None:
        if is_git_repo(os.path.dirname(os.path.realpath(__file__))):
            print('==================')
            print()
            label = subprocess.check_output(["git", "describe", '--always'], cwd=os.path.dirname(os.path.realpath(__file__)))
            print('imp_utils1 Version (git hashtag):', label.strip())
            print('==================')

        if is_git_repo(os.path.dirname(os.path.realpath(superconfig.__file__))):
            print('==================')
            print()
            label = subprocess.check_output(["git", "describe", '--always'], cwd=os.path.dirname(os.path.realpath(superconfig.__file__)))
            print('SuperConfig Version (git hashtag):', label.strip())
            print('==================')

    cfg_filenames = [args[0]] + options.other_states
    params_file = args[1]

    print(args)
    params = params_reader.read_params(params_file)

    print('Parameters:')
    print()
    print('Config files:', ', '.join(cfg_filenames))
    print('Parameters file:', params_file)
    for opt in vars(options):
        print(opt, getattr(options, opt))
    for k, v in params.__dict__.items():
        if not k.startswith('__'):
            print(k, v)
    print('==================')
    print()

    if options.verbose:
        IMP.set_log_level(IMP.VERBOSE)
    else:
        IMP.set_log_level(IMP.SILENT)

    logging.basicConfig(stream=sys.stdout)
    if params.debug:
        logging.getLogger().setLevel(logging.DEBUG)

    if options.outdir is not None:
        out_dir = options.outdir
    else:
        out_dir = os.getcwd()

    subprocess.call('mkdir -p {0}'.format(out_dir), shell=True)
    if options.multi_mode:
        subprocess.call('mkdir -p {0}/logs'.format(out_dir), shell=True)
        subprocess.call('mkdir -p {0}/runscripts'.format(out_dir), shell=True)

        #run itself many times

        if options.daemon and params.cluster_submission_command:
            start = options.start_idx
            """
            Submit a limited number of jobs a time,
            depending how many jobs you have now running and in the queue
            """
            done = 0
            while True:
                nruns = get_no_jobs_to_run(options.min_concurrent_jobs, options.max_concurrent_jobs)
                nruns = min(nruns, options.njobs-done)
                nruns = max(0, nruns)
                if nruns:
                    do_runs(args[0], params_file, out_dir, start, nruns, options)

                done += nruns
                start += nruns
                print("Run:", done, 'out of', options.njobs)
                if done >= options.njobs:
                    break
                time.sleep(60)
        else:
            """
            Just submit all jobs
            """
            do_runs(args[0], params_file, out_dir, options.start_idx, options.njobs, options)
    else:
        r = setup_the_system(params, cfg_filenames, params_file, out_dir, options)

    ###########################################################################
    ############                Main optimization                  ############
    ###########################################################################

        # keep for debugging
        # r.write_as_rmf('ini.rmf', [])
        # sys.exit()

        # for serie in r.config.series:
        #     for state_mols in serie.molecules:
        #         for mol in state_mols:
        #             hiers = IMP.pmi.tools.input_adaptor(mol)
        #             rbs, beads = IMP.pmi.tools.get_rbs_and_beads(hiers)
        #             for a, b in zip(beads, beads[1:]):
        #                 print(IMP.core.get_distance(IMP.core.XYZ(a), IMP.core.XYZ(b)))

        if params.protocol == 'denovo_MC-SA':
            run_DeNovoMCSAProtocol(r, params, options)

        elif params.protocol == 'denovo_MC-SA-CG':
            run_DeNovoMCSACGProtocol(r, params, options)

        elif params.protocol == 'refine':
            run_RefineProtocol(r, params, options)

        elif params.protocol == 'all_combinations':
            run_all_combinations_Protocol(r, params, options)

        elif params.protocol == 'custom':
            params.custom_protocol(r, params, options)

if __name__ == '__main__':
    main()
