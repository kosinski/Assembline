#!/usr/bin/python
import sys
from collections import defaultdict
import glob
import os
import subprocess
import random
from itertools import groupby
from optparse import OptionParser
from pathlib import Path
import re

def parse_score_file(f):
    '''Iterate models in score files.

    Support both single model standard score files and multi-model score files from all_combinations.

    Very hacky, will be replaced when the output file system will be reconstructed.
    '''
    model_iter = (x[1] for x in groupby(f, lambda line: line.startswith('model ') or '_run' in line)) #the '_run_' in line only for backward compatibility with all_combinations

    for model in model_iter:
        model_line_items = model.__next__().strip().split()
        if model_line_items[0] == 'score':
            model_id = None
        else:
            if len(model_line_items) == 2:
                model_id = model_line_items[1]
            else:
                model_id = model_line_items[0]


        model_entries = []
        if model_line_items[0] == 'score':
            model_line_iterator = model
            model_entries.append(['total_score', float(model_line_items[1]), None, None])
        else:
            model_line_iterator = model_iter.__next__()

        for line in model_line_iterator:
            if line.startswith('"'):
                items = list(line.split(','))
                model_entries.append([items[0]] + list(map(float, items[1:])))
            elif line.startswith('score'):
                items = list(line.split(' '))
                model_entries.append(['total_score', float(items[1]), None, None])

        yield model_id, model_entries

def save(scores, fn):
    header = ','.join(scores[0].keys())
    header = header.replace('"','').replace(' ','_')

    lines = []
    for score in scores:
        lines.append(','.join(map(str, score.values())))

    with open(fn, 'w') as f:
        f.write(header + '\n')
        # Cannot use:
        # f.write('\n'.join(lines))
        # because for long lists I get MemoryError
        for line in lines:
            f.write(line + '\n')

    print("{0} done".format(fn))

def main():
    usage = "usage: %prog [options] "
    parser = OptionParser(usage=usage)

    parser.add_option("--split", dest="split", action="store_true", default=False,
                      help="Whether to create two random lists for sampling exhaustiveness")

    parser.add_option("--multi", dest="multi", action="store_true", default=False,
                      help="Search for all score files in subdirectories of the current directory. Useful when combining runs or analyzing refinement of many models.")


    (options, args) = parser.parse_args()

    #TODO: replace the following with python only code (Path('./').rglob('scores/*scores*txt') etc.)
    if not options.multi:
        subprocess.call('find scores/ -iname "*scores*txt" | xargs grep -m 1 score > all_scores.txt', shell=True)
        print("all_scores.txt done")
        subprocess.call('find logs/ -iname "*total_score_log.txt" > total_score_logs.txt', shell=True)
        print("total_score_logs.txt done")
    else:
        subprocess.call('find */* -iname "*scores*.txt" | xargs grep -m 1 score > all_scores.txt', shell=True)
        print("all_scores.txt done")
        subprocess.call('find */* -iname "*total_score_log.txt" > total_score_logs.txt', shell=True)
        print("total_score_logs.txt done")

    subprocess.call('cat all_scores.txt | sort -nk 2 > all_scores_sorted.txt', shell=True)
    print("all_scores_sorted.txt done")
    subprocess.call('uniq -f1 all_scores_sorted.txt > all_scores_sorted_uniq.txt', shell=True)
    print("all_scores_sorted_uniq.txt done")

    if options.split:

        with open("all_scores.txt", "r") as f:
            data = f.read().split('\n')
        if len(data[-1]) == 0:
            data = data[:-1]
        random.shuffle(data)
        print(data[:int(len(data)/2)][0].split(' '))
        # for x in data[int(len(data)/2):]:
        #     print(x, x.split(' '))
        sample_A = sorted(data[:int(len(data)/2)], key=lambda x: float(x.split(' ')[1]))
        sample_B = sorted(data[int(len(data)/2):], key=lambda x: float(x.split(' ')[1]))

        with open("scores_sorted_sample_A.txt", 'w') as f:
            f.write('\n'.join(sample_A))

        with open("scores_sorted_sample_B.txt", 'w') as f:
            f.write('\n'.join(sample_B))

    if not options.multi:
        score_files = glob.glob('scores/*scores*txt')
    else:
        score_files = Path('./').rglob('scores/*scores*txt')

    scores = []
    for fn in score_files:
        with open(fn) as f:
            if options.multi:
                out_fn = re.sub(r'scores([0-9]*).txt', r'model\1.txt', str(fn)).replace('/scores/','/models_txt/')
            else:
                out_fn = os.path.basename(fn).replace('scores.txt','')
            
            for model_id, model_items in parse_score_file(f):
                if model_id is None:
                    scores.append({'model': out_fn})
                else:
                    scores.append({'model': model_id})

                for items in model_items:
                    restr_name = items[0]
                    score = items[1]
                    weight = items[3]
                    scores[-1][restr_name] = score

    if len(scores) == 0:
        print('No score files found. Something crashed?')
        sys.exit()

    save(scores, fn='all_scores.csv')
    scores_uniq = list({v['total_score']:v for v in scores}.values()) # https://stackoverflow.com/questions/11092511/python-list-of-unique-dictionaries
    save(scores_uniq, fn='all_scores_uniq.csv')
    scores_sorted = sorted(scores, key=lambda s: s['total_score'])
    save(scores_sorted, fn='all_scores_sorted.csv')
    scores_sorted_uniq = list({v['total_score']:v for v in scores_sorted}.values()) # https://stackoverflow.com/questions/11092511/python-list-of-unique-dictionaries
    save(scores_sorted_uniq, fn='all_scores_sorted_uniq.csv')

if __name__ == '__main__':
    main()