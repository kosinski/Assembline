#!/usr/bin/python
import os
import sys
import re
import argparse
import random
from shutil import copyfile
import subprocess
import csv
import glob
from collections import defaultdict

import imp_utils1
import imp_utils1.tools

########################################################################################
# DISCLAIMER:  Script written by Vasileios Rantos in Jan Kosinski Lab at EMBL Hamburg. #
# This Master script is used before the exhaustiveness/precision analysis pipeline     #
# in order to prepare automatically the input for downstream analysis.                 #
#                                                                                      #
########################################################################################

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def create_ids_scores(score_file_data,num_models,model_path,out_dir,select_score,noshuffle=False, score_thresh=None, top_percent=None, state_id=''):
    """Create essential input files for sampling exhaustiveness analysis"""

    if score_thresh is not None:
        print(select_score)
        score_file_data.sort(key=lambda x: float(x[select_score]))
        score_file_data = list(filter(lambda x: float(x[select_score]) <= score_thresh, score_file_data))

        noshuffle = False

        print('\nNumber of models below the specified score threshold of {0}: {1}\n'.format(score_thresh, len(score_file_data)))

    if top_percent is not None:
        score_file_data.sort(key=lambda x: float(x[select_score]))
        score_file_data = score_file_data[:int(len(score_file_data)*top_percent/100)]

        noshuffle = False
        
        print('\nNumber of models within the specified percentage of {0}: {1}\n'.format(top_percent, len(score_file_data)))

    if not noshuffle:
        random.shuffle(score_file_data)



    score_file_data = score_file_data[:num_models]

    sample_A = score_file_data[:len(score_file_data)//2]
    sample_B = score_file_data[len(score_file_data)//2:]

    model_counter = 0
    identities = defaultdict(list)
    scores = defaultdict(list)


    for sample_name,sample in zip(['A','B'],[sample_A, sample_B]):

        for row in sample:
            if row['model'].endswith('model{0}.txt'.format(state_id)) or not row['model'].endswith('.txt'): #TODO: BAD HACK
                scores['scores_'+sample_name].append(row[select_score])

                if '/' in row['model']: #id is already a path (e.g. in "multi" mode of extract_scores.py)
                    ori_fn = model_path+row['model'].replace('models_txt', 'models_rmf').replace('model{0}.txt'.format(state_id), 'model{0}.rmf'.format(state_id))
                    symlink_rel_fn = os.path.join('sample_'+sample_name, os.path.basename(row['model']).replace('model{0}.txt'.format(state_id), '')+"model{0}.rmf3".format(state_id))
                else:
                    ori_fn = model_path+"models_rmf/"+row['model']+"model{0}.rmf".format(state_id)
                    symlink_rel_fn = os.path.join('sample_'+sample_name, row['model']+"model{0}.rmf3".format(state_id))
                symlink_fn = os.path.join(out_dir, symlink_rel_fn)
                
                identities['Identities_'+sample_name].append(symlink_rel_fn+" "+str(model_counter))
                os.symlink(ori_fn, symlink_fn)
                model_counter += 1

        #save score files in the specified output dir
        with open(out_dir+"/scores{0}.txt".format(sample_name),'w') as f:
            f.write('\n'.join(map(str, scores['scores_'+sample_name])))
            f.write('\n')

        #save the Identities files
        with open(os.path.join(out_dir, "Identities_{0}.txt".format(sample_name)),'w') as f:
            f.write("\n".join(identities['Identities_'+sample_name]))
            f.write('\n')

        print('Concatenating RMF files for sample {0}. This may take a while.'.format(sample_name))
        #create concatenated rmfs of all models per sample in the respective sample dirs
        sample_rmf = "%s/sample_%s/sample_%s_models.rmf3"% (out_dir,sample_name,sample_name)
        models = [os.path.join(out_dir, x.split()[0]) for x in identities['Identities_'+sample_name]]
        """Use a custom version of rmf_cat that can merge RMF files coming from trajectories
        that do not have the same conformation in the first frame:"""
        imp_utils1.tools.rmf_cat(models, sample_rmf)


# user options
parser = argparse.ArgumentParser(description="Input preparation for sampling exhaustiveness & precision analysis")
parser.add_argument('--scores', '-s', dest="score_file", help='Specify path to csv file with scores of models e.g. "-s all_scores_uniq.csv"', default="./all_scores_uniq.csv")
parser.add_argument('--density', '-d', dest="density_file", help='Specify path to txt file with density subunits', default="./density.txt")
parser.add_argument('--num', '-n', type=int, dest="num_models", help='Specify number of models to extract and analyze', default=None)
parser.add_argument('--out', '-o', dest="out_dir", help='Specify path and dir name for output folder', default="./good_scoring_models_auto")

parser.add_argument('--noshuffle', dest="noshuffle", help='Do not shuffle the list of models to create samples with random best models selected', action='store_true', default=False)

parser.add_argument('--keyword', '-k', dest="select_score", help='Specify keyword for score selection of models to extract and analyze as displayed in all_scores_uniq.csv column header', default="total_score")
parser.add_argument('--score_thresh', dest="score_thresh", help='Score threshold', default=None, type=float)
parser.add_argument('--top_percent', dest="top_percent", help='Percentage of top scoring models to analyze', default=None, type=float)
parser.add_argument('--state_id', dest="state_id", help='In multistate modeling, state id to analyze', default='', type=str)

args = parser.parse_args()


# store the user defined options & check if user specified options are valid
# first test if scores txt file exists and is readable in specified location
score_file = args.score_file
#convert to absolute path for all_scores_uniq.csv file
score_file = os.path.abspath(score_file)


if os.path.isfile(score_file) and os.access(score_file, os.R_OK):
    with open(score_file, 'r') as file:
        csv_reader = csv.DictReader(file)
        #store in a list all lines of score file
        score_file_data = list(csv_reader)

        #check if score term was found
        if args.select_score not in csv_reader.fieldnames:
            print("Error: Score keyword specified not found. Please try again!\n")
            exit(0)

    #gather total models number
    if not args.state_id:
        num_all_models = len(score_file_data)
    else:
        num_all_models = len([row for row in score_file_data if row['model'].endswith('model'+args.state_id+'.txt')])

else:
    print("Error: File with scores not found (invalid path and/or filename) or not readable. Please try again!\n")
    exit(0)

num_models = args.num_models
if num_models:
    if num_all_models < num_models:
        print("More models requested compared to existing. Following models will be extracted: "+str(num_all_models)+"\n")
        num_models = num_all_models

#default models are all the ones included in the txt file
elif not num_models:
    num_models = num_all_models

else:
    print("Error: Wrong numerical format was specified. Please try again!\n")
    exit(0)


# check if output dir name exists else create dir in path
out_dir = args.out_dir
#convert to absolute path for all_scores_uniq.csv file
out_dir = os.path.abspath(out_dir)


if not os.path.exists(out_dir):
    os.makedirs(out_dir)
    os.makedirs(out_dir+"/sample_A")
    os.makedirs(out_dir+"/sample_B")
else:
    print("Error: Output dir name already exists! Please specify other dir name and try again!\n")
    exit(0)

#prepare to copy and check correctness of density file
density_file = args.density_file
#convert to absolute path for all_scores_uniq.csv file
density_file = os.path.abspath(density_file)

if os.path.isfile(density_file) and os.access(density_file, os.R_OK):
    copyfile(density_file, out_dir+"/density.txt")
else:
    print("Error: File with densities not found (invalid path and/or filename) or not readable. Please try again!\n")
    exit(0)

# start with printing user options
print("\nScore file path: "+score_file+"\n")
print("Density file path: "+density_file+"\n")
print("Number of models to extract: "+str(num_models)+"\n")
print("Score keyword for extracting models: "+args.select_score+"\n")
print("Formatted input will be stored here: "+out_dir+"\n")


#path with all score models rmf etc. should be in the same dir with all_scores_uniq.csv
model_path_prem = score_file.split('/')[:-1]
model_path = '/'.join(model_path_prem)+'/'

# produce input for sampling exhaustiveness
create_ids_scores(score_file_data,num_models,model_path,out_dir,select_score=args.select_score,noshuffle=args.noshuffle, score_thresh=args.score_thresh, top_percent=args.top_percent, state_id=args.state_id)

