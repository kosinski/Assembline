from optparse import OptionParser
import os
import sys
import itertools
from collections import defaultdict
import superconfig
import numpy
import pickle

import RMF
import IMP

def get_chain_coords(model1):
    m = IMP.Model()
    fh1 = RMF.open_rmf_file_read_only(model1)
    states1 = IMP.rmf.create_hierarchies(fh1,m)
    hier1 = states1[0]

    chains = hier1.get_children()
    chains_coords = []
    for chain in chains:
        chains_coords.append([IMP.core.XYZ(a).get_coordinates() for a in IMP.atom.get_leaves(chain)])

    return chains_coords

def compare(model1, model2):
    '''
    This works only for homo-dimers at the moment
    '''
    chains_coords1 = get_chain_coords(model1)
    chains_coords2 = get_chain_coords(model2)
    # print(chains_coords1[0][:10])
    # print(chains_coords2[0][:10])
    dist_transformations = []

    #compare A to A B to B
    coords1_AB = chains_coords1[0] + chains_coords1[1]
    coords1_BA = chains_coords1[0] + chains_coords1[1]
    coords2_AB = chains_coords2[0] + chains_coords2[1]

    pairs = [
        [coords1_AB, coords2_AB],
        [coords1_BA, coords2_AB]
    ]

    for p1, p2 in pairs:
        transformation = IMP.algebra.get_transformation_aligning_first_to_second(p1, p2)
        p1_tr = [transformation.get_transformed(n)
                        for n in p1]
        dist = IMP.algebra.get_rmsd(p1_tr, p2)
        dist_transformations.append([dist, transformation])

    # for chain1_coords in chains_coords1:
    #     for chain2_coords in chains_coords2:
    #         transformation = IMP.algebra.get_transformation_aligning_first_to_second(chain1_coords, chain2_coords)
    #         chain1_coords_tr = [transformation.get_transformed(n)
    #                         for n in chain1_coords]

    #         dist = IMP.algebra.get_rmsd(chain1_coords_tr, chain2_coords)
    #         print(dist)
    #         dists.append(dist)

    output = min(dist_transformations, key=lambda x: x[0])

    return output

def main():
    usage = "usage: %prog [options] cfg_filename solutions_list"
    parser = OptionParser(usage=usage)

    parser.add_option("-o", "--out_prefix", dest="out_prefix", default=None,
                      help="optional prefix for files with output")
    
    (options, args) = parser.parse_args()

    cfg_filename = args[0]

    models_fn = args[1]

    config = superconfig.Config(cfg_filename)

    with open(models_fn) as f:
        models = f.read().splitlines()

    dists = defaultdict(dict)

    i = 1 
    for model1, model2 in itertools.combinations(models,2):
        # print(model1, model2)
        dist, transformation = compare(model1, model2)
        # sys.exit()

        dists[model1][model2] = dist
        dists[model2][model1] = dist

        if i%100==0:
            print(i)

        i = i + 1

    for model in models:
        dists[model][model] = 0.0

    out = [[' ']+models[:]]
    for model1 in models:
        row = [model1]
        for model2 in models:
            row.append(str(dists[model1][model2]))

        out.append(row)

    #format for sampling exhaustiveness tests:
    numModels = len(models)
    distmat = numpy.zeros((numModels,numModels))
    for i in range(numModels-1):
        for j in range(i+1,numModels):
             distmat[i][j] = dists[models[i]][models[j]]

    if options.out_prefix:
        with open(options.out_prefix+'.txt', 'w') as f:
            f.write('\n'.join([','.join(map(str,fields)) for fields in out]))
        pickle.dump(distmat,open(options.out_prefix+'.pkl',"wb"))
    else:
        print('\n'.join([','.join(map(str,fields)) for fields in out]))

    

if __name__ == '__main__':
    main()