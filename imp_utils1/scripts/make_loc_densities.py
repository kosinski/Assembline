import os

import IMP.atom
import IMP.pmi.dof
import IMP.pmi.analysis
import IMP.rmf
import RMF

from optparse import OptionParser

import imp_utils1

def main():
    usage = "usage: %prog [options] rmf_files"
    parser = OptionParser(usage=usage)

    parser.add_option("-o", "--outdir", dest="outdir",
                      help="optional DIR with output", metavar="DIR")

    (options, args) = parser.parse_args()

    files = args[0]

    densities = {}
    voxel_size = 3.0
    with open(files) as f:
        for i,line in enumerate(f):
            filename = line.strip()
            print(i, filename)
            temp_model = IMP.Model()
            rh = RMF.open_rmf_file_read_only(filename)
            states = IMP.rmf.create_hierarchies(rh, temp_model)

            for c in states[0].get_children():
                name = c.get_name()
                if name not in densities:
                    densities[name] = imp_utils1.ModelDensity(
                            voxel_size=voxel_size)

                densities[name].add(IMP.core.get_leaves(c), resolution=5)
    
    for name, dens in densities.items():
        if options.outdir:
            filename_prefix = os.path.join(options.outdir, name)
        dens.write_mrc(filename_prefix)

if __name__ == '__main__':
    main()