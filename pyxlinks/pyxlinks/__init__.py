import sys
import csv
from itertools import product, chain, combinations, permutations, groupby
from operator import itemgetter
from collections import defaultdict, deque
import re
import os
import copy
import json
from . import minify_json
from . import aaindex
import math
import weakref
import random

try:
    from sklearn.metrics import matthews_corrcoef, confusion_matrix, classification_report
except ImportError:
    sys.stderr.write("Can't import sklearn (used only for debugging)\n")

try:
    import numpy as np
except ImportError:
    sys.stderr.write("Can't import numpy, ContactMatrix will not work\n")

try:
    import matplotlib
    import matplotlib.pyplot as plt
    import matplotlib.transforms as mtransforms

except ImportError:
    sys.stderr.write("Cant't import matplotlib, some things will not work\n")

try:
    import pylab
except ImportError:
    sys.stderr.write("Cant't import pylab, some things will not work\n")

class Xlinker(object):
    def __init__(self):
        self.stupid_length = None
        self.ext_side_chain1_len = None
        self.ext_side_chain2_len = None

    def get_len_with_sidechains(self):
        return self.stupid_length + self.ext_side_chain1_len + self.ext_side_chain2_len



class LysineLysineXlinker(Xlinker):
    def __init__(self):
        super(LysineLysineXlinker, self).__init__()
        self.ext_side_chain1_len = 7
        self.ext_side_chain2_len = 7

class DSS(LysineLysineXlinker):
    def __init__(self):
        super(DSS, self).__init__()
        self.stupid_length = 11.4


class DSG(LysineLysineXlinker):
    def __init__(self):
        super(DSG, self).__init__()
        self.stupid_length = 7.7

def get_unique(seq, idfun=None):
    '''
    Uniquify iterable based on idfun function that returns unique marker.

    Based on:
    http://www.peterbe.com/plog/uniqifiers-benchmark
    http://stackoverflow.com/questions/480214/how-do-you-remove-duplicates-from-a-list-in-python-whilst-preserving-order
    '''

    if idfun is None:
        def idfun(x): return x

    seen_dict = {}

    unique = []
    for x in seq:
        marker = idfun(x)
        if marker not in seen_dict:
            seen_dict[marker] = x
            unique.append(x)
        else:
            #always choose the one with higher ld-Score
            #this may be expensive, but I needed quick fix:
            x_prev = seen_dict[marker]
    
            score = get_score(x)
            score_prev = get_score(x_prev)
            if '-' not in (score, score_prev) and score > score_prev:
                    idx = unique.index(x_prev)
                    unique[idx] = x
                    seen_dict[marker] = x

            if x.get('distance') and x_prev.get('distance'):
                if x['distance'] < x_prev['distance']:
                    idx = unique.index(x_prev)
                    unique[idx] = x
                    seen_dict[marker] = x                    


    return unique


def _xlink2unique_marker_by_prots_resi_and_xlinker(x):
    pos1 = get_AbsPos(x, 1)
    if pos1 is None:
        pos1 = 'n/a'
    pos2 = get_AbsPos(x, 2)
    if pos2 is None:
        pos2 = 'n/a'

    return frozenset([(get_protein(x, 1), pos1), (get_protein(x, 2), pos2), x.get('xlinker')])

def _xlink2unique_marker_by_prots_resi(x):
    pos1 = get_AbsPos(x, 1)
    if pos1 is None:
        pos1 = 'n/a'
    pos2 = get_AbsPos(x, 2)
    if pos2 is None:
        pos2 = 'n/a'    

    return frozenset([(get_protein(x, 1), pos1), (get_protein(x, 2), pos2)])

def _xlink2unique_marker_by_id(x):
    return x['Id']

def _absolute_unique_marker(x):
    keys = sorted(x.keys())

    return frozenset([x[key] for key in keys if key not in 'distance'])

def xquest_file_list_to_xlink_sets(xquest_file_list_filename, parent_dir=None):
    xquest_file_list = []
    with open(xquest_file_list_filename, 'rU') as f:
        for line in f:
            if not line.startswith('#'):
                filename, xlinker = line.strip().split()
                if parent_dir is not None:
                    filename = os.path.join(parent_dir, filename)
                xquest_file_list.append([filename, xlinker])

    xlinks_sets = []

    for xquest_filename, xlinker in xquest_file_list:
        xlink_set = XlinksSet(filename=xquest_filename,
                                xlinker=xlinker
            )
        xlinks_sets.append(xlink_set)

    return xlinks_sets

def xquest_file_list_to_merged_xlink_set(xquest_file_list_filename, parent_dir=None):
    return sum(xquest_file_list_to_xlink_sets(xquest_file_list_filename, parent_dir))

def is_xquest_monolink(xlink):
    return '-' in (xlink['Protein1'], xlink['Protein2']) and (xlink.get('Type') == 'monolink' or xlink.get('XLType') == 'monolink')

def is_minimal_format_mono_link(xlink):
    if 'Id' in xlink:
        return len(xlink['Id'].split('-')) == 3 and '-' in (xlink['Protein1'], xlink['Protein2']) and 'n/a' in (xlink['AbsPos1'], xlink['AbsPos2'])
    else:
        return '-' in (xlink['Protein1'], xlink['Protein2']) and 'n/a' in (xlink['AbsPos1'], xlink['AbsPos2'])

def is_mono_link(xlink):
    return is_xquest_monolink(xlink) or is_minimal_format_mono_link(xlink)

def is_xquest_loop_link(xlink):
    return 'n/a' in (xlink['AbsPos1'], xlink['AbsPos2']) and not is_mono_link(xlink)

def is_minimal_format_loop_link(xlink):
    if 'Id' in xlink:
        return len(xlink['Id'].split('-')) == 3 and (xlink['Protein1'] == xlink['Protein2'])
    else:
        return False

def is_loop_link(xlink):
    return is_xquest_loop_link(xlink) or is_minimal_format_loop_link(xlink)

def is_intra(xlink):
    # return xlink['Protein1'] == xlink['Protein2'] or (not is_mono_link(xlink) and '-' in (xlink['Protein1'], xlink['Protein2']))
    return xlink['Protein1'] == xlink['Protein2'] or is_loop_link(xlink)

def is_inter(xlink):
    return not is_mono_link(xlink) and not is_intra(xlink)

def do_pepts_overlap(xlink):
    pepts = get_pepts(xlink)

    if None not in pepts and len(pepts) == 2:
        len1 = len(pepts[0])
        len2 = len(pepts[1])
        rel_pos1, rel_pos2 = get_rel_pos(xlink)

        start1 = int(get_AbsPos(xlink, 1)) - int(rel_pos1[0])
        start2 = int(get_AbsPos(xlink, 2)) - int(rel_pos2[0])

        range1 = set(range(start1,start1+len1))
        range2 = set(range(start2,start2+len2))

        return len(range1.intersection(range2)) > 0

    return False


def is_clearly_dimeric(xlink):
    if get_protein(xlink, 1) == get_protein(xlink, 2):
        if get_AbsPos(xlink, 1) == get_AbsPos(xlink, 2) or do_pepts_overlap(xlink):
            return True

    return False

def get_xlink_type(xlink):
    if is_mono_link(xlink):
        return 'monolink'
    elif is_loop_link(xlink):
        return 'looplink'
    elif is_intra(xlink):
        return 'intralink'
    elif is_inter(xlink):
        return 'interlink'

def print_xlink_nice(xlink):
    print(xlink.get('Protein1'), xlink.get('Protein2'), xlink.get('AbsPos1'), xlink.get('AbsPos2'), xlink.get('Type'), xlink.get('XLType'), get_score(xlink))

def get_mono_link_pos(mono_xlink):
    for pos in (mono_xlink['AbsPos1'], mono_xlink['AbsPos2']):
        if pos != 'n/a':
            return pos

    return 'n/a'

def get_mono_link_protein(mono_xlink):
    for protein in (mono_xlink['Protein1'], mono_xlink['Protein2']):
        if protein != '-':
            return protein

def get_loop_link_protein(xlink):
    for protein in (xlink['Protein1'], xlink['Protein2']):
        if protein != '-':
            return protein

def get_AbsPos(xlink, index):
    '''
    Return AbsPos as string

    '''
    if index not in (1, 2):
        raise ValueError('Index must be 1 or 2')

    ori_abs_pos = xlink['AbsPos'+str(index)]
    if index == 1:
        if is_mono_link(xlink):
            return str(get_mono_link_pos(xlink))

        return str(ori_abs_pos)

    elif index == 2:
        try:
            int(ori_abs_pos)
        except ValueError:  
            if is_mono_link(xlink):
                return None
            elif is_loop_link(xlink):
                if 'Id' in xlink:
                    data = xlink['Id'].split('-')
                    if data[1].startswith('K'):
                        rel_pos1 = data[1][1:]

                        if data[2].startswith('K'):
                            rel_pos2 = data[2][1:]

                            start = int(get_AbsPos(xlink, 1)) - int(rel_pos1)
                            return str(start+int(rel_pos2))
        else:
            return str(ori_abs_pos)

    return 'n/a'

def get_protein(xlink, index):
    if index not in (1, 2):
        raise ValueError('Index must be 1 or 2')

    ori_protein = xlink['Protein'+str(index)]        

    if index == 1:
        if is_mono_link(xlink):
            return get_mono_link_protein(xlink)

        elif is_loop_link(xlink):
            return get_loop_link_protein(xlink)

        return ori_protein

    elif index == 2:
        if is_mono_link(xlink):
            return '-'       

        elif is_loop_link(xlink):
            return get_loop_link_protein(xlink)

        else:
            return ori_protein

def get_rel_pos(xlink):
    """Return:
    o two lists of ints - for inter and intra
    o list of ints for looplinks
    """
    pepts = xlink['Id'].split('-')

    if len(pepts) == 4: #normal xlinks
        rel_pos1 = [int(pepts[2][1:])]
        rel_pos2 = [int(pepts[3][1:])]

        return rel_pos1, rel_pos2

    if len(pepts) == 3: #looplinks, monolinks:
        rel_pos1 = [int(pepts[1][1:])]

        # if pepts[2].startswith('K'): #looplink
        if pepts[2][0].isalpha():
            rel_pos1.append(int(pepts[2][1:]))

        rel_pos2 = None

        return rel_pos1, rel_pos2

    return None, None

def get_pepts(xlink):
    if 'Id' in xlink:
        pepts = xlink['Id'].split('-')

        if len(pepts) == 4: #normal xlinks
            return pepts[0], pepts[1]
        else: #len(pepts) == 3: #looplinks, monolinks:
            return pepts[0], None

    return None, None

def get_score_key(xlink):
    keys = ['ld.Score', 'ld-Score', 'score', 'Score']
    for key in keys:
        if key in xlink:
            return key

def get_score(xlink):
    keys = ['ld.Score', 'ld-Score', 'score', 'Score']
    for key in keys:
        try:
            return float(xlink[key])
        except KeyError:
            pass
        except ValueError:
            if xlink[key] == '-':
                return xlink[key]

def set_score(xlink, score):
    score_key = get_score_key(xlink)
    xlink['Score'] = score

class XlinksSet(object):
    def __init__(self, filename=None, csv_data=None, xlinker=None, xlink_set_data=None, fieldnames=None, description=None, add_extra_fields=True):
        
        self.data = []
        self.filtered = []
        self.fieldnames = []
        self.grouped_by_both_pos = defaultdict(list)
        self.description = description
        self.add_extra_fields = add_extra_fields

        if filename is not None:
            try:
                csvfile = open(filename, 'r')
                dialect = csv.Sniffer().sniff(csvfile.readline(), ['\t', ','])
            except UnicodeDecodeError:
                csvfile.close()
                csvfile = open(filename, 'r', encoding="utf-8")
                dialect = csv.Sniffer().sniff(csvfile.readline(), ['\t', ','])
            finally:
                
                csvfile.seek(0)

                reader = csv.DictReader(csvfile, dialect=dialect)

                self.fieldnames = reader.fieldnames

                try:
                    rank_field_idx = self.fieldnames.index('No.')
                except ValueError:
                    pass
                else:
                    self.fieldnames[rank_field_idx] = 'Rank'



                for row in reader:

                    ungrouped = self._get_ungrouped_rows(row)

                    for final_row in ungrouped:
                        if add_extra_fields:
                            if final_row.get('xlinker') is None or xlinker is not None:
                                final_row['xlinker'] = xlinker

                            if final_row.get('source') is None:
                                final_row['source'] = filename

                        if 'No.' in final_row:
                            final_row['Rank'] = final_row['No.']
                            del final_row['No.']

                        # abs1 = get_AbsPos(final_row, 1)
                        # abs2 = get_AbsPos(final_row, 2)
                        # final_row['AbsPos1'] = abs1
                        # final_row['AbsPos2'] = abs2

                    self.data.extend(ungrouped)

                csvfile.close()

        elif xlink_set_data is not None:
            self.data = xlink_set_data

        if fieldnames is not None:
            self.fieldnames = fieldnames

        if add_extra_fields:
            if 'xlinker' not in self.fieldnames:
                self.fieldnames.append('xlinker')

            if 'source' not in self.fieldnames:
                self.fieldnames.append('source')

    def _get_ungrouped_rows(self, xquest_row):
        '''there was a situation when two variants of the same protein
        were mixed in one sample, and for this protein
        there were always two names and two numbers in the same xquest row e.g.:
1,KDHPFGFVAVPTK-AKADTLK-a1-b2,"sp|P63279|UBC9_HUMAN,sp|P63279|UBC9",sp|P49792|RBP2_HUMAN,xlink,inter-protein xl,AvA_1305_1_2_4_2.07244.07244.4_AvA_1305_1_2_4_2.07228.07228.4,"18, 18",423, ...
        '''
        protein1_data = [xquest_row['Protein1'], xquest_row['AbsPos1']]
        protein2_data = [xquest_row['Protein2'], xquest_row['AbsPos2']]

        out_data = []
        for protein_data in (protein1_data, protein2_data):
            prot, pos = protein_data
            prots = prot.split(',')
            positions = pos.split(',')
            if len(prots) > 1 and len(positions) > 1:
                out_data.append(list(zip(prots, positions)))
            else:
                out_data.append([(prot, pos)])


        out_rows = []
        for partner1, partner2 in product(*out_data):
            prot1, pos1 = [s.strip() for s in partner1]
            prot2, pos2 = [s.strip() for s in partner2]

            row = xquest_row.copy()
            row['Protein1'] = prot1
            row['AbsPos1'] = pos1
            row['Protein2'] = prot2
            row['AbsPos2'] = pos2
            out_rows.append(row)

        # return [out_rows[0]] #TODO: return out_rows when make_input_for_crosslinkviewer.py works well with repeated uniprotids
        return out_rows

    def __add__(self, other):
        new_xlinks_set = XlinksSet(xlink_set_data=self.data + other.data)
        new_xlinks_set.fieldnames = self.fieldnames[:]
        return new_xlinks_set

    def __radd__(self, other): #for sum() to work
        if other == 0: #sum first does 0 + XlinksSet so this must overloaded:
            #(http://stackoverflow.com/questions/1218710/pythons-sum-and-non-integer-values/1218735)
            other = XlinksSet(xlink_set_data=[])
            other.fieldnames = self.fieldnames[:]
        new_xlinks_set = XlinksSet(xlink_set_data=other.data + self.data)
        new_xlinks_set.fieldnames = other.fieldnames[:]
        
        return new_xlinks_set

    def iter_xlinks(self):
        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        for xlink in data:
            yield xlink

    def get_difference(self, other_XlinkSet, same_xlink_marker=None):
        out = []
        if same_xlink_marker is None:
            same_xlink_marker = _absolute_unique_marker
        
        other_xlink_markers = [same_xlink_marker(x) for x in other_XlinkSet.iter_xlinks()]

        for xlink in self.iter_xlinks():
            if same_xlink_marker(xlink) not in other_xlink_markers:
                out.append(xlink)

        return XlinksSet(xlink_set_data=out, fieldnames=self.fieldnames[:], description=self.description, add_extra_fields=self.add_extra_fields)

    def get_common(self, other_XlinkSet, same_xlink_marker=None):
        out = []
        if same_xlink_marker is None:
            same_xlink_marker = _absolute_unique_marker
        
        other_xlink_markers = [same_xlink_marker(x) for x in other_XlinkSet.iter_xlinks()]

        for xlink in self.iter_xlinks():
            if same_xlink_marker(xlink) in other_xlink_markers:
                out.append(xlink)

        return XlinksSet(xlink_set_data=out, fieldnames=self.fieldnames[:], description=self.description, add_extra_fields=self.add_extra_fields)

    def get_protein_names(self):
        '''Get all protein names present in the xlink set'''
        names = set([])
        for xlink in self.data:
            if xlink['Protein1'] != '-':
                names.add(xlink['Protein1'])
            if xlink['Protein2'] != '-':            
                names.add(xlink['Protein2'])

        return names

    def get_inter_intra_for_prot(self, name):
        out = []
        for xlink in self.data:
            names = set([xlink['Protein1'], xlink['Protein2']])
            if name in names:
                out.append(xlink)

        return XlinksSet(xlink_set_data=out, fieldnames=self.fieldnames[:], description=self.description, add_extra_fields=self.add_extra_fields)

    def get_for_prot_pair(self, prot1_name, prot2_name):
        query = set([prot1_name, prot2_name])
        out = []
        for xlink in self.data:
            names = set([xlink['Protein1'], xlink['Protein2']])
            if query == names:
                out.append(xlink)

        return XlinksSet(xlink_set_data=out, fieldnames=self.fieldnames[:], description=self.description, add_extra_fields=self.add_extra_fields)

    def get_mono_links_for_prot(self, prot_name):
        query = set([prot_name, '-'])
        out = []
        for xlink in self.data:
            names = set([xlink['Protein1'], xlink['Protein2']])
            if query == names:
                out.append(xlink)

        return XlinksSet(xlink_set_data=out, fieldnames=self.fieldnames[:], description=self.description, add_extra_fields=self.add_extra_fields)        

    def get_unique(self):
        '''
        Perhaps should give deep copy of self.data
        '''
        if len(self.filtered) > 0:
            source = self.filtered
        else:
            source = self.data

        return XlinksSet(xlink_set_data=get_unique(source, idfun=_absolute_unique_marker),
                            fieldnames=self.fieldnames[:], description=self.description, add_extra_fields=self.add_extra_fields
            )

    def get_unique_by_prots_resi(self):
        '''
        Perhaps should give deep copy of self.data
        '''
        if len(self.filtered) > 0:
            source = self.filtered
        else:
            source = self.data

        return XlinksSet(xlink_set_data=get_unique(source, idfun=_xlink2unique_marker_by_prots_resi),
                            fieldnames=self.fieldnames[:], description=self.description, add_extra_fields=self.add_extra_fields
            )

    def get_unique_by_prots_resi_and_xlinker(self):
        '''
        Perhaps should give deep copy of self.data
        '''
        if len(self.filtered) > 0:
            source = self.filtered
        else:
            source = self.data

        return XlinksSet(xlink_set_data=get_unique(source, idfun=_xlink2unique_marker_by_prots_resi_and_xlinker),
                            fieldnames=self.fieldnames[:], description=self.description, add_extra_fields=self.add_extra_fields
            )

    def get_unique_set_for_dist_restraints(self):
        return self.get_unique_by_prots_resi_and_xlinker()

    def get_unique_by_id(self):
        '''
        Perhaps should give deep copy of self.data
        '''
        if len(self.filtered) > 0:
            source = self.filtered
        else:
            source = self.data

        return XlinksSet(xlink_set_data=get_unique(source, idfun=_xlink2unique_marker_by_id),
                            fieldnames=self.fieldnames[:], description=self.description, add_extra_fields=self.add_extra_fields
            )

    def intersection_by_prots_resi(self, xlinks_set):
        if len(self.filtered) > 0:
            source = self.filtered
        else:
            source = self.data

        if len(xlinks_set.filtered) > 0:
            target = xlinks_set.filtered
        else:
            target = xlinks_set.data

        source_markers = [_xlink2unique_marker_by_prots_resi(x) for x in source]
        target_markers = [_xlink2unique_marker_by_prots_resi(x) for x in target]
        intersection = set(source_markers) & set(target_markers)

        out = []
        added_markers = []
        for x in target + source:
            xmarker = _xlink2unique_marker_by_prots_resi(x)
            if xmarker in intersection and xmarker not in added_markers:
                out.append(x)
                added_markers.append(xmarker)

        return XlinksSet(xlink_set_data=out,
                            fieldnames=self.fieldnames[:], description=self.description, add_extra_fields=self.add_extra_fields
            )

    def get_random_subset(self, fraction):
        '''
        Perhaps should give deep copy of self.data
        '''
        if len(self.filtered) > 0:
            source = self.filtered
        else:
            source = self.data

        size = int(round(fraction*len(source)))
        return XlinksSet(xlink_set_data=random.sample(source, size),
                            fieldnames=self.fieldnames[:], description=self.description, add_extra_fields=self.add_extra_fields
            )

    def print_xlinks(self):
        for xlink in self.data:
            print(xlink)

    def calc_scores(self, dist):
        out = []
        if len(self.filtered) > 0:
            source = self.filtered
        else:
            source = self.data

        satisfied = 0
        violated = 0
        violation_distance = 0.
        violation_distances = []
        soft_satisfaction_score = 0.
        for xlink in source:
            d = float(xlink['distance'])
            if d <= dist:
                satisfied = satisfied + 1
                
            else:
                violated = violated + 1
                violation_distance = violation_distance + d - dist
                violation_distances.append(d)

            max_dist = dist + 5.
            if d <= dist:
                soft_satisfaction_score = soft_satisfaction_score + 1
            elif d <= max_dist:
                soft_satisfaction_score = soft_satisfaction_score + (1-(d-dist)/(max_dist-dist))

        soft_satisfaction_score = soft_satisfaction_score / len(source) * 100
        print('satisfied', satisfied)
        print('violated', violated)
        print('satisfied [%]', float(satisfied)/len(source) * 100)
        print('violated [%]', float(violated)/len(source) * 100)
        print('violation_distance', violation_distance)
        print('average violated distance', float(sum(violation_distances))/len(violation_distances) if len(violation_distances) > 0 else float('nan'))
        print('soft_satisfaction_score', soft_satisfaction_score, '[%]')

    def filterBy(self, f):
        out = []
        if len(self.filtered) > 0:
            source = self.filtered
        else:
            source = self.data

        for xlink in source:
            if f(xlink):
                out.append(xlink)

        self.filtered = out

    def filterByScore(self, threshold):

        def filter_fn(x):
            try:
                return get_score(x) >= threshold
            except ValueError:
                return True

        self.filterBy(filter_fn)

    def filterByLdScore(self, threshold):
        #For backward compatibility
        return self.filterByScore(threshold)

    def filterByType(self, types):

        def filter_fn(x):
            if is_mono_link(x) and 'mono' in types:
                return True
            if is_intra(x) and 'intra' in types:
                return True
            if is_inter(x) and 'inter' in types:
                return True

        self.filterBy(filter_fn)

    def filterByDistance(self, max_dist, use_min_dist=False):

        def filter_fn(x):
            if use_min_dist is False:
                return float(x['distance']) <= max_dist
            else:
                return float(x['distance']) > max_dist

        self.filterBy(filter_fn)

    def filterByProteins(self, proteins):

        def filter_fn(x):
            protein1 = get_protein(x, 1)
            protein2 = get_protein(x, 2)
            common = set(proteins) & set([protein1, protein2])
            if len(common) > 0:
                return True

        self.filterBy(filter_fn)

    def clear_filtering(self):
        self.filtered = []

    def print_for_interaction_viewer_old(self, names_to_ids=None, filtered_only=False):
        out_data = [','.join(['protein1', 'protein2', 'residue1', 'residue2'])]

        if filtered_only and len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        for xlink in data:
            if not is_mono_link(xlink):
                if names_to_ids:
                    protein1 = names_to_ids[xlink['Protein1']]
                    protein2 = names_to_ids[xlink['Protein2']]
                else:
                    protein1 = xlink['Protein1']
                    protein2 = xlink['Protein2']


                #add xx| prefixes if name is like O89079|COPE_MOUSE because viewer requires names like sp|O89079|COPE_MOUSE
                m = re.match('[^|]+\|[^|]+$', protein1)
                if m:
                    protein1 = 'xx|'+protein1
                m = re.match('[^|]+\|[^|]+$', protein2)
                if m:
                    protein2 = 'xx|'+protein2                

                out_data.append(','.join([protein1, protein2, xlink['AbsPos1'], xlink['AbsPos2'], get_score(xlink)]))

        return '\n'.join(out_data)

    def print_for_interaction_viewer(self, names_to_ids=None, filtered_only=False):
        out_data = [','.join(['Score', 'Id', 'Protein1', 'Protein2', 'Residue1', 'Residue2'])]

        if filtered_only and len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        for xlink in data:
            if not is_mono_link(xlink):
                if names_to_ids:
                    protein1 = names_to_ids[xlink['Protein1']]
                    protein2 = names_to_ids[xlink['Protein2']]
                else:
                    protein1 = xlink['Protein1']
                    protein2 = xlink['Protein2']


                #add xx| prefixes if name is like O89079|COPE_MOUSE because viewer requires names like sp|O89079|COPE_MOUSE
                m = re.match('[^|]+\|[^|]+$', protein1)
                if m:
                    protein1 = 'xx|'+protein1
                m = re.match('[^|]+\|[^|]+$', protein2)
                if m:
                    protein2 = 'xx|'+protein2                

                out_data.append(','.join([get_score(xlink), xlink['Id'], protein1, protein2, xlink['AbsPos1'], xlink['AbsPos2']]))

        return '\n'.join(out_data)

    def get_not_mono_links(self):
        out = []
        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        for xlink in data:
            if not is_mono_link(xlink):
                out.append(xlink)

        return out

    def get_not_mono_links_as_xlinks_set(self):
        out = []
        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        for xlink in data:
            if not is_mono_link(xlink):
                out.append(xlink)

        return XlinksSet(xlink_set_data=out,
                            fieldnames=self.fieldnames[:], description=self.description
            )

    def get_mono_links(self):
        out = []
        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        for xlink in data:
            if is_mono_link(xlink):
                out.append(xlink)

        return out

    def get_mono_links_as_xlinks_set(self):
        out = []
        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        for xlink in data:
            if is_mono_link(xlink):
                out.append(xlink)

        return XlinksSet(xlink_set_data=out,
                            fieldnames=self.fieldnames[:], description=self.description
            )

    def group_by_pos(self):
        out = {}
        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        for xlink in data:
            pos_id = 1
            for protein in (get_protein(xlink, 1), get_protein(xlink, 2)):
                
                if protein not in ('-', 'n\a'):
                    # xlink_pos = xlink['AbsPos'+str(pos_id)]
                    xlink_pos = get_AbsPos(xlink, pos_id)
                    if protein in out:
                        if xlink_pos in out[protein]:
                            out[protein][xlink_pos].append(xlink)
                        else:
                            out[protein][xlink_pos] = [xlink]
                    else:
                        out[protein] = {xlink_pos: [xlink]}
                pos_id = pos_id + 1

        return out

    def get_deep_copy(self):
        data = []
        for row in self.data:
            new_row = copy.deepcopy(row)
            data.append(new_row)

        new_xlinks_set = XlinksSet(xlink_set_data=data)
        new_xlinks_set.fieldnames = self.fieldnames[:]
        new_xlinks_set.description = self.description

        return new_xlinks_set

    def group_by_both_pos(self):
        self.grouped_by_both_pos = defaultdict(list)

        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        for xlink in data:
            abs1 = get_AbsPos(xlink, 1)
            abs2 = get_AbsPos(xlink, 2)
            prot1 = get_protein(xlink, 1)
            prot2 = get_protein(xlink, 2)


            if is_loop_link(xlink):
                prot2 = prot1

            if is_mono_link(xlink):
                prot2 = None

            key = frozenset([(prot1, abs1),(prot2, abs2)])

            self.grouped_by_both_pos[key].append(xlink)

    def get_by_both_pos(self, prot1, pos1, prot2, pos2):
        if not self.grouped_by_both_pos:
            self.group_by_both_pos()

        if pos1 is not None:
            pos1 = str(pos1)

        if pos2 is not None:
            pos2 = str(pos2)

        key = frozenset([(prot1, pos1), (prot2, pos2)])

        return self.grouped_by_both_pos.get(key)

    def get_by_pos(self, prot, pos):
        return self.get_by_both_pos(prot, pos, None, None)

    def renumber_resi(self, xquest_name, resi_map, remove_mapped_to_none=True):
        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        DEBUG = False

        for xlink in data[:]:
            if DEBUG:
                print('BEFORE:', xlink['Protein1'], xlink['Protein2'], xlink['AbsPos1'], xlink['AbsPos2'], xlink['Type'], xlink['XLType'], get_score(xlink))

            for pos in (1, 2):
                old_pos = None
                try:
                    old_pos = int(get_AbsPos(xlink, pos))
                except:
                    pass
                else:
                    prot_name = xlink['Protein'+str(pos)]
                    if prot_name == xquest_name:
                        if old_pos in resi_map:
                            xlink['AbsPos'+str(pos)] = resi_map[old_pos]
                            peppos = 'PepPos' + str(pos)
                            if peppos in xlink and (xlink[peppos] is not None) and (xlink[peppos] != '-'):
                                if int(xlink[peppos]) in resi_map:
                                    xlink[peppos] = resi_map[int(xlink[peppos])]

                        if resi_map[old_pos] is None and remove_mapped_to_none:
                            self.remove_xlink(xlink)

            if DEBUG:
                print('AFTER:', xlink['Protein1'], xlink['Protein2'], xlink['AbsPos1'], xlink['AbsPos2'], xlink['Type'], xlink['XLType'], get_score(xlink))

    def remove_xlink(self, xlink):
        try:
            self.filtered.remove(xlink)
        except:
            pass

        try:
            self.data.remove(xlink)
        except:
            pass


    def rename_protein_names(self, old_name, new_name):
        '''Get all protein names present in the xlink set'''
        for xlink in self.data:
            if xlink['Protein1'] == old_name:
                xlink['Protein1'] = new_name
            if xlink['Protein2'] == old_name:
                xlink['Protein2'] = new_name

    def rename_protein_names_from_dict(self, rename_dict):
        '''Get all protein names present in the xlink set'''
        for xlink in self.data:
            for i in ('1', '2'):
                old_name = xlink['Protein'+i]
                new_name = rename_dict.get(old_name)
                if new_name is not None:
                    xlink['Protein'+i] = new_name


    def get_xlinks_not_in_another(self, another):
        '''Return new xlink set with xlinks not found in another'''

        if len(self.filtered) > 0:
            source = self.filtered
        else:
            source = self.data


        markers_another = [_xlink2unique_marker_by_prots_resi(x) for x in another.data]

        out = []
        for xlink in source:
            if _xlink2unique_marker_by_prots_resi(xlink) not in markers_another:
                out.append(xlink)

        return XlinksSet(xlink_set_data=out,
                            description=self.description,
                            fieldnames=self.fieldnames[:]
            )

    def get_field_column(self, fieldname):
        if len(self.filtered) > 0:
            source = self.filtered
        else:
            source = self.data

        col = []
        for xlink in source:
            col.append(xlink[fieldname])

        return col

    def get_derived_column(self, fieldnames, fn):
        '''
        Return column of data for xlinks processed by function based on several fieldnames

        o fieldnames - names of xlink fields [list]
        o fn - function to process data, fn(xlink, fieldnames)
        '''
        if len(self.filtered) > 0:
            source = self.filtered
        else:
            source = self.data

        col = []
        for xlink in source:
            col.append(fn(xlink, fieldnames))

        return col

    def save_to_file(self, filename, quoting=None, fieldnames=None):
        if quoting is None:
            quoting = csv.QUOTE_NONE

        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        if not fieldnames:
            fieldnames = self.fieldnames
        
        if isinstance(filename, str):
            f = open(filename, 'w')
        else:
            f = filename
        try:
            wr = csv.DictWriter(f, fieldnames, quoting=quoting, extrasaction='ignore')
            wr.writerow(dict((fn,fn) for fn in fieldnames)) #do not use wr.writeheader() to support also python 2.6
            wr.writerows(data)
        finally:
            if isinstance(filename, str):
                f.close()

    def print_out(self, quoting=None, fieldnames=None):
        if quoting is None:
            quoting = csv.QUOTE_NONE

        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        if not fieldnames:
            fieldnames = self.fieldnames
        
        wr = csv.DictWriter(sys.stdout, fieldnames, quoting=quoting, extrasaction='ignore')
        wr.writerow(dict((fn,fn) for fn in fieldnames)) #do not use wr.writeheader() to support also python 2.6
        wr.writerows(data)

    def print_xlink_csv(self, xlink, fieldnames=None, quoting=None, no_fieldnames=False):

        if quoting is None:
            quoting = csv.QUOTE_NONE


        if not fieldnames:
            fieldnames = self.fieldnames
        
        wr = csv.DictWriter(sys.stdout, fieldnames, quoting=quoting, extrasaction='ignore')
        if not no_fieldnames:
            wr.writerow(dict((fn,fn) for fn in fieldnames)) #do not use wr.writeheader() to support also python 2.6
        wr.writerows([xlink])

    def clean(self):
        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        for xlink in data:
            if is_loop_link(xlink):
                xlink['Protein2'] = get_protein(xlink, 2)
                xlink['AbsPos2'] = get_AbsPos(xlink, 2)


    def gen_CNS_restraints(self):
        restr = []
        if len(self.filtered) > 0:
            data = self.filtered
        else:
            data = self.data

        for xlink in data:
            line = 'assign (segid {name1} and resid {resi1} and name NZ) (segid {name2} and resid {resi2} and name NZ) 10.0 10.0 16.0'.format(
                    name1=get_protein(xlink, 1),
                    resi1=get_AbsPos(xlink, 1),
                    name2=get_protein(xlink, 2),
                    resi2=get_AbsPos(xlink, 2),
                )
            restr.append(line)

        return restr

def mergeDataSets(cfg):
    '''Create merged XlinksSet object where all names were renamed to component names.

    Can handle input XlinksSets with different prot names, and unify the names to component names.
    '''
    xlink_data_cfgs = [d for d in cfg.data if d['type'] == XQUEST_DATA_TYPE]

    xlinkDataSets = []
    for x_cfg in xlink_data_cfgs:
        xlinkDataSets.append(xquest_file_list_to_merged_xlink_set(cfg.find_path(x_cfg['xquest_file_list_filename']), parent_dir=cfg.find_path(x_cfg['xquest_files_parent_dir'])))

    xlinkSetsCopies = []
    for xlinkDataSet in xlinkDataSets:
        xlinkSetsCopies.append(xlinkDataSet.get_deep_copy())

    for xlinkSet, xlink_data_cfg in zip(xlinkSetsCopies, xlink_data_cfgs):
        xquest_to_component_names = xlink_data_cfg['xquest_to_component_names']

        rename_dict = {}
        for xquest_name, comp_names in xquest_to_component_names.items():
            rename_dict[xquest_name] = comp_names[0]

        xlinkSet.rename_protein_names_from_dict(rename_dict)

    xlinksSetsMerged = sum(xlinkSetsCopies)

    return xlinksSetsMerged


def print_xlinks_and_data(xlink_rows, xlinkAnalyzer=None):
    out = []
    xquest_to_chains = {}
    in_structure_count = 0

    if xlinkAnalyzer is not None:
        for data_item in xlinkAnalyzer.cfg.data:
            if data_item['type'] == XQUEST_DATA_TYPE:
                xquest_to_chains.update(data_item['xquest_to_chains'])

    for xlink in xlink_rows:
        for_print = []
        resi = []

        resi_for_debug_print = []

        if is_mono_link(xlink):
            prot = get_mono_link_protein(xlink)
            pos = get_mono_link_pos(xlink)
            for_print.append(','.join((prot,pos)))

            if xlinkAnalyzer is not None:
                chains = xquest_to_chains.get(prot)
                prot_chain = chains[0] #TODO: support multiple chains
                resi.append((prot_chain, pos))
        else:
            for i in (1,2):
                prot = get_protein(xlink, i)
                pos = get_AbsPos(xlink, i)
                for_print.append(','.join((prot,pos)))

                resi_for_debug_print.append(pos)


                if xlinkAnalyzer is not None:
                    chains = xquest_to_chains.get(prot)
                    prot_chain = chains[0] #TODO: support multiple chains
                    resi.append((prot_chain, pos))


        if xlinkAnalyzer is not None:
            if len(resi) == 2:
                try:
                    dist = xlinkAnalyzer.contactMatrix.matrix[resi[0][0], int(resi[0][1])][resi[1][0], int(resi[1][1])]
                    in_structure_count = in_structure_count + 1
                except KeyError:
                    dist = 'at least one residue not in structure'
                for_print.append(str(dist))

        out.append('-'.join(for_print))

        # if len(resi_for_debug_print) == 2:
        #     out.append('grep {0} * | grep {1} |less'.format(resi_for_debug_print[0], resi_for_debug_print[1]))

    if xlinkAnalyzer is not None:
        print('of this {0} xlinks can be mapped to structure (e.g. because some parts of the structure are missing)'.format(in_structure_count))

    print('\n'.join(out))


def draw_xlink_set_comparison(data, labels, outfilename):
    fig = plt.figure()

    fig.subplots_adjust(bottom=0, top=0.8)
    ax = fig.add_subplot(111)

    ax.matshow(np.array(data), cmap=plt.cm.gist_yarg)
    for y, row in enumerate(data):
        for x, field in enumerate(row):
            plt.annotate(str(field), (x,y), xycoords='data', backgroundcolor='white', fontsize=18)

    # plt.colorbar()
    plt.xticks(list(range(len(labels))), labels, rotation=30, ha='left')
    plt.yticks(list(range(len(labels))), labels, rotation='horizontal')

    ax.grid(False)

    plt.savefig(outfilename)
    plt.close()


def list_powerset(lst):
    '''Get list of all subsets
    Based on http://rosettacode.org/wiki/Power_set#Python'''

    # the power set of the empty set has one element, the empty set
    result = [[]]
    for x in lst:
        # for every additional element in our set
        # the power set consists of the subsets that don't
        # contain this element (just take the previous power set)
        # plus the subsets that do contain the element (use list
        # comprehension to add [x] onto everything in the
        # previous power set)
        result.extend([subset + [x] for subset in result])
    return result

def powerset(s):
    '''Get set of all subsets
    Based on http://rosettacode.org/wiki/Power_set#Python'''
    return frozenset(list(map(frozenset, list_powerset(list(s)))))

def sort_subsets(xlinks_sets, max_len=None, min_ld_score=None):
    
    data = []
    all_xlinks = sum(xlinks_sets)
    if min_ld_score is not None:
        all_xlinks.filterByLdScore(min_ld_score)

    all_unique = all_xlinks.get_unique_by_prots_resi_and_xlinker()
    all_unique_xlinks = len(get_unique(all_unique.get_not_mono_links(), idfun=_xlink2unique_marker_by_prots_resi))
    all_unique_monolinks = len(get_unique(all_unique.get_mono_links(), idfun=_xlink2unique_marker_by_prots_resi))

    for subset in powerset(xlinks_sets):
        if len(subset) >= 1:
            if max_len is not None and len(subset) > max_len:
                continue

            xlinks_merged = sum(subset)
            if min_ld_score is not None:
                xlinks_merged.filterByLdScore(min_ld_score)

            xlinks_merged.description = ' '.join([x.description for x in subset])
            
            unique = xlinks_merged.get_unique_by_prots_resi_and_xlinker()
            unique_xlinks = len(get_unique(unique.get_not_mono_links(), idfun=_xlink2unique_marker_by_prots_resi))
            unique_monolinks = len(get_unique(unique.get_mono_links(), idfun=_xlink2unique_marker_by_prots_resi))
            data.append({'subset':xlinks_merged.description, 'unique_xlinks':unique_xlinks, 'unique_monolinks':unique_monolinks})

    max_number = 20

    if max_len is not None:
        max_len_info = 'with length {0} '.format(max_len)
    else:
        max_len_info = ''

    print('{0} best {1}for xlinks:'.format(max_number,max_len_info))
    for subset in sorted(data, key=lambda x: x['unique_xlinks'], reverse=True)[:max_number]:
        print(subset, str(int(round(subset['unique_xlinks']/float(all_unique_xlinks)*100, 0)))+'%')

    print('{0} best {1}for monolinks:'.format(max_number,max_len_info))
    for subset in sorted(data, key=lambda x: x['unique_monolinks'], reverse=True)[:max_number]:
        print(subset, str(int(round(subset['unique_monolinks']/float(all_unique_monolinks)*100, 0)))+'%')

def add_set_features(xlink_sets, data_cfg, contactMatrix, pdb_filename=None, min_ld_score=None, sasa=None, xlink_len_threshold=None):
    import Bio.PDB
    from dssp import BioDSSPfixed
    Bio.PDB.DSSP = BioDSSPfixed

    for xlink_set in xlink_sets:
        xlinkAnalyzer = XlinkAnalyzer(data_cfg)
        xlinkAnalyzer.contactMatrix = contactMatrix
        xlinkAnalyzer.load_xlinks(from_cfg=False, xlinksSet=xlink_set, xlink_len_threshold=xlink_len_threshold)

        xlinkAnalyzer.add_seq_features_to_xlinks()

        if pdb_filename is not None:
            pdb_structure = Bio.PDB.PDBParser().get_structure(pdb_filename, pdb_filename)
            biodssp = BioDSSPfixed.DSSP(pdb_structure[0], pdb_filename+ ' 2>/dev/null ', dssp="dssp")
            xlinkAnalyzer.biodssp = biodssp
            xlinkAnalyzer.ss = biodssp
            if sasa is not None:
                xlinkAnalyzer.asa = sasa
            else:
                xlinkAnalyzer.asa = biodssp
            xlinkAnalyzer.gen_xlinks_possible_in_structure(xlink_len_threshold=min_ld_score)
            xlinkAnalyzer.add_struct_features_to_xlinks()

def get_worse_rASA(row):

    out = []
    for rASA in (row['rASA1'], row['rASA2']):
        if isinstance(rASA, float):
            out.append(rASA)
    if len(out) > 0:
        return min(out)

def get_lower_number(row, fieldnames):
    vals = []
    for fieldname in fieldnames:
        if isinstance(row[fieldname], float) or isinstance(row[fieldname], int):
            vals.append(row[fieldname])

    if len(vals) > 0:
        return min(vals)

def get_mean_number(row, fieldnames):
    vals = []
    for fieldname in fieldnames:
        if isinstance(row[fieldname], float) or isinstance(row[fieldname], int):
            vals.append(row[fieldname])

    if len(vals) > 0:
        return np.mean(vals)

def draw_boxplot(data, xticks, filename, title=None):
    fig = plt.figure()
    fig.suptitle(title)
    fig.subplots_adjust(bottom=0.2)
    ax = fig.add_subplot(111)
    ax.patch.set_facecolor('white') #comment out to have nice gray background
    ax.boxplot(data, sym='.')
    pylab.xticks(list(range(1, len(xticks)+1)), xticks, rotation=30, ha='right')

    plt.savefig(filename)
    plt.close()

def get_data(xlinks_set, fn):
    xlinks_set_data = []

    for val in fn(xlinks_set): #xlinks_set.get_field_column('Hub_score_total'):
        if isinstance(val, float) or isinstance(val, int):
            xlinks_set_data.append(val)

    return xlinks_set_data


def draw_feature_comparison(xlink_sets, fn, filename, labels=None, title=None):

    data = []
    xticks = []

    i = 0
    for xlinks_set in xlink_sets:

        xlinks_set_data = get_data(xlinks_set, fn)
        data.append(xlinks_set_data)

        if not labels:
            xtick = xlinks_set.description
        else:
            xtick = labels[i]

        xticks.append('%s (%s) ' %(xtick, str(len(xlinks_set_data))))

        i = i  +1

    draw_boxplot(data, xticks, filename=filename, title=title)

def setBoxColors(bp):
    try:
        bp['boxes'][0].set_linewidth(1.0)
        bp['boxes'][0].set_color('#A0A0A0')
        bp['boxes'][0].set_edgecolor('#2B2B2B')
        # print pylab.getp(bp['boxes'][0])
        pylab.setp(bp['caps'][0], color='#2B2B2B')
        pylab.setp(bp['caps'][1], color='#2B2B2B')
        pylab.setp(bp['whiskers'][0], color='#2B2B2B', linestyle='-')
        # print pylab.getp(bp['whiskers'][0])
        pylab.setp(bp['whiskers'][1], color='#2B2B2B', linestyle='-')
        pylab.setp(bp['fliers'][0], color='#2B2B2B')
        pylab.setp(bp['fliers'][1], color='#2B2B2B')
        pylab.setp(bp['medians'][0], color='#2B2B2B')
    except IndexError:
        sys.stderr.write('missing boxplot element for first dataset\n')
    try:
        bp['boxes'][1].set_linewidth(1.0)
        bp['boxes'][1].set_color('#DE1B1B')
        bp['boxes'][1].set_edgecolor('#2B2B2B')        
        pylab.setp(bp['caps'][2], color='#2B2B2B')
        pylab.setp(bp['caps'][3], color='#2B2B2B')
        pylab.setp(bp['whiskers'][2], color='#2B2B2B', linestyle='-')
        pylab.setp(bp['whiskers'][3], color='#2B2B2B', linestyle='-')
        pylab.setp(bp['fliers'][2], color='#2B2B2B')
        pylab.setp(bp['fliers'][3], color='#2B2B2B')
        pylab.setp(bp['medians'][1], color='#2B2B2B')
    except IndexError:
        sys.stderr.write('missing boxplot element no data for second dataset\n')

def draw_shades(boxplots):
    points1 = []
    points1_upper = []
    points1_lower = []
    points2 = []
    points2_upper = []
    points2_lower = []

    for bp in boxplots:
        try:
            whiskers = bp['whiskers'][0]
            
        except IndexError:
            pass
        else:
            xydata = pylab.getp(whiskers, 'xydata')
            points1_upper.append(xydata[1])

        try:
            whiskers = bp['whiskers'][1]            
        except IndexError:
            pass
        else:
            xydata = pylab.getp(whiskers, 'xydata')
            points1_lower.append(xydata[1])

        try:
            whiskers = bp['whiskers'][2]
            
        except IndexError:
            pass
        else:
            xydata = pylab.getp(whiskers, 'xydata')
            points2_upper.append(xydata[1])

        try:
            whiskers = bp['whiskers'][3]            
        except IndexError:
            pass
        else:
            xydata = pylab.getp(whiskers, 'xydata')
            points2_lower.append(xydata[1])

    draw_shade = True
    if draw_shade:
        points1_lower.reverse()
        points1 = points1_upper + points1_lower

        if len(points1) > 2:
            # points1 = map(list, points1)
            # shade = plt.Polygon(points1, fc='#E0E0E0', fill=None, edgecolor='#A0A0A0', linewidth=2)
            shade = plt.Polygon(points1, fc='#E0E0E0')
            plt.gca().add_patch(shade)

        points2_lower.reverse()
        points2 = points2_upper + points2_lower

        if len(points2) > 2:
            # points2 = map(list, points2)
            # shade = plt.Polygon(points2, fc='#F3FAB6', fill=None, edgecolor='#62ab4b', linewidth=2)
            shade = plt.Polygon(points2, fc='#f88b82')
            plt.gca().add_patch(shade)

def draw_feature_comparison_combined(xlink_sets, combined_with, fn, filename, labels=None, title=None, ylabel=None):
    data1 = []
    data2 = []
    xticks = []

    i = 0    
    for xlinks_set1, xlinks_set2 in zip(xlink_sets, combined_with):
        xlinks_set_data1 = get_data(xlinks_set1, fn)
        data1.append(xlinks_set_data1)        

        if not labels:
            xtick = xlinks_set1.description
        else:
            xtick = labels[i]

        xticks.append('%s (%s) ' %(xtick, str(len(xlinks_set_data1))))

        xlinks_set_data2 = get_data(xlinks_set2, fn)
        data2.append(xlinks_set_data2)    

        xticks.append('%s (%s) ' %(xtick, str(len(xlinks_set_data2))))

        i = i + 1

    # for xlinks_set in combined_with:
    #     xlinks_set_data = get_data(xlinks_set, fn)
    #     data2.append(xlinks_set_data)

    data_sets = list(zip(data1, data2))

    fig = plt.figure()
    # fig.suptitle(title)
    fig.subplots_adjust(bottom=0.2)
    ax = fig.add_subplot(111)
    ax.xaxis.grid(False)
    ax.yaxis.grid(True, linestyle='-', color='#F6F6F6')
    ax.set_ylabel(ylabel, color='black', fontsize=18)
    ax.patch.set_facecolor('white') #comment out to have nice gray background

    '''
    3*len(data1) - we have 2*len(data1) boxplots
                (len(data1) = len(data2))
    '''
    positions = list(range(1, 3*len(data1)+1))
    chunks = [positions[i:i+3] for i in range(0, len(positions), 3)]
    xticks_positions = []
    for chunk in chunks:
        xticks_positions.extend(chunk[:2])

    boxplots = []
    for i, data_set in enumerate(data_sets):
        bp = ax.boxplot(data_set, positions=chunks[i][:2], sym='.', widths = 0.8, patch_artist=True)
        setBoxColors(bp)
        boxplots.append(bp)

    box_artist1 = None
    box_artist2 = None

    #find boxplot with both datasets and use for legend
    for bp in boxplots:
        if len(bp['boxes']) == 2:
            box_artist1 = bp['boxes'][0]
            box_artist2 = bp['boxes'][1]

    ax.legend([box_artist1,box_artist2], ['All from given condition', 'Exclusive in given condition'], fontsize=14, bbox_to_anchor=(0, 0, 1, 1), bbox_transform=plt.gcf().transFigure)

    draw_shades(boxplots)

    pylab.xlim(0, 3*len(data1)+1)
    pylab.xticks(xticks_positions, xticks, rotation=30, ha='right')


    plt.savefig(filename)
    plt.close()

def draw_feature_comparisons(xlink_sets, filename_suffix='', labels=None, title_suffix='', combined_with=None):
    file_format = 'png'
    features = [
        {
            'fn': lambda xlinks_set: getattr(xlinks_set, 'get_field_column')('Hub_score_total'),
            'filename': '/tmp/Hub_score_total'+filename_suffix+'.'+file_format,
            'title': 'Hub score',
            'ylabel': 'Total hub score of cross-linked peptides'
        },
        {
            'fn': lambda xlinks_set: getattr(xlinks_set, 'get_field_column')('HydrophobicityTotal'),
            'filename': '/tmp/HydrophobicityTotal'+filename_suffix+'.'+file_format,
            'title': 'Hydrophobicity',
            'ylabel': 'Total hydrophibicity of cross-linked peptides'
        },
        {
            'fn': lambda xlinks_set: getattr(xlinks_set, 'get_field_column')('chargedTotal'),
            'filename': '/tmp/chargedTotal'+filename_suffix+'.'+file_format,
            'title': 'Charged total',
            'ylabel': 'Total charged at ph 2 of cross-linked peptides'
        },

        {
            'fn': lambda xlinks_set: getattr(xlinks_set, 'get_field_column')('LengthTotal'),
            'filename': '/tmp/LengthTotal'+filename_suffix+'.'+file_format,
            'title': 'Length total',
            'ylabel': 'Total length of cross-linked peptides'
        },
        {
            'fn': lambda xlinks_set: getattr(xlinks_set, 'get_derived_column')(['rASA1', 'rASA2'], get_lower_number),
            'filename': '/tmp/worse_rASA'+filename_suffix+'.'+file_format,
            'title': 'Worse rASA',
            'ylabel': 'Minimal solvent accessbility of Lys residues'
        },
        {
            'fn': lambda xlinks_set: getattr(xlinks_set, 'get_derived_column')(['rASA1', 'rASA2'], get_mean_number),
            'filename': '/tmp/mean_rASA'+filename_suffix+'.'+file_format,
            'title': 'Mean rASA',
            'ylabel': 'Mean solvent accessbility of Lys residues'
        }

    ]

    for feature in features:
        if combined_with is None:
            draw_feature_comparison(xlink_sets, feature['fn'], feature['filename'], labels=labels, title=feature['title']+title_suffix)
        else:
            draw_feature_comparison_combined(xlink_sets, combined_with, feature['fn'], feature['filename'].replace('.'+file_format,'.combined.'+file_format), labels=labels,
                title=feature['title']+title_suffix,
                ylabel=feature['ylabel'])

def iter_one_against_others(xlinks_sets):

    for i, xlinks_set in enumerate(xlinks_sets):
        one = xlinks_set
        others = xlinks_sets[:i] + xlinks_sets[i+1:]

        # print one.description, 'vs', ', '.join([o.description for o in others])
        others = sum(others)
        others.description = 'others'
        yield one, others

def draw_count_histogram(labels, counts, filename, title):
    fig = plt.figure()
    fig.suptitle(title)
    fig.subplots_adjust(bottom=0.2)
    ax = fig.add_subplot(111)
    ax.xaxis.grid(False)
    ax.yaxis.grid(True, linestyle='-', color='#F6F6F6')
    ax.patch.set_facecolor('white') #comment out to have nice gray background

    ax.bar(list(range(1, len(labels)+1)), counts, width=0.3, color='#348ABD')
    pylab.xticks(list(range(1, len(labels)+1)), labels, rotation=30, ha='right')
    plt.setp(ax.get_xticklabels(), fontsize=12)

    plt.savefig(filename)
    plt.close()

def draw_xlink_count_histogram(xlinks_sets, labels, filename):
    counts = []
    for xlinks_set in xlinks_sets:
        counts.append(len(xlinks_set.get_not_mono_links_as_xlinks_set().data))

    draw_count_histogram(labels, counts, filename, title='xlink counts')

def draw_monolink_count_histogram(xlinks_sets, labels, filename):
    counts = []
    for xlinks_set in xlinks_sets:
        counts.append(len(xlinks_set.get_mono_links_as_xlinks_set().data))

    draw_count_histogram(labels, counts, filename, title='monolink counts')

def draw_xlink_monolink_count_histogram(xlinks_sets, labels, filename):
    monolink_counts = []
    xlink_counts = []
    for xlinks_set in xlinks_sets:
        monolink_counts.append(len(xlinks_set.get_mono_links_as_xlinks_set().data))
        xlink_counts.append(len(xlinks_set.get_not_mono_links_as_xlinks_set().data))

    # counts = zip(xlink_counts, monolink_counts)
    fig = plt.figure()
    # fig.suptitle(title)
    fig.subplots_adjust(bottom=0.2)
    ax = fig.add_subplot(111)
    ax.xaxis.grid(False)
    ax.yaxis.grid(True, linestyle='-', color='#F6F6F6')
    ax.patch.set_facecolor('white') #comment out to have nice gray background
    ax.set_ylabel('Number of cross/mono-links', color='black', fontsize=18)


    width = 0.3
    lefts = list(range(1, len(labels)+1))
    b1 = ax.bar(lefts, xlink_counts, width=width, color='#FF9009')
    b2 = ax.bar([i+width for i in lefts], monolink_counts, width=width, color='#348ABD')
    ax.legend([b1, b2], ['Crosslinks', 'Monolinks'], fontsize=18, loc=2)

    pylab.xticks([i+width for i in lefts], labels, rotation=30, ha='right')
    plt.setp(ax.get_xticklabels(), fontsize=14)
    plt.setp(ax.get_yticklabels(), fontsize=18)
    plt.savefig(filename)
    plt.close()

class InputError(Exception): pass

def is_in_pairs(pairs, xlinks_set1, xlinks_set2):
    return (xlinks_set1.description, xlinks_set2.description) in [(x[0].description, x[1].description) for x in pairs]

def get_set(xsets, xset):
    for x in xsets:
        if x.description == xset.description:
            return x

def compare_sets(xlinks_sets,
                data_cfg=None,
                pdb_filename=None,
                find_best_subset=False,
                max_subset_len=None,
                print_graphs=False,
                save_differences_in_csv=False,
                min_ld_score=None,
                compare_features=False,
                labels=None,
                pairs=None,
                resi_sasa=None,
                exclude_violated=False):
    '''Compare sets of xlinks
    by printing data on how many unique xlinks can be found in a set comparing to others.

    All xlink sets must have consistent protein names and residue numbering.

    labels - dict - mapping of xlinks_sets descriptions to labels
    '''

    #TODO: raise InputError if there is xlink_set in pairs not listed in xlinks_sets


    if min_ld_score is not None:
        for xlinks_set in xlinks_sets:
            xlinks_set.filterByLdScore(min_ld_score)

    if find_best_subset:
        sort_subsets(xlinks_sets, max_subset_len, min_ld_score)

    unique_sets = []
    for i, xlinks_set in enumerate(xlinks_sets):
        if xlinks_set.description is None:
            xlinks_set.description = 'set' + str(i)

        unique_sets.append(xlinks_set.get_unique_by_prots_resi())

    if not labels:
        labels = {}
        for xlinks_set in unique_sets:
            # labels.append(xlinks_set.description)
            labels[xlinks_set.description] = xlinks_set.description


    draw_xlink_count_histogram(unique_sets, [labels.get(u.description) or u.description for u in unique_sets], filename='xlink_counts.png')
    draw_monolink_count_histogram(unique_sets, [labels.get(u.description) or u.description for u in unique_sets], filename='monolink_counts.png')

    draw_xlink_monolink_count_histogram(unique_sets, [labels.get(u.description) or u.description for u in unique_sets], filename='counts.png')


    biodssp = None
    acc_thresh = None
    if data_cfg is not None and pdb_filename is not None:
        xlink_possible_fn = lambda r1, r2: is_pair_crosslinkable(r1, r2, biodssp, acc_thresh=acc_thresh)
        monolink_possible_fn = lambda r1: is_crosslinkable(r1, biodssp, acc_thresh=acc_thresh)

        xlinkAnalyzer = XlinkAnalyzer(data_cfg)

        xlinkAnalyzer.make_contact_matrix(pdb_filename, possible_fn=xlink_possible_fn)

    else:
        xlinkAnalyzer = None

    if compare_features and xlinkAnalyzer:
        add_set_features(unique_sets, data_cfg, xlinkAnalyzer.contactMatrix, pdb_filename, min_ld_score, sasa=resi_sasa, xlink_len_threshold=30)


    xlinks_comparison_results = []
    monolinks_comparison_results = []

    
    if exclude_violated:
        temp = []
        for xlinks_set in unique_sets:
            xlinks_set.filterBy(lambda x: x['Expected'] in ('Yes', 'ND'))

            temp.append(XlinksSet(xlink_set_data=xlinks_set.filtered,
                            fieldnames=xlinks_set.fieldnames[:], description=xlinks_set.description
            ))
        unique_sets = temp

    for xlinks_set1 in unique_sets:

        monolinks_count_row = []
        xlinks_count_row = []
        for xlinks_set2 in unique_sets:
            if pairs and not is_in_pairs(pairs, xlinks_set1, xlinks_set2):
                continue

            if xlinks_set1 is xlinks_set2:
                xlinks_count_row.append(0)
                monolinks_count_row.append(0)
                continue

            print(xlinks_set1.description, xlinks_set2.description)

            name1 = xlinks_set1.description
            name2 = xlinks_set2.description

            diff_xlinks_set = xlinks_set1.get_xlinks_not_in_another(xlinks_set2)

            unique_diff_xlinks = diff_xlinks_set.get_not_mono_links_as_xlinks_set()
            print('Number of unique xlinked residue pairs in {0}:'.format(name1), len(xlinks_set1.get_not_mono_links_as_xlinks_set().data))
            print('Number of unique xlinked residue pairs in {0}:'.format(name2), len(xlinks_set2.get_not_mono_links_as_xlinks_set().data))
            print('Number of unique residue pairs xlinked in {0} but not {1}:'.format(name1, name2), len(unique_diff_xlinks.data))
            xlinks_count_row.append(len(unique_diff_xlinks.data))
            print_xlinks_and_data(unique_diff_xlinks.data, xlinkAnalyzer)

            unique_diff_monolinks = diff_xlinks_set.get_mono_links_as_xlinks_set()
            print('Number of unique monolinked residues in {0}:'.format(name1), len(xlinks_set1.get_mono_links_as_xlinks_set().data))
            print('Number of unique monolinked residues in {0}:'.format(name2), len(xlinks_set2.get_mono_links_as_xlinks_set().data))
            print('Number of unique monolinked residues in {0} but not {1}:'.format(name1, name2), len(unique_diff_monolinks.data))
            monolinks_count_row.append(len(unique_diff_monolinks.data))
            print_xlinks_and_data(unique_diff_monolinks.data, xlinkAnalyzer)

            if save_differences_in_csv:
                filename = name1 + '_not_in_' + name2 + '.csv'
                diff_xlinks_set.save_to_file(filename, quoting=None, fieldnames=None)


            common_xlinks_set = xlinks_set1.get_common(xlinks_set2, same_xlink_marker=_xlink2unique_marker_by_prots_resi)
            unique_common_xlinks = common_xlinks_set.get_not_mono_links_as_xlinks_set()
            print('Number of common xlinks:', len(unique_common_xlinks.data))

        xlinks_comparison_results.append(xlinks_count_row)
        monolinks_comparison_results.append(monolinks_count_row)

    if compare_features and xlinkAnalyzer:
        not_mono_links_for_combined_graph = []
        mono_links_for_combined_graph = []
        not_mono_links = [x.get_not_mono_links_as_xlinks_set() for x in unique_sets if not x.description.startswith('blank_')]
        mono_links = [x.get_mono_links_as_xlinks_set() for x in unique_sets if not x.description.startswith('blank_')]
        draw_feature_comparisons(not_mono_links, filename_suffix='_xlinks', labels=[labels.get(u.description) or u.description for u in not_mono_links], title_suffix=' xlinks')
        draw_feature_comparisons(mono_links, filename_suffix='_monolinks', labels=[labels.get(u.description) or u.description for u in mono_links], title_suffix=' monolinks')

        diff_xlinks_sets = []

        not_mono_links_for_combined_graph.append(not_mono_links)
        mono_links_for_combined_graph.append(mono_links)


        if pairs:
            pairs_iterator = lambda uu: ((get_set(uu, x1), get_set(uu, x2)) for x1, x2 in pairs)
        else:
            pairs_iterator = iter_one_against_others

        diff_labels = []
        for one, others in pairs_iterator(unique_sets):
            print(one.description, others.description)
            # not_mono_links = [x.get_not_mono_links_as_xlinks_set() for x in [one, others]]
            # mono_links = [x.get_mono_links_as_xlinks_set() for x in [one, others]]
            # draw_feature_comparisons(not_mono_links, filename_suffix='_xlinks_'+one.description, labels=[labels[u.description] for u in unique_sets], title_suffix=' xlinks')
            # draw_feature_comparisons(mono_links, filename_suffix='_monolinks_'+one.description, labels=[labels[u.description] for u in unique_sets], title_suffix=' monolinks')

            diff_xlinks_set = one.get_xlinks_not_in_another(others)

            
            if others.description.startswith('blank_'):
                diff_xlinks_set.description = '%s' % labels[one.description]
            else:
                part1 = labels[one.description]
                if labels.get(others.description) is not None:
                    part2 = ' - %s' % labels.get(others.description)
                else:
                    part2 = ''
                diff_xlinks_set.description = '%s%s' % (part1, part2)

            if save_differences_in_csv:
                filename = one.description + '_not_in_' + others.description + '.csv'
                diff_xlinks_set.save_to_file(filename, quoting=None, fieldnames=None)


            diff_labels.append(diff_xlinks_set.description)
            diff_xlinks_sets.append(diff_xlinks_set)

        not_mono_links = [x.get_not_mono_links_as_xlinks_set() for x in diff_xlinks_sets]
        mono_links = [x.get_mono_links_as_xlinks_set() for x in diff_xlinks_sets]

        not_mono_links_for_combined_graph.append(not_mono_links)
        mono_links_for_combined_graph.append(mono_links)

        draw_feature_comparisons(not_mono_links, filename_suffix='_unique_xlinks', labels=diff_labels, title_suffix=' xlinks')
        draw_feature_comparisons(mono_links, filename_suffix='_unique_monolinks', labels=diff_labels, title_suffix=' monolinks')

        draw_feature_comparisons(not_mono_links_for_combined_graph[0], filename_suffix='_combined_xlinks', labels=diff_labels, title_suffix=' xlinks', combined_with=not_mono_links_for_combined_graph[1])
        draw_feature_comparisons(mono_links_for_combined_graph[0], filename_suffix='_combined_monolinks', labels=diff_labels, title_suffix=' monolinks', combined_with=mono_links_for_combined_graph[1])

    if print_graphs:
        draw_xlink_set_comparison(xlinks_comparison_results, [labels.get(u.description) or u.description for u in unique_sets], outfilename='xlinks_comparison_results.png')
        draw_xlink_set_comparison(monolinks_comparison_results, [labels.get(u.description) or u.description for u in unique_sets], outfilename='monolinks_comparison_results.png')



class Peptides(object):

    def __init__(self):
        self.by_mod_resi = defaultdict(list)
        self.ordered_list = []
        self.unique_str_set = set([])
        self.unique_set = set([])


    def add(self, pept_seq, start, mod_pos, fully_cleaved):
        '''start - counting resi from 1'''
        mod_pos = tuple(mod_pos)
        pept =  {
                    'seq': pept_seq,
                    'start_resi': start,
                    'mod_pos': mod_pos,
                    'fully_cleaved': fully_cleaved
                }
        self.ordered_list.append(pept)

        for pos in mod_pos:
            self.by_mod_resi[start+pos].append(pept)

        self.unique_str_set.add(pept_seq)

        self.unique_set.add((pept_seq, start, mod_pos))



def cleave(sequence, rule, missed_cleavages=0, overlap=False):
    """Cleaves a polypeptide sequence using a given rule.

    Code modified from http://pythonhosted.org/pyteomics/
    
    Parameters
    ----------
    sequence : str
        The sequence of a polypeptide.
    rule : str    
        A string with a regular expression describing the C-terminal site of
        cleavage.    
    missed_cleavages : int, optional
        The maximal number of allowed missed cleavages. Defaults to 0.
    overlap : bool, optional
        Set this to :py:const:`True` if the cleavage rule is complex and
        it is important to get all possible peptides when the matching
        subsequences overlap (e.g. 'XX' produces overlapping matches when
        the sequence contains 'XXX'). Default is :py:const:`False`.
        Use with caution: enabling this results in exponentially growing
        execution time.

    Returns
    -------
    out : set
        A set of unique (!) peptides.

    Examples
    --------
    >>> cleave('AKAKBK', expasy_rules['trypsin'], 0)
    set(['AK', 'BK'])
    >>> cleave('AKAKBKCK', expasy_rules['trypsin'], 2)
    set(['CK', 'AKBK', 'BKCK', 'AKAK', 'AKBKCK', 'AK', 'AKAKBK', 'BK'])

    """
    peptides = set()
    cleavage_sites = deque([0], maxlen=missed_cleavages+2)
    for i in chain([x.end() for x in re.finditer(rule, sequence)],
                   [None]):
        cleavage_sites.append(i)
        for j in range(0, len(cleavage_sites)-1):
            pept = (sequence[cleavage_sites[j]:cleavage_sites[-1]], cleavage_sites[j])
            peptides.add(pept)
        if overlap and i not in (0, None):
            peptides.update(
                    cleave(sequence[i:], rule, missed_cleavages, overlap))

    # if '' in peptides:
    #     peptides.remove('')

    out = set()

    for p in peptides:
        if p[0] == '':
            continue
        if re.search(rule, p[0]):
            fully_cleaved = False
        else:
            fully_cleaved = True

        out.add((p[0], p[1], fully_cleaved))

    return out


expasy_rules = {
    'arg-c':         'R',
    'asp-n':         '\w(?=D)',
    'bnps-skatole' : 'W',
    'caspase 1':     '(?<=[FWYL]\w[HAT])D(?=[^PEDQKR])',
    'caspase 2':     '(?<=DVA)D(?=[^PEDQKR])',
    'caspase 3':     '(?<=DMQ)D(?=[^PEDQKR])',
    'caspase 4':     '(?<=LEV)D(?=[^PEDQKR])',
    'caspase 5':     '(?<=[LW]EH)D',
    'caspase 6':     '(?<=VE[HI])D(?=[^PEDQKR])',
    'caspase 7':     '(?<=DEV)D(?=[^PEDQKR])',
    'caspase 8':     '(?<=[IL]ET)D(?=[^PEDQKR])',
    'caspase 9':     '(?<=LEH)D',
    'caspase 10':    '(?<=IEA)D',
    'chymotrypsin low specificity' : '([FY](?=[^P]))|(W(?=[^MP]))',
    'chymotrypsin high specificity':
        '([FLY](?=[^P]))|(W(?=[^MP]))|(M(?=[^PY]))|(H(?=[^DMPW]))',
    'clostripain':   'R',
    'cnbr':          'M',
    'enterokinase':  '(?<=[DN][DN][DN])K',
    'factor xa':     '(?<=[AFGILTVM][DE]G)R',
    'formic acid':   'D',
    'glutamyl endopeptidase': 'E',
    'granzyme b':    '(?<=IEP)D',
    'hydroxylamine': 'N(?=G)',
    'iodosobezoic acid': 'W',
    'lysc':          'K',
    'ntcb':          '\w(?=C)',
    'pepsin ph1.3':  '((?<=[^HKR][^P])[^R](?=[FLWY][^P]))|'
                     '((?<=[^HKR][^P])[FLWY](?=\w[^P]))',
    'pepsin ph2.0':  '((?<=[^HKR][^P])[^R](?=[FL][^P]))|'
                     '((?<=[^HKR][^P])[FL](?=\w[^P]))',
    'proline endopeptidase': '(?<=[HKR])P(?=[^P])',
    'proteinase k':  '[AEFILTVWY]',
    'staphylococcal peptidase i': '(?<=[^E])E',
    'thermolysin':   '[^DE](?=[AFILMV])',
    'thrombin':      '((?<=G)R(?=G))|'
                     '((?<=[AFGILTVM][AFGILTVWA]P)R(?=[^DE][^DE]))',
    'trypsin':       '([KR](?=[^P]))|((?<=W)K(?=P))|((?<=M)R(?=P))'
    }
"""
This dict contains regular expressions for cleavage rules of the most
popular proteolytic enzymes. The rules were taken from the
`PeptideCutter tool
<http://ca.expasy.org/tools/peptidecutter/peptidecutter_enzymes.html>`_
at Expasy.
"""



def make_xlinked_peptides(sequence, missed_cleavages=0, min_pept_length=None, max_pept_length=None, rule='[KR](?=[^P])'):
    '''
    Returns list of peptides like:
    [('M@EVVVSETP@RIK', 0), ('M@EVVVSETP@R', 0), ...]
    (peptide, start_index)
    @ means modified lys,
    double @ in peptide means looplink

    This function implementation is working but stupid and not really fast and may be not suitable for large dbs.
    For PolIII (17 proteins) it takes 1-2 seconds.
    '''
    # rule = expasy_rules['trypsin']
    # rule = '[KR](?=[^P])'
    peptides = set()
    prev_seq = None
    for g in re.finditer('K', sequence):
        new_seq = sequence[:g.start()] + '@' + sequence[g.end():]
        peptides.update(cleave(new_seq, rule=rule, missed_cleavages=missed_cleavages, overlap=False))

        if prev_seq:
            new_seq_for_looplinks = prev_seq[:g.start()] + '@' + prev_seq[g.end():]
            peptides.update(cleave(new_seq_for_looplinks, rule=rule, missed_cleavages=missed_cleavages, overlap=False))

        prev_seq = new_seq


    filtered = []

    for pept in peptides:
        if not '@' in pept[0]:
            continue

        pept_len = len(pept[0])
        if min_pept_length is not None and pept_len < min_pept_length:
            continue

        if max_pept_length is not None and pept_len > max_pept_length:
            continue

        filtered.append(pept)


    # temp = sorted(filter(lambda x: '@' in x[0], peptides), key=lambda z: z[1])
    temp = sorted(filtered, key=lambda z: z[1])



    peptides_out = Peptides()
    for pept in temp:
        pept_seq = pept[0]
        start = pept[1]
        fully_cleaved = pept[2]
        mod_pos = [g.start() for g in re.finditer('@', pept_seq)]
        # if len(mod_pos) == 2:
        #     print pept_seq, mod_pos
        #     import sys
        #     sys.exit()
        peptides_out.add(pept_seq.replace('@', 'K'), start + 1, mod_pos, fully_cleaved)


    return peptides_out


def test_theoretical_peptides(xlinks, peptides, rule):
    '''Just a test if all observed peptides and their modifications are in theoretical
    (for debugging)
    '''
    for xlink in xlinks.data:
        pepts = xlink['Id'].split('-')
        pept1, pept2 = get_pepts(xlink)
        rel_pos1, rel_pos2 = get_rel_pos(xlink)
        if len(pepts) == 4: #normal xlinks
            # rel_pos1 = [int(pepts[2][1:])]
            # rel_pos2 = [int(pepts[3][1:])]

            start_resi1 = int(get_AbsPos(xlink, 1))-rel_pos1[0] + 1
            start_resi2 = int(get_AbsPos(xlink, 2))-rel_pos2[0] + 1

        if len(pepts) == 3: #looplinks, monolinks:
            # rel_pos1 = [int(pepts[1][1:])]

            # if pepts[2].startswith('K'): #looplink
            #     rel_pos1.append(int(pepts[2][1:]))

            # rel_pos2 = None
            # start_resi1 = int(xlink['AbsPos1'])-rel_pos1[0] + 1
            start_resi1 = int(get_AbsPos(xlink, 1))-rel_pos1[0] + 1
            start_resi2 = None

        for pept, comp, start_resi, rel_pos in ((pept1, xlink['Protein1'], start_resi1, rel_pos1), (pept2, xlink['Protein2'], start_resi2, rel_pos2)):
            if pept is not None:
                if comp in peptides and (pept, start_resi, tuple([x-1 for x in rel_pos])) not in peptides[comp].unique_set:
                    print('Alert with observed:', xlink['Id'], xlink['Protein1'], xlink['Protein2'], (pept, start_resi, tuple([x-1 for x in rel_pos])), 'len', len(pept))
                    observed_rel_pos = tuple([x-1 for x in rel_pos])
                    test_peptide = pept+'X' #add X to have cleavage site at the end, because sometimes observed_rel_pos may be terminal K (xquest bug or sth like that)
                    observed_missed_cleavages = len(list(re.finditer(rule, test_peptide))) - len(observed_rel_pos)
                    terminal_modified = 'No'
                    for pos in observed_rel_pos:
                        if pos+1 == len(pept):
                            terminal_modified = 'Yes'
                            break
                    print('o', (pept, start_resi, observed_rel_pos), 'missed_cleavages:', observed_missed_cleavages, ', terminal modified: ', terminal_modified)
                    
                    for p in peptides[comp].unique_set:
                        if p[0] == pept:
                            print('t', p)

def bool2yes_no(v):
    if v is False:
        return 'No'
    elif v is True:
        return 'Yes'
    else:
        return v

def convert(input):
    if isinstance(input, dict):
        return dict([(convert(key), convert(value)) for key, value in input.items()])
    elif isinstance(input, list):
        return [convert(element) for element in input]
    elif isinstance(input, str):
        return input.encode('utf-8')
    else:
        return input

def read_json(filename):
    '''
    Use minify_json to allow comments in json
    '''
    with open(filename) as f:
        data = convert(json.loads(minify_json.json_minify(f.read())))

    return data 

def combinations_with_replacement(iterable, r):
    '''I don't use itertools.combinations_with_replacement to support python 2.6
    Code of this function copied from Python doc
    '''
    pool = tuple(iterable)
    n = len(pool)
    for indices in product(list(range(n)), repeat=r):
        if sorted(indices) == list(indices):
            yield tuple(pool[i] for i in indices)

XQUEST_DATA_TYPE = 'xquest'
SEQUENCES_DATA_TYPE = 'sequences'

class Config(object):
    '''Config class for inputing data'''
    def __init__(self, cfg_filename=None):
        self.components = []
        # self.component_selections = {}
        # self.component_colors = {}
        self.chain_to_comp = {}
        self.component_chains = {}
        self.sequences = {}

        self.xquest_files_parent_dir = None
        self.chain_to_xquest_name = None

        if cfg_filename:
            self.load_from_file(cfg_filename)

            self.cfg_filename = cfg_filename

    def load_from_file(self, filename):
        from Bio import SeqIO
        data = read_json(filename)
        self.components = data.get('components')
        self.component_chains = data.get('component_chains')


        for comp, chains in self.component_chains.items():
            for chain in chains:
                self.chain_to_comp[chain] = comp
        
        self.data = data.get('data')

        for data_item in self.data:
            if data_item['type'] == XQUEST_DATA_TYPE:
                data_item['chain_to_xquest_name'] = {}
                for xquest_name, c_names in data_item['xquest_to_component_names'].items():
                    for name in c_names:
                        for chain in self.component_chains[name]:
                            data_item['chain_to_xquest_name'][chain] = xquest_name


                data_item['xquest_to_chains'] = {}
                for xquest_name, c_names in data_item['xquest_to_component_names'].items():
                    for name in c_names:
                        data_item['xquest_to_chains'][xquest_name] = self.component_chains[name]

            elif data_item['type'] == SEQUENCES_DATA_TYPE:
                self.sequences = {}
                for filename in data_item['filenames']:
                    with open(self.find_path(filename)) as f:
                        for rec in SeqIO.parse(f, "fasta"):
                            seq_name = rec.description
                            if seq_name in data_item['seq_to_component_names']:
                                comps = data_item['seq_to_component_names'][seq_name]
                                for comp in comps:
                                    self.sequences[comp] = str(rec.seq)

    def find_path(self, path):
        '''
        Try to guess if path is absolute or relative and look in current dir
        '''
        if os.path.exists(path) or os.path.isabs(path):
            return path
        else:
            relative_to_current = os.path.join(os.getcwd(), path)
            if os.path.exists(relative_to_current):
                return relative_to_current

            elif hasattr(self, 'cfg_filename') and self.cfg_filename:
                cfg_dirname = os.path.dirname(os.path.realpath(self.cfg_filename))
                relative_to_config_file = os.path.join(cfg_dirname, path)
                if os.path.exists(relative_to_config_file):
                    return relative_to_config_file

        return path

    def get_seq(self, component):
        return self.sequences.get(component)

class ContactMatrix(object):
    '''
    Usage:

    def is_lys_lys(residue1, residue2):
        if residue1.get_resname() == 'LYS' and residue2.get_resname() == 'LYS':
            return True
        else:
            return False

    structure = Bio.PDB.PDBParser().get_structure(pdb_filename, pdb_filename)
    contactMatrix = ContactMatrix(structure, keep_fn=is_lys_lys)

    print contactMatrix.matrix['A', 2]['A', 11]
    print contactMatrix.matrix['A', 11]['A', 2]
    print contactMatrix.matrix['A', 2]['P', 255]

    TODO: make a superclass with general contactMatrix and biopdb subclass
    '''

    def __init__(self, biopdb_struct=None, keep_fn=None):
        self.structure = biopdb_struct
        self.matrix = defaultdict(dict)
        self.contact_list = []

        if self.structure:
            self.calc_dist_matrix(keep_fn=keep_fn)

    def calc_residue_dist(self, residue_one, residue_two, atom_name1='CA', atom_name2='CA'):
        """Returns the C-alpha distance between two residues"""

        diff_vector  = residue_one[atom_name1].coord - residue_two[atom_name2].coord
        return np.sqrt(np.sum(diff_vector * diff_vector))

    def calc_dist_matrix(self, keep_fn=None) :
        """Returns a matrix of C-alpha distances between two chains"""
        chains = list(self.structure[0])
        chain_pairs = combinations_with_replacement(chains, 2)

        self.matrix = defaultdict(dict)
        self.contact_list = []

        seen = set([])
        seen_add = seen.add
        for chain1, chain2 in chain_pairs:
            for residue1, residue2 in product(chain1, chain2):
                if residue1 is not residue2:
                    if keep_fn is not None and not keep_fn(residue1, residue2):
                        continue

                    dist = self.calc_residue_dist(residue1, residue2)

                    key1 = (chain1.id, residue1.id[1])
                    key2 = (chain2.id, residue2.id[1])

                    c = (key1, key2, dist)
                    if frozenset(c) not in seen:
                        self.contact_list.append(c)

                    seen_add(frozenset(c))

                    for key, other_key in [(key1, key2), (key2, key1)]:
                        self.matrix[key][other_key] = dist


    def iter_contacts(self, filter_fn=None):
        for contact in self.contact_list:
            if filter_fn is not None and not filter_fn(contact):
                continue
            yield contact

    def is_in_range(self, contact, range_val):
        if contact[2] <= range_val:
            return True

    def iter_contacts_in_range(self, range_val):
        for c in self.iter_contacts(filter_fn=lambda x: self.is_in_range(x, range_val)):
            yield c

def is_lys_lys(residue1, residue2):
    return residue1.get_resname() == 'LYS' and residue2.get_resname() == 'LYS'

def is_crosslinkable(residue, biodssp=None, acc_thresh=None):
    if biodssp and acc_thresh:
        full_id = residue.get_full_id()
        chain_id = full_id[2]
        resi_id = full_id[3][1]
        ss, rel_aa = get_dssp_features(biodssp, chain_id, resi_id)
        return residue.get_resname() == 'LYS' and rel_aa > acc_thresh
    else:
        return residue.get_resname() == 'LYS'

def is_pair_crosslinkable(residue1, residue2, biodssp=None, acc_thresh=None):
    return is_crosslinkable(residue1, biodssp=biodssp, acc_thresh=acc_thresh) and is_crosslinkable(residue2, biodssp=biodssp, acc_thresh=acc_thresh)


def get_monolinks_possible_in_structure(pdb_structure, possible_fn=None):
    import Bio.PDB
    if isinstance(pdb_structure, str):
        pdb_structure = Bio.PDB.PDBParser().get_structure(pdb_structure, pdb_structure)
    out = []
    chains = pdb_structure[0]
    for chain in chains:
        for residue in chain:
            if possible_fn and not possible_fn(residue):
                continue
            out.append((chain.id, residue.id[1]))

    return out


def parse_xwalk_resi_field(resi_field):
    aa_type, resi_id, chain_id, atom_type = resi_field.split('-')
    return aa_type, resi_id, chain_id, atom_type

def get_xlinks_possible_in_structure_from_xwalk(xwalk_dists_filename, xlink_len_threshold=None, possible_fn=None):
    out = []
    with open(xwalk_dists_filename, 'rU') as f:
        for line in f:
            items = line.split()

            aa_type1, resi_id1, chain_id1, atom_type1 = parse_xwalk_resi_field(items[2])
            resi_id1 = int(resi_id1)
            aa_type2, resi_id2, chain_id2, atom_type2 = parse_xwalk_resi_field(items[3])
            resi_id2 = int(resi_id2)

            eucl_dist = float(items[5])
            sas_dist = float(items[6])
            out.append(((chain_id1, resi_id1), (chain_id2, resi_id2), eucl_dist))

    return out

aaindex.init(os.path.dirname(aaindex.__file__))

class XlinkAnalyzerA(object):
    def __init__(self, data_config):
        if isinstance(data_config, str):
            self.cfg = Config(data_config)
        else:
            self.cfg = data_config

        self.theoretical_peptides = {}
        self.possible_xlinks = []
        self.possible_monolinks = []
        self.cleavage_rule = '[KR](?=[^P])'
        self.xlink_len_threshold = None

        self.contactMatrix = None

        self.seq_features_in_fieldnames = False
        self.struct_features_in_fieldnames = False
        self.ss = False
        self.asa = False

    def load_xlinks(self, from_cfg=True, xlinksSet=None, xlink_len_threshold=None):
        '''
        If xlinksSet, xquest names should be renamed to component names already
        '''
        if from_cfg:
            #this will rename all xquest names to component names based on self.cfg
            self.xlinks = mergeDataSets(self.cfg)
        elif xlinksSet:
            self.xlinks = xlinksSet

        self.xlinks.group_by_both_pos()

        if xlink_len_threshold:
            self.xlink_len_threshold = xlink_len_threshold

        for obs_xlink in self.xlinks.data:
            if 'Observed' not in obs_xlink:
                obs_xlink['Observed'] = 'Yes'

            if 'Expected' not in obs_xlink:
                resi1 = get_AbsPos(obs_xlink, 1)
                if resi1 is not None:
                    resi1 = int(resi1)

                resi2 = get_AbsPos(obs_xlink, 2)
                if resi2 is not None:
                    resi2 = int(resi2)

                comp1 = obs_xlink['Protein1']
                comp2 = obs_xlink['Protein2']
                if comp2 == '-':
                    comp2 = None

                try:
                    chain1 = self.cfg.component_chains[comp1][0]
                except KeyError:
                    chain1 = None
                try:
                    chain2 = self.cfg.component_chains[comp2][0]
                except KeyError:
                    chain2 = None

                if self.contactMatrix and ((chain1, resi1) in self.contactMatrix.matrix and (chain2, resi2) in self.contactMatrix.matrix[chain1, resi1]):
                    dist = self.contactMatrix.matrix[chain1, resi1][chain2, resi2]

                    if dist <= self.xlink_len_threshold:
                        expected = 'Yes'
                    else:
                        expected = 'No'
                else:
                    expected = 'ND'

                if is_mono_link(obs_xlink):
                    expected = 'Yes'

                obs_xlink['Expected'] = expected
                
        self.csv_fieldnames = ['Rank', 'Id', 'Protein1', 'AbsPos1', 'Protein2', 'AbsPos2', 'Type', 'Observed', 'Expected']
        for fieldname in self.xlinks.fieldnames:
            if fieldname not in self.csv_fieldnames:
                self.csv_fieldnames.append(fieldname)



    def make_xlinked_peptides_for_all_seqs(self, missed_cleavages=0, min_pept_length=None, max_pept_length=None):
        out = {}
        for comp in self.cfg.components:
            seq = self.cfg.get_seq(comp)
            peptides = make_xlinked_peptides(seq, missed_cleavages=missed_cleavages, min_pept_length=min_pept_length, max_pept_length=max_pept_length, rule=self.cleavage_rule)
            out[comp] = peptides

        self.theoretical_peptides = out

    def add_theoretical_xlinks(self):
        for pos1, pos2, dist in self.possible_xlinks:
            chain1, resi1 = pos1
            chain2, resi2 = pos2
            comp1 = self.cfg.chain_to_comp.get(chain1)
            comp2 = self.cfg.chain_to_comp.get(chain2)

            obs_xlinks = self.xlinks.get_by_both_pos(comp1, resi1, comp2, resi2)
            if obs_xlinks is not None:
                for obs_xlink in obs_xlinks:
                    obs_xlink['Observed'] = 'Yes'
                    obs_xlink['Expected'] = 'Yes'

            else:
                pepts1 = self.theoretical_peptides[comp1].by_mod_resi.get(resi1)
                pepts2 = self.theoretical_peptides[comp2].by_mod_resi.get(resi2)


                if None in (pepts1, pepts2):
                    print('No theoretical_xlink for pair', str(resi1), str(resi2), 'of ', comp1, comp2)

                else:
                    for pept1, pept2 in product(pepts1, pepts2):
                        if pept1 == pept2: #looplinks
                            theoretical_xlink = '{pept1}-a{rel_pos1}-b{rel_pos2}'.format(
                                    pept1=pept1['seq'],
                                    rel_pos1=resi1-pept1['start_resi']+1,
                                    rel_pos2=resi2-pept1['start_resi']+1
                            )
                            xlink_type = 'looplink'

                        else: #normal xlinks
                            if len(pept1['mod_pos']) > 1 or len(pept2['mod_pos']) > 1: #do not do xlinking to looplinked peptides
                                continue

                            theoretical_xlink = '{pept1}-{pept2}-a{rel_pos1}-b{rel_pos2}'.format(
                                    pept1=pept1['seq'],
                                    pept2=pept2['seq'],
                                    rel_pos1=resi1-pept1['start_resi']+1,
                                    rel_pos2=resi2-pept2['start_resi']+1
                            )

                            if comp1 == comp2:
                                xlink_type = 'intralink'
                            else:
                                xlink_type = 'interlink'

                        row = {
                            'Id': theoretical_xlink,
                            'Protein1': comp1,
                            'AbsPos1': resi1,
                            'Protein2': comp2,
                            'AbsPos2': resi2,
                            'Type': xlink_type,
                            'Observed': 'No',
                            'Expected': 'Yes'
                        }
                        for field in self.xlinks.fieldnames:
                            if field not in row:
                                row[field] = '-'

                        if 'ld-Score' not in row:
                            row['ld-Score'] = '-'
                        self.xlinks.data.append(row)


        for pos in self.possible_monolinks:
            chain1, resi1 = pos

            comp1 = self.cfg.chain_to_comp.get(chain1)
            obs_xlinks = self.xlinks.get_by_both_pos(comp1, resi1, None, None)

            if obs_xlinks is not None:
                for obs_xlink in obs_xlinks:
                    obs_xlink['Observed'] = 'Yes'
                    obs_xlink['Expected'] = 'Yes'

            else:

                pepts1 = self.theoretical_peptides[comp1].by_mod_resi.get(resi1)

                if pepts1 is None:
                    print('No theoretical monolink peptide for ', str(resi1), 'of ', comp1)
                else:
                    for pept1 in pepts1:
                        if len(pept1['mod_pos']) > 1: #do not do monolink in looplinked peptides
                            continue

                        theoretical_xlink = '{pept1}-K{rel_pos1}-000'.format(
                                pept1=pept1['seq'],
                                rel_pos1=resi1-pept1['start_resi']+1
                        )


                        comp2 = resi2 = None

                        row = {
                            'Id': theoretical_xlink,
                            'Protein1': comp1,
                            'AbsPos1': resi1,
                            'Protein2': '-',
                            'AbsPos2': 'n/a',
                            'Type': 'monolink',
                            'Observed': 'No',
                            'Expected': 'Yes'

                        }
                        for field in self.xlinks.fieldnames:
                            if field not in row:
                                row[field] = '-'
                        if 'ld-Score' not in row:
                            row['ld-Score'] = '-'

                        self.xlinks.data.append(row)

        self.xlinks.grouped_by_both_pos = defaultdict(list)
        self.xlinks.group_by_both_pos()

    def test_theoretical_peptides(self):
        test_theoretical_peptides(self.xlinks, self.theoretical_peptides, self.cleavage_rule)

    def add_seq_deriv_features_csv_fieldnames(self):
        self.csv_fieldnames.extend(['Hydrophobicity1', 'Hydrophobicity2', 'HydrophobicityTotal'])
        self.csv_fieldnames.extend(['Mass1', 'Mass2', 'MassTotal'])
        self.csv_fieldnames.extend(['Length1', 'Length2', 'LengthTotal'])
        self.csv_fieldnames.extend(['hydrophobic1', 'hydrophilic1', 'charged1'])
        self.csv_fieldnames.extend(['hydrophobic2', 'hydrophilic2', 'charged2'])  
        self.csv_fieldnames.extend(['hydrophobicTotal', 'hydrophilicTotal', 'chargedTotal', 'net_charge'])
        self.csv_fieldnames.extend(['rt_Guo1986_1', 'rt_Guo1986_2', 'rt_Guo1986Total'])
        self.csv_fieldnames.extend(['rt_Meek1980_1', 'rt_Meek1980_2', 'rt_Meek1980Total'])

    def add_seq_deriv_features_to_row(self, row, pept1_seq, pept2_seq):

        row['Hydrophobicity1'] = get_hydrophobicity(pept1_seq)
        row['Hydrophobicity2'] = get_hydrophobicity(pept2_seq)

        row['HydrophobicityTotal'] = get_combined_feature(pept1_seq, pept2_seq, fn=get_hydrophobicity)

        row['Mass1'] = get_mass(pept1_seq)
        row['Mass2'] = get_mass(pept2_seq)
        row['MassTotal'] = get_combined_feature(pept1_seq, pept2_seq, fn=get_mass)

        row['Length1'] = get_length(pept1_seq)
        row['Length2'] = get_length(pept2_seq)
        row['LengthTotal'] = get_combined_feature(pept1_seq, pept2_seq, fn=get_length)

        ratios1 = get_subjective_aatypes_ratios(pept1_seq)
        row['hydrophobic1'] = ratios1['hydrophobic']
        row['hydrophilic1'] = ratios1['hydrophilic']
        row['charged1'] = ratios1['charged']

        ratios2 = get_subjective_aatypes_ratios(pept2_seq)
        row['hydrophobic2'] = ratios2['hydrophobic']
        row['hydrophilic2'] = ratios2['hydrophilic']
        row['charged2'] = ratios2['charged']

        row['rt_Guo1986_1'] = get_rt_Guo1986(pept1_seq)
        row['rt_Guo1986_2'] = get_rt_Guo1986(pept2_seq)
        row['rt_Guo1986Total'] = get_combined_feature(pept1_seq, pept2_seq, fn=get_rt_Guo1986)        

        row['rt_Meek1980_1'] = get_rt_Meek1980(pept1_seq)
        row['rt_Meek1980_2'] = get_rt_Meek1980(pept2_seq)
        row['rt_Meek1980Total'] = get_combined_feature(pept1_seq, pept2_seq, fn=get_rt_Meek1980)      

        if None in (pept1_seq, pept2_seq):
            ratios_both = None
            for pept in (pept1_seq, pept2_seq):
                if pept is not None:

                    ratios_both = get_subjective_aatypes_ratios(pept)
                    break
            if ratios_both is None:
                ratios_both = get_subjective_aatypes_ratios(None)
        else:
            ratios_both = get_subjective_aatypes_ratios(pept1_seq+pept2_seq)

        row['hydrophobicTotal'] = ratios_both['hydrophobic']
        row['hydrophilicTotal'] = ratios_both['hydrophilic']
        row['chargedTotal'] = ratios_both['charged']
        row['net_charge'] = ratios_both['net_charge']

    def get_hub_potential(self, chain, resi_id):
        all_contacts = self.possible_xlinks_contact_matrix.matrix[chain, resi_id]

        return len(all_contacts)

    def get_total_hub_score(self, hub_score1, hub_score2):

        out = []
        for score in (hub_score1, hub_score2):
            if isinstance(score, int):
                out.append(score)
        if len(out) > 0:
            return sum(out)
        else:
            return 'ND'


    def add_seq_features(self, xlink):
        pept1_seq, pept2_seq = get_pepts(xlink)
        if pept1_seq is None and pept2_seq is None:
            return

        if not self.seq_features_in_fieldnames:
            self.seq_features_in_fieldnames = True
            
            self.add_seq_deriv_features_csv_fieldnames()

            self.csv_fieldnames.extend(['FullyCleaved1', 'FullyCleaved2'])

        self.add_seq_deriv_features_to_row(xlink, pept1_seq, pept2_seq)


        rel_pos1, rel_pos2 = get_rel_pos(xlink)
        if pept1_seq:
            fully_cleaved1 = bool2yes_no(is_fully_cleaved(pept1_seq, self.cleavage_rule, mod_rel_pos=rel_pos1))
        else:
            fully_cleaved1 = 'n/a'
        
        if pept2_seq:
            fully_cleaved2 = bool2yes_no(is_fully_cleaved(pept2_seq, self.cleavage_rule, mod_rel_pos=rel_pos2))
        else:
            fully_cleaved2 = 'n/a'

        xlink['FullyCleaved1'] = fully_cleaved1
        xlink['FullyCleaved2'] = fully_cleaved2



    def add_seq_features_to_xlinks(self):
        for xlink in self.xlinks.data:
            self.add_seq_features(xlink)


    def add_struct_features_to_xlinks(self):
        '''
        Here we have to iterate over self.possible_xlinks and self.possible_monolinks
        instead of self.xlinks.data because in the case of homomeric complexes
        for every xlink or monolink there may be several copies of protein
        and thus data may be different for given xlinked pair or monolinked resi
        depending from which copy it comes from.
        '''


        if not self.struct_features_in_fieldnames:
            self.struct_features_in_fieldnames = True

            if self.ss:
                self.csv_fieldnames.extend(['ss1', 'ss2'])

            if self.asa:
                self.csv_fieldnames.extend(['rASA1', 'rASA2'])

            if self.contactMatrix:
                self.csv_fieldnames.extend(['Hub_score1', 'Hub_score2', 'Hub_score_total'])

        if self.contactMatrix or self.asa or self.ss:
            all_observed_keys = set([])

            for key in self.xlinks.grouped_by_both_pos:
                all_observed_keys.add(key)


            for pos1, pos2, dist in self.possible_xlinks:
                chain1, resi1 = pos1
                chain2, resi2 = pos2
                comp1 = self.cfg.chain_to_comp.get(chain1)
                comp2 = self.cfg.chain_to_comp.get(chain2)

                xlinks = self.xlinks.get_by_both_pos(comp1, resi1, comp2, resi2)
                if xlinks is not None:
                    if self.contactMatrix:
                        hub_score1 = self.get_hub_potential(chain1, resi1)
                        hub_score2 = self.get_hub_potential(chain2, resi2)
                        hub_score_total = self.get_total_hub_score(hub_score1, hub_score2)

                    if self.ss:
                        ss1 = self.get_ss(chain1, resi1)
                        ss2 = self.get_ss(chain1, resi1)

                        rel_aa1 = self.get_rasa(chain1, resi1)
                        rel_aa2 = self.get_rasa(chain2, resi2)


                    for xlink in xlinks:
                        if self.contactMatrix:
                            xlink['Hub_score1'] = hub_score1
                            xlink['Hub_score2'] = hub_score2
                            xlink['Hub_score_total'] = hub_score_total                    

                        if self.ss:
                            xlink['ss1'] = ss1
                            xlink['rASA1'] = rel_aa1
                            xlink['ss2'] = ss2
                            xlink['rASA2'] = rel_aa2

                key = frozenset([(comp1, str(resi1)), (comp2, str(resi2))])
                if key in all_observed_keys:
                    all_observed_keys.remove(key)

            for pos in self.possible_monolinks:
                chain1, resi1 = pos

                if self.contactMatrix:
                    if (chain1, resi1) in self.contactMatrix.matrix:
                        hub_score1 = self.get_hub_potential(chain1, resi1)
                    else:
                        hub_score1 = 'ND'

                    hub_score_total = self.get_total_hub_score(hub_score1, 'NA')


                if self.ss:
                    ss1 = self.get_ss(chain1, resi1)
                    ss2 = self.get_ss(chain1, resi1)

                    rel_aa1 = self.get_rasa(chain1, resi1)
                    rel_aa2 = self.get_rasa(chain2, resi2)

                comp1 = self.cfg.chain_to_comp.get(chain1)
                xlinks = self.xlinks.get_by_both_pos(comp1, resi1, None, None)

                if xlinks is not None:
                    for xlink in xlinks:
                        if self.contactMatrix:
                            xlink['Hub_score1'] = hub_score1
                            xlink['Hub_score2'] = 'n/a'
                            xlink['Hub_score_total'] = hub_score_total


                        if self.ss:
                            xlink['ss1'] = ss1
                            xlink['rASA1'] = rel_aa1
                            xlink['ss2'] = 'n/a'
                            xlink['rASA2'] = 'n/a'

                key = frozenset(frozenset([(comp1, str(resi1)), (None, None)]))
                if key in all_observed_keys:
                    all_observed_keys.remove(key)


            # add observed xlinks and monolinks but not possible in structure:
            for xlink_key in all_observed_keys:
                obs_xlinks = self.xlinks.grouped_by_both_pos[xlink_key]

                for obs_xlink in obs_xlinks:
                    resi1 = get_AbsPos(obs_xlink, 1)
                    if resi1 is not None:
                        resi1 = int(resi1)

                    resi2 = get_AbsPos(obs_xlink, 2)
                    if resi2 is not None:
                        resi2 = int(resi2)

                    comp1 = get_protein(obs_xlink, 1)
                    comp2 = get_protein(obs_xlink, 2)
                    if comp2 == '-':
                        comp2 = None
    
                    try:
                        chain1 = self.cfg.component_chains[comp1][0]
                    except KeyError:
                        chain1 = None
                    try:
                        chain2 = self.cfg.component_chains[comp2][0]
                    except KeyError:
                        chain2 = None

                    if self.contactMatrix:
                        if (chain1, resi1) in self.contactMatrix.matrix:
                            hub_score1 = self.get_hub_potential(chain1, resi1)
                        else:
                            hub_score1 = 'ND'

                        if (chain2, resi2) in self.contactMatrix.matrix:
                            hub_score2 = self.get_hub_potential(chain2, resi2)
                        else:
                            hub_score2 = 'ND'

                        hub_score_total = self.get_total_hub_score(hub_score1, hub_score2)

                        obs_xlink['Hub_score1'] = hub_score1
                        obs_xlink['Hub_score2'] = hub_score2
                        obs_xlink['Hub_score_total'] = hub_score_total

                    if self.ss:
                        if resi1 is not None:
                            ss1 = self.get_ss(chain1, resi1)
                            rel_aa1 = self.get_rasa(chain1, resi1)
                        else:
                            ss1 = 'ND'
                            rel_aa1 = 'ND'

                        if resi2 is not None:
                            ss2 = self.get_ss(chain2, resi2)
                            rel_aa2 = self.get_rasa(chain2, resi2)
                        else:
                            ss2 = 'ND'
                            rel_aa2 = 'ND'

                        obs_xlink['ss1'] = ss1
                        obs_xlink['rASA1'] = rel_aa1
                        obs_xlink['ss2'] = ss2
                        obs_xlink['rASA2'] = rel_aa2


    def gen_xlinks_possible_in_structure(self, xlink_len_threshold, possible_fn=None):
        self.xlink_len_threshold = xlink_len_threshold

        self.possible_xlinks = list(self.contactMatrix.iter_contacts_in_range(self.xlink_len_threshold))

        self.make_possible_xlinks_contact_matrix()

    def gen_xlinks_possible_in_structure_from_xwalk(self, xwalk_dists_filename, xlink_len_threshold, possible_fn=None):
        self.possible_xlinks = get_xlinks_possible_in_structure_from_xwalk(xwalk_dists_filename,
                                                                            xlink_len_threshold,
                                                                            possible_fn=possible_fn)


        self.make_possible_xlinks_contact_matrix()

    def make_possible_xlinks_contact_matrix(self):
        self.possible_xlinks_contact_matrix = ContactMatrix()
        for key1, key2, dist in self.possible_xlinks:
            for key, other_key in [(key1, key2), (key2, key1)]:
                self.possible_xlinks_contact_matrix.matrix[key][other_key] = dist


    def predict_monolinkable(self, min_pept_length=None, max_pept_length=None, min_ld_score=None):
        '''
        1. Get list of monolinking positions
        2. 
        2. For every position get corresponding xlinks objects
        3. 

        '''
        self.csv_fieldnames.append('Observable')
        possible_monolinks = self.possible_monolinks #TODO: make it from all possible from sequence

        xlinks_to_predict = []


        # self.write_to_file('/tmp/dupa.csv', self.xlinks.data)


        unique = []
        seen_dict = {}

        outside_len_range = []
        for pos in possible_monolinks:
            chain1, resi1 = pos
            comp1 = self.cfg.chain_to_comp.get(chain1)

            xlinks = self.xlinks.get_by_both_pos(comp1, resi1, None, None)
            ok_xlinks = []
            if xlinks is None:
                continue
            for xlink in xlinks:
                pept = get_pepts(xlink)[0]
                if pept is None:
                    continue

                pept_len = len(pept)
                if min_pept_length is not None and pept_len < min_pept_length:
                    xlink['Observable'] = 'No'
                    outside_len_range.append(xlink)

                elif max_pept_length is not None and pept_len > max_pept_length:
                    xlink['Observable'] = 'No'
                    outside_len_range.append(xlink)

                else:
                    # if xlink['FullyCleaved1'] == 'Yes':

                    row = xlink
                    pept1, pept2 = get_pepts(row)
                    prot1 = get_protein(row, 1)
                    prot2 = get_protein(row, 2)
                    pos1 = get_AbsPos(row, 1)
                    pos2 = get_AbsPos(row, 2)


                    key = frozenset([(prot1, pept1, pos1), (prot2, pept2, pos2)])

                    if key not in seen_dict:
                        unique.append(row)
                        seen_dict[key] = row
                    else:
                        row_prev = seen_dict[key]

                        if '-' not in (row['ld-Score'], row_prev['ld-Score']) and (float(row['ld-Score']) > float(row_prev['ld-Score'])) or \
                             row_prev['Observed'] == 'No' and row['Observed'] == 'Yes':
                            idx = unique.index(row_prev)
                            unique[idx] = row
                            seen_dict[key] = row


                        ok_xlinks.append(xlink)

            xlinks_to_predict.extend(ok_xlinks)
        xlinks_to_predict = unique

        new_xlinks_set = XlinksSet(xlink_set_data=xlinks_to_predict)

        new_xlinks_set.fieldnames = self.csv_fieldnames

        # print len(outside_len_range), " of positions non-detectable because peptide outside peptide length range"

        # if min_ld_score is not None:
        #     new_xlinks_set.filterByLdScore(min_ld_score)

        # new_xlinks_set.save_to_file('/tmp/test.csv')
        clf = LogisticRegressionMonolinks(new_xlinks_set)
        clf.predict(True)


        # for xlink in self.xlinks.data:
        #     if is_mono_link(xlink):
        #         if xlink['Observed'] == 'No' and 'Observable' not in xlink:
        #             print 'q', xlink
                
                # print xlink['Observed'], xlink['Observable']

    def add_monolinkability(self):
        fake_monolinks = XlinksSet(fieldnames=self.xlinks.fieldnames)

        for xlink in self.xlinks.data:
            if is_mono_link(xlink) or is_loop_link(xlink):
                fake_monolink = copy.deepcopy(xlink)
                fake_monolinks.data.append(fake_monolink)
                fake_monolink['ori_xlink'] = xlink
                fake_monolink['prot_id'] = 1
            else:
                
                pept1, pept2 = get_pepts(xlink)
                prot1 = get_protein(xlink, 1)
                prot2 = get_protein(xlink, 2)
                pos1 = get_AbsPos(xlink, 1)
                pos2 = get_AbsPos(xlink, 2)

                monolinks = self.xlinks.get_by_both_pos(prot1, pos1, None, None)
                if monolinks is not None:
                    for monolink in monolinks:
                        fake_monolink = copy.deepcopy(monolink)
                        fake_monolinks.data.append(fake_monolink)
                        fake_monolink['ori_xlink'] = xlink
                        fake_monolink['prot_id'] = 1

                monolinks = self.xlinks.get_by_both_pos(prot2, pos2, None, None)
                if monolinks is not None:
                    for monolink in monolinks:
                        fake_monolink = copy.deepcopy(monolink)
                        fake_monolinks.data.append(fake_monolink)
                        fake_monolink['ori_xlink'] = xlink
                        fake_monolink['prot_id'] = 2

        clf = LogisticRegressionMonolinks(fake_monolinks)
        clf.predict(outproba=True)

        for fake_monolink in fake_monolinks.data:
            ori_xlink = fake_monolink['ori_xlink']
            if fake_monolink['prot_id'] == 1:
                if 'Monolinkability1' not in ori_xlink:
                    ori_xlink['Monolinkability1'] = fake_monolink['Observability']
                else:
                    if fake_monolink['Observability'] > ori_xlink['Monolinkability1']:
                        ori_xlink['Monolinkability1'] = fake_monolink['Observability']

            if fake_monolink['prot_id'] == 2:
                if 'Monolinkability2' not in ori_xlink:
                    ori_xlink['Monolinkability2'] = fake_monolink['Observability']
                else:
                    if fake_monolink['Observability'] > ori_xlink['Monolinkability2']:
                        ori_xlink['Monolinkability2'] = fake_monolink['Observability']

        self.csv_fieldnames.extend(['Monolinkability1', 'Monolinkability2'])

    def write_to_file(self, outfilename, rows):
        with open(outfilename, 'w') as f:
            wr = csv.DictWriter(f, self.csv_fieldnames, quoting=csv.QUOTE_NONNUMERIC)
            wr.writerow(dict((fn,fn) for fn in self.csv_fieldnames)) #do not use wr.writeheader() because python 2.6 doesn't have it
            wr.writerows(rows)

class XlinkAnalyzer(XlinkAnalyzerA):
    def __init__(self, data_config_filename):
        super(XlinkAnalyzer, self).__init__(data_config_filename)

        self.biodssp = None
        self.expected_vs_observed_list = None


    def make_contact_matrix(self, pdb_structure, possible_fn=None):
        import Bio.PDB
        if isinstance(pdb_structure, str):
            pdb_structure = Bio.PDB.PDBParser().get_structure(pdb_structure, pdb_structure)

        self.contactMatrix = ContactMatrix(pdb_structure, keep_fn=possible_fn)


    def gen_monolinks_possible_in_structure(self, pdb_structure, possible_fn=None):
        self.possible_monolinks = get_monolinks_possible_in_structure(pdb_structure, possible_fn=possible_fn)

    def make_expected_vs_observed_lists(self):
        pass

    def get_ss(self, chain, resi):
        ss = get_dssp_features(self.ss, chain, resi)[0]
        if ss is None:
            return 'ND'

        return ss

    def get_rasa(self, chain, resi):
        if isinstance(self.asa, dict):
            key = frozenset([chain, resi])
            rasa = self.asa.get(key)
        else:
            rasa = get_dssp_features(self.ss, chain, resi)[1]
        if rasa is None:
            return 'ND'

        return rasa



class Classifier(object):
    def __init__(self, xlinksSet):
        self.xlinksSet = xlinksSet

        self.X = self.xlinks_to_x()
        self.X = self.scale()

    def xlinks_to_x(self):
        x = []

        if len(self.xlinksSet.filtered) > 0:
            source = self.xlinksSet.filtered
        else:
            source = self.xlinksSet.data

        for xlink in source:

            sample = []
            for feature in self.features:
                if feature in xlink:
                    sample.append(xlink[feature])

            x.append(sample)

        return x

    def _mean_and_std(self, X, axis=0, with_mean=True, with_std=True):
        """Compute mean and std deviation for centering, scaling.

        Copied from scikit to avoid dependency.

        Zero valued std components are reset to 1.0 to avoid NaNs when scaling.
        """
        X = np.asarray(X)
        Xr = np.rollaxis(X, axis)

        if with_mean:
            mean_ = Xr.mean(axis=0)
        else:
            mean_ = None

        if with_std:
            std_ = Xr.std(axis=0)
            if isinstance(std_, np.ndarray):
                std_[std_ == 0.0] = 1.0
            elif std_ == 0.:
                std_ = 1.
        else:
            std_ = None

        return mean_, std_


    def scale(self):
        '''
        Equivalent of scikit preprocessing.scale

        Modified from scikit to avoid dependency.
        '''
        X = np.asarray(self.X)
        axis = 0
        with_mean=True
        with_std=True
        mean_, std_ = self._mean_and_std(
            X, axis, with_mean=with_mean, with_std=with_std)

        X = X.copy()
        # Xr is a view on the original array that enables easy use of
        # broadcasting on the axis in which we are interested in
        Xr = np.rollaxis(X, axis)
        if with_mean:
            Xr -= mean_
        if with_std:
            Xr /= std_
        
        return X


class LogisticRegression(Classifier):

    def sigmoid(self, x):
        return 1 / (1 + math.exp(-x))

    def calc_prob(self, x):
        return self.sigmoid(np.dot(x, self.coef) + self.intercept)

    def predict(self, outproba=False):
        #http://stackoverflow.com/questions/18993867/scikit-learn-logistic-regression-model-coefficients-clarification?rq=1



        Y_pred = []
        proba = []
        for x in self.X:
            prob = self.calc_prob(x)
            proba.append(prob)
            if prob < self.threshold: #Optimized to obtain False Negative Rate (predicting observed mono-link as non-observable) of 10%
                pred = 0
            else:
                pred = 1
            Y_pred.append(pred)

        def grouprows(rows, fn):
            grouped = defaultdict(list)
            for row in rows:
                key = fn(row)
                if key is not None:
                    grouped[key].append(row)

            return grouped

        def expected_not_observed_vs_observed(row):
            if row['Observed'] == 'No' and row['Expected'] == 'Yes':
                return 'Expected, not observed'
            elif row['Observed'] == 'Yes':
                return 'Observed'

        if len(self.xlinksSet.filtered) > 0:
            source = self.xlinksSet.filtered
        else:
            source = self.xlinksSet.data

        debug = False
        if debug:
            Y_true = []
            for xlink in source:
                if expected_not_observed_vs_observed(xlink) == 'Observed':
                    Y_true.append(1)
                elif expected_not_observed_vs_observed(xlink) == 'Expected, not observed':
                    Y_true.append(0)
                else:
                    print(xlink)


            try:
                print(classification_report(Y_true, Y_pred, target_names=['expected_not_observed (0)', 'observed (1)']))
                print("Matthew's correlation coefficient", matthews_corrcoef(Y_true, Y_pred))

                print("Confusion matrix:")
                print(confusion_matrix(Y_true, Y_pred))
            except NameError:
                print('Can\'t run scikit learn debugging')

        i = 0
        for xlink, pred in zip(source, Y_pred):
            if pred == 0:
                xlink['Observable'] = 'No'
            else:
                xlink['Observable'] = 'Yes'

            if outproba:
                xlink['Observability'] = proba[i]

            i = i + 1

class LogisticRegressionMonolinks(LogisticRegression):

    #no miss cleavage:
    coef = [[ 3.2632905 ],
 [-5.91856087],
 [ 0.17850994],
 [-2.26663429],
 [ 1.91028649],
 [-0.88619864],
 [ 0.61562281]]
    intercept = [-0.05197526]  
    threshold = 0.341161166093

    #one miss cleavage:
 #    coef = [[ 3.5226243 ],                                                                                                                                                
 # [-6.66592069],                                                                                                                                                
 # [-0.05697272],                                                                                                                                                
 # [-2.36960541],                                                                                                                                                
 # [ 3.37827101],                                                                                                                                                
 # [-0.05633886],                                                                                                                                                
 # [ 0.214924  ]]
 #    intercept = [0.36877842]  
 #    threshold = 0.405499365863 

    features = ['LengthTotal', 'MassTotal', 'HydrophobicityTotal', 'chargedTotal', 'net_charge', 'rt_Guo1986Total', 'rt_Meek1980Total']


def create_pops_sasas(pops_out):
    pops_data = []
    with open(pops_out) as f:
        for line in f.read().splitlines():
            pops_data.append(line.split())

    out = {}
    for (resn, chain, resi_id), g in groupby(pops_data, key=lambda l: (l[2], l[3], l[4])): #group by resi and chain and resi_id
        #(by chain so if last resi of a chain is LYS and first resi of subsequent chain is LYS they don't get grouped)
        #by resi_id so I have resi_id to use

        sasa = None
        sel_sasas = []
        atom_sasas = []
        if resn == 'LYS':

            for atom_data in g:
                atom_data = list(atom_data)


                atom = atom_data[1]
                if atom in ('NZ', 'CE'):
                    sel_sasas.append(float(atom_data[5]))
                atom_sasas.append(float(atom_data[5]))

            if len(sel_sasas) == 0:
                sasa = np.mean(atom_sasas)
            else:
                sasa = np.mean(sel_sasas)
            out[frozenset([chain, int(resi_id)])] = sasa

    return out

def get_dssp_features(biodssp, chain, resi_id, ehl2=True):
    dssp_conversion_to_EHL2 = {'G' : 'H',
                              'I' : 'H',
                              'H' : 'H',
                              'E' : 'E',
                              'B' : 'C',
                              'L' : 'C',
                              'T' : 'C',
                              'S' : 'C',
                              '-' : 'C', #This is specific for Bio.PDB.DSSP: it replaces '' from DSSP ouput with '-'
                              ' ': 'C' #don't know whether use blank or nothing!
                              }

    key = (chain, (' ', int(resi_id), ' '))
    try:
        entry = biodssp.property_dict[key]
    except KeyError:
        return None, None

    if ehl2:
        ss = dssp_conversion_to_EHL2[entry[1]]
    else:
        ss = entry[1]

    rel_aa = entry[3]

    return ss, rel_aa


def get_sum_of(pept_seq, table_name):
    vals = []

    for s in pept_seq:
        idx = aaindex.get(table_name)
        vals.append(idx.get(s))

    return sum(vals)

def get_avg_of(pept_seq, table_name):
    vals = []

    for s in pept_seq:
        idx = aaindex.get(table_name)
        vals.append(idx.get(s))

    return float(sum(vals))/len(vals) if len(vals) > 0 else float('nan')

def get_hydrophobicity(pept_seq):
    if not pept_seq:
        return 'n/a'

    return get_avg_of(pept_seq, table_name='KYTJ820101')

def get_rt_Guo1986(pept_seq):
    if not pept_seq:
        return 'n/a'

    return get_avg_of(pept_seq, table_name='GUOD860101')

def get_rt_Meek1980(pept_seq):
    if not pept_seq:
        return 'n/a'

    return get_avg_of(pept_seq, table_name='MEEJ800102')

def get_length(pept_seq):
    if not pept_seq:
        return 'n/a'

    return len(pept_seq)

def get_mass(pept_seq):
    if not pept_seq:
        return 'n/a'

    monoisotopic_protein_weights = {
    #copied from Biopython Bio.Data.IUPACData.py
        "A": 89.05,
        "C": 121.02,
        "D": 133.04,
        "E": 147.05,
        "F": 165.08,
        "G": 75.03,
        "H": 155.07,
        "I": 131.09,
        "K": 146.11,
        "L": 131.09,
        "M": 149.05,
        "N": 132.05,
        "P": 115.06,
        "Q": 146.07,
        "R": 174.11,
        "S": 105.04,
        "T": 119.06,
        "V": 117.08,
        "W": 204.09,
        "Y": 181.07,
        }


    atom_weights = {
    #copied from Biopython Bio.Data.IUPACData.py
    # For Center of Mass Calculation.
    # Taken from http://www.chem.qmul.ac.uk/iupac/AtWt/ & PyMol
        'H'  :   1.00794,
        'He' :   4.002602,
        'Li' :   6.941,
        'Be' :   9.012182,
        'B'  :  10.811,
        'C'  :  12.0107,
        'N'  :  14.0067,
        'O'  :  15.9994,
        'F'  :  18.9984032,
        'Ne' :  20.1797,
        'Na' :  22.989770,
        'Mg' :  24.3050,
        'Al' :  26.981538,
        'Si' :  28.0855,
        'P'  :  30.973761,
        'S'  :  32.065,
        'Cl' :  35.453,
        'Ar' :  39.948,
        'K'  :  39.0983,
        'Ca' :  40.078,
        'Sc' :  44.955910,
        'Ti' :  47.867,
        'V'  :  50.9415,
        'Cr' :  51.9961,
        'Mn' :  54.938049,
        'Fe' :  55.845,
        'Co' :  58.933200,
        'Ni' :  58.6934,
        'Cu' :  63.546,
        'Zn' :  65.39,
        'Ga' :  69.723,
        'Ge' :  72.64,
        'As' :  74.92160,
        'Se' :  78.96,
        'Br' :  79.904,
        'Kr' :  83.80,
        'Rb' :  85.4678,
        'Sr' :  87.62,
        'Y'  :  88.90585,
        'Zr' :  91.224,
        'Nb' :  92.90638,
        'Mo' :  95.94,
        'Tc' :  98.0,
        'Ru' : 101.07,
        'Rh' : 102.90550,
        'Pd' : 106.42,
        'Ag' : 107.8682,
        'Cd' : 112.411,
        'In' : 114.818,
        'Sn' : 118.710,
        'Sb' : 121.760,
        'Te' : 127.60,
        'I'  : 126.90447,
        'Xe' : 131.293,
        'Cs' : 132.90545,
        'Ba' : 137.327,
        'La' : 138.9055,
        'Ce' : 140.116,
        'Pr' : 140.90765,
        'Nd' : 144.24,
        'Pm' : 145.0,
        'Sm' : 150.36,
        'Eu' : 151.964,
        'Gd' : 157.25,
        'Tb' : 158.92534,
        'Dy' : 162.50,
        'Ho' : 164.93032,
        'Er' : 167.259,
        'Tm' : 168.93421,
        'Yb' : 173.04,
        'Lu' : 174.967,
        'Hf' : 178.49,
        'Ta' : 180.9479,
        'W'  : 183.84,
        'Re' : 186.207,
        'Os' : 190.23,
        'Ir' : 192.217,
        'Pt' : 195.078,
        'Au' : 196.96655,
        'Hg' : 200.59,
        'Tl' : 204.3833,
        'Pb' : 207.2,
        'Bi' : 208.98038,
        'Po' : 208.98,
        'At' : 209.99,
        'Rn' : 222.02,
        'Fr' : 223.02,
        'Ra' : 226.03,
        'Ac' : 227.03,
        'Th' : 232.0381,
        'Pa' : 231.03588,
        'U'  : 238.02891,
        'Np' : 237.05,
        'Pu' : 244.06,
        'Am' : 243.06,
        'Cm' : 247.07,
        'Bk' : 247.07,
        'Cf' : 251.08,
        'Es' : 252.08,
        'Fm' : 257.10,
        'Md' : 258.10,
        'No' : 259.10,
        'Lr' : 262.11,
        'Rf' : 261.11,
        'Db' : 262.11,
        'Sg' : 266.12,
        'Bh' : 264.12,
        'Hs' : 269.13,
        'Mt' : 268.14,
    }

    masses = []
    for aa in pept_seq:
        masses.append(monoisotopic_protein_weights[aa])

    no_peptide_bonds = len(pept_seq) - 1
    water_loss_mass = no_peptide_bonds * (atom_weights['O'] + 2* atom_weights['H'])
    mass = sum(masses) - water_loss_mass

    #TODO: add protons according to charge state (hydrogen mass * number of positive resi)
    return mass


def get_subjective_aatypes_ratios(pept_seq):
    #http://www.mcponline.org/content/3/9/908/T2.expansion.html
    subjective_aatypes = {
        'W': 'hydrophobic',
        'F': 'hydrophobic',
        'L': 'hydrophobic',
        'I': 'hydrophobic',
        'M': 'hydrophobic',
        'V': 'hydrophobic',
        'Y': 'hydrophobic',
        'A': 'hydrophobic',
        'C': 'hydrophobic',
        'T': 'hydrophilic',
        'S': 'hydrophilic',
        'N': 'hydrophilic',
        'Q': 'hydrophilic',
        'G': 'hydrophilic',
        'H': 'charged', #we have pH 2-3
        'P': 'hydrophilic',
        'E': 'hydrophilic', #we have pH 2-3
        'D': 'hydrophilic', #we have pH 2-3
        'R': 'charged',
        'K': 'charged'
    }


    outs = {
        'hydrophobic': 0,
        'hydrophilic': 0,
        'charged': 0
    }

    if not pept_seq:
        return {
        'hydrophobic': 'n/a',
        'hydrophilic': 'n/a',
        'charged': 'n/a'
        }


    for s in pept_seq:
        outs[subjective_aatypes[s]] += 1

    pept_len = len(pept_seq)



    for k, v in outs.items():
        outs[k] = (float(v)/pept_len)

    outs['net_charge'] = 0
    for s in pept_seq:
        if subjective_aatypes[s] == 'charged': #we have pH 2-3 so just counting charges (all are positive)
            outs['net_charge'] += 1

    return outs



def get_combined_feature(pept1_seq, pept2_seq, fn):
    if None in (pept1_seq, pept2_seq):
        for pept_seq in (pept1_seq, pept2_seq):
            if pept_seq is not None:
                return fn(pept_seq)
        return 'n/a'
    else:
        return fn(pept1_seq+pept2_seq)

def is_fully_cleaved(pept_seq, rule, mod_rel_pos=None):
    '''
    mod_rel_pos - int or list of ints (e.g. for looplinks), numbering starting from 1
    '''
    if mod_rel_pos is not None:
        if isinstance(mod_rel_pos, int):
            mod_rel_pos = [mod_rel_pos]
        
        for pos in mod_rel_pos:
            pept_seq = pept_seq[:pos-1] + '@' + pept_seq[pos:]

    if re.search(rule, pept_seq):
        fully_cleaved = False
    else:
        fully_cleaved = True

    return fully_cleaved

class WrongModifiedAA(Exception): pass

class MXDB_XlinksSet(object):
    def __init__(self, filename, fasta_filename):
        from Bio import SeqIO
        self.read_mxdb_out(filename)

        self.pept_data_rgxp = ''
        self.modified_aa = 'K'

        self.sequences = {}
        with open(fasta_filename) as f:
            for rec in SeqIO.parse(f, "fasta"):
                seq_name = rec.description
                self.sequences[seq_name] = str(rec.seq)

    def read_mxdb_out(self, filename):
        out = []
        with open(filename, 'rU') as csvfile: #'U' is necessary, otherwise sometimes crashes with _csv.Error: new-line character seen in unquoted field - do you need to open the file in universal-newline mode?
            dialect = csv.Sniffer().sniff(csvfile.readline(), ['\t'])
            csvfile.seek(0)

            reader = csv.DictReader(csvfile, dialect=dialect)

            for row in reader:
                out.append(row)

            self.data = out
            self.fieldnames = reader.fieldnames

    def get_pepts_and_rel_pos(self, xlink):
        pept_data = xlink['Annotation']
        m = re.match('(?P<part1_1>.*'+self.modified_aa+')\+[0-9.]+(?P<part1_2>[A-Z]*) & (?P<part2_1>.*'+self.modified_aa+')\+[0-9.]+(?P<part2_2>[A-Z]*)', pept_data)
        if m:
            part1_1 = ''.join(re.findall('[a-zA-Z]+', m.group('part1_1')))
            part1_2 = ''.join(re.findall('[a-zA-Z]+', m.group('part1_2')))
            part2_1 = ''.join(re.findall('[a-zA-Z]+', m.group('part2_1')))
            part2_2 = ''.join(re.findall('[a-zA-Z]+', m.group('part2_2')))

            pept1 = part1_1 + part1_2
            pept2 = part2_1 + part2_2
            

            rel_pos1 = len(part1_1) - 1
            rel_pos2 = len(part2_1) - 1

            return pept1, rel_pos1, pept2, rel_pos2
        else:
            return None

    def make_xquest_Id_field(self, pept1, rel_pos1, pept2, rel_pos2):
        return '{0}-{2}-a{1}-b{3}'.format(pept1, str(rel_pos1+1), pept2, str(rel_pos2+1))

    def get_proteins(self, xlink):
        proteins = xlink['Protein'].split(' & ')
        return proteins

    def _check_abs_pos(self, protein, pos):
        if self.sequences[protein][pos] != self.modified_aa:
            raise WrongModifiedAA

    def get_abs_pos_from_rel_pos(self, protein, pept_seq, rel_pos):
        '''
        rel_pos - [int] counting from 0

        May return a list of pos if match of pept_seq to prot_seq is ambigous
        '''
        prot_seq = self.sequences[protein]
        pept_positions = [m.start() for m in re.finditer('(?='+pept_seq+')', prot_seq)]
        abs_positions = [pos+rel_pos for pos in pept_positions]
        for pos in abs_positions:
            self._check_abs_pos(protein, pos)

        return abs_positions

    def get_as_xlinks_set(self):
        out = []
        for xlink in self.data:
            pept1, rel_pos1, pept2, rel_pos2 = self.get_pepts_and_rel_pos(xlink)
            protein1, protein2 = self.get_proteins(xlink)

            abs_positions1 = self.get_abs_pos_from_rel_pos(protein1, pept1, rel_pos1)
            abs_positions2 = self.get_abs_pos_from_rel_pos(protein2, pept2, rel_pos2)
            for abs_pos1, abs_pos2 in product(abs_positions1, abs_positions2):
                out.append({
                        'Id': self.make_xquest_Id_field(pept1, rel_pos1, pept2, rel_pos2),
                        'Protein1': protein1,
                        'Protein2': protein2,
                        'AbsPos1': abs_pos1+1,
                        'AbsPos2': abs_pos2+1,
                        'ld-Score': xlink['SVM-Score']
                    })

        xlinks_set = XlinksSet(xlink_set_data=out)
        xlinks_set.fieldnames = ['Id', 'Protein1', 'Protein2', 'AbsPos1','AbsPos2', 'ld-Score']

        return xlinks_set
