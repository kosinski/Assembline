#!/usr/bin/python
from optparse import OptionParser

import pyxlinks

def main():
    usage = """usage: %prog [options] mxdb_out_file sequences.fasta > xquest_out_filename
    """
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    mxdb_out_filename = args[0]
    sequences_filename = args[1]

    mxdb_xlinks_set = pyxlinks.MXDB_XlinksSet(mxdb_out_filename, sequences_filename)

    xlinks_set = mxdb_xlinks_set.get_as_xlinks_set()

    xlinks_set.print_out()
    
if __name__ == '__main__':
    main()

