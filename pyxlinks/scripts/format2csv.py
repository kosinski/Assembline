#!/usr/bin/python
from optparse import OptionParser

import pyxlinks

def main():
    usage = """usage: %prog [options] xquest_filename > out.csv
    Convert to proper csv.

    E.g:
    python format2csv.py ours_apo.tsv > out.csv
    """
    parser = OptionParser(usage=usage)

    parser.add_option("--no_fieldnames", action="store_true", dest="no_fieldnames", default=False,
                      help="Do not print fieldnames [default: %default]")

    (options, args) = parser.parse_args()

    xquest_filename = args[0]


    xlink_set = pyxlinks.XlinksSet(xquest_filename)
    
    xlink_set.print_out()

if __name__ == '__main__':
    main()