#!/usr/bin/python
from optparse import OptionParser

from pyxlinks import XlinksSet

def main():
    usage = "usage: %prog [options] xquest_filename1 xquest_filename2 ... > some_name.csv"
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()


    xquest_filenames = args

    out_xlink_set = XlinksSet(filename=xquest_filenames[0], add_extra_fields=False)

    for xquest_filename in xquest_filenames[1:]:
        other_xlink_set = XlinksSet(filename=xquest_filename, add_extra_fields=False)
        out_xlink_set = out_xlink_set.intersection_by_prots_resi(other_xlink_set)

    out_xlink_set.print_out()

if __name__ == '__main__':
    main()