#!/usr/bin/python
import sys
import os
from optparse import OptionParser
import string
import random

import pyxlinks

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def main():
    usage = "usage: %prog [options] xquest_file_filename1 xquest_file_filename2 ...as many files you want"
    parser = OptionParser(usage=usage)

    parser.add_option("-l", "--min_ld_score", type="float", dest="min_ld_score", default=None,
                      help="min_ld_score [default: %default]")

    parser.add_option("-d", "--data", dest="data_config_filename",
                      help="optional data file with info about xlinks file", metavar="FILE")

    parser.add_option("--pdb", dest="pdb_filename",
                      help="optional pdb_filename, requires --data option", metavar="FILE")

    parser.add_option("--pairs", dest="pairs_filename",
                      help="optional file listing pairs to compare", metavar="FILE")

    parser.add_option("--find_best_subset", action="store_true", dest="find_best_subset", default=False,
                      help="Find best subset, useful if you have test set with different xlinking conditions, and you want to find which conditions give optimal result [default: %default]")

    parser.add_option("--max_subset_len", type="int", dest="max_subset_len", default=None,
                      help="Maximum number of sets in the best subset [default: %default]")

    parser.add_option("--print_graphs", action="store_true", dest="print_graphs", default=False,
                      help="Whether to print graphs with pairwise comparisons [default: %default]")

    parser.add_option("--compare_features", action="store_true", dest="compare_features", default=False,
                      help="Whether to print graphs with pairwise comparisons [default: %default]")

    parser.add_option("--out_csv", action="store_true", dest="save_differences_in_csv", default=False,
                      help="Save difference sets in csv files [default: %default]")

    parser.add_option("--exclude_violated", action="store_true", dest="exclude_violated", default=False,
                      help="Exclude violated from statistics [default: %default]")


    parser.add_option("--labels", dest="labels",
                      help="optional file with labels to be used for graphs", metavar="FILE")

    parser.add_option("--pops", dest="pops",
                      help="POPS output made with pops --pdb file.pdb --atomOut --noHeaderOut --noTotalOut command", metavar="FILE")

    (options, args) = parser.parse_args()

    xquest_file_list_filenames = args


    if options.pairs_filename and len(xquest_file_list_filenames):
        print('Error: EITHER provide list of xquest list filenames OR pairs filename')
        sys.exit(1)

    if options.data_config_filename:
        config = pyxlinks.Config(options.data_config_filename)
    else:
        config = None

    labels = {}
    if options.labels:
        # with open(options.labels) as f:
        #     labels = f.read().splitlines()
        #     if len(labels) != len(xlinks_sets):
        #         print 'Error: Number of labels different than input xlinks sets'
        #         sys.exit(1)
        with open(options.labels, 'rU') as f:
            for line in f:
                line = line.strip('\n')
                filename, label = line.split(' ', 1)
                label = '\n'.join(label.split('\\n'))
                labels[filename.replace('.csv', '')] = label

    xlinks_sets = []

    pairs = None
    if options.pairs_filename:
        # xquest_file_list_filenames = []
        pairs = []
        with open(options.pairs_filename) as f:
            for line in f:
                line.strip('\n')
                pair = line.split()
                if len(pair) != 2:
                    print('Error: pair %s should have two items seperated by space')
                    sys.exit(1)
                
                xlinks_set_pair = []
                i = 1
                for filename in pair:
                    if filename == 'None': #hack to use pairs and show diff between a set and empty set
                        if i == 2:
                            #create empty set
                            xlinks_set = pyxlinks.XlinksSet()
                            xlinks_set.description = 'blank_' + id_generator()
                            labels[xlinks_set.description] = xlinks_set.description
                        else:
                            print('Error: only second in pair can be none')
                            sys.exit(1)                            
                    else:
                        xlinks_set = pyxlinks.XlinksSet(filename=xquest_filename)
                    if xlinks_set.description not in [x.description for x in xlinks_sets]:
                        # xquest_file_list_filenames.append(filename)

                        xlinks_sets.append(xlinks_set)
                    xlinks_set_pair.append(xlinks_set)
                    i = i + 1
                pairs.append(xlinks_set_pair)
    else:
        for filename in xquest_file_list_filenames:
            xlinks_sets.append(pyxlinks.XlinksSet(filename=filename))


    for all_xlinks_set in xlinks_sets:

        if config and options.pdb_filename: #need to rename to comp names to have mapping to chains

            xlink_data_cfgs = [d for d in config.data if d['type'] == pyxlinks.XQUEST_DATA_TYPE]

            for xlink_data_cfg in xlink_data_cfgs:
                # if os.path.basename(filename) == os.path.basename(xlink_data_cfg['xquest_file_list_filename']):
                if all_xlinks_set.description == xlink_data_cfg['name']:
                    xquest_to_component_names = xlink_data_cfg['xquest_to_component_names']

                    rename_dict = {}
                    for xquest_name, comp_names in xquest_to_component_names.items():
                        rename_dict[xquest_name] = comp_names[0]

                    all_xlinks_set.rename_protein_names_from_dict(rename_dict)

                    xlink_data_cfg['xquest_to_chains'] = {}
                    for xquest_name, c_names in xlink_data_cfg['xquest_to_component_names'].items():
                        for name in c_names:
                            xlink_data_cfg['xquest_to_chains'][name] = config.component_chains[name]

        # xlinks_sets.append(all_xlinks_set)

    # if options.min_ld_score:
    #     for xlinks_set in xlinks_sets:
    #         xlinks_set.filterByLdScore(options.min_ld_score)



    if options.pops:
        sasa = pyxlinks.create_pops_sasas(options.pops)
    else:
        sasa = None


    pyxlinks.compare_sets(xlinks_sets,
                        data_cfg=config,
                        pdb_filename=options.pdb_filename,
                        find_best_subset=options.find_best_subset,
                        max_subset_len=options.max_subset_len,
                        print_graphs=options.print_graphs,
                        save_differences_in_csv=options.save_differences_in_csv,
                        min_ld_score=options.min_ld_score,
                        compare_features=options.compare_features,
                        labels=labels,
                        pairs=pairs,
                        resi_sasa=sasa,
                        exclude_violated=options.exclude_violated)

if __name__ == '__main__':
    main()