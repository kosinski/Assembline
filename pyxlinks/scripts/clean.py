#!/usr/bin/python
from optparse import OptionParser

import pyxlinks


def main():
    usage = """usage: %prog [options] xquest_filename > outfile
    E.g:
    Generate xlink file with:
    - AbsPos2 for looplinks field (default is n/a)
    - Prot2 for looplinks field (default is -)
    """
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    xquest_filename = args[0]

    xlink_set = pyxlinks.XlinksSet(xquest_filename)
    xlink_set.clean()
    xlink_set.print_out()


if __name__ == '__main__':
    main()
