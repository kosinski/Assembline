#!/usr/bin/python
'''
Based on:
* http://www2.warwick.ac.uk/fac/sci/moac/people/students/peter_cock/python/protein_contact_map/
* http://pythonhosted.org/pyteomics/
'''

from optparse import OptionParser
from itertools import product, chain, combinations
from collections import defaultdict, deque
import json
import re
import csv
import os

import Bio.PDB
# from Bio import SeqIO

import pyxlinks
from pyxlinks import aaindex

from dssp import BioDSSPfixed
Bio.PDB.DSSP = BioDSSPfixed
# from Bio.SCOP.Raf import to_one_letter_code

def main():
    usage = "usage: %prog [options] pdb_filename outfilename"
    parser = OptionParser(usage=usage)

    parser.add_option("-d", "--data", dest="data_config_filename",
                      help="optional data file with info about xlinks file", metavar="FILE")

    parser.add_option("-t", "--thresh", type="float", dest="xlink_len_threshold", default=30.,
                      help="xlink_len_threshold")

    parser.add_option("-m", "--miss", type="int", dest="missed_cleavages", default=0,
                      help="Max number of missed cleavages")

    parser.add_option("-a", "--minp", type="int", dest="min_pept_length", default=None,
                      help="min_pept_length")

    parser.add_option("-b", "--maxp", type="int", dest="max_pept_length", default=None,
                      help="max_pept_length")

    parser.add_option("-r", "--min_rasa", type="float", dest="min_rasa", default=None,
                      help="minimum relative accessible surface area (rASA) for modified residue")

    parser.add_option("-x", "--xwalk", dest="xwalk_dists_filename",
                      help="optional xwalk output file to use for structuraly plausible xlinks", metavar="FILE")

    (options, args) = parser.parse_args()


    xlinkAnalyzer = pyxlinks.XlinkAnalyzer(options.data_config_filename)


    xlinkAnalyzer.make_xlinked_peptides_for_all_seqs(options.missed_cleavages, options.min_pept_length, options.max_pept_length)


    pdb_filename = args[0]
    outfilename = args[1]

    pdb_structure = Bio.PDB.PDBParser().get_structure(pdb_filename, pdb_filename)


    biodssp = BioDSSPfixed.DSSP(pdb_structure[0], pdb_filename+ ' 2>/dev/null ', dssp="dssp")
    xlinkAnalyzer.biodssp = biodssp
    xlinkAnalyzer.ss = biodssp
    xlinkAnalyzer.asa = biodssp
    if options.min_rasa is not None:
        acc_thresh=options.min_rasa
    else:
        biodssp = None
        acc_thresh = None

    xlink_possible_fn = lambda r1, r2: pyxlinks.is_pair_crosslinkable(r1, r2, biodssp, acc_thresh=acc_thresh)
    monolink_possible_fn = lambda r1: pyxlinks.is_crosslinkable(r1, biodssp, acc_thresh=acc_thresh)


    if options.xwalk_dists_filename:
        xlinkAnalyzer.make_contact_matrix(pdb_structure, possible_fn=xlink_possible_fn) #necessary for make_expected_vs_observed_lists
        xlinkAnalyzer.gen_xlinks_possible_in_structure_from_xwalk(options.xwalk_dists_filename, xlink_len_threshold=options.xlink_len_threshold, possible_fn=xlink_possible_fn)
    else:
        xlinkAnalyzer.make_contact_matrix(pdb_structure, possible_fn=xlink_possible_fn)
        xlinkAnalyzer.gen_xlinks_possible_in_structure(xlink_len_threshold=options.xlink_len_threshold)

    xlinkAnalyzer.gen_monolinks_possible_in_structure(pdb_structure, possible_fn=monolink_possible_fn)


    xlinkAnalyzer.load_xlinks()


    xlinkAnalyzer.test_theoretical_peptides()

    xlinkAnalyzer.add_theoretical_xlinks()
    xlinkAnalyzer.add_seq_features_to_xlinks()
    xlinkAnalyzer.add_struct_features_to_xlinks()
    # xlinkAnalyzer.predict_monolinkable(6, 50, min_ld_score=30)
    # xlinkAnalyzer.make_expected_vs_observed_lists_old()

    # xlinkAnalyzer.write_to_file(outfilename, xlinkAnalyzer.expected_vs_observed_list)
    xlinkAnalyzer.add_monolinkability()
    xlinkAnalyzer.write_to_file(outfilename, xlinkAnalyzer.xlinks.data)


if __name__ == '__main__':
    main()