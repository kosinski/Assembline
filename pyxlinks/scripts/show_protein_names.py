#!/usr/bin/python
from optparse import OptionParser


import pyxlinks


def main():
    usage = """usage: %prog [options] xquest_filename1 xquest_filename2 xquest_filename3  ...
    xquest_filename1 xquest_filename2 xquest_filename3  ... - one or more xquest file
    """
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    xquest_filenames = args

    for xquest_filename in xquest_filenames:
        xlinks_set = pyxlinks.XlinksSet(filename=xquest_filename)
        print(xquest_filename + ':')
        print('\n'.join(xlinks_set.get_protein_names()))

if __name__ == '__main__':
    main()

