#!/usr/bin/python
from optparse import OptionParser

from pyxlinks import XlinksSet

def main():
    usage = "usage: %prog [options] xquest_filename fraction > some_name.csv"
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()


    xquest_filename = args[0]
    fraction = float(args[1])

    xlink_set = XlinksSet(filename=xquest_filename, add_extra_fields=False)
    all_unique = xlink_set.get_random_subset(fraction)

    all_unique.print_out()

if __name__ == '__main__':
    main()