#!/usr/bin/python
from optparse import OptionParser

import pyxlinks


def main():
    usage = """usage: %prog [options] xquest_filenames > outfile
    """
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    # xquest_filename = args[0]
    xquest_filenames = args

    # xlink_set = pyxlinks.XlinksSet(xquest_filename)

    xlinks_sets = []
    for filename in xquest_filenames:
        xlink_set = pyxlinks.XlinksSet(filename)

        xlinks_sets.append(xlink_set)

    xlink_set = sum(xlinks_sets)


    xlink_set = xlink_set.get_unique_set_for_dist_restraints()
    print('\n'.join(xlink_set.gen_CNS_restraints()))

if __name__ == '__main__':
    main()
