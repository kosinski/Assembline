#!/usr/bin/python
from optparse import OptionParser

import pyxlinks

def main():
    usage = "usage: %prog [options] xquest_filenames > outfilename"
    parser = OptionParser(usage=usage)

    parser.add_option("--do_not_add_extra_fields", action="store_true", dest="do_not_add_extra_fields", default=False,
                      help="Whether to add columns with xlinker and source [default: %default]")

    (options, args) = parser.parse_args()

    xquest_filenames = args[:]

    if options.do_not_add_extra_fields:
        add_extra_fields = False
    else:
        add_extra_fields = True

    xlink_set = None
    for filename in xquest_filenames:
        next_xlink_set = pyxlinks.XlinksSet(filename, add_extra_fields=add_extra_fields)
        if not xlink_set:
            xlink_set = next_xlink_set
        else:
            xlink_set = xlink_set.get_common(next_xlink_set, same_xlink_marker=pyxlinks._xlink2unique_marker_by_prots_resi)

    xlink_set.print_out()



if __name__ == '__main__':
    main()