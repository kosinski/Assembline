#!/usr/bin/python
from optparse import OptionParser

import pyxlinks


def main():
    usage = """usage: %prog [options] xquest_filenames > outfile
    E.g:
    Generate xlink file with:
    - AbsPos2 for looplinks field (default is n/a)
    - Prot2 for looplinks field (default is -)
    """
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    # xquest_filename = args[0]
    xquest_filenames = args

    # xlink_set = pyxlinks.XlinksSet(xquest_filename)

    xlinks_sets = []
    for filename in xquest_filenames:
        xlink_set = pyxlinks.XlinksSet(filename)

        xlinks_sets.append(xlink_set)

    xlink_set = sum(xlinks_sets)

    xlink_set.clean()
    xlink_set = xlink_set.get_unique_by_prots_resi()
    # xlink_set = xlink_set.get_not_mono_links_as_xlinks_set()
    xlink_set.print_out(fieldnames=['Protein1', 'Protein2', 'AbsPos1', 'AbsPos2', 'Type', 'ld-Score'])


if __name__ == '__main__':
    main()
