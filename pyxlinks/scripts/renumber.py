#!/usr/bin/python
import sys
from optparse import OptionParser

from cogent3.parse.fasta import MinimalFastaParser

import pyxlinks

class TooFewSequences(Exception): pass
def seqs_to_cols(aln_seqs):
    if len(aln_seqs) < 2:
        raise TooFewSequences('Provide at least two sequences as input')

    return list(zip(*aln_seqs))


def aln_cols_to_num_map(aln_cols):
    '''
    Return mapping from first to second
    '''
    a_i = 1
    b_i = 1
    num_map = {}
    for a, b in aln_cols:
        if '-' not in (a, b):
            num_map[a_i] = b_i
        elif b == '-':
            num_map[a_i] = None
        if a != '-':
            a_i = a_i + 1
        if b != '-':
            b_i = b_i + 1

    return num_map

def get_resi_maps(xquest_names, fasta_filenames):
    xquest_names = set(xquest_names)

    maps = {}
    for fasta_filename in fasta_filenames:
        with open(fasta_filename) as f:
            parser = MinimalFastaParser(f)
            seqs = []
            renumber_from_name = None
            for name, seq in parser:
                if name in xquest_names:
                    renumber_from_name = name
                    xquest_names.remove(name)

                seqs.append(seq)
                if len(seq) == 2:
                    break        

            maps[renumber_from_name] = aln_cols_to_num_map(seqs_to_cols(seqs))

    return maps

def main():
    usage = """usage: %prog [options] xquest_filename fasta_filename1 fasta_filename2 ... > xquest_out_filename
    xquest_filename - xQuest-like ouput file in CSV or tab seperated format
    fasta_filename1 fasta_filename2 ... - alignments of sequences used for xQuest (renumber from) 
        to sequences to which to renumber to.
        First sequence must be sequence (renumber from) with name from xQuest, second any name.
    """
    parser = OptionParser(usage=usage)

    parser.add_option("--remove_mapped_to_none", action="store_true", dest="remove_mapped_to_none", default=True,
                      help="Do not print those xlinks for which mapping cannot be done because of aligning to a gap [default: %default]")

    (options, args) = parser.parse_args()

    xquest_filename = args[0]
    fasta_filenames = args[1:]

    xlinks_set = pyxlinks.XlinksSet(filename=xquest_filename)
    xquest_names = xlinks_set.get_protein_names()

    resi_maps = get_resi_maps(xquest_names, fasta_filenames)

    for xquest_name in xquest_names:
        try:
            resi_map = resi_maps[xquest_name]
        except KeyError:
            sys.stderr.write('WARNING: Sequence %s not found in alignments.\n' % xquest_name)
        else:
            xlinks_set.renumber_resi(xquest_name, resi_map, remove_mapped_to_none=options.remove_mapped_to_none)

    xlinks_set.print_out()

if __name__ == '__main__':
    main()

