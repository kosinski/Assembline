Repository of Conda configuration files, code and manual for Assembline

This is just a dump of the code from our local development repositories, 
which include unittests and newest developments. 
If you wish to contribute and access those repositories please contact: Jan Kosinski jan.kosinski@embl.de


Conda building instructions for developers:

module load Anaconda3/2020.07
module load git

conda create --name condaBuild
source activate condaBuild
conda install conda-build

conda-build --py 3.8 --py 3.9 --py 3.10 .


anaconda login
anaconda upload /g/kosinski/kosinski/conda-bld/linux-64/assembline-0.99-py38_0.tar.bz2 --force
conda convert -f --platform osx-64 /g/kosinski/kosinski/conda-bld/linux-64/assembline-0.99-py38_0.tar.bz2 -o /g/kosinski/kosinski/conda-bld
anaconda upload /g/kosinski/kosinski/conda-bld/osx-64/assembline-0.99-py38_0.tar.bz2 --force

#test
conda create --name Assembline
#install dependencies according to the instructions in the manual

module load Chimera
source activate Assembline
conda install -c kosinskilab assembline --force-reinstall

