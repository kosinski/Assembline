from distutils.core import setup

setup(name='pdbx',
    version='1.0',
    packages=['pdbx', 'pdbx.writer', 'pdbx.reader'])
