#!/bin/bash

echo "Building"
cd $SRC_DIR/pdb_utils
$PYTHON setup.py install

cd $SRC_DIR/pyxlinks
$PYTHON setup.py install

cd $SRC_DIR/efitter
$PYTHON setup.py install

cd $SRC_DIR/imp_utils1
$PYTHON setup.py install

cd $SRC_DIR/SuperConfig
$PYTHON setup.py install

#Do not use PDBX cmake way to simplify life
cp $RECIPE_DIR/pdbx_setup.py $SRC_DIR/pdbx/setup.py
cd $SRC_DIR/pdbx
$PYTHON setup.py install
#this would be they way:
# mkdir build
# cd build
# cmake ../
# make install


cd $SRC_DIR/pdb-tools
$PYTHON setup.py install
