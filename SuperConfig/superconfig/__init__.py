import os
import sys
import json
from collections import defaultdict
import csv
import tempfile
import string
from copy import deepcopy
import math
import pprint
from itertools import groupby
import re

from . import minify_json

import pyxlinks


XLINK_ANALYZER_DATA_TYPE = 'Xlink Analyzer'
XQUEST_DATA_TYPE = 'xquest'
SEQUENCES_DATA_TYPE = 'sequences'
PDB_FILES_DATA_TYPE = 'pdb_files'
INTERACTING_RESI_DATA_TYPE = 'interacting_resi'
EM_DATA_TYPE = 'em_map'
INI_FITS_DATA_TYPE = 'ini_fits' #Deprecated, kept for backward compatibility
ALTERNATIVE_POSITIONS_DATA_TYPE = 'alternative_positions'
CONNECTIVITY_DATA_TYPE = 'connectivity'
SIMILAR_ORIENTATION_DATA_TYPE ='similar_orientation'

def fasta_iter(fh):
    """Return iterator over FASTA file with multiple sequences.

    Modified from Brent Pedersen
    Correct Way To Parse A Fasta File In Python
    given a fasta file. yield tuples of header, sequence

    :param fh: File Handle to the FASTA file

    :return: 2-element tuple with header and sequence strings
    """

    # ditch the boolean (x[0]) and just keep the header or sequence since
    # we know they alternate.
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))

    for header in faiter:
        # drop the ">"
        headerStr = header.__next__()[1:].strip()

        # join all sequence lines to one.
        seq = "".join(s.strip() for s in faiter.__next__())

        yield (headerStr, seq)

def is_coord_line(line):
    """Check if a PDB line belongs to the ATOM section (skipping HETATM lines!).
    """
    return line[0:6] in ('ATOM  ', 'ANISOU', 'TER   ')

def coord_lines(f):
    """Return a generator over lines in the ATOM section.
    """
    for line in f:
        if is_coord_line(line):
            yield line

def resi_grouper(line):
    """Helper function for grouping PDB lines into residues.
    """

    return line[21] + line[22:26]

def chain_grouper(line):
    """Helper function for grouping PDB lines into chains.
    """
    return line[21]

def ranges_from_list(i):
    """Convert an increasing list of integers into a range list
    """
    # https://stackoverflow.com/questions/4628333/converting-a-list-of-integers-into-range-in-python
    for a, b in groupby(enumerate(i), lambda pair: pair[1] - pair[0]):
        b = list(b)
        yield b[0][1], b[-1][1]

def get_residue_ids(f, chain_id=None):
    """Get residue ids from a PDB file or a specific chain of a PDB file.

    :param f: PDB file handle
    :param chain_id: chain ID, defaults to None, optional

    :return: a list of tuples, each tuple containing chain id (str) and residue ID (int)
    """
    out = []
    for _chain_id, chain_lines in groupby(coord_lines(f), chain_grouper):
        resi_nums = []
        if chain_id is not None and _chain_id != chain_id:
            continue
        for k, lines in groupby(chain_lines, resi_grouper):
            line = next(lines)
            out.append((_chain_id, int(line[22:26])))

    return out

class XlinkData(object):
    """A class wrapping pyxlinks.XlinksSet, adding properties needed for SuperConfig functionalities.

    Reads passed crosslink files and merges them into a single pyxlinks.XlinksSet.

    :params name: custom name to be used for this dataset
    :params dataType: type of the passed crosslink files, XQUEST_DATA_TYPE or XLINK_ANALYZER_DATA_TYPE
    :type dataType: str
    :params xlink_filenames: list of crosslink files in a format supported by pyxlinks, XQUEST_DATA_TYPE or XLINK_ANALYZER_DATA_TYPE
    :type xlink_filenames: list, (of strings)
    :params xlink_to_component_names: mapping of sequence names in xlink files to Component names (currently Component=Subunit)
    :type xlink_to_component_names: dict
    """
    def __init__(self,
                 name,
                 dataType,
                 xlink_filenames,
                 xlink_to_component_names):

        self.name = name
        self.type = dataType

        xlinks_sets = []
        for xquest_filename in xlink_filenames:
            xlink_set = pyxlinks.XlinksSet(filename=xquest_filename)
            xlinks_sets.append(xlink_set)
        self.all_xlinks_set = sum(xlinks_sets)

        self.xlink_to_component_names = xlink_to_component_names

    def get_component_from_xlink_name(self, name):
        """Return a Component (Subunit) name given the name in an xlink file
        """
        if name not in ('-', 'n/a'):
            try:
                data = self.xlink_to_component_names[name]
                return data[0]
            except:
                return None

class Subunit(object):
    """Class representing a subunit.

    Instances of this class represent a subunit TYPE, not a molecule COPY.
    E.g. for a complex with 3 copies of protein X would still have a single Subunit X.
    
    :params name: name of the subunit
    :params chain_ids: list of chain ids for all copies, 
                       in multistate models, each state will have same chain ids
    :params color: RGBA color specification, list of four 0-1 values
    :params domains: domain specification, e.g.:
                    [
                                {
                                    "name": "propeller1",
                                    "ranges": [
                                        [
                                            15,
                                            425
                                        ]
                                    ]
                                },
                                {
                                    "name": "propeller2",
                                    "ranges": [
                                        [
                                            430,
                                            732
                                        ]
                                    ]
                                }
                    ]
    :params series: list of Serie objects for this subunit
    :params copies_per_state: lists of lists, each list stores PMI molecules (copies and clones) for this subunit
    """
    def __init__(self, name, chain_ids, color=None, domains=None):
        self.name = name
        self.chain_ids = chain_ids
        self.color = color
        self.domains = domains or []
        self.series = []
        self.copies_per_state = [] 

    def __repr__(self):
        return self.name

class InvalidSerie(Exception):
    pass

class Serie(object):
    """Class representing a Serie. 
    Serie is a group of related subunit copies e.g. related by symmetry.

    Overall very messy class, to be refactored.

    :param subunit: Subunit instance for this Serie
    :param name: Name of the serie
    :param mode: 'auto' or 'input'
                 a parameter used in imp_utils1 to define whether PMI copies ('input') or clones ('auto') should be created
    :param chainIds: Chains of this Subunit to be included in this Serie
    :param cell_count: How many elements in the Series
    :param tr3d: Transformation relating Subunits in this Serie
    :type tr3d: IMP Transformation3D, to be changed (SuperConfig should be independent on IMP)
    :param inipos: 'input' or name of a symmetrical transformation in config
    :param ref_serie: Reference Serie. If ini_pos is a transformation, imp_utils1 will transform
                      each copy in this serie relatively to the ref_serie by that transformation
    :params states: List of state indices this serie should act on. Not sure if it's working.
    """
    def __init__(self, subunit, name, mode, chainIds=[], cell_count=1, tr3d=None, inipos='input', ref_serie=None, states=[0]):
        self.name = name
        if mode is None:
            mode = 'input'
        if mode in ('input', 'auto'):
            self.mode = mode
        else:
            raise Exception('Mode {} is not supported'.format(mode))
        self.subunit = subunit
        self.chainIds = chainIds
        self.cell_count = cell_count
        self.tr3d = tr3d
        self.inipos = inipos
        self.ref_serie = ref_serie
        self.states = states

        self.chain_extender = None # will hold a generator yielding chain IDs for related copies
        self.molecules = [] #list of lists, molecules for each state
        self.molecules_copy_ids = [] #list of lists, molecule copy IDs for each state
        self.tr3ds = [] #list of lists, each state will be populated by imp_utils1 to contain a respective tr3d for each copy for each state


    def __repr__(self):
        return 'Serie_{name}_{subunit}'.format(name=self.name, subunit=self.subunit.name)

    def add_next_chain(self):
        """Add the next free chain ID for this Serie and Subunit.
        It is used to generate chain IDs for copies,
        e.g for a chain A, it would generate A1, A2, A3,..., etc.
        """
        if self.chain_extender is None:
            self.chain_extender = get_chain_extender(self.chainIds[0])
            next(self.chain_extender)  # first is the original chain
        self.chainIds.append(next(self.chain_extender))

        return self.chainIds[-1]

def get_chain_extender(ori_chain_id):
    """Generate multi-character chain IDs given the base chain ID,
    e.g for a chain A, it would generate A1, A2, A3,... etc.
    """
    i = 0
    while True:
        if i == 0:
            yield ori_chain_id
        else:
            yield ori_chain_id+str(i)

        i = i + 1

def add_state_to_json_cfg(json_cfg, state_idx=0):
    """Iterate through the entire JSON config and add "state" or "states" properties
    to the relevant objects, if these properties are not defined for those objects.

    Used in merge_jsons for adding state IDs when multiple JSON files are provdied to Config.

    :param json_cfg: the entire object as read from JSON system configuration file
    :param state_idx: ID of the state to add

    :return: None, modifies the passed JSON object in place
    """
    state_suffix = '_State'+str(state_idx)
    for key, item in json_cfg.items():
        if key == 'series': #new style series definition
            for serie in item:
                serie['states'] = [state_idx]
                serie['name'] += state_suffix
                if 'tr3d' in serie:
                    serie['tr3d'] += state_suffix
        if key == 'symmetry':
            if 'series' in item:
                for serie in item['series']:
                    serie['states'] = [state_idx]
                    serie['name'] += state_suffix
                    if 'tr3d' in serie:
                        serie['tr3d'] += state_suffix
            if 'sym_tr3ds' in item:
                for serie in item['sym_tr3ds']:
                    serie['name'] += state_suffix

            if 'apply_symmetry' in item:
                for data_obj in item['apply_symmetry']:
                    data_obj['sym'] += state_suffix
                    for sel in data_obj['selectors']:
                        print(sel)
                        sel['serie'] += state_suffix
        if key == 'rigid_bodies':
            for rb_cfg in item:
                for component in rb_cfg['components']:
                    component['state'] = state_idx
                    if 'serie' in component:
                        component['serie'] += state_suffix
        if key == 'data':
            for data_obj in item:
                if 'name' in data_obj:
                    data_obj['name'] += state_suffix
                if data_obj['type'] == PDB_FILES_DATA_TYPE:
                    for data_element in data_obj['data']:
                        for component in data_element['components']:
                            component['state'] = state_idx
                            if 'serie' in component:
                                component['serie'] += state_suffix
                if 'optimized_components' in data_obj:
                    for component in data_obj['optimized_components']:
                        component['state'] = state_idx
                        if 'serie' in component:
                            component['serie'] += state_suffix
                if data_obj['type'] in (CONNECTIVITY_DATA_TYPE, ):
                    for data_element in data_obj['data']:
                        for selector in data_element:
                            selector['state'] = state_idx
                            if 'serie' in selector:
                                selector['serie'] += state_suffix

                if data_obj['type'] in (SIMILAR_ORIENTATION_DATA_TYPE,):
                    for data_element in data_obj['data']:
                        for pair in data_element:
                            for selector in pair:
                                selector['state'] = state_idx
                                if 'serie' in selector:
                                    selector['serie'] += state_suffix

def find_path(path, cfg_filename=None, project_dir=None):
    """Try to guess if path is absolute or relative and look in the current dir
    """
    if os.path.exists(path) or os.path.isabs(path):
        return path
    else:
        if project_dir:
            abs_path = os.path.join(project_dir, path)
            if os.path.exists(abs_path):
                return abs_path

        relative_to_current = os.path.join(os.getcwd(), path)
        if os.path.exists(relative_to_current):
            return relative_to_current

        elif cfg_filename:
            cfg_dirname = os.path.dirname(os.path.realpath(cfg_filename))
            relative_to_config_file = os.path.join(cfg_dirname, path)
            if os.path.exists(relative_to_config_file):
                return relative_to_config_file

    return path

def merge_jsons(filenames, multistate=True, project_dir=None):
    """Merge multiple json configs, taking each json as a separate state for multi-state modeling.

    :params: a list of JSON system configuration files

    :return: the entire object merging objects read from JSON system configuration files
    """
    out = read_json(filenames[0])
    if multistate:
        out['number_of_states'] = len(filenames)
        state_idx = 0
        add_state_to_json_cfg(out, state_idx=state_idx)

    for fn in filenames[1:]:
        cfg = read_json(fn)
        if multistate:
            state_idx += 1
            add_state_to_json_cfg(cfg, state_idx)

        #merge configuration objects from the remaining json into the first one
        for key, item in cfg.items():
            if key not in out:
                out[key] = item
            else:
                if key == 'symmetry':
                    for item_key, item_val in item.items():
                        if item_key not in out[key]:
                            out[key][item_key] = item_val
                        else:
                            out[key][item_key].extend(item_val)
                elif key in ('rigid_bodies', 'series'):
                    if key in out:
                        out[key].extend(cfg[key])
                    else:
                        out[key] = cfg[key]
                elif key == 'data':
                    for data_item in item:
                        if data_item['type'] not in (XLINK_ANALYZER_DATA_TYPE, XQUEST_DATA_TYPE, SEQUENCES_DATA_TYPE, INI_FITS_DATA_TYPE): #these are not supported or are intrinsically multistate and taken from the first json
                            if data_item['type'] == PDB_FILES_DATA_TYPE:
                                for cfg in data_item['data']:
                                    for comp_cfg in cfg['components']:
                                        comp_cfg['filename'] = find_path(comp_cfg['filename'], cfg_filename=fn)
                            out['data'].append(data_item)

    return out

class Config(object):
    """Modeling-Software-agnostic class to represent a biological system.
        Parses JSON
        Creates Subunit objects
        Creates series
        Prepares data in an accessible way
        Provides utility functions for handling the system

    :param cfg_file_or_files: A JSON config file or multiple JSON files.
                              If multiple files are provided, each will define a new State.

    """
    def __init__(self, cfg_file_or_files=None, do_read_data=True, project_dir=None):
        self.in_cfg = None
        self.subunits = []
        self.symmetry_cfg = None
        self.series = []
        self.em_map = None
        self.rigid_bodies = []
        self.struct_files = []
        self.xlinksSetsMerged = None
        self.number_of_states = 1
        self.project_dir = project_dir
        self.do_read_data = do_read_data

        if isinstance(cfg_file_or_files, str):
            self.cfg_filenames = [cfg_file_or_files]
        else:
            self.cfg_filenames = cfg_file_or_files

        if self.cfg_filenames:
            if len(self.cfg_filenames) == 1:
                self.load_from_file(self.cfg_filenames[0])
            else:
                self.load_from_json(merge_jsons(self.cfg_filenames, project_dir=project_dir))

    def load_from_file(self, filename):
        self.load_from_json(read_json(filename))

    def load_from_json(self, json_cfg):
        self.in_cfg = json_cfg
        # pprint.pprint(self.in_cfg)
        # with open('temp.json', 'w') as f:
        #     pprint.pprint(self.in_cfg, f)
        if self.cfg_filenames:
            self.number_of_states = len(self.cfg_filenames)
        self.create_subunits()
        if self.symmetry_cfg is None:
            self.symmetry_cfg = self.in_cfg.get('symmetry')
        self.build_series()
        self.read_pdbfilenames()
        self.add_fits_from_dir()
        self.read_rigid_bodies()
        if self.do_read_data:
            self.read_data()

    def _validate_comp_cfg(self, comp_cfg):
        if comp_cfg.get('domain') is not None:
            subunit = self.get_subunit_by_name(comp_cfg['subunit'])
            if subunit and self.get_domain_by_name(subunit, comp_cfg.get('domain')) is None:
                raise AttributeError('A reference to domain "{0}" used but not defined'.format(comp_cfg.get('domain')))

    def _validate_cfg(self, cfg):
        for comp_cfg in cfg['components']:
            self._validate_comp_cfg(comp_cfg)

    def validate_serie_cfg(self, cfg):
        """Validate the specification of an individual serie
        """
        required_attributes = ('name', 'subunit')
        for attr in required_attributes:
            if attr not in cfg:
                raise InvalidSerie('Attribute {0} missing in series {1}'.format(attr, pprint.pformat(cfg, indent=4)))

        subunit = self.get_subunit_by_name(cfg['subunit'])
        if subunit is None:
            raise InvalidSerie("The subunit {0} for serie {1} is not specified in the subunits list".format(cfg['subunit'], cfg['name']))

        defaults = {
            'mode': 'input',
            'cell_count': 1,
            'inipos': 'input',
            'mode': 'input'
        }

        for key, val in defaults.items():
            if key not in cfg:
                cfg[key] = val
                print('Setting "{0}" for "{1}" attribute for serie {2}'.format(val, key, cfg['name']))

        # if (any condition for this?)
        #     raise InvalidSerie('chainIds of serie "{0}" for subunit "{1}" do not match chainIds of its subunit "{2}" ("{3}")'.format(cfg['name'], cfg['subunit'], subunit.name, ','.join(subunit.chain_ids)))

        if cfg.get('chainIds') and len(cfg['chainIds']) > 1 and len(cfg['chainIds']) != cfg['cell_count']:
            raise InvalidSerie("""The number of chainIds in the series {0} not equal to cell_count.
                chainIds of a serie must be one of the following:
                o a list of chainIds to be used for each copy of a subunit in this series
                o a list with a single chain ID for auto-generation of the other chain IDs
                o not provided (chainIds will be read from the corresponding subunit)""".format(cfg['name']))


        #TODO: check, in case tr3d name is provided for inipos, if the tr3d is defined
        #TODO: check, in case ref_serie name is provided, if the ref_serie is defined
        #TODO: handle states
            # if 'states' not in cfg:
            #     if 'state' in cfg:
            #         cfg['states'] = [cfg['state']]
            #     else:
            #         cfg['states'] = list(range(self.number_of_states))

    def create_subunits(self):
        """Create Subunits based on definitions in the self.in_cfg.

        Store subunits in self.subunits list.
        """
        self.subunits = []
        for subunit in self.in_cfg.get('subunits'):
            sub = Subunit(subunit['name'],
                          subunit['chainIds'],
                          subunit.get('color'),
                          subunit.get('domains'))

            self.subunits.append(sub)

    def build_series(self):
        """Build Series based on the "series" config or auto-build them based on Subunits.
        """
        self.series = []
        series = None

        if self.in_cfg:
            series = self.in_cfg.get('series')

        #Reading old style series
        if not series:
            if self.symmetry_cfg:
                series = self.symmetry_cfg.get('series')

        if series:
            #validate and set defaults
            for serie_ini_cfg in series:
                self.validate_serie_cfg(serie_ini_cfg)

            #build the series
            for serie_ini_cfg in series:
                subunit = self.get_subunit_by_name(serie_ini_cfg['subunit'])

                serie = Serie(
                        name = serie_ini_cfg['name'],
                        mode = serie_ini_cfg.get('mode'),
                        subunit = subunit,
                        chainIds = serie_ini_cfg.get('chainIds') or [],
                        cell_count = serie_ini_cfg['cell_count'],
                        tr3d = serie_ini_cfg.get('tr3d'),
                        inipos = serie_ini_cfg['inipos'],
                        ref_serie = serie_ini_cfg.get('ref_serie'),
                        states = serie_ini_cfg.get('states') or [0]
                    )
                """
                set up the chain ids
                Logic:
                o if len(serie.chainIds) == serie.cell_count, just use those chains for the copies
                o if not serie.chainIds - autogenerate based on subunit.chain_ids, taking care chain ids do not repeat
                o if len(serie.chainIds) < serie.cell_count - autogenerate the missing ones
                o chains between different states can repeat
                """
                if not serie.chainIds or len(serie.chainIds) != serie.cell_count: #else: if len(serie.chainIds) == serie.cell_count, just use those chains for the copies
                    prev_series_for_this_subunit = [s for s in self.get_series_by_subunit(subunit) if serie.states == s.states] 
                    if prev_series_for_this_subunit:
                        for prev_serie in prev_series_for_this_subunit:
                            if not serie.chainIds or serie.chainIds[0] == prev_serie.chainIds[0]:
                                serie.chain_extender = prev_series_for_this_subunit[0].chain_extender #doesn't matter from which serie the chain_extender is taken, they all point to the same instance
                                if set(serie.chainIds).issubset(prev_serie.chainIds):
                                    serie.chainIds = [] 

                        for i in range(0, serie.cell_count-len(serie.chainIds)):
                            serie.add_next_chain()
                    else:
                        if not serie.chainIds:
                            serie.chainIds = subunit.chain_ids[:]
                        if len(serie.chainIds) < serie.cell_count:
                            for i in range(0, serie.cell_count-len(serie.chainIds)):
                                serie.add_next_chain()


                subunit.series.append(serie)
                self.series.append(serie)

        #monomeric series
        for state_idx in range(self.number_of_states):
            for subunit in self.subunits:
                if subunit.name not in [s.subunit.name for s in self.series if state_idx in s.states]:
                    serie = Serie(
                            name = subunit.name,
                            mode = None,
                            subunit = subunit,
                            chainIds = subunit.chain_ids[:],
                            cell_count = 1,
                            tr3d = None,
                            inipos = 'input',
                            ref_serie = None,
                            states = [state_idx]
                        )

                    subunit.series.append(serie)
                    self.series.append(serie)

        #set chain IDs generated by the series in the Subunit objects:
        for serie in self.series:
            serie.subunit.chain_ids.extend(serie.chainIds)
        for subunit in self.subunits:
            subunit.chain_ids = sorted(set(subunit.chain_ids))

        print("Summary of created series of subunits:")
        print("There are {0} series for {1} subunits".format(len(self.series), len(self.subunits)))
        print("The series:")
        for serie in self.series:
            print("{0}: {1} molecules with chain IDs: {2}".format(serie, len(serie.chainIds), ','.join(serie.chainIds)))

        #TODO: check for chain id overlap
        for serie in self.series:
            for other_serie in self.series:
                if serie is not other_serie and serie.states == other_serie.states:
                    if len(set(serie.chainIds).intersection(set(other_serie.chainIds))) > 0:
                        raise Exception('Chains of serie "{0}" ({1}) and serie "{2}" ({3}) overlap'.format(serie, ','.join(serie.chainIds), other_serie, ','.join(other_serie.chainIds)))
                    if serie.name == other_serie.name and serie.subunit is other_serie.subunit:
                        raise InvalidSerie('Serie {0} and {1} have the same names and refer to the same subunit.\nIt is not allowed to have two series with the same name and referring to the same subunit.'.format(serie, other_serie))

    def add_auto_symmetries(self, use_constraints=True):
        if not self.symmetry_cfg:
            self.symmetry_cfg = {}
        if not self.symmetry_cfg.get('apply_symmetry'):
            self.symmetry_cfg['apply_symmetry'] = []
        for serie in self.series:
            for state_idx in serie.states:
                if use_constraints:
                    restraint_type = "symmetry_constraint"
                else:
                    restraint_type = "symmetry_restraint"
                if serie.tr3d:
                    cfg = {
                        "sym": serie.tr3d,
                        "restraint_type": restraint_type,
                        "selectors": [
                            {"subunit": serie.subunit.name, "state": state_idx, "serie": serie.name, "copies": [i]} for i in range(serie.cell_count)
                        ]
                    }
                    self.symmetry_cfg['apply_symmetry'].append(cfg)

                if serie.inipos != 'input':
                    for i in range(serie.cell_count):
                        cfg = {
                            "sym": serie.inipos,
                            "restraint_type": restraint_type,
                            "selectors": [
                                {"subunit": serie.subunit.name, "state": state_idx, "serie": serie.ref_serie, "copies": [i]},
                                {"subunit": serie.subunit.name, "state": state_idx, "serie": serie.name, "copies": [i]}
                            ]
                        }
                        self.symmetry_cfg['apply_symmetry'].append(cfg)

                        if serie.tr3d:
                            break #constrain only the first copy because other copies from this series will be constrained by serie.tr3d

    def read_pdbfilenames(self):
        for data_item in self.in_cfg.get('data'):
            if data_item['type'] == PDB_FILES_DATA_TYPE:
                for cfg in data_item['data']:
                    for comp_cfg in cfg['components']:
                        comp_cfg['filename'] = self.find_path(comp_cfg['filename'])

                new_cfgs = self._add_auto_states(data_item['data'])
                new_cfgs = self._add_auto_series(new_cfgs)
                new_cfgs_valid = []
                for c in new_cfgs:
                    for comp in c['components']:
                        if self.get_subunit_by_name(comp['subunit']) is None:
                            print(("WARNING: No subunit named " + comp['subunit'] + 'but was specified in pdb files'))
                            continue
                        else:
                            if c not in new_cfgs_valid:
                                new_cfgs_valid.append(c)

                new_cfgs = self._add_auto_copies(new_cfgs_valid)

                self._clean_cfgs(new_cfgs)

                for cfg in new_cfgs:
                    self._validate_cfg(cfg)

                self.struct_files.extend(new_cfgs)

    def read_rigid_bodies(self):
        if self.in_cfg.get('rigid_bodies'):

            for rb_cfg in self.in_cfg.get('rigid_bodies'):
                if rb_cfg.get('positions'):
                    rb_cfg['positions'] = self._read_positions(rb_cfg)

            rbs = self._add_auto_states(self.in_cfg.get('rigid_bodies'))
            # pprint.pprint(rbs)
            # print("----------")
            rbs = self._add_auto_series(rbs)
            rbs = self._add_auto_copies(rbs)

            self._clean_cfgs(rbs)

            names = []
            for cfg in rbs:
                self._validate_cfg(cfg)

                name = '_'.join(c['name'] for c in cfg['components'])
                prev_names = [x for x in names if x.rsplit('_',1)[0] == name]
                name = name + '_' + str(len(prev_names)+1)
                cfg['name'] = name
                names.append(name)
            self.rigid_bodies = rbs

    def read_data(self):
        self.data = []
        for data_item in self.in_cfg.get('data'):
            if data_item.get('active') in (True, None):
                self.data.append(data_item)

        for data_item in self.data:
            if data_item['type'] == XQUEST_DATA_TYPE:
                data_item['chain_to_xquest_name'] = {}
                for xquest_name, c_names in list(data_item['mapping'].items()):
                    for name in c_names:
                        if name in [s.name for s in self.subunits]:
                            for chain in self.get_subunit_by_name(name).chain_ids:
                                data_item['chain_to_xquest_name'][chain] = xquest_name
                        else:
                            print(("A subunit {0} present in xlink data not found in subunits config".format(name)))

                data_item['xquest_to_chains'] = {}
                for xquest_name, c_names in list(data_item['mapping'].items()):
                    for name in c_names:
                        if name in [s.name for s in self.subunits]:
                            data_item['xquest_to_chains'][xquest_name] = self.get_subunit_by_name(name).chain_ids
                        else:
                            print(("A subunit {0} present in xlink data not found in subunits config".format(name)))

            # if data_item['type'] in (CONNECTIVITY_DATA_TYPE,):
            #     this_data = data_item['data']
            #     new_comp1 = self._add_auto_series(this_data['comp1'])
            #     new_comp1 = self._add_auto_copies(new_comp1)
            #     self._clean_cfgs(new_comp1)
            #     print(new_comp1)
            #     this_data['comp1'] = new_comp1
            #     new_comp2 = self._add_auto_series(this_data['comp2'])
            #     new_comp2 = self._add_auto_copies(new_comp2)
            #     self._clean_cfgs(comp2)
            #     this_data['comp2'] = new_comp2
            #     print(new_comp2)

        self.load_xlink_data()
        self.read_fits()

    def load_xlink_data(self):
        """Load data from crosslink files.

        Calling this function will overwrite any previously loaded crosslinks.
        """
        xlink_data_cfgs = []
        for data_item in self.data:
            if data_item['type'] in (XQUEST_DATA_TYPE, XLINK_ANALYZER_DATA_TYPE):
                xlink_data_cfgs.append(data_item)

        xlink_data_sets = []
        for dataItem in xlink_data_cfgs:
            xlinkData = XlinkData(
                dataItem['name'],
                dataItem['type'],
                [self.find_path(path) for path in dataItem['fileGroup']["files"]],
                xlink_to_component_names=dataItem['mapping'])
            xlink_data_sets.append(xlinkData)

        if len(xlink_data_sets) > 0:
            self.xlinksSetsMerged = self.mergeXlinkSets(xlink_data_sets)

    def mergeXlinkSets(self, xlinkDataSets):
        '''Create merged pyxlinks.XlinksSet object from all where all names were renamed to component names.

        Can handle input XlinksSets with different prot names, and unify the names to component names.
        '''
        xlinkSetsCopies = []
        for xlinkDataSet in xlinkDataSets:
            xlinkSetsCopies.append(xlinkDataSet.all_xlinks_set.get_deep_copy())

        for xlinkSet, xlinkDataSet in zip(xlinkSetsCopies, xlinkDataSets):
            for xlink in xlinkSet.data:
                if xlink['Protein1'] != '-':
                    compName = xlinkDataSet.get_component_from_xlink_name(xlink['Protein1'])
                    if compName is not None:
                        xlink['Protein1'] = compName
                if xlink['Protein2'] != '-':
                    compName = xlinkDataSet.get_component_from_xlink_name(xlink['Protein2'])
                    if compName is not None:
                        xlink['Protein2'] = compName

        xlinksSetsMerged = sum(xlinkSetsCopies)

        return xlinksSetsMerged

    def find_path(self, path):
        """Try to guess if path is absolute or relative and look in the current dir
        """
        if os.path.exists(path) or os.path.isabs(path):
            return path
        else:
            if self.project_dir:
                abs_path = os.path.join(self.project_dir, path)
                if os.path.exists(abs_path):
                    return abs_path

            relative_to_current = os.path.join(os.getcwd(), path)
            if os.path.exists(relative_to_current):
                return relative_to_current

            elif self.cfg_filenames:
                for fn in self.cfg_filenames:
                    cfg_dirname = os.path.dirname(os.path.realpath(fn))
                    relative_to_config_file = os.path.join(cfg_dirname, path)
                    if os.path.exists(relative_to_config_file):
                        return relative_to_config_file

        return path

    def get_pdbfilenames(self, subunit_name):
        out = []
        for data_item in self.in_cfg.get('data'):
            if data_item['type'] == PDB_FILES_DATA_TYPE:
                for cfg in data_item['data']:
                    for comp_cfg in cfg['components']:
                        if comp_cfg['subunit'] == subunit_name:
                            filename = self.find_path(comp_cfg['filename'])
                            if filename not in out:
                                out.append(filename)

        return out

    def get_path_for_pdb_filename(self, fn):
        for data_item in self.in_cfg.get('data'):
            if data_item['type'] == PDB_FILES_DATA_TYPE:
                for cfg in data_item['data']:
                    for comp_cfg in cfg['components']:
                        if fn == os.path.basename(comp_cfg['filename']):
                            return self.find_path(comp_cfg['filename'])

    def change_pdb_dir(self, pdb_dir):
        for cfg in self.struct_files:
            for comp_cfg in cfg['components']:
                comp_cfg['filename'] = os.path.join(pdb_dir, os.path.basename(comp_cfg['filename']))

    def add_fits_from_dir(self):
        for data_item in self.in_cfg.get('data'):
            if data_item['type'] in (ALTERNATIVE_POSITIONS_DATA_TYPE,):
                if 'add_from_dir' in data_item:
                    for pdb_cfg in self.struct_files:
                        if 'freeze' in pdb_cfg or 'positions' in pdb_cfg:
                            continue
                        pdb_fn = pdb_cfg['components'][0]['filename']
                        positions_fn = self.find_path(os.path.join(
                            data_item['add_from_dir'],
                            os.path.basename(pdb_fn),
                            'solutions_pvalues.csv'
                            ))

                        if not os.path.exists(positions_fn):
                            positions_fn = self.find_path(os.path.join(
                            data_item['add_from_dir'],
                            os.path.basename(pdb_fn),
                            'solutions.csv'
                            ))
                        pdb_cfg['positions'] = positions_fn
                        pdb_cfg['positions_type'] = data_item.get('positions_type')
                        pdb_cfg['max_positions'] = data_item.get('max')
                        pdb_cfg['positions_score'] = data_item.get('score') or 'log_BH_adjusted_pvalues_one_tailed'


    def read_fits(self):
        '''Deprecated, kept for backward compatibility'''
        self.ini_fits = defaultdict(list)

        for data_item in self.data:
            if data_item['type'] in (INI_FITS_DATA_TYPE,):

                for name, cfg in data_item['fits'].items():
                    solutions_filenames = cfg['filenames']
                    pdb_filenames = self.get_pdbfilenames(name)
                    # if len(pdb_filenames) != len(solutions_filenames):
                    #     print "WARN"
                        #raise MustProvideAsManyFitsAsPDBfiles

                    maxfits_cfg = cfg.get('max')
                    #check if maxfit_cfg is int or list of ints
                    try:
                        int(maxfits_cfg)
                    except TypeError:
                        list(map(int, maxfits_cfg))
                    else:
                        maxfits_cfg = [maxfits_cfg] * len(solutions_filenames)

                    for input_pdb_filename, solutions_filename, maxfits in zip(pdb_filenames, solutions_filenames, maxfits_cfg):
                        print(input_pdb_filename, solutions_filename, maxfits)
                        if solutions_filename is None:
                            tr3ds = []
                        else:
                            solutions_filename = self.find_path(solutions_filename)
                            if cfg['type'] == 'colores':
                                tr3ds = read_solutions_colores(input_pdb_filename, solutions_filename)
                            elif cfg['type'] == 'chimera':
                                if 'score' in cfg:
                                    score = cfg['score']
                                else:
                                    score = 'log_BH_adjusted_pvalues_one_tailed'
                                tr3ds = read_solutions_chimera(solutions_filename, maxfits=maxfits, score=score)
                                if len(tr3ds) == 0:
                                    print("WARNING: No fitting solutions read from "+solutions_filename)
                            else:
                                tr3ds = []
                        self.ini_fits[name].append(tr3ds)

    def _add_auto_states(self, cfgs):
        out = []
        for cfg in cfgs:
            if cfg.get('foreach_state') == True:
                for state_idx in range(self.number_of_states):
                    new_cfg = deepcopy(cfg)
                    for comp_cfg in new_cfg['components']:
                        if 'state' in comp_cfg or 'states' in comp_cfg:
                            raise AttributeError('config cannot contain "state" or "states" if "foreach_state is True')
                        comp_cfg['states'] = [state_idx]
                    out.append(new_cfg)
            else:
                for comp_cfg in cfg['components']:
                    if 'states' not in comp_cfg:
                        if 'state' in comp_cfg:
                            comp_cfg['states'] = [comp_cfg['state']]
                        else:
                            comp_cfg['states'] = list(range(self.number_of_states))

                out.append(cfg)

        return out

    def _add_auto_series(self, cfgs):
        out = []
        for cfg in cfgs:
            if cfg.get('foreach_serie') == True:
                added = []
                for serie in self.series:
                    if serie.subunit.name in [comp_cfg['subunit'] for comp_cfg in cfg['components']] and serie.name not in added and all([serie.states == comp_cfg['states'] for comp_cfg in cfg['components']]):
                        new_cfg = deepcopy(cfg)
                        for comp_cfg in new_cfg['components']:
                            if 'serie' in comp_cfg:
                                raise AttributeError('config cannot contain "serie" if "foreach_serie is True')

                            # new_cfg['components'].append(deepcopy(comp_cfg))
                            # new_cfg['components'][-1]['serie'] = serie.name
                            # if comp_cfg['subunit'] == serie.subunit.name:
                            comp_cfg['serie'] = serie.name
                            added.append(serie.name)

                        out.append(new_cfg)
            else:
                out.append(cfg)

        return out

    def _add_auto_copies(self, cfgs):
        out = []
        for cfg in cfgs:
            if cfg.get('foreach_copy') == True:
                lens = []
                for comp_cfg in cfg['components']:
                    if 'copies' in comp_cfg:
                        raise AttributeError('config cannot contain "copies" if "foreach_copy" is True')

                    serie = self.get_serie_by_subunit_and_name(self.get_subunit_by_name(comp_cfg['subunit']), comp_cfg['serie'])
                    if serie:
                        lens.append(serie.cell_count)
                if not lens:
                    print('Could not find any matching series for components in '+pprint.pformat(cfg, indent=4))

                max_serie_len = max(lens)
                i = 0
                while i < max_serie_len:
                    new_cfg = deepcopy(cfg)
                    for comp_cfg in new_cfg['components']:
                        comp_cfg['copies'] = [i]


                    out.append(new_cfg)
                    i = i + 1
            else:
                out.append(cfg)

        return out

    def _clean_cfgs(self, cfgs):
        for cfg in cfgs:
            if cfg.get('foreach_copy'):
                del cfg['foreach_copy']
            if cfg.get('foreach_serie'):
                del cfg['foreach_serie']
            if cfg.get('foreach_state'):
                del cfg['foreach_state'] 

    def _read_positions(self, selector):
        '''Read positions to sample of the rigid body derived from this cfg, e.g. from systematic fitting
        '''
        if selector.get('positions') and self.do_read_data:
            positions_filename = self.find_path(selector.get('positions'))
            max_positions = selector.get('max_positions')
            positions_type = selector.get('positions_type') or 'chimera'

            print("Reading positions from {0} for {1}".format(positions_filename, selector))
            if positions_type == 'colores':
                tr3ds = read_solutions_colores(input_pdb_filename, positions_filename)
            elif positions_type == 'chimera':
                if 'positions_score' in selector:
                    score = selector['positions_score']
                else:
                    score = 'log_BH_adjusted_pvalues_one_tailed'
                tr3ds = read_solutions_chimera(positions_filename, maxfits=max_positions, score=score)
                if len(tr3ds) == 0:
                    print("WARNING: No fitting solutions read from "+positions_filename)
            else:
                print("WARNING: No fitting solutions read from "+positions_filename)
                tr3ds = []

            return tr3ds    

    def define_rbs_from_pdbs(self):
        if len(self.rigid_bodies) > 0:
            raise Exception('Cannot define rigid bodies from PDBs because rigid bodies have been already defined')

        added_rb_cfgs = {}
        for pdb_cfg in self.struct_files:
            if pdb_cfg.get('positions'):
                if len(set(comp['filename'] for comp in pdb_cfg['components'])) > 1:
                    print('PDB specification for rigid body {0} contains different PDB filenames AND positions file. Sure you want it?'.format(pdb_cfg))                

                pdb_cfg['positions'] = self._read_positions(pdb_cfg)
                
            rb_cfg = deepcopy(pdb_cfg)
            rb_pdbfilenames = []
            for comp_cfg in pdb_cfg['components']:
                pdb_filename = os.path.basename(comp_cfg['filename'])
                if pdb_filename not in rb_pdbfilenames:
                    rb_pdbfilenames.append(pdb_filename)

            if not 'name' in pdb_cfg:
                rb_cfg['name'] = '_'.join([os.path.splitext(s)[0] for s in rb_pdbfilenames])
            prev_names = [x for x in added_rb_cfgs.keys() if x.rsplit('_',1)[0] == rb_cfg['name']]
            rb_cfg['name'] = rb_cfg['name'] + '_' + str(len(prev_names)+1)
            added_rb_cfgs[rb_cfg['name']] = rb_cfg

            self.rigid_bodies.append(rb_cfg)

    def get_subunit_by_name(self, name):
        for subunit in self.subunits:
            if subunit.name == name:
                return subunit

    def get_subunit_by_chain(self, chain_id):
        for subunit in self.subunits:
            if chain_id in subunit.chain_ids:
                return subunit

    def get_domain_by_name(self, subunit_obj, domain_name):
        for subunit in self.subunits:
            if subunit.name == subunit_obj.name:
                for domain in subunit.domains:
                    if domain['name'] == domain_name:
                        return domain

    def get_serie_by_pmi_mol(self, pmi_mol):
        for serie in self.series:
            for state_molecules in serie.molecules:
                if pmi_mol in state_molecules:
                    return serie

    def get_copy_in_serie_by_pmi_mol(self, pmi_mol):
        for serie in self.series:
            for i, state_molecules in enumerate(serie.molecules):
                if pmi_mol in state_molecules:
                    return i

    def get_subunit_by_pmi_mol(self, pmi_mol):
        serie = self.get_serie_by_pmi_mol(pmi_mol)

        return serie.subunit

    def get_series_by_name(self, name):
        out = []
        for serie in self.series:
            if serie.name == name:
                out.append(serie)

        return out

    def get_serie_by_subunit_and_name(self, subunit, name):
        for serie in self.series:
            if serie.name == name and serie.subunit is subunit:
                return serie

    def get_series_by_subunit(self, subunit):
        out = []
        for serie in self.series:
            if serie.subunit is subunit:
                out.append(serie)

        return out

    def get_serie_by_subunit_and_chain(self, subunit, chain_id):
        for serie in self.series:
            if serie.subunit is subunit and chain_id in serie.chainIds:
                return serie


    def get_seq(self, component):
        for data_item in self.data:
            if data_item['type'] == SEQUENCES_DATA_TYPE:
                for path in data_item['fileGroup']['files']:
                    filename = self.find_path(path)
                    with open(filename) as f:
                        for seq_name, seq in fasta_iter(f):
                            if seq_name in data_item['mapping']:
                                if component in data_item['mapping'][seq_name]:
                                    return str(seq)

    def get_accession(self, component):
        #try to get from sequences
        for data_item in self.data:
            if data_item['type'] == SEQUENCES_DATA_TYPE:
                for path in data_item['fileGroup']['files']:
                    filename = self.find_path(path)
                    with open(filename) as f:
                        for seq_name, seq in fasta_iter(f):
                            if seq_name in data_item['mapping']:
                                if component in data_item['mapping'][seq_name]:
                                    is_uniprot = re.match(r'..\|(.+)\|(.+?)[\s$]', seq_name)
                                    return {'db_name': 'UniProt', 'accession': is_uniprot.groups()[1], 'db_code': is_uniprot.groups()[1]}

        #TODO: get from subunit annotations? E.g. XlinkAnalyzer would add uniprot's there

    def get_domain_resi_indexes(self, subunit, domain):
        domain = self.get_domain_by_name(subunit, domain)
        resi_ids = []
        for domrange in domain['ranges']:
            try:
                resi_ids.extend(list(range(domrange[0], domrange[1]+1))) #when domrange is (start, end) tuple
            except TypeError:
                resi_ids.append(domrange) #when domrange is int

        return resi_ids

    def get_resi_ids_from_comp_cfg(self, comp_cfg):
        chain_id = comp_cfg.get('chain_id')
        if chain_id is None:
            subunit = self.get_subunit_by_name(comp_cfg['subunit'])
            if subunit is not None:
                chain_id = subunit.chain_ids[0]

        resi_ids = []
        if 'filename' in comp_cfg:
            with open(self.find_path(comp_cfg['filename'])) as f:
                resi_ids = [x[1] for x in get_residue_ids(f, chain_id=chain_id)]
        elif 'domain' in comp_cfg:
            resi_ids = self.get_domain_resi_indexes(subunit, comp_cfg['domain'])
        else:
            sequence = self.get_seq(comp_cfg['subunit'])
            resi_ids = list(range(1, len(sequence)+1))

        if 'resi_ranges' in comp_cfg:
            resi_ids_from_ranges = []
            for r_range in comp_cfg['resi_ranges']:
                try:
                    resi_ids_from_ranges.extend(list(range(r_range[0], r_range[1]+1)))  #when range is (start, end) tuple
                except TypeError:
                    resi_ids_from_ranges.append(r_range) #when range is int

            if not resi_ids:
                resi_ids = resi_ids_from_ranges
            else:
                resi_ids = [r for r in resi_ids if r in resi_ids_from_ranges]

        return resi_ids

    def get_nice_name_from_comp_cfg(self, comp_cfg):
        if 'name' in comp_cfg:
            return comp_cfg['name']
        else:
            keys = ['subunit', 'domain', 'serie', 'copies', 'resi_ranges']
            elements = []
            for key in keys:
                if key in comp_cfg:
                    if key == 'copies':
                        element = f'copies{",".join(map(str, comp_cfg[key]))}'
                    elif key == 'resi_ranges':
                        element = f'resi_ranges{comp_cfg[key]}'
                    else:
                        element = comp_cfg[key]

                    elements.append(element)

            return '_'.join(elements)

    def is_selector_subset_of_another(self, cfg1, cfg2, fields=('subunit', 'state', 'states', 'domain', 'serie', 'copies', 'chain_id', 'chainIds', 'filename', 'resi_ranges')):
        """Determine if cfg1 is a subset of the cfg2?
        """
        filtered1 = dict((k, v) for k,v in cfg1.items() if k in fields)
        filtered2 = dict((k, v) for k,v in cfg2.items() if k in fields)

        if filtered1 == filtered2:
            return True

        states1 = filtered1.get('states') or [filtered1.get('state')]
        states2 = filtered2.get('states') or [filtered2.get('state')]
        if states1 and states2:
            if states1 != [None] and states2 != [None] and not set(states1).issubset(set(states2)):
                return False
            if states1 == [None] and states2 != [None]:
                return False

        if 'subunit' in filtered1 and 'subunit' in filtered2:
            if filtered1['subunit'] != filtered2['subunit']:
                return False

        if 'serie' in filtered1 and 'serie' in filtered2:
            if filtered1['serie'] != filtered2['serie']:
                return False

        if 'copies' in filtered1 and 'copies' in filtered2:
            if not set(filtered1['copies']).issubset(set(filtered2['copies'])):
                return False

        chain_ids1 = self.get_chain_ids_for_selector(filtered1)
        chain_ids2 = self.get_chain_ids_for_selector(filtered2)
        if chain_ids1 and chain_ids2:
            if not set(chain_ids1).issubset(set(chain_ids2)):
                return False

        resi_ids1 = self.get_resi_ids_from_comp_cfg(filtered1)
        resi_ids2 = self.get_resi_ids_from_comp_cfg(filtered2)

        if not set(resi_ids1).issubset(set(resi_ids2)):
            return False

        return True

    def do_selectors_overlap(self, cfg1, cfg2, fields=('subunit', 'state', 'states', 'domain', 'serie', 'copies', 'chain_id', 'chainIds', 'filename', 'resi_ranges')):
        """Determine if cfg1 has common set of residues with the cfg2
        """
        filtered1 = dict((k, v) for k,v in cfg1.items() if k in fields)
        filtered2 = dict((k, v) for k,v in cfg2.items() if k in fields)

        if filtered1 == filtered2:
            return True

        states1 = filtered1.get('states') or [filtered1.get('state')]
        states2 = filtered2.get('states') or [filtered2.get('state')]
        if states1 and states2:
            if states1 != [None] and states2 != [None] and not set(states1).intersection(set(states2)):
                return False

        if 'subunit' in filtered1 and 'subunit' in filtered2:
            if filtered1['subunit'] != filtered2['subunit']:
                return False

        if 'serie' in filtered1 and 'serie' in filtered2:
            if filtered1['serie'] != filtered2['serie']:
                return False

        if 'copies' in filtered1 and 'copies' in filtered2:
            if not set(filtered1['copies']).intersection(set(filtered2['copies'])):
                return False

        chain_ids1 = self.get_chain_ids_for_selector(filtered1)
        chain_ids2 = self.get_chain_ids_for_selector(filtered2)
        if chain_ids1 and chain_ids2:
            if not set(chain_ids1).intersection(set(chain_ids2)):
                return False

        resi_ids1 = self.get_resi_ids_from_comp_cfg(filtered1)
        resi_ids2 = self.get_resi_ids_from_comp_cfg(filtered2)

        if set(resi_ids1).intersection(set(resi_ids2)):
            return True

        return False

    def get_chain_ids_for_selector(self, selector):
        chain_ids = selector.get('chainIds') or [selector.get('chain_id')]
        if chain_ids != [None]:
            return set(chain_ids)
        else:
            subunit = None
            series = None
            copies = None
            if 'subunit' in selector:
                subunit = self.get_subunit_by_name(selector['subunit'])

            if 'serie' in selector:
                series = self.get_series_by_name(selector['serie'])
            if not subunit and series:
                subunit = serie.subunit

            all_chain_id_sets = []
            if subunit:
                all_chain_id_sets.append(set(subunit.chain_ids))

            if series:
                all_series_chains = []
                for serie in series:
                    all_series_chains.extend(serie.chainIds)
                
                all_chain_id_sets.append(set(all_series_chains))

            if 'copies' in selector and series:
                chains_for_copies = []
                for i in selector['copies']:
                    for serie in series:
                        chains_for_copies.append(serie.chainIds[i])
                all_chain_id_sets.append(set(chains_for_copies))
            # print('>', all_chain_id_sets)
            return set.intersection(*all_chain_id_sets)



    def is_selector_symmetry_constrained_or_restrained(self, selector, sym_types=['symmetry_constraint', 'symmetry_restraint']):

        if 'apply_symmetry' not in self.symmetry_cfg:
            return False

        if selector.get('copies') and selector.get('copies') == [0]:
            return False #Assuming copy 0 cannot be symmetry constrained

        #TODO: check if selector is not constrained to ref_serie!!

        #remove keys of the copies
        selector = deepcopy(selector)
        selector.pop('copies', None)
        selector.pop('chain_id', None)
        selector.pop('chainIds', None)
        selector.pop('filename', None)

        for restr in self.symmetry_cfg['apply_symmetry']:
            if restr['restraint_type'] in sym_types:
                for i, other_selector in enumerate(restr['selectors']):
                    if self.do_selectors_overlap(selector, other_selector):
                        return True 

    def is_selector_symmetry_constrained(self, selector):
        return self.is_selector_symmetry_constrained_or_restrained(selector, sym_types=['symmetry_constraint'])

    def is_selector_symmetry_restrained(self, selector):
        return self.is_selector_symmetry_constrained_or_restrained(selector, sym_types=['symmetry_restraint'])

    def is_symmetry_constrained(self, obj_cfg):
        are_constrained = []
        for selector in obj_cfg['components']:
            are_constrained.append(self.is_selector_symmetry_constrained(selector))

        return all(are_constrained)

class MustProvideAsManyFitsAsPDBfiles(Exception):
    pass


def read_solutions_colores(input_pdb_filename, solutions_filename):
    """This is not tested, do not use
    """
    raise NotImplementedError
    import IMP
    import IMP.algebra
    import situs_utils

    test = False
    tempdir = tempfile.mkdtemp()
    if test:
        print(tempdir)

    out = []
    try:
        ori_coords = situs_utils.read_coords(input_pdb_filename)
        ori_center = situs_utils.calc_center(ori_coords)
        #colores first moves fit component to center of coord system
        ini_rot = [[1,0,0],[0,1,0],[0,0,1]]
        ini_trans = [-x for x in ori_center]

        ini_rot3d = IMP.algebra.get_rotation_from_matrix(ini_rot[0][0], ini_rot[0][1], ini_rot[0][2],
                                                             ini_rot[1][0], ini_rot[1][1], ini_rot[1][2],
                                                            ini_rot[2][0], ini_rot[2][1], ini_rot[2][2])
        ini_trans3d = IMP.algebra.Vector3D(*ini_trans)
        ini_tr3d= IMP.algebra.Transformation3D(ini_rot3d, ini_trans3d)

        if test:
            situs_utils.write_pdb_solutions(input_pdb_filename, solutions_filename, out_dir=tempdir, n=1)

        i = 0
        for sol in situs_utils.iter_solutions(solutions_filename):
            sol_rot = transform_pdb.euler_to_matrix(*sol['euler_rot'], in_degrees=True)
            sol_trans = sol['center']

            # sol_rot3d = IMP.algebra.get_rotation_from_matrix(sol_rot[0][0], sol_rot[0][1], sol_rot[0][2],
            #                                                  sol_rot[1][0], sol_rot[1][1], sol_rot[1][2],
            #                                                 sol_rot[2][0], sol_rot[2][1], sol_rot[2][2])
            sol_rot3d = IMP.algebra.get_rotation_from_matrix(sol_rot[0][0], sol_rot[1][0], sol_rot[2][0],
                                                             sol_rot[0][1], sol_rot[1][1], sol_rot[2][1],
                                                            sol_rot[0][2], sol_rot[1][2], sol_rot[2][2])

            sol_trans3d = IMP.algebra.Vector3D(*sol_trans)
            sol_tr3d= IMP.algebra.Transformation3D(sol_rot3d, sol_trans3d)
            
            comb_tr3d = sol_tr3d * ini_tr3d
            out.append(comb_tr3d)

            if test:
                write_transformed(input_pdb_filename, os.path.join(tempdir, 'trans'+str(i)+'.pdb'), tr3d=comb_tr3d, out_c_id=None)
                break
            i = i + 1

    finally:
        if not test:
            shutil.rmtree(tempdir)

    return out


def read_solutions_chimera(solutions_filename, maxfits=None, score='score_z_c'):
    out = []
    if score in ('overlap', 'cc_score', 'cam_score', 'score_z_c', 'score_z'):
        def get_score(row):
            return -float(row[score]) #inverse sign to make the lower the better
    elif score in ('BH_adjusted_pvalues', 'BH_adjusted_pvalues_one_tailed', 'pvalues'):
        def get_score(row):
            return float(row[score])
    elif score in ('log_BH_adjusted_pvalues', 'log_BH_adjusted_pvalues_one_tailed', 'log_pvalues_one_tailed'):
        def get_score(row):
            return math.log10(float(row[score.lstrip('log_')]))

    with open(solutions_filename, 'rU') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.readline(), ['|',','])
        csvfile.seek(0)

        solutions = csv.DictReader(csvfile, dialect=dialect)

        for i, row in enumerate(solutions):
            if maxfits is not None and i == maxfits:
                break

            out.append(
                {   
                    'score': get_score(row), 
                    'rotation': list(map(float, row['rotation'].split())),
                    'translation': list(map(float, row['translation'].split()))
                }
                )


    return out


def convert(_input):
    """Convert json to a Python object.
    """
    if isinstance(_input, dict):
        return dict([(convert(key), convert(value)) for key, value in list(_input.items())])
    elif isinstance(_input, list):
        return [convert(element) for element in _input]
    elif isinstance(_input, bytes):
        return _input.decode('utf-8')
    elif isinstance(_input, str):
        return _input
    else:
        return _input


def read_json(filename):
    """Read a json file, removing comments (in "//" and "/*  */" formats).

    JSON format in general doesn't support comments.
    We accept comments in our JSON files, but they need to be removed before reading,
    using 
    """
    with open(filename) as f:
        data = convert(json.loads(minify_json.json_minify(f.read())))
    return data


def are_selectors_equal(cfg1, cfg2, fields=('subunit', 'state', 'states', 'domain', 'serie', 'copies', 'chain_id', 'chainIds', 'filename', 'resi_ranges')):
    """Are the two config selectors equal?
    """
    filtered1 = dict((k, v) for k,v in cfg1.items() if k in fields)
    filtered2 = dict((k, v) for k,v in cfg2.items() if k in fields)

    return filtered1 == filtered2

def get_resi_ranges_as_resi_ids(resi_ranges):
    out = []
    for rrange in resi_ranges:
        out.extend(list(range(rrange[0], rrange[1]+1)))

    return out