Run
===

#. Run the fitting
   
    .. code-block:: bash

        fit.py efitter_params.py 

    The fitting runs will execute in the background and take from a few dozens of minutes to several hours depending on the case.

    In the output, you should have got the following directory structure: ::

        master_outdir/ # Directory specified with "master_outdir" parameter in params.py
            config_prefix1/ # named after config_prefix specifications in fitmap_args in params.py
                map1.mrc/   # named after the filenames of the EM maps used
                    map1.mrc  # symbolic link to the reference map
                    pdb_file_name1.pdb/     # named after the pdb file names used for fitting
                        solutions.csv   # the list of solutions and their scores
                        log_err.txt     # standard error log
                        log_out.txt     # standeard output log
                        run.sh          # sbatch script used for running the job
                        ori_pdb.pdb     # symbolic link to the original query file
                        map1.mrc        # symbolic link to the reference map
                    pdb_file_name2.pdb/
                    pdb_file_name3.pdb/
                    config.txt          # A config file for fitting, saved FYI.
                map2.mrc/
            config_prefix2/
            config_prefix3/

    .. note:: The fitting is complete when each of the ``.pdb`` directories contains ``solutions.csv`` file.

        Inspect the ``log_out.txt`` files for status and ``log_err.txt`` for error messages.


#. Upon completion, calculate p-values:
   

    .. code-block:: bash

        genpval.py <fitting directory/master_outdir>

    This should create additional files in each ``.pdb`` directory::

        Rplots.pdf
        solutions_pvalues.csv

    The ``solutions_pvalues.csv`` is crucial for the global optimization step.