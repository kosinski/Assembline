2. Recombinations
=================

#. Enter the output directory of the global optimization run

#. Prepare a JSON file for recombination

    .. code-block:: bash

        setup_recombination.py \
            --json <the JSON file that was used for the global optimization run> \
            --scores all_scores_uniq.csv \
            -o <output directory for the new fit libraries> \
            --json_outfile <desired name for JSON config for recombinations> \
            --project_dir <original project dir> \
            --score_thresh <score threshold for selecting the models> \
            --top <number of top scoring models to use for extracting the fit libraries>

    You can use either ``--score_thresh`` or ``--top or both``.

    This command will create a file specified in ``json_outfile``, which is the input JSON for recombination.

#. Enter the original project directory

#. Run using the same ``params.py`` as for the previous global optimization run.
   
   Optionally, you can reduce the number of Simulated Annealing steps, as the number of the fits in the libraries is likely lower,
   and the optimization converges faster.

   Run in the same way as the :doc:`combinations`, but now pointing to the new JSON generated above and outputing to a subdirectory, 
   and adding ``--prefix`` option to distinguish the recombined models from the original ones as in the example below:

   .. code-block:: bash
   
       assembline.py \
            --traj \
            --models \
            -o out \
            --multi \
            --start_idx 0 \
            --njobs 1000 \
            --prefix recomb \
            config_recomb.json params.py &>log&

   This will output to the same directory, so all models can be analyzed together.