Extract scores
==============

Output all scores (and scoring terms) of the produced models

To get models with the best scores and all scoring terms reported run the following (obligatory step for downstream analysis with imp-sampcon):

.. code-block:: bash

    extract_scores.py

or 

.. code-block:: bash

    extract_scores.py --multi

for :doc:`refinement` of multiple models.

It will create a couple of files:

.. code-block:: bash

    total_score_logs.txt (a list of scores with total scores in trajectories, only if print_total_score_to_files was set to True in params file
    all_scores_sorted_uniq.txt (txt file with models and sorted non-redundant total scores)
    all_scores_sorted.txt (txt file with models and sorted total scores)
    all_scores.txt (txt file with models and total scores)
    all_scores_uniq.csv (CSV file with sorted non-redundant total scores)
    all_scores_sorted_uniq.csv (CSV file with models and sorted non-redundant total scores)
    all_scores_sorted.csv (CSV file with models and sorted total scores)
    all_scores.csv (CSV file with models and total scores)

These files are used as input for other scripts. You can also use them for your own statistics, filter them before running the scripts.


Plot histograms of all scores
-----------------------------

.. code-block:: bash

    plot_scores.R all_scores.csv