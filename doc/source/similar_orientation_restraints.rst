Similar orientation restraints
==============================

These restraints promote similar orientation between pairs of identical subunits, domains or residues.

They can be defined in the :doc:`json` like this:

.. code-block:: json

    {
        "type": "similar_orientation",
        "active": true,
        "name": "Ely5-Nup120_similar_orientation",
        "comment": "We assume Ely5 interacts in the same way with all copies of Nup120",
        "data": [
            [
                [
                    {"subunit": "Nup120", "serie": "NR_1", "copies": [0]}, {"subunit": "Ely5", "serie": "NR_1", "copies": [0]}
                ],
                [
                    {"subunit": "Nup120", "serie": "NR_2", "copies": [0]}, {"subunit": "Ely5", "serie": "NR_2", "copies": [0]}
                ]
            ]
        ],
        "weight": 10,
        "k": 1,
        "first_copy_only": true,
        "repr_resolution": 10
    }



**Parameters**

type
    Must be "similar_orientation"
active
    true or false, whether the restraint should be created, you can set to false to save on loading time if you don't need this restraint
    (even if it's true, you still need to add it to a scoring function to have it an effect)
name
    whatever name. Use this name to refer to this restraint in :doc:`params` when defining the scoring functions
comment
    Optional comment, useful for documenting
data
    A **pair of pairs** of :doc:`selectors` between which the interaction restraints should be defined
weight
    Weight of this restraint
first_copy_only
    true or false, Apply only to the first molecule copy of each series?
repr_resolution
    The resolution of representation to which this restraint should be applied
k
    Spring constant for the harmonic potential, redundant to weight, ignore
