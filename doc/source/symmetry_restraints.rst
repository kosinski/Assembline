Symmetry restraints and constraints
===================================

Symmetry can be restrained by either **constraints** or **restraints**.

**Constraint** enforces exact symmetry, with no deviations, while **restraints** only promote symmetry with strength determined by restraint weight.

Follow instructions:

    * :doc:`symmetry_constraints`
      
    * :doc:`symmetry_restraints_intro`