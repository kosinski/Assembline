About fit libraries
===================

Fit libraries are lists of possible positions (non-redundant fits) of your input structures in the EM map.
The positions are used in the :doc:`combinations` step to generate a large number of combinations of the fits
through a Monte Carlo integrative modelling procedure. Each position (fit) has its associated score and p-value,
which are used as EM restraints during the modelling.

The fit libraries are typically generated by fitting every input structure into the EM map separately, a procedure handled by the ``Efitter`` pipeline of Assembline.

.. note:: This part of the Assembline pipeline (``efitter``) will generate all requested libraries per input structure in a single run (automated) according to user's predefined parameters (see :doc:`efitter_params`, and following figure).

.. image:: images/assembline_efitter_graph.png
  :width: 1100
  :alt: Efitter pipeline for fit libraries