System requirements
===================

Currently only UNIX-based systems supported. Should work both on Linux and OS X.

We recommend using a computer cluster but Assembline can be also run on a standalone workstation.

.. warning :: Some scripts might not work on OS X. All tests were performed with bash (i.e. not tested in other shells).

Installation with Anaconda
==========================

Installation should take less than 1 hour.

Install dependencies
--------------------

#. UCSF Chimera 1.14 (https://www.cgl.ucsf.edu/chimera/download.html)
   
   ``chimera`` command must be available in your command line.

#. Xlink Analyzer plugin to Chimera, version 1.1 and higher (https://www.embl-hamburg.de/XlinkAnalyzer/XlinkAnalyzer.html)

    It is only needed on your local workstation for input preparation and analysis.

Install Anaconda
----------------

Anaconda is a free and open-source distribution of the Python and R programming languages for scientific
computing, which aims to simplify package management and deployment. 

We will use Anaconda to create a "virtual environment" in which Assembline will be installed.

Download and install version for Python 3 from https://www.anaconda.com/distribution/

Create virtual Anaconda environment
-----------------------------------

.. code-block:: bash

    conda create --name Assembline python=3.8

Install Python dependencies
---------------------------

#. Activate ("enter") the environment:

    .. code-block:: bash

        source activate Assembline

    or depending on your computer setup:

    .. code-block:: bash
    
        conda activate Assembline

#. Install `Integrative Modeling Platform (IMP) <https://integrativemodeling.org/>`_ version at least 2.14::

        conda config --add channels conda-forge
        conda install -y imp=2.16.0

#. Install other dependencies: ::

        conda install -y scipy numpy scikit-learn matplotlib pandas
        conda install -y -c salilab pyrmsd
        conda install -y -c conda-forge hdbscan

#. [optional] Install Modeller. Only needed for building full atom representation of loops modelled as flexible beads

    .. code-block:: bash

        conda install -y -c salilab modeller
    
    Follow the displayed instruction to edit ``lib/modeller-9.25/modlib/modeller/config.py`` inserting your Modeller license key

Install Gnuplot
---------------

#. [optional] Install Gnuplot. Only needed for analysis steps.
    .. code-block:: bash

        conda install -y -c conda-forge gnuplot

Install R
---------

The easiest is to install R in your Anaconda environment::

    conda install -y r-base r-fdrtool r-psych r-ggplot2 r-tidyr r-data.table

If for any reason you want to install R separately, install R from https://www.r-project.org/ and make sure: 
   
    * ``Rscript`` is available in your command line.

    * The following R packages are installed:

        * fdrtool
    
        * psych
          
        * ggplot2
          
        * tidyr
          
        * data.table


Install Assembline
------------------

.. code-block:: bash

    conda install -y -c kosinskilab assembline

For your information - this will also install some external dependencies:

* PDBX https://github.com/soedinglab/pdbx
* PDB_TOOLS https://github.com/haddocking/pdb-tools


.. warning:: Always activate the environment before using the software by::

        source activate Assembline

    or depending on your computer setup: ::

        conda activate Assembline




For developers
==============

Create the environment and install all dependencies, but do not install Assembline from Anaconda.

Instead:

Clone our repositories:
-----------------------

In your ``software_dir``:

  .. code-block:: bash

    git clone git@git.embl.de:kosinski/efitter.git
    git clone git@git.embl.de:kosinski/pyxlinks.git
    git clone git@git.embl.de:kosinski/pdb_utils.git
    mkdir Assembline
    cd Assembline
    git clone git@git.embl.de:kosinski/imp_utils1.git
    git clone git@git.embl.de:kosinski/SuperConfig.git


Clone PDBX and PDB-TOOLS
------------------------

In your ``software_dir``:

.. code-block:: bash

    git clone https://github.com/soedinglab/pdbx.git
    git clone https://github.com/haddocking/pdb-tools.git

Setup before running
--------------------

After installation run sth like the below every time before using the
pipeline:

::

    module load Anaconda3
    source activate Assembline
    export PYTHONPATH=<your path>Assembline/SuperConfig:$PYTHONPATH
    export PYTHONPATH=<your path>Assembline/imp_utils1:$PYTHONPATH
    export PYTHONPATH=<your path>pyxlinks:$PYTHONPATH
    export PYTHONPATH=<your path>situs_utils:$PYTHONPATH
    export PYTHONPATH=<your path>pdb_utils:$PYTHONPATH
    export PYTHONPATH=<your path>/pdb-tools/:$PYTHONPATH
    export PYTHONPATH=<your path>/pdbx:$PYTHONPATH

of course replacing the path with your path.

And, use the full paths to the scripts:

    * Assembline scripts are located in ``Assembline/imp_utils1/scripts``
      
    * Efitter scripts (those used for generating and analyzing fit libraries) are in ``efitter/scripts``

Explanations:
-------------

-  ``git clone`` - creates local copies of software in so-called "git
   repositories" e.g. at git.embl.de
