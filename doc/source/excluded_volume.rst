Excluded volume (steric) restraints
===================================

Excluded volume restraints, or steric restraints, prevent overlap between molecules.

The excluded volume restraints are defined in the :doc:`params` through blocks like this:

.. code-block:: python

    ev_restraints = [
        {
            'name': 'ev_restraints_lowres',
            'weight': 10,
            'repr_resolution': 10
        },
        {
            'name': 'ev_restraints_highres',
            'weight': 10,
            'repr_resolution': 1
        }
    ]

In this block you define a list of possible excluded volume restraints. Each gets a custom name, weight and resolution of the representation it is assigned. You decide which will be used later in the definition of scoring functions (see :doc:`params`).

The representation resolution will match the closest existing, e.g. if you have mixed representation resolution, 10 and 1 for rigid bodies and 1 for flexible beads, and 
specify ``repr_resolution`` of 10, it will be matched to the resolution 10 of rigid bodies and 1 of flexible beads.


Parameters:

name
    a custom name

weight
    weight

repr_resolution
    which representation resolution to use for this restraint

copies
    which molecule copies are included in this restraint

distance_cutoff
    distance cutoff for non-bonded list

slack
    slack for non-bonded lists


Read more about distance_cutoff and slack: https://integrativemodeling.org/2.14.0/doc/ref/classIMP_1_1container_1_1ClosePairContainer.html#aa7b183795bd28ab268e5e84a5ad0cd99, 

