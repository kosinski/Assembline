Glossary
========

.. glossary::

    constraint
        enforced and not able to be violated sets of values in modelling (e.g. distances, coordinates etc.)

    restraint
        favored and able to be violated (up to some degree) sets of values in modelling (e.g. distances, coordinates etc.)

    series
        groups of modelling targets (a kind of target selection) to which specific modelling parameters can be applied

    subunit
        single protein chain which is part of the modelling target system

    particle
        set of system entities like atom, group of atoms, residues, protein chains etc. (depends on hierarchy)  

    selector
        method for grouping (or selecting) particles, subunits, system states etc.

    representation resolution
        target system's coarse grained or even atomic representation

    transformation matrix
        sets of coordinates dictating the final rotation and translation moves of particles