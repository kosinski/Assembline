1. Global optimization
======================

#. Enter the project directory
   
#. Prepare the :doc:`json` file, name it ``X_config.json`` for example.

#. Add fit libraries to :doc:`json` as described in :doc:`fit_libs`

#. Create :doc:`params`, name it ``params.py`` for example.
   
   For the global optimization, the following parameters are important:

   * ``protocol``

       .. code-block:: python
       
           protocol = 'denovo_MC-SA'
       
       or, if you have flexible beads and want to optimize them using Conjugate Gradient:

          .. code-block:: python
       
           protocol = 'denovo_MC-SA-CG'

       .. note:: The ``protocol`` parameter takes as argument keywords specifying the Assembline modelling mode to be used as described in :doc:`params`.

    * in ``scoring_functions``, ``discrete_restraints`` must be added to all scoring functions to indicate restraints from fit libraries.
        
        .. warning:: Do not run global optimization without discrete restraints as it will completely ruin the transformation matrices for the fit libraries

    * ``discrete_restraints_weight``
      

#. Run the modeling as described in :doc:`run`
