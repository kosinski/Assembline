Crosslink restraints
====================

Crosslink restraints restrain distance between crosslinked residues.

Loading crosslink data
----------------------

Follow the guideline in :doc:`json` of how to add crosslinks using the graphical interface of Xlink Analyzer.

Defining the restraints
-----------------------

Crosslinks restraints are defined in the :doc:`params` by adding blocks like this:

.. code-block:: python

        add_xlink_restraints = True
        x_min_ld_score = 25
        x_weight = 100.0
        x_xlink_distance = 30
        x_xlink_score_type='HarmonicUpperBound'

and then adding ``xlink_restraints`` to ``scoring_functions`` (see :doc:`params`).

If multiple representation resolutions are defined, the crosslinks will be added to the highest resolution possible (the smallest beads).

Parameters
----------

x_xlink_score_type
    A type of crosslink restraint.

    Options:

    'HarmonicUpperBound' - 0 below x_xlink_distance, harmonic above, distance calculated between centers of the beads

    'HarmonicUpperBoundSphereDistancePairScore' - 0 below x_xlink_distance, harmonic above, distance calculated between surfaces of the beads

    'XlinkScore' - 0 below x_xlink_distance, 1 above

    'LogHarmonic' - A log harmonic potential with the maximum at x_xlink_distance

    'CombinedHarmonic': - harmonic above distance of 35 A and log harmonic with the maximum at x_xlink_distance below 35 A

    default: **HarmonicUpperBound**
x_min_ld_score
    Only crosslinks with the confidence score above this threshold will be used as restraints. The score must be defined in "score" column of crosslink CSV files (see also Xlink Analyzer documentation), default: **30.0**
x_weight
    Weight of crosslink restraints, default: **1.0**
x_xlink_distance
    Target or maximual crosslink distance (depending on the implementation), default: **30.0**
x_k
    Spring constant for the harmonic potential, default: **1.0**
x_inter_xlink_scale
    Multiply the weight of inter-molecule crosslinks by this value, default: **1.0**
x_first_copy_only
    Apply crosslinks only to the first molecule copy of each series, default: **False**
x_skip_pair_fn
    A Python function to skip specific crosslinks.
    Arguments: p1, p2, component1, component2, xlink
    Return True to skip the crosslink, default: **None**
x_log_filename
    Optional log file name for printing more information about added and skipped crosslinks, default: **None**
x_score_weighting
    Scale crosslink weights by their score, default: **False**
xlink_reweight_fn
    A custom Python function to scale crosslink weights.
    Arguments: xlink, weight
    Return final weight (float), default: **None**
x_random_sample_fraction
    Take this random fraction of crosslinks for modeling, default: **1.0**