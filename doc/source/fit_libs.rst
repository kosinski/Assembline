Adding precomputed fitting libraries to JSON
============================================

#. Fit every structure using our Efitter pipeline, calculate ``solutions_pvalues.csv`` files using ``genpval``, as described in the previous steps.

#. For each structure, add the following "positions" lines to the ``pdb_files`` (if ``add_rigid_bodies_from_pdb`` set to ``True``) or ``rigid_bodies`` specifications (see :doc:`input_structures` and :doc:`rigid_bodies`)

    .. code-block:: JSON

            {
                "components": [
                    { 
                    "name": "required, keep same as subunit name",
                    "subunit": "required, keep same as subunit name",
                    "chain_id": "required, which chain from the PDB file should be read as input for this subunit",
                    "filename": "yourpath.pdb"
                    }
                ],
                "positions": "<path>/solutions_pvalues.csv",
                "positions_type": "chimera - optional, chimera is default",
                "max_positions": "optional, by default all fits are read",
                "positions_score": "log_BH_adjusted_pvalues_one_tailed is default",
            }


    For example:

    .. code-block:: JSON

            {
                "components": [
                    { 
                    "name": "<subunit_name1>",
                    "subunit": "<subunit_name1>",
                    "chain_id": "E",
                    "filename": "yourpath.pdb"
                    }
                ],
                "positions": "<path>/solutions_pvalues.csv",
                "positions_type": "chimera",
                "max_positions": 10000,
                "positions_score": "log_BH_adjusted_pvalues_one_tailed"
            }

    .. warning:: If you do have the ``rigid_body`` block specification and ``add_rigid_bodies_from_pdb`` set to ``False`` in ``params.py``, the positions MUST be added there, not in ``pdb_files``,
        otherwise they won't have an effect.
    
    

#. If there is 1-to-1 mapping between PDB files and rigid bodies AND you want to take the same number of fits for every structure:

    1. Fit every structure using our fitting pipeline, calculate ``solutions_pvalues.csv`` files (as above)

    2. Add the following "alternative_positions" definition to the JSON in the data section:

        .. code-block:: JSON

                {
                    "active": true,
                    "type": "alternative_positions",
                    "positions_type": "chimera",
                    "add_from_dir": "<fitting master dir>/<parameter set fitting dir>/<map dir>", 
                    "max": 10000,
                    "score": "log_BH_adjusted_pvalues_one_tailed"
                }
