3. Refinement
=============

Refine the top-scoring models from previous modelling modes/runs
----------------------------------------------------------------

#. Enter your main project directory (where your X_config.json is)

#. Prepare a template JSON file for refinement

    .. code-block:: bash

        gen_refinement_template.py --out_json refine_template.json --params params.py X_config.json

#. Modify the template according to your needs, e.g.:

    #. Add restraints specific for the refinement
    #. Change weights of restraints
    #. Re-define the rigid bodies
    #. Add extra subunits and their PDB files
       
    See  `JSON setup in the Elongator tutorial <https://elongator-tutorial.readthedocs.io/en/latest/json_setup_refinement.html>`_ for an example.

#. Create a ``params_refine.py`` file as in :doc:`params` but now with the `refine` protocol and adjusting the scoring function and other settings, as appropriate for your case.
   
    See  `params set up in the Elongator tutorial <https://elongator-tutorial.readthedocs.io/en/latest/params_setup_refinement.html>`_ for an example.

Refine a single model
---------------------

#. Set up a refinement directory

    .. code-block:: bash

        model_id=0014032model
        setup_refine.py \
            --model $model_id \
            --previous_json X_config.json \
            --refine_json_template refine_template.json \
            --refine_json_outname refine.json \
            --previous_outdir <output directory of the denovo run>/\
            --refine_outdir <desired output directory for the refinement, e.g. out/refinement>

    The script will create a new refinement directory and copy all input files there.

#. Perform test run

    .. code-block:: bash

        mkdir -p <desired output directory for the refinement>/$model_id/testout
        assembline.py \
            --traj \
            --models \
            -o <desired output directory for the refinement>/$model_id/testout \
            --prefix refine_"$model_id"_000000 \
            <desired output directory for the refinement>/$model_id/refine.json \
            params_refine.py

#. Run 

    .. code-block:: bash

        assembline.py \
            --traj \
            --models \
            -o <desired output directory for the refinement>/$model_id/out \
            --multi \
            --start_idx 0 \
            --njobs 3 \
            --prefix refine_"$model_id" \
            <desired output directory for the refinement>/$model_id/refine.json \
            params_refine.py


Refine multiple models
----------------------

#. Set up a refinement directory

    .. code-block:: bash

        setup_refine.py \
            --top 10 \
            --scores <output directory of the denovo run>/all_scores_uniq.csv \
            --previous_json X_config.json \
            --refine_json_template refine_template.json \
            --refine_json_outname refine.json \
            --previous_outdir <output directory of the denovo run>/\
            --refine_outdir <desired output directory for the refinement, e.g. out/refinement>

#. Run recursively:

    If the output directory for the refinement was defined above as ``out/refinement``:

    .. code-block:: bash

        for model_id in `ls --color=never out/refinement`;
            do
                echo $model_id
                assembline.py \
                    --models \
                    -o out/refinement/"$model_id"/out \
                    --multi \
                    --start_idx 0 \
                    --njobs 3 \
                    --prefix refine_"$model_id" out/refinement/"$model_id"/refine.json \
                    params_refine.py
            done
