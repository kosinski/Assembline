Check the logs
==============

The ``assembline.py`` script prints various logs to the screen or to the log files indicated in the cluster submission template.

The ``log`` includes:

* the list of final parameter values

* summary of the molecular system created: molecules, series, chain IDs, rigid bodies, etc.
  
* restraints that are being created
  
* optimization logs:

    * restraint values used for optimization, before and after optimization, and between each simulated annealing temperature change


At this point you need to inspect the ``logs`` and determine whether the diagnostic messages in ``logs`` directory are matching what you intended:

* Are all mappable crosslinks added properly as restraints?

* Do the created rigid bodies match what you expected?

* Is the number of connectivity restraints ok?

* Are the Monte Carlo moves as expected from the configuration file? 

* The log displays how many times each rigid body moved, is the number reasonable?

    * E.g. if you ask for 10,000 steps at each simulated annealing temperature and have 5 rigid bodies and 500 flexible beads,
        each rigid body only a small number of moves (each step is a single move) and you may need to increase the number of steps.
        For flexible beads it is not a problem if the "denovo_MC-SA-CG" protocol has been specified in ``params.py`` - they would move substantially with additional Conjugate gradient steps

* Are the Monte Carlo acceptance rates reasonable?

    * at high Simulated Annealing temperatures the fraction of upward moves should be high, and decrease with lower temperature

    * the number of downward movements should be non-zero and decrease with temperature
