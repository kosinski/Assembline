Running more
============

If the exhaustiveness is not met, run more jobs:

.. code-block:: bash

    assembline.py --traj --models -o out --multi --start_idx 10000 --njobs 10000 config.json params.py &>log&

which will run 10000 more jobs with models named with a prefix starting from ```0010000```
and repeat the sampling exhaustiveness analysis above

.. note:: To perform :doc:`sampling_exhaustiveness` analysis visit the relevant setion in the manual.