Symmetry constraints
====================

Symmetry :term:`constraints` can be used to enforce symmetry of a complex.
In contrast to symmetry :term:`restraints`, the constraints cannot be violated and enforce perfect symmetry.
The modelling protocols comprising Assembline will also use constraints to speed up calculations - only the reference particle from each 
constraint set will be moved and the remaining particles will be automatically set to follow the symmetry.
Constraints are also faster to calculate than restraints.

So, whenever you are fine with enforcing perfect symmetry - use constraints.

To use constraints:

#. Set ``add_symmetry_constraints = True`` in ``params.py``
   
#. Define symmetry axes transformations as in :doc:`symmetry_tr3ds`
    
#. Define which components (subunits or specific regions of a subunit) should be constrained as in the :doc:`applying_symmetry`

.. note:: You can use both symmetry constraints and restraints, applying constraints to some subunits or regions, and restraints to others!
