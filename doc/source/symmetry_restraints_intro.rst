Symmetry restraints
===================

Symmetry :term:`restraints` can be used to favor symmetry of a complex.
In contrast to symmetry :term:`constraints`, the restraints can be violated and only favor symmetry, with "strength" dependent on their user-defined weight.

They can be used when expect some deviations from a perfect symmetry.

To use restraints:

#. Set ``add_symmetry_restraints = True`` in ``params.py``

#. Optionally, set ``symmetry_restraints_weight = <some value>`` in ``params.py``
   
#. Define symmetry axes transformations as in :doc:`symmetry_tr3ds`
    
#. Define which components (subunits or specific regions of a subunit) should be restrained as in the :doc:`applying_symmetry`

#. Add ``sym_restraints`` to the scoring function in ``params.py``, for example:
   
    .. code-block:: python

        scoring_functions = {
            'score_func_lowres': {
                'restraints': [
                    'xlink_restraints',
                    'conn_restraints',
                    'ev_restraints_lowres',
                    'FitRestraint',
                    'sym_restraints'
                ]
            }
        }


.. note:: You can use both symmetry constraints and restraints, applying constraints to some subunits or regions, and restraints to others!

