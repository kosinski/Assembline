Quick test of convergence
=========================

Run quick test to assess convergence of the model score in randomly selected modelling trajectories

.. code-block:: bash

    cd <output directory>
    plot_convergence.R total_score_logs.txt 20

(change ``20`` to have less or more trajectories in the plots).

.. note :: The above is a quick and optional step before running the complete sampling exhaustiveness analysis for your modelling runs. 

Open the resulting ``convergence.pdf`` to visualize the convergence.