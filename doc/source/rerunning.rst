Modify and re-run
=================

Typical adjustments that need to be done:
-----------------------------------------

* Adjust the weights of xlink (``x_weight``), alternative positions (``discrete_restraints_weight``) etc.

    * Currently, you would run the first run and adjust the weights so the scores are of similar scale. 

* Adjust KT in MonteCarlo Simulated Annealing so it is on the same range as the total score (e.g. for score of 100000 you would not use KT of 1 but rather 100-10000)

* Increase/decrease resolution to adjust the speed of calculations

* Add/remove flexible beads

* Modify sampling protocol

* Add additional restraints


Was the calculation quick?
--------------------------

Then you may consider increasing the resolution in ``params.py`` of the representation to a higher one (i.e. resolution) hence probably more accurate one, e.g.: setting:

    .. code-block:: python

        struct_resolutions = [0]