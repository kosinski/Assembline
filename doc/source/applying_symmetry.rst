Applying symmetry
=================

.. note:: This is not necessary if you want to apply symmetry to entire subunits.
    In that case it is enough you specify ``tr3d`` arguments in the series, setting them to the names of 
    transformation matrices defined above.

Specification of ``apply_symmetry``:


    .. code-block:: json
    
        "apply_symmetry": [
            {
                "sym": // name of the symmetry transformation specified in "symmetry",
                "restraint_type": "symmetry_restraint" or "symmetry_constraint",
                "selectors": [
                    // list of selectors of regions that should be restrained.
                    // the number of selectors must equal the order of the symmetry
                    // so for 2-fold - two selectors, 8-fold - eight selectors
                ]
            }
        ]


For example:

    .. code-block:: json
    
        "apply_symmetry": [
            {
                "sym": "2fold",
                "restraint_type": "symmetry_restraint",
                "selectors": [
                    {"subunit": "EccB", "serie": "2fold", "resi_ranges": [[85, 260], [345,400], [408,490]], "copies": [0]},
                    {"subunit": "EccB", "serie": "2fold", "resi_ranges": [[85, 260], [345,400], [408,490]], "copies": [1]}
                ]
            }
        ]

will restrain only specified residue ranges of the copy 0 with the copy 1 of EccB subunit.